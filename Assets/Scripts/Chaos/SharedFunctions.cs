﻿using UnityEngine;
using UnityEngine.UI;

public class SharedFunctions : MonoBehaviour {
	public bool playModeIsEnabled() {
		return !levelEditorModeIsEnabled();
	}

	public bool levelEditorModeIsEnabled() {
		const string KEY = "Level Editor Mode";
		string value = PlayerPrefs.GetString(KEY);
		return value == "Enabled";
	}

	public RouteMenuButtonsBehaviour getRouteMenuButtonsBehaviourScript() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		return go.GetComponent<RouteMenuButtonsBehaviour>();
	}

	public void markClickedIndependentItemAsOrdinaryOne() {
		const string TAG = "Clicked Item";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var ii = go.GetComponent<IndependentItem>();
		go.tag = ii.getItemOrdinaryTag();
	}

	public InputField getInputFieldComponentFrom(string name) {
		GameObject go = GameObject.FindGameObjectWithTag(name);
		return go.GetComponent<InputField>();
	}

	public GameObject getBonusesList() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var l = go.GetComponent<PlayScreen>();
		return l.getBonusesList();
	}

	public GameObject[] getDestroyables() {
		const string TAG = "Destroy";
		return GameObject.FindGameObjectsWithTag(TAG);
	}

	public GameObject[] getUndestroyables() {
		const string TAG = "Undestroy";
		return GameObject.FindGameObjectsWithTag(TAG);
	}

	public GameObject[] getBalls() {
		const string TAG = "Ball";
		return GameObject.FindGameObjectsWithTag(TAG);
	}

	public Vector3 projectInput(Vector3 inputPosition) {
		const float Z = -6;
		var mainCamera = Camera.main;
		var v = mainCamera.ScreenToWorldPoint(inputPosition);
		v.z = Z;
		return v;
	}

	public void makeItemLookLikeItIsSelected(GameObject item) {
		const float Z = -6;
		var t = item.transform;
		var position = t.position;
		float x = position.x;
		float y = position.y;
		t.position = new Vector3(x, y, Z);
	}

	public void makeItemLookLikeItIsDeselected(GameObject item) {
		const float Z = 0;
		var t = item.transform;
		var position = t.position;
		float x = position.x;
		float y = position.y;
		t.position = new Vector3(x, y, Z);
	}
}