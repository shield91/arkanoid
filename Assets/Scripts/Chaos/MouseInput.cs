﻿using UnityEngine;

public class MouseInput : UserInput {
	private void OnMouseDown() {
		onInputDown();
	}

	protected override Vector3 getInputPosition() {
		return Input.mousePosition;
	}

	private void OnMouseDrag() {
		onInputDrag();
	}

	private void OnMouseUp() {
		onInputUp();
	}
}