﻿using UnityEngine;

public class Platform : MonoBehaviour {
	private bool dragModeIsEnabled;

	void Start() {
		dragModeIsEnabled = false;
	}

	void OnCollisionEnter2D(Collision2D c) {
		Vector2 v = transform.position;
		v.y = -4.7f;
		transform.position = v;
	}

	void OnMouseDown() {
		dragModeIsEnabled = true;
	}

	void OnMouseDrag() {	
		if (draggingIsPossible()) {
			movePlatformToInputPosition();
		}
	}

	private bool draggingIsPossible() {
		return dragModeIsEnabled;
	}

	private void movePlatformToInputPosition() {
		const int FIELD_DEPTH = 9;
		Vector3 v = getVectorForPlatformPosition();
		Vector3 w = getVectorForInputPosition();
		float x = getHorizontalPositionForPlatform();
		float y = v.y;
		w.z = FIELD_DEPTH;
		transform.position = new Vector2(x, y);
	}

	private Vector3 getVectorForPlatformPosition() {
		return transform.position;
	}

	private Vector3 getVectorForInputPosition() {
		Camera c = Camera.main;
		Vector3 v = Input.mousePosition;
		return c.ScreenToWorldPoint(v);
	}


	private float getHorizontalPositionForPlatform() {
		if (platformHitsTheWall()) {
			return getPositionWhenPlatformHitsTheWall();
		} else {
			return getPositionWhenPlatformIsBetweenTheWalls();
		}
	}

	private bool platformHitsTheWall() {
		bool ba = platformHitsTheLeftWall();
		bool bb = platformHitsTheRightWall();
		return ba || bb;
	}

	private bool platformHitsTheLeftWall() {
		const float LEFT_WALL = -4.1f;
		Vector3 v = getVectorForInputPosition();
		return  LEFT_WALL >= v.x;
	}

	private bool platformHitsTheRightWall() {
		const float RIGHT_WALL = 4.1f;
		Vector3 v = getVectorForInputPosition();
		return  v.x >= RIGHT_WALL;
	}

	private float getPositionWhenPlatformHitsTheWall() {
		if (platformHitsTheLeftWall()) {
			return getPositionWhenPlatformHitsTheLeftWall(); 
		} else {
			return getPositionWhenPlatformHitsTheRightWall();
		}
	}

	private float getPositionWhenPlatformHitsTheLeftWall() {
		return -4.1f;
	}

	private float getPositionWhenPlatformHitsTheRightWall() {
		return 4.1f;
	}

	private float getPositionWhenPlatformIsBetweenTheWalls() {
		Vector3 v = getVectorForInputPosition();
		return v.x;
	}

	void OnMouseUp() {
		dragModeIsEnabled = false;
	}
}