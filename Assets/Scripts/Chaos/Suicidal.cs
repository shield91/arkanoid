﻿using UnityEngine;

public class Suicidal : MonoBehaviour {
    public float delay = 1f;    
    
	void Start() {
		var obj = gameObject;
		float t = delay;
        Destroy(obj, t);
    }
}