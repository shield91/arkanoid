﻿using UnityEngine;
using System.Collections;

public class Hinge : MonoBehaviour {	
	SpringJoint2D spring;
	LineRenderer line;
	// Use this for initialization
	void Start() {
		spring = GetComponent<SpringJoint2D>();
		line = GetComponent<LineRenderer>();
		spring.enabled = false;
	}
	
	// Update is called once per frame
	void Update() {
		if (Input.touchCount > 0) {
			Touch touch = Input.touches[0];
			switch(touch.phase){
			case TouchPhase.Began:
				StartDrag();
				break;
			case TouchPhase.Ended:
				StopDrag();
				break;
			case TouchPhase.Moved:
				ProceedDrag(Camera.main.ScreenToWorldPoint(touch.position));
				break;
			}
		}
	}

	void StartDrag() {
		spring.enabled = true;
		line.enabled = true;
	}
	
	void StopDrag() {
		spring.enabled = false;
		line.enabled = false;
	}
	
	void ProceedDrag(Vector2 target) {
		var p = spring.connectedBody.position;
		var mp = Input.mousePosition;
		mp.z = 9;
		var q = Camera.main.ScreenToWorldPoint(mp);
		//var bp = spring.connectedBody.position;
		spring.anchor = q;// - bp;
		line.SetPosition (0, new Vector3(p.x, p.y, 0));
		line.SetPosition (1, new Vector3(q.x, q.y, 0));
	}
	
	void OnMouseDown() {
		StartDrag();
	}

	void OnMouseDrag() {
		if (spring.enabled == true) {
			Vector2 cursorPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);//getting cursor position
			ProceedDrag(cursorPosition);
		}
	}

	void OnMouseUp() {
		StopDrag();
	}
}