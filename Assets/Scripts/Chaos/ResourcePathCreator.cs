namespace AssemblyCSharp
{
	public class ResourcePathCreator {
		private string[] splittedTag;
		private string path;
		private int tagCurrentPartIndex;

		public ResourcePathCreator(string tag) {
			setSplitted(tag);
			resetPath();
			resetTagCurrentPartIndex();
		}

		private void setSplitted(string tag) {
			var value = getValueForSplittedTagFrom(tag);
			setSplittedTag(value);
		}

		private string[] getValueForSplittedTagFrom(string tag) {
			var separator = getSeparatorForTagSplitting();
			return tag.Split(separator);
		}

		private char getSeparatorForTagSplitting() {
			return ' ';
		}

		private void setSplittedTag(string[] value) {
			splittedTag = value;
		}

		private void resetPath() {
			var value = getValueForPathByDefault();
			setPath(value);
		}

		private string getValueForPathByDefault() {
			return string.Empty;
		}

		private void setPath(string value) {
			path = value;
		}

		private void resetTagCurrentPartIndex() {
			int value = getValueForTagCurrentPartIndexByDefault();
			setTagCurrentPartIndex(value);
		}

		private int getValueForTagCurrentPartIndexByDefault() {
			return 0;
		}

		private void setTagCurrentPartIndex(int value) {
			tagCurrentPartIndex = value;
		}

		public string getPathToResource() {
			createPath();
			return getCreatedPath();
		}

		private void createPath() {
			while(thereAreTagPartsNeedToBeProcessed()) {
				buildPath();
			}
		}

		private bool thereAreTagPartsNeedToBeProcessed() {
			int i = getTagCurrentPartIndex();
			int n = getNumberOfTagParts();
			return i < n;
		}

		private int getTagCurrentPartIndex() {
			return tagCurrentPartIndex;
		}

		private int getNumberOfTagParts() {
			var tag = getSplittedTag();
			return tag.Length;
		}

		private string[] getSplittedTag() {
			return splittedTag;
		}

		private void buildPath() {
			addTagCurrentPartToPathIfThisPartCanBeAdded();
			moveOnToTheNextTagPart();
		}

		private void addTagCurrentPartToPathIfThisPartCanBeAdded() {
			if (tagCurrentPartCanBeAddedToPath()) {
				addTagCurrentPartToPath();
			}
		}

		private bool tagCurrentPartCanBeAddedToPath() {
			var a = tagCurrentPartDiffersFromTheBlackListFirstWord();
			var b = tagCurrentPartDiffersFromTheBlackListSecondWord();
			return a && b;
		}

		private bool tagCurrentPartDiffersFromTheBlackListFirstWord() {
			var b = tagCurrentPartEqualsToTheBlackListFirstWord();
			return !b;
		}

		private bool tagCurrentPartEqualsToTheBlackListFirstWord() {
			var s = getTagCurrentPartByItsIndex();
			var value = getFirstWordOfTheBlackList();
			return s.Equals(value);
		}

		private string getTagCurrentPartByItsIndex() {
			var index = getTagCurrentPartIndex();
			var tag = getSplittedTag();
			return tag[index];
		}

		private string getFirstWordOfTheBlackList() {
			return "Level";
		}

		private bool tagCurrentPartDiffersFromTheBlackListSecondWord() {
			var b = tagCurrentPartEqualsToTheBlackListSecondWord();
			return !b;
		}

		private bool tagCurrentPartEqualsToTheBlackListSecondWord() {
			var s = getTagCurrentPartByItsIndex();
			var value = getSecondWordOfTheBlackList();
			return s.Equals(value);
		}

		private string getSecondWordOfTheBlackList() {
			return "Editor";
		}

		private void addTagCurrentPartToPath() {
			appendTagCurrentPartToPath();
			addSpaceIfTagCurrentPartIsNotTheLastOne();
		}

		private void appendTagCurrentPartToPath() {
			var value = getValueForPathWithAppendedTagCurrentPart();
			setPath(value);
		}

		private string getValueForPathWithAppendedTagCurrentPart() {
			var a = getPath();
			var b = getTagCurrentPartByItsIndex();
			return a + b;
		}

		private string getPath() {
			return path;
		}

		private void addSpaceIfTagCurrentPartIsNotTheLastOne() {
			if (tagCurrentPartIsNotTheLastOne()) {
				addSpaceToPath();
			}
		}

		private bool tagCurrentPartIsNotTheLastOne() {
			var b = tagCurrentPartIsTheLastOne();
			return !b;
		}

		private bool tagCurrentPartIsTheLastOne() {
			int i = getTagCurrentPartIndex();
			int j = getIndexOfTagLastPart();
			return i == j;
		}

		private int getIndexOfTagLastPart() {
			int n = getNumberOfTagParts();
			int m = getValueForRetrievingTagLastPartIndex();
			return n - m;
		}

		private int getValueForRetrievingTagLastPartIndex() {
			return 1;
		}

		private void addSpaceToPath() {
			var value = getValueForPathWithAddedSpace();
			setPath(value);
		}

		private string getValueForPathWithAddedSpace() {
			var a = getPath();
			var b = getValueForSpace();
			return a + b;
		}

		private string getValueForSpace() {
			return " ";
		}

		private void moveOnToTheNextTagPart() {
			int value = getValueForMovingOnToTheNextTagPart();
			setTagCurrentPartIndex(value);
		}

		private int getValueForMovingOnToTheNextTagPart() {
			int index = getTagCurrentPartIndex();
			int increment = getIncrementForMovingOnToTheNextTagPart();
			return index + increment;
		}

		private int getIncrementForMovingOnToTheNextTagPart() {
			return 1;
		}

		private string getCreatedPath() {
			return getPath();
		}
	}
}