using AssemblyCSharp;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndLevel : MonoBehaviour {
	private void Start() {

		freezeTime();


//		updatePlayerProgress();

		setUpNextButtonVisibility();
		processGameEnd();

	}

	private void freezeTime() {
		var value = getValueForFreezingTime();
		Time.timeScale = value;
	}

	private float getValueForFreezingTime() {
		return 0f;
	}

	private void updatePlayerProgress() {
		var ps = getPlayScreenComponentForThisLevel();
		ps.updatePlayerProgress();
	}

	private PlayScreen getPlayScreenComponentForThisLevel() {
		var lt = getLevelTemplateForThisLevel();
		return lt.GetComponentInChildren<PlayScreen>();
	}

	private Transform getLevelTemplateForThisLevel() {
		var c = getCanvasForThisLevel();
		return c.parent;
	}

	private Transform getCanvasForThisLevel() {
		return transform.parent;
	}

	private void setUpNextButtonVisibility() {
		var button = getGameObjectForNextButton();
		var value = getValueForNextButtonVisibility();
		button.SetActive(value);
	}

	private GameObject getGameObjectForNextButton() {
		var button = getTransformForNextButton();
		return button.gameObject;
	}

	private Transform getTransformForNextButton() {
		const string NAME = "Next";
		return transform.Find(NAME);
	}

	private bool getValueForNextButtonVisibility() {
		if (currentLevelIsTheLastOne()) {
			return getValueForNextButtonToBeInvisible();
		} else {
			return getValueForNextButtonVisibilityInOtherCases();
		}
	}

	private bool currentLevelIsTheLastOne() {
		if (playerIsInCampaignMode()) {
			return currentLevelIsTheLastOneInCampaign();
		} else {
			return currentLevelIsTheLastFreeOne();
		}
	}

	private bool playerIsInCampaignMode() {
		const string KEY = "Chosen Campaign Index";
		return PlayerPrefs.HasKey(KEY);
	}

	private bool currentLevelIsTheLastOneInCampaign() {
		int lastLevelIndex = getIndexOfLastLevelInCampaign();
		return currentLevelIndexEquals(lastLevelIndex);
	}

	private int getIndexOfLastLevelInCampaign() {
		int number = getNumberOfLevelsInCampaign();
		return getPreviousValueFor(number);
	}

	private int getNumberOfLevelsInCampaign() {
		var levels = getLevelsOfChosenCampaign();
		return levels.Count;
	}

	private List<Level> getLevelsOfChosenCampaign() {
		var campaign = getChosenCampaignFromLevelsSet();
		return campaign.levels;
	}

	private Campaign getChosenCampaignFromLevelsSet() {
		var campaigns = getCampaignsFromLevelsSet();
		var index = getIndexOfChosenCampaign();
		return campaigns[index];
	}

	private List<Campaign> getCampaignsFromLevelsSet() {
		var levels = getSetOfLevels();
		return levels.campaigns;
	}

	private Levels getSetOfLevels() {
		var serializer = getSerializerForLevelsSet();
		var textReader = getTextReaderForLevelsSet();
		return serializer.Deserialize(textReader) as Levels;
	}

	private XmlSerializer getSerializerForLevelsSet() {
		var type = typeof(Levels);
		return new XmlSerializer(type);
	}

	private TextReader getTextReaderForLevelsSet() {
		var s = getStringForLevelsSet();
		return new StringReader(s);
	}

	private string getStringForLevelsSet() {
		const string KEY = "Levels";
		return PlayerPrefs.GetString(KEY);
	}

	private int getIndexOfChosenCampaign() {
		const string KEY = "Chosen Campaign Index";
		return PlayerPrefs.GetInt(KEY);
	}

	private int getPreviousValueFor(int number) {
		int decrement = getValueForRetrievingLastLevel();
		return number - decrement;
	}

	private int getValueForRetrievingLastLevel() {
		return 1;
	}

	private bool currentLevelIndexEquals(int lastLevelIndex) {
		int i = getIndexOfChosenLevel();
		return i == lastLevelIndex;
	}

	private int getIndexOfChosenLevel() {
		const string KEY = "Chosen Level Index";
		return PlayerPrefs.GetInt(KEY);
	}

	private bool currentLevelIsTheLastFreeOne() {
		int lastLevelIndex = getIndexOfLastFreeLevel();
		return currentLevelIndexEquals(lastLevelIndex);
	}

	private int getIndexOfLastFreeLevel() {
		int number = getNumberOfFreeLevels();
		return getPreviousValueFor(number);
	}

	private int getNumberOfFreeLevels() {
		var levels = getFreeLevelsFromLevelsSet();
		return levels.Count;
	}

	private List<Level> getFreeLevelsFromLevelsSet() {
		var levels = getSetOfLevels();
		return levels.freeLevels;
	}

	private bool getValueForNextButtonToBeInvisible() {
		return !currentLevelIsTheLastOne();
	}

	private bool getValueForNextButtonVisibilityInOtherCases() {
		if (playerIsInCampaignMode()) {
			return getValueForNextButtonVisibilityInCampaignMode();
		} else {
			return getValueForNextButtonVisibilityInFreeMode();
		}
	}

	private bool getValueForNextButtonVisibilityInCampaignMode() {
		return levelIsSuccessfullyFinished();
	}

	private bool levelIsSuccessfullyFinished() {
		var b = allFailureConditionsHaveBeenPassed();
		return !b;
	}

	private bool allFailureConditionsHaveBeenPassed() {
		var ps = getPlayScreenComponentForThisLevel();
		return ps.allFailureConditionsHaveBeenPassed();
	}

	private bool getValueForNextButtonVisibilityInFreeMode() {
		return levelIsSuccessfullyFinished();
	}

	private void processGameEnd() {
		if (gameEndedSuccessfully()) {
			processSuccessfulGameEnd();
		}
	}

	private bool gameEndedSuccessfully() {
		var go = getGameObjectForNextButton();
		return go.activeInHierarchy;
	}

	private void processSuccessfulGameEnd() {
		if (playerIsInCampaignMode()) {
			processSuccessfulCampaignLevelEnd();
		} else {
			processSuccessfulFreeLevelEnd();
		}
	}

	private void processSuccessfulCampaignLevelEnd() {
		if (nextLevelIsUnpaid()) {
			setUpNextLevelCost();
		}
	}

	private bool nextLevelIsUnpaid() {
		return !nextLevelIsPaid();
	}

	private bool nextLevelIsPaid() {
		var engine = getSearchEngineForCampaignLevels();
		return engine.levelIsPurchased();
	}

	private CampaignLevelsSearchEngine getSearchEngineForCampaignLevels() {
		int targetLevelNumber = getNumberOfTargetCampaignLevel();
		return new CampaignLevelsSearchEngine(targetLevelNumber);
	}

	private int getNumberOfTargetCampaignLevel() {
		return getNumberForNextCampaignLevel();
	}

	private int getNumberForNextCampaignLevel() {
		var level = getNextLevelInCampaign();
		return level.number;
	}

	private Level getNextLevelInCampaign() {
		var levels = getLevelsOfChosenCampaign();
		var index = getIndexOfNextLevel();
		return levels[index];
	}

	private int getIndexOfNextLevel() {
		int index = getIndexOfChosenLevel();
		int increment = getValueForRetrievingNextLevel();
		return index + increment;
	}

	private int getValueForRetrievingNextLevel() {
		return 1;
	}

	private void setUpNextLevelCost() {
		setNextLevelCost();
		showNextLevelCost();
	}

	private void setNextLevelCost() {
		var cost = getTextComponentFromNextLevelCost();
		var value = getStringValueForNextLevelCost();
		cost.text = value;
	}

	private Text getTextComponentFromNextLevelCost() {
		var cost = getTransformForNextLevelCost();
		return cost.GetComponent<Text>();
	}

	private Transform getTransformForNextLevelCost() {
		var name = getNameForNextLevelCost();
		return transform.Find(name);
	}

	private string getNameForNextLevelCost() {
		return "Next Level Cost";
	}

	private string getStringValueForNextLevelCost() {
		var value = getValueForNextLevelCost();
		return value.ToString();
	}

	private int getValueForNextLevelCost() {
		var level = getNextCampaignLevelFromLevelsSet();
		return level.cost;
	}

	private Level getNextCampaignLevelFromLevelsSet() {
		var levels = getLevelsOfChosenCampaign();
		var index = getIndexOfNextLevel();
		return levels[index];
	}

	private void showNextLevelCost() {
		var go = getGameObjectForNextLevelCost();
		var value = getValueForNextLevelCostToBeVisible();
		go.SetActive(value);
	}

	private GameObject getGameObjectForNextLevelCost() {
		var cost = getTransformForNextLevelCost();
		return cost.gameObject;
	}

	private bool getValueForNextLevelCostToBeVisible() {
		return true;
	}

	private void processSuccessfulFreeLevelEnd() {
		if (nextLevelIsClosed()) {
			openNextFreeLevel();
		}
	}

	private bool nextLevelIsClosed() {
		return !nextLevelIsOpen();
	}

	private bool nextLevelIsOpen() {
		var engine = getSearchEngineForFreeLevels();
		return engine.levelIsOpened();
	}

	private FreeLevelsSearchEngine getSearchEngineForFreeLevels() {
		int targetLevelNumber = getNumberOfTargetFreeLevel();
		return new FreeLevelsSearchEngine(targetLevelNumber);
	}

	private int getNumberOfTargetFreeLevel() {
		return getNumberForNextFreeLevel();
	}

	private int getNumberForNextFreeLevel() {
		var level = getNextFreeLevelFromLevelsSet();
		return level.number;
	}

	private Level getNextFreeLevelFromLevelsSet() {
		var levels = getFreeLevelsFromLevelsSet();
		var index = getIndexOfNextLevel();
		return levels[index];
	}

	private void openNextFreeLevel() {
		var playerProgress = getProgressOfPlayer();
		var freeLevels = playerProgress.freeLevels;
		var item = getItemForNextFreeLevel();
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		freeLevels.Add(item);
		serializer.Serialize(stream, playerProgress);
		stream.Close();
	}

	private PlayerProgress getProgressOfPlayer() {
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
		stream.Close();
		return playerProgress;
	}

	private XmlSerializer getSerializerForPlayerProgress() {
		var type = typeof(PlayerProgress);
		return new XmlSerializer(type);
	}

	private Stream getStreamForPlayerProgress() {
		var path = getPathToPlayerProgress();
		var mode = getFileModeForStream();
		return new FileStream(path, mode);
	}

	private string getPathToPlayerProgress() {
		const string KEY = "Path To Player Progress";
		return PlayerPrefs.GetString(KEY);
	}

	private FileMode getFileModeForStream() {
		return FileMode.OpenOrCreate;
	}

	private PlayerProgressLevel getItemForNextFreeLevel() {
		var value = getNumberForNextFreeLevel();
		var item = new PlayerProgressLevel();
		item.number = value;
		return item;
	}

	public void Next() {
		if (playerIsInCampaignMode()) {
			advanceInCampaign();
		} else {
			loadNextFreeLevel();
		}
	}

	private void advanceInCampaign() {
		if (nextLevelCanBePurchased()) {
			loadNextLevelInCampaign();
		} else {
			goToShopScreen();
		}
	}

	private bool nextLevelCanBePurchased() {
		int cost = getCostOfNextLevel();
		int balance = getBalanceOnPlayerAccount();
		return cost <= balance;
	}

	private int getCostOfNextLevel() {
		if (nextLevelIsPaid()) {
			return getValueInCaseOfPaidNextLevel();
		} else {
			return getValueForNextLevelCost();
		}
	}

	private int getValueInCaseOfPaidNextLevel() {
		return 0;
	}

	private int getBalanceOnPlayerAccount() {
		var playerProgress = getProgressOfPlayer();
		return playerProgress.balanceInCoins;
	}

	private void loadNextLevelInCampaign() {
		if (nextLevelIsPaid()) {
			loadPurchasedNextLevelInCampaign();
		} else {
			purchaseNextLevelInCampaign();
		}
	}

	private void loadPurchasedNextLevelInCampaign() {
		loadOpenedLevel();
	}

	private void loadOpenedLevel() {
		setNextLevelAsChosenOne();
		reloadThisScreen();
	}

	private void setNextLevelAsChosenOne() {
		var key = getKeyForChosenLevelIndex();
		var value = getIndexOfNextLevel();
		PlayerPrefs.SetInt(key, value);
	}

	private string getKeyForChosenLevelIndex() {
		return "Chosen Level Index";
	}

	private void reloadThisScreen() {
		var sceneName = getSceneNameForThisScreen();
		SceneManager.LoadScene(sceneName);
	}

	private string getSceneNameForThisScreen() {
		var activeScene = SceneManager.GetActiveScene();
		return activeScene.name;
	}

	private void purchaseNextLevelInCampaign() {
		buyNextLevelInCampaign();
		loadPurchasedNextLevelInCampaign();
	}

	private void buyNextLevelInCampaign() {
		var playerProgress = getProgressOfPlayer();
		var campaigns = playerProgress.campaigns;
		var chosenCampaignIndex = getIndexOfChosenCampaign();
		var chosenCampaign = campaigns[chosenCampaignIndex];
		var chosenCampaignLevels = chosenCampaign.levels;
		var item = getItemForNextLevelInCampaign();
		var cost = getCostOfNextLevel();
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		playerProgress.balanceInCoins -= cost;
		chosenCampaignLevels.Add(item);
		serializer.Serialize(stream, playerProgress);
		stream.Close();
	}

	private PlayerProgressLevel getItemForNextLevelInCampaign() {
		var value = getNumberForNextCampaignLevel();
		var item = new PlayerProgressLevel();
		item.number = value;
		return item;
	}

	private void goToShopScreen() {
		loadShopScreen();
	}

	private void loadShopScreen() {
		var sceneName = getSceneNameForShopScreen();
		SceneManager.LoadScene(sceneName);
	}

	private string getSceneNameForShopScreen() {
		return "Shop Screen";
	}

	private void loadNextFreeLevel() {
		if (nextLevelIsOpen()) {
			loadOpenedLevel();
		} else {
			openNextLevel();
		}
	}

	private void openNextLevel() {
		openNextFreeLevel();
		loadOpenedLevel();
	}

	public void Retry() {
		reloadThisScreen();
	}

	public void Quit() {
		if (survivalModeIsOn()) {
			goToStartScreen();
		} else {
			goToLevelSelectScreen();
		}
	}

	private bool survivalModeIsOn() {
		const string KEY = "Survival Mode Is On";
		return PlayerPrefs.HasKey(KEY);
	}

	private void goToStartScreen() {
		disableSurvivalMode();
		loadStartScreen();
	}

	private void disableSurvivalMode() {
		var key = getKeyForSurvivalMode();
		PlayerPrefs.DeleteKey(key);
	}

	private string getKeyForSurvivalMode() {
		return "Survival Mode Is On";
	}

	private void loadStartScreen() {
		var sceneName = getSceneNameForStartScreen();
		SceneManager.LoadScene(sceneName);
	}

	private string getSceneNameForStartScreen() {
		return "Start Screen";
	}

	private void goToLevelSelectScreen() {
		loadLevelSelectScreen();
	}

	private void loadLevelSelectScreen() {
		var sceneName = getSceneNameForLevelSelectScreen();
		SceneManager.LoadScene(sceneName);
	}

	private string getSceneNameForLevelSelectScreen() {
		return "LevelSelect";
	}
}