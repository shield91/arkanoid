﻿using UnityEngine;
using AssemblyCSharp;
using System.Xml;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Xml.Serialization;
using System.IO;
using System;
using UnityEngine.UI;

public class LevelSelect : MonoBehaviour {
	private GameObject buttonForCurrentLevel;
	private bool campaignLevelIsPaid;
	private int currentLevelIndex;


	private XmlDocument levelsList;
	private int currentUserCreatedSelectLevelButtonPosition;

	private void Start() {		
		//setUpAddNewLevelButtonVisibillity
		hideAddNewLevelButtonIfItIsNecessary();
		loadPlayerCoins();
		loadLevels();
	}

	private void loadLevels() {
		    //campaignModeIsOn
		if (playerIsInCampaign()) {
			loadCampaignLevels();
		} else {
			loadFreeLevels();
		}			
	}
				//campaignModeIsOn
	private bool playerIsInCampaign() {
		var key = getKeyForChosenCampaignIndex();
		return PlayerPrefs.HasKey(key);
	}

	private string getKeyForChosenCampaignIndex() {
		return "Chosen Campaign Index";
	}

	private void loadChosenCampaignLevels() {
		resetCurrentLevelIndex();
		resetButtonForCurrentLevel();
		loadCampaignLevels();
	}

	private void resetCurrentLevelIndex() {
		int value = getValueForCurrentLevelIndexByDefault();
		setCurrentLevelIndex(value);
	}

	private int getValueForCurrentLevelIndexByDefault() {
		return 0;
	}

	private void setCurrentLevelIndex(int value) {
		currentLevelIndex = value;
	}

	private void resetButtonForCurrentLevel() {
		var value = getValueForButtonForCurrentLevelByDefault();
		setButtonForCurrentLevel(value);
	}

	private GameObject getValueForButtonForCurrentLevelByDefault() {
		return null;
	}

	private void setButtonForCurrentLevel(GameObject value) {
		buttonForCurrentLevel = value;
	}

	private void loadCampaignLevels() {
		while (thereAreCampaignLevelsNeedToBeLoaded()) {
			loadCurrentCampaignLevel();
		}			
	}

	private bool thereAreCampaignLevelsNeedToBeLoaded() {
		//Може замінити поведінкою "getCurrentCampaignLevelIndex()" і в тілі цієї поведінки прописати
		//рядок "return getCurrentLevelIndex()"?
		int i = getCurrentLevelIndex();
		int n = getNumberOfCampaignLevels();
		return i < n;
	} 

	private int getCurrentLevelIndex() {
		return currentLevelIndex;
	}

	private int getNumberOfCampaignLevels() {
		var levels = getLevelsFromChosenCampaign();
		return levels.Count;
	}

	private List<Level> getLevelsFromChosenCampaign() {
		var campaign = getChosenCampaignByItsIndex();
		return campaign.levels;
	}

	private Campaign getChosenCampaignByItsIndex() {
		var campaigns = getCampaignsFromLevelsSet();
		var index = getChosenCampaignIndexFromPlayerPreferences();
		return campaigns[index];					
	}

	private List<Campaign> getCampaignsFromLevelsSet() {
		var levels = getSetOfLevels();
		return levels.campaigns;
	}

	private Levels getSetOfLevels() {
		
		//У скрипті MainMenu.cs провернути щось подібне:
		//https://stackoverflow.com/questions/2407302/convert-xmldocument-to-string
		//(це аби можна було запихувати "Levels Set.xml" в string навіть в стані (режимі) редактора рівнів).
//		if (levelEditorModeIsOn()) {
//			return getSetOfLevelsInLevelEditorMode();
//		} else {
//			return getSetOfLevelsInPlayMode();
//		}
			//levelEditorModeIsOn
		if (PlayerPrefs.HasKey ("Level Editor Mode")) {
			//getSetOfLevelsInLevelEditorMode
			var serializer = getSerializerForLevelsSet();
			var stream = new FileStream (PlayerPrefs.GetString ("Path To Levels"), FileMode.OpenOrCreate);
			var o = serializer.Deserialize (stream) as Levels;
			stream.Close();
			return o;

				//float
		} else {
			//getSetOfLevelsInPlayMode
			var serializer = getSerializerForLevelsSet();
			var textReader = getTextReaderForLevelsSet();
			return serializer.Deserialize(textReader) as Levels;	
		}


//		var serializer = getSerializerForLevelsSet();
//		var textReader = getTextReaderForLevelsSet();
//		return serializer.Deserialize(textReader) as Levels;
	}

	private XmlSerializer getSerializerForLevelsSet() {
		var type = typeof(Levels);
		return new XmlSerializer(type);	
	}

	private TextReader getTextReaderForLevelsSet() {
		var s = getStringForLevelsSet();
		return new StringReader(s);
	}

	private string getStringForLevelsSet() {
		var key = getKeyForLevelsSet();
		return PlayerPrefs.GetString(key);
	}

	private string getKeyForLevelsSet() {
		return "Levels";
	}

	private int getChosenCampaignIndexFromPlayerPreferences() {
		var key = getKeyForChosenCampaignIndex();
		return PlayerPrefs.GetInt(key);
	}

	private void loadCurrentCampaignLevel() {
		resetDataAboutCampaignLevelPayment();
		loadCurrentLevelOfChosenCampaign();
		moveOnToTheNextCampaignLevel();
	}

	private void resetDataAboutCampaignLevelPayment() {
		var value = getValueForDataAboutCampaignLevelPaymentByDefault();
		setDataAboutCampaignLevelPayment(value);
	}

	private bool getValueForDataAboutCampaignLevelPaymentByDefault() {
		return getValueForUnpaidCampaignLevel();
	}

	private bool getValueForUnpaidCampaignLevel() {
		return false;
	}

	private void setDataAboutCampaignLevelPayment(bool value) {
		campaignLevelIsPaid = value;
	}

	private void loadCurrentLevelOfChosenCampaign() {
		
			//currentCampaignLevelIsPaidInPlayMode
		if (currentCampaignLevelIsPaid () && playModeIsEnabled ()) {
			loadPaidCampaignLevel ();
		} else {
			loadUnpaidCampaignLevel ();
		}
//			P
//			loadpc
//			p
//		}


//		if (currentCampaignLevelIsPaid()) {
//			loadPaidCampaignLevel();
//		} else {
//			loadUnpaidCampaignLevel();
//		}
	}

	private bool currentCampaignLevelIsPaid() {
		var engine = getSearchEngineForCampaignLevels();
		return engine.levelIsPurchased();
	}

	private CampaignLevelsSearchEngine getSearchEngineForCampaignLevels() {
		int targetLevelNumber = getNumberOfTargetCampaignLevel();
		return new CampaignLevelsSearchEngine(targetLevelNumber);
	}

	private int getNumberOfTargetCampaignLevel() {
		var level = getCurrentLevelByItsIndex();
		return level.number;
	}

	private void loadPaidCampaignLevel() {
		instantiateButtonForPaidCampaignLevel();
		setUpButtonForPaidCampaignLevel();
	}

	private void instantiateButtonForPaidCampaignLevel() {
		var value = getInstantiatedButtonForPaidCampaignLevel();
		setButtonForCurrentLevel(value);
	}

	private GameObject getInstantiatedButtonForPaidCampaignLevel() {
		var original = getOriginalForButtonForPaidCampaignLevel();
		var parent = getParentForButtonForPaidCampaignLevel();
		return Instantiate(original, parent);		
	}

	private GameObject getOriginalForButtonForPaidCampaignLevel() {
		var o = getLoadedOriginalForButtonForPaidCampaignLevel();
		return (GameObject) o;
	}

	private UnityEngine.Object getLoadedOriginalForButtonForPaidCampaignLevel() {
		var path = getPathForOriginalForButtonForPaidCampaignLevel();
		return Resources.Load(path);	
	}

	private string getPathForOriginalForButtonForPaidCampaignLevel() {
		return "Order/Ordinary Level Button";
	}

	private Transform getParentForButtonForPaidCampaignLevel() {
		var name = getNameForParentForButtonForPaidCampaignLevel();
		return transform.Find(name);
	}

	private string getNameForParentForButtonForPaidCampaignLevel() {
		return "Panel/Scroll Panel/Buttons";
	}

	private void setUpButtonForPaidCampaignLevel() {
		setUpTextOfButtonForPaidCampaignLevel();
		setUpNameOfButtonForPaidCampaignLevel();
	}

	private void setUpTextOfButtonForPaidCampaignLevel() {
		var t = getTextComponentFromButtonForPaidCampaignLevel();
		var value = getValueForTextOfButtonForPaidCampaignLevel();
		t.text = value;
	}

	private Text getTextComponentFromButtonForPaidCampaignLevel() {
		var go = getButtonForCurrentLevel();
		return go.GetComponentInChildren<Text>();
	}

	private GameObject getButtonForCurrentLevel() {
		return buttonForCurrentLevel;
	}

	private string getValueForTextOfButtonForPaidCampaignLevel() {
		int number = getNumberFromCurrentCampaignLevel();
		return number.ToString();
	}
	//Може замінити на "getNumberFromCurrentCampaignLevel()"?
//		.
	private int getNumberFromCurrentCampaignLevel() {
		
		try {
			var level = getCurrentLevelByItsIndex();
			return level.number;
		} catch(Exception e) {
			return getButtonForCurrentLevel ().transform.parent.childCount;
		}

//		var level = getCurrentLevelByItsIndex();
//		return level.number;
	}

	private Level getCurrentLevelByItsIndex() {
		var levels = getLevelsFromChosenCampaign();
		var index = getCurrentLevelIndex();
		return levels[index];
	}

	private void setUpNameOfButtonForPaidCampaignLevel() {
		var go = getButtonForCurrentLevel();
		var value = getNameForButtonForCurrentLevel();
		go.name = value;
	}

	private string getNameForButtonForCurrentLevel() {
		var header = getHeaderForNameForButtonForCurrentLevel();
		var value = getValueForNameForButtonForCurrentLevel();
		return header + value;
	}

	private string getHeaderForNameForButtonForCurrentLevel() {
		return "Level ";
	}

	private string getValueForNameForButtonForCurrentLevel() {
		var t = getTextComponentFromButtonForCurrentLevel();
		return t.text;	
	}

	private Text getTextComponentFromButtonForCurrentLevel() {
		return getTextComponentFromButtonForPaidCampaignLevel();
	}

	private void loadUnpaidCampaignLevel() {
		instantiateButtonForUnpaidCampaignLevel();
		setUpButtonForUnpaidCampaignLevel();
	}

	private void instantiateButtonForUnpaidCampaignLevel() {
		var value = getInstantiatedButtonForUnpaidCampaignLevel();
		setButtonForCurrentLevel(value);
	}

	private GameObject getInstantiatedButtonForUnpaidCampaignLevel() {
		var original = getOriginalForButtonForUnpaidCampaignLevel();
		var parent = getParentForButtonForUnpaidCampaignLevel();
		return Instantiate(original, parent);
	}

	private GameObject getOriginalForButtonForUnpaidCampaignLevel() {
		var o = getLoadedOriginalForButtonForUnpaidCampaignLevel();				
		return (GameObject) o;
	}

	private UnityEngine.Object getLoadedOriginalForButtonForUnpaidCampaignLevel() {
		var path = getPathForOriginalForButtonForUnpaidCampaignLevel();				   
		return Resources.Load(path);
	}

	private string getPathForOriginalForButtonForUnpaidCampaignLevel() {
		return "Order/Campaign Unpaid Level Button";
	}

	private Transform getParentForButtonForUnpaidCampaignLevel() {
		return getParentForButtonForPaidCampaignLevel();
	}

	private void setUpButtonForUnpaidCampaignLevel() {
		setUpNumberForButtonForUnpaidCampaignLevel();
		setUpCostForButtonForUnpaidCampaignLevel();
		setUpNameForButtonForUnpaidCampaignLevel();
	}

	private void setUpNumberForButtonForUnpaidCampaignLevel() {
		var t = getTextComponentFromNumberOfButtonForUnpaidCampaignLevel();
		var value = getValueForNumberOfButtonForUnpaidCampaignLevel();
		t.text = value;
	}

	private Text getTextComponentFromNumberOfButtonForUnpaidCampaignLevel() {
		var t = getTransformFromNumberOfButtonForUnpaidCampaignLevel();
		return t.GetComponent<Text>();
	}

	private Transform getTransformFromNumberOfButtonForUnpaidCampaignLevel() {
		var t = getTransformFromButtonForUnpaidCampaignLevel();
		var name = getNameForNumberOfButtonForUnpaidCampaignLevel();
		return t.Find(name);
	}

	private Transform getTransformFromButtonForUnpaidCampaignLevel() {
		var go = getButtonForCurrentLevel();
		return go.transform;
	}

	private string getNameForNumberOfButtonForUnpaidCampaignLevel() {
		return "Number";
	}

	private string getValueForNumberOfButtonForUnpaidCampaignLevel() {
		var number = getNumberFromCurrentCampaignLevel();
		return number.ToString();
	}

	private void setUpCostForButtonForUnpaidCampaignLevel() {
		var t = getTextComponentFromCostOfButtonForUnpaidCampaignLevel();
		var value = getValueForCostOfButtonForUnpaidCampaignLevel();
		t.text = value;
	}

	private Text getTextComponentFromCostOfButtonForUnpaidCampaignLevel() {
		var t = getTransformFromCostOfButtonForUnpaidCampaignLevel();				
		return t.GetComponent<Text>();
	}

	private Transform getTransformFromCostOfButtonForUnpaidCampaignLevel() {
		var t = getTransformFromButtonForUnpaidCampaignLevel();				
		var name = getNameForCostOfButtonForUnpaidCampaignLevel();
		return t.Find(name);
	}

	private string getNameForCostOfButtonForUnpaidCampaignLevel() {
		return "Cost";
	}

	private string getValueForCostOfButtonForUnpaidCampaignLevel() {
		var cost = getCostFromCurrentCampaignLevel();				   
		return cost.ToString();
	}
	//Може перейменувати в "getCostFromCurrentCampaignLevel()"?
	private int getCostFromCurrentCampaignLevel() {
		
		try {
			//getCostFromCurrentLevel()
			var level = getCurrentLevelByItsIndex();
			return level.cost;
		} catch (Exception e) {
			//getCostFromCurrentLevelInCaseOf(e)
			//getCostFromCurrentLevelBecause
			//getCostFromCurrentLevelBecauseOf(e)
			return 0;
		}

//		var level = getCurrentLevelByItsIndex();
//		return level.cost;
	}

	private void setUpNameForButtonForUnpaidCampaignLevel() {
		var go = getButtonForCurrentLevel();
		var value = getValueForNameForButtonForUnpaidCampaignLevel();
		go.name = value;
	}

	private string getValueForNameForButtonForUnpaidCampaignLevel() {
		var t = getTextComponentFromNumberOfButtonForUnpaidCampaignLevel();
		return t.text;
	}

	private void moveOnToTheNextCampaignLevel() {
		int value = getValueForMovingOnToTheNextCampaignLevel();
		setCurrentLevelIndex(value);
	}

	private int getValueForMovingOnToTheNextCampaignLevel() {
		int index = getCurrentLevelIndex();
		int increment = getIncrementForMovingOnToTheNextCampaignLevel();
		return index + increment;
	}

	private int getIncrementForMovingOnToTheNextCampaignLevel() {
		return 1;
	}

	private void loadFreeLevels() {
		while (thereAreFreeLevelsNeedToBeLoaded()) {
			loadCurrentFreeLevel();
		}
	}

	private bool thereAreFreeLevelsNeedToBeLoaded() {
		int index = getCurrentLevelIndex();
		int number = getNumberOfFreeLevels();
		return index < number;
	}

	private int getNumberOfFreeLevels() {
		var levels = getFreeLevelsFromLevelsSet();
		return levels.Count;
	}

	private List<Level> getFreeLevelsFromLevelsSet() {
		var levels = getSetOfLevels();
		return levels.freeLevels;
	}

	private void loadCurrentFreeLevel() {
		instantiateButtonForFreeLevel();
		setUpButtonForFreeLevel();
		moveOnToTheNextFreeLevel();
	}

	private void instantiateButtonForFreeLevel() {
		var value = getInstantiatedButtonForFreeLevel();
		setButtonForCurrentLevel(value);
	}

	private GameObject getInstantiatedButtonForFreeLevel() {
		return getInstantiatedButtonForPaidCampaignLevel();
	}

	private void setUpButtonForFreeLevel() {
		setUpTextOfButtonForFreeLevel();
		setUpNameOfButtonForFreeLevel();
		setUpAvailabilityOfButtonForFreeLevel();
	}

	private void setUpTextOfButtonForFreeLevel() {
		var t = getTextComponentFromButtonForFreeLevel();
		var value = getValueForTextOfButtonForFreeLevel();
		t.text = value;
	}

	private Text getTextComponentFromButtonForFreeLevel() {
		return getTextComponentFromButtonForPaidCampaignLevel();
	}

	private string getValueForTextOfButtonForFreeLevel() {
		int number = getNumberFromCurrentFreeLevel();
		return number.ToString();
	}

	private int getNumberFromCurrentFreeLevel() {
		
		//try {
		//	return getNumberFromCurrentFreeLevel();
		//} catch (Exception e) {
		//	return getNumberFromCurrentFreeLevelBecauseOf(e);
		//}
		try {
			var level = getCurrentFreeLevelByItsIndex();
			return level.number;
		} catch (Exception e) {
			return getButtonForCurrentLevel ().transform.parent.childCount;

//			return getCurrentLevelIndex();

//			l
//			reet
//			return currentLevelIndex;
		}

//		var level = getCurrentFreeLevelByItsIndex();
//		return level.number;
	}

	private Level getCurrentFreeLevelByItsIndex() {
		var levels = getFreeLevelsFromLevelsSet();
		var index = getCurrentLevelIndex();
		return levels[index];
	}

	private void setUpNameOfButtonForFreeLevel() {
		setUpNameOfButtonForPaidCampaignLevel();
	}

	private void setUpAvailabilityOfButtonForFreeLevel() {
		var b = getButtonComponentFromButtonForFreeLevel();
		var value = getValueForAvailabilityOfButtonForFreeLevel();
		b.interactable = value;
	}

	private Button getButtonComponentFromButtonForFreeLevel() {
		var go = getButtonForCurrentLevel();
		return go.GetComponentInChildren<Button>();
	}

	private bool getValueForAvailabilityOfButtonForFreeLevel() {
		if (buttonForTheFirstFreeLevelIsUnavailable()) {
			return getValueForAvailableButtonForTheFirstFreeLevel();
		} else {
			return getValueForAvailableButtonForCurrentFreeLevel();
		}
	}

	private bool buttonForTheFirstFreeLevelIsUnavailable() {
		int number = getNumberOfFreeLevelsFromPlayerProgress();
		int value = getValueForButtonForTheFirstFreeLevelBeingUnavailable();
		return number == value;
	}

	private int getNumberOfFreeLevelsFromPlayerProgress() {
		var levels = getFreeLevelsFromPlayerProgress();
		return levels.Count;
	}

	private List<PlayerProgressLevel> getFreeLevelsFromPlayerProgress() {
		var playerProgress = getProgressOfPlayer();
		return playerProgress.freeLevels;	
	}

	private PlayerProgress getProgressOfPlayer() {
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
		stream.Close();
		return playerProgress;
	}

	private XmlSerializer getSerializerForPlayerProgress() {
		var type = typeof(PlayerProgress);
		return new XmlSerializer(type);
	}

	private Stream getStreamForPlayerProgress() {
		var path = getPathForPlayerProgress();
		var mode = getModeForPlayerProgress();
		return new FileStream(path, mode);
	}

	private string getPathForPlayerProgress() {
		var key = getKeyForPathToPlayerProgress();
		return PlayerPrefs.GetString(key);
	}

	private string getKeyForPathToPlayerProgress() {
		return "Path To Player Progress";
	}

	private FileMode getModeForPlayerProgress() {
		return FileMode.OpenOrCreate;
	}

	private int getValueForButtonForTheFirstFreeLevelBeingUnavailable() {
		return 0;	
	}
					
	private bool getValueForAvailableButtonForTheFirstFreeLevel() {
		grantAccessToButtonForTheFirstFreeLevel();
		return getValueForButtonForFreeLevelBeingAvailable();
	}

	private void grantAccessToButtonForTheFirstFreeLevel() {
		var serializer = getSerializerForPlayerProgress();
		var o = getProgressOfPlayer();
		var stream = getStreamForPlayerProgress();
		var freeLevels = o.freeLevels;
		var item = getItemForTheFirstFreeLevel();
		freeLevels.Add(item);
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private PlayerProgressLevel getItemForTheFirstFreeLevel() {
		var item = getItemForFreeLevelByDefault();
		item.number = getNumberForItemForTheFirstFreeLevel();
		return item;
	}

	private PlayerProgressLevel getItemForFreeLevelByDefault() {
		return new PlayerProgressLevel();
	}

	private int getNumberForItemForTheFirstFreeLevel() {
		return 1;
	}

	private bool getValueForButtonForFreeLevelBeingAvailable() {
		return true;
	}
				 
	private bool getValueForAvailableButtonForCurrentFreeLevel() {
		
		var engine = getSearchEngineForFreeLevels();
		return engine.levelIsOpened () || !playModeIsEnabled ();

//			leveledim;
//		n

		//		..

		//levelIsOpened || LevelEditorModeIsEnabled
		//Po


//		var engine = getSearchEngineForFreeLevels();
//		return engine.levelIsOpened();
	}

	private FreeLevelsSearchEngine getSearchEngineForFreeLevels() {
		int targetLevelNumber = getNumberOfTargetFreeLevel();
		return new FreeLevelsSearchEngine(targetLevelNumber);
	}

	private int getNumberOfTargetFreeLevel() {
		return getNumberFromCurrentFreeLevel();
	}

	private void moveOnToTheNextFreeLevel() {
		moveOnToTheNextCampaignLevel();
	}

	private void initializeFields() {
		levelsList = getLoadedListOfLevels();
		currentUserCreatedSelectLevelButtonPosition = 2;
	}

	private XmlDocument getLoadedListOfLevels() {
		LevelsListLoader l = new LevelsListLoader();
		return l.getList();
	}

	private void hideAddNewLevelButtonIfItIsNecessary() {
		if (playModeIsEnabled()) {
			hideAddNewLevelButton();
		}
	}

	private bool playModeIsEnabled() {
		const string KEY = "Level Editor Mode";
		string value = PlayerPrefs.GetString(KEY);
		return value != "Enabled";
	}

	private void hideAddNewLevelButton() {
		const string TAG = "Add New Level Button";
		var go = GameObject.FindGameObjectWithTag(TAG);
		go.SetActive(false);
	}

	private void loadUserCreatedSelectLevelButtons() {
		while (thereAreButtonsNeedToBeLoaded()) {
			loadCurrentUserCreatedSelectLevelButton();
		}
	}

	private bool thereAreButtonsNeedToBeLoaded() {
		const string XPATH = "Levels/Level";
		var levels = levelsList.SelectNodes(XPATH);
		int n = levels.Count;
		int i = currentUserCreatedSelectLevelButtonPosition;
		return i < n;
	}

	private void loadCurrentUserCreatedSelectLevelButton() {
		loadButton();
		moveOnToTheNextUserCreatedSelectLevelButton();
	}

	private void loadButton() {
		string buttonName = getNameForSavedButton();
		var c = new SelectLevelButtonCreator(buttonName);
		c.setUpButton();
	}

	private string getNameForSavedButton() {
		const string XPATH = "Levels/Level";
		const string NAME = "Number";
		const string HEADER = "Level ";
		int i = currentUserCreatedSelectLevelButtonPosition;
		var levels = levelsList.SelectNodes(XPATH);
		var level = levels[i];
		var xac = level.Attributes;
		var xa = xac[NAME];
		string levelNumber = xa.Value;
		return HEADER + levelNumber;
	}

	private void moveOnToTheNextUserCreatedSelectLevelButton() {
		++currentUserCreatedSelectLevelButtonPosition;
	}

	public void startLevel(int levelNumber) {
		const string KEY = "Loaded level number";
		const string NAME = "Level Base";
		int value = levelNumber;
		PlayerPrefs.SetInt(KEY, value);
		Application.LoadLevel(NAME);
	}

	public void addNewLevel() {
		
		//if (campaignModeIsOn()) {
		//		addNewCampaignLevel();
		//} else {
		//		addNewFreeLevel();
		//}
		if (playerIsInCampaign ()) {
			setCurrentLevelIndex (getNumberOfCampaignLevels ());
			loadUnpaidCampaignLevel ();
//			loadCurrentCampaignLevel ();

			//saveCurrentCampaignLevel 
			var serializer = getSerializerForLevelsSet();
			var stream = new FileStream (PlayerPrefs.GetString ("Path To Levels"), FileMode.OpenOrCreate);
			var o = serializer.Deserialize (stream) as Levels;
			var item = new Level();
			item.number = getNumberFromCurrentCampaignLevel ();
//				getNumberForItemForTheFirstFreeLevelgetNumberfo
				//getnumberforcu
//			item.number = getCurrentLevelIndex();
			item.cost = 0;

			stream.Close ();

			stream = new FileStream (PlayerPrefs.GetString ("Path To Levels"), FileMode.OpenOrCreate);

			o.campaigns [getChosenCampaignIndexFromPlayerPreferences ()].levels
				.Add (item);
			serializer.Serialize(stream, o);
//				getchsi
			stream.Close();

		} else {
			setCurrentLevelIndex (getNumberOfFreeLevels ());
			loadCurrentFreeLevel ();

			//saveNewFreeLevel
			var serializer = getSerializerForLevelsSet();
			var stream = new FileStream (PlayerPrefs.GetString ("Path To Levels"), FileMode.OpenOrCreate);
			var o = serializer.Deserialize (stream) as Levels;
			var item = new Level();
			item.number = getCurrentLevelIndex ();

			stream.Close();

			stream = new FileStream (PlayerPrefs.GetString ("Path To Levels"), FileMode.OpenOrCreate);

			o.freeLevels.Add (item);
			serializer.Serialize (stream, o);
			stream.Close ();
		}

//			P

		//if (playerIsInCampaign())

//		string buttonName = getNameForNewButton();
//		var c = new SelectLevelButtonCreator(buttonName);
//		c.setUpButton();
	}

	private string getNameForNewButton() {
		const string A = "Level ";
		int newLevelNumber = getLevelNumberForNewButton();
		return A + newLevelNumber;
	}

	private int getLevelNumberForNewButton() {
		const string TAG = "Go To Chosen Level Button";
		var go = GameObject.FindGameObjectsWithTag(TAG);
		return go.Length + 1;
	}


	public void loadPlayerCoins() {		
		string balance;
		var stream = new FileStream(PlayerPrefs.GetString("Path To Player Progress"),
			FileMode.OpenOrCreate);
		try {
			var serializer = new XmlSerializer(typeof(PlayerProgress));
			var playerProgress =  serializer.Deserialize(stream) as PlayerProgress;
			stream.Close();
			balance = playerProgress.balanceInCoins.ToString();
		} catch (Exception e) {
			var serializer = new XmlSerializer(typeof(PlayerProgress));
			var o = new PlayerProgress();
			o.balanceInCoins = 1000;
			serializer.Serialize(stream, o);
			stream.Close();
			balance = o.balanceInCoins.ToString();
		}

		transform.Find ("Coins/Value").GetComponent<Text> ().text = balance;
	}


	public void goToPreviousScreen() {
		if (PlayerPrefs.HasKey ("Chosen Campaign Index")) {
			PlayerPrefs.DeleteKey ("Chosen Campaign Index");
			SceneManager.LoadScene ("Campaigns Screen");
		} else {
			SceneManager.LoadScene ("Start Screen");
		}
	}


	public void updateCostForChosenLevel() {
		var serializer = getSerializerForLevelsSet();
		var stream = new FileStream (PlayerPrefs.GetString ("Path To Levels"), FileMode.OpenOrCreate);
		var o = serializer.Deserialize (stream) as Levels;

		stream.Close();

		stream = new FileStream (PlayerPrefs.GetString ("Path To Levels"), FileMode.OpenOrCreate);

		try {
		o.campaigns [getChosenCampaignIndexFromPlayerPreferences ()]
			.levels [PlayerPrefs.GetInt ("Chosen Level Index")]
			.cost = int.Parse (transform.Find ("Cost Pop-Up Window/Input Field")
					.GetComponent<InputField> ().text);
		} catch (Exception e) {
		}

//		int.TryParse (transform.Find ("Cost Pop-Up Window/Input Field")
//			.GetComponent<InputField> ().text, out o.campaigns [getChosenCampaignIndexFromPlayerPreferences ()]
//			.levels [PlayerPrefs.GetInt ("Chosen Level Index")]
//			.cost);

		serializer.Serialize(stream,o);
		stream.Close();

//		int.TryParse
//		Debug.Log (transform.Find ("Cost Pop-Up Window/Input Field")
//			.GetComponent<InputField> ().text + " coins");

//		'
//		f
//				ш
//			ggii
//			з
	}
}