﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using System.Xml.Serialization;
using AssemblyCSharp;

public class MainMenu : MonoBehaviour {
	private void Start() {
		disableLevelEditorMode();
		setUpPathToPlayerProgress();
		setUpPathToLevelsSet();
		//giveDailyBonusAtFirstOpportunity
		giveDailyBonusIfItIsPossible();
	}



	public

//	private
	//Може замінити на "turnOffLevelEditorMode()"?
	void disableLevelEditorMode() {
		var key = getKeyForSettingUpLevelEditorMode();
		PlayerPrefs.DeleteKey(key);
	}
	//Може замінити на "getKeyForSwitchingLevelEditorMode()", "getKeyForTogglingLevelEditorMode"?
	private string getKeyForSettingUpLevelEditorMode() {
		return "Level Editor Mode";
	}

	private void setUpPathToPlayerProgress() {
		var key = getKeyForSettingUpPathToPlayerProgress();
		var value = getValueForSettingUpPathToPlayerProgress();
		PlayerPrefs.SetString(key, value);
	}

	private string getKeyForSettingUpPathToPlayerProgress() {
		return "Path To Player Progress";
	}

	private string getValueForSettingUpPathToPlayerProgress() {
		var filePath = getFilePathForPlayerProgress();
		var fileName = getFileNameForPlayerProgress();
		return filePath + fileName;
	}

	private string getFilePathForPlayerProgress() {
		Debug.Log ( Application.persistentDataPath );

		return Application.persistentDataPath;
	}

	private string getFileNameForPlayerProgress() {
		return "/Player Progress.xml";
	}


	public

//	private

	void setUpPathToLevelsSet() {
		if (levelEditorModeIsEnabled()) {
			setUpPathToLevelsSetInLevelEditorMode();
		} else {
			setUpPathToLevelsSetInPlayMode();
		}
	}

	private bool levelEditorModeIsEnabled() {
		var key = getKeyForSettingUpLevelEditorMode();
		return PlayerPrefs.HasKey(key);
	}


//	.
//	private 
	public
	void setUpPathToLevelsSetInLevelEditorMode() {
		var key = getKeyForSettingUpPathToLevelsSetInLevelEditorMode();
		var value = getValueForSettingUpPathToLevelsSetInLevelEditorMode();
		PlayerPrefs.SetString(key, value);
	}

	private string getKeyForSettingUpPathToLevelsSetInLevelEditorMode() {
		return "Path To Levels";
	}

	private string getValueForSettingUpPathToLevelsSetInLevelEditorMode() {
		
		return transform.Find("Levels Set Location Pop-Up Window")
			.GetComponentInChildren<InputField>().text;
		//		.

//		return "C:\\Users\\Shield\\Desktop\\Levels.xml";
	}

	private void setUpPathToLevelsSetInPlayMode() {
		var key = getKeyForSettingUpPathToLevelsSetInPlayMode();
		var value = getValueForSettingUpPathToLevelsSetInPlayMode();
		PlayerPrefs.SetString(key, value);	
	}

	private string getKeyForSettingUpPathToLevelsSetInPlayMode() {
		return "Levels";
	}

	private string getValueForSettingUpPathToLevelsSetInPlayMode() {
		var ta = getTextAssetForLevelsSet();
		return ta.text;
	}

	private TextAsset getTextAssetForLevelsSet() {
		var o = getObjectForLevelsSet();
		return (TextAsset) o;
	}

	private Object getObjectForLevelsSet() {
		var path = getPathForLevelsSet();
		var systemTypeInstance = getSystemTypeInstanceForLevelsSet();
		return Resources.Load(path, systemTypeInstance);
	}

	private string getPathForLevelsSet() {
		return "Order/Levels";
	}

	private System.Type getSystemTypeInstanceForLevelsSet() {
		return typeof(TextAsset);
	}
	//giveDailyBonusAtFirstOpportunity
	private void giveDailyBonusIfItIsPossible() {
		if (playerEntersGameForTheFirstTime()) {
			giveDailyBonus();
		} else if (playerEntersGameAfterRequiredTimeHasPassed()) {
			giveDailyBonus();
		}
	}

	private bool playerEntersGameForTheFirstTime() {
		var b = playerEntersGameAtLeastForTheSecondTime();
		return !b;
	}

	private bool playerEntersGameAtLeastForTheSecondTime() {
		var key = getKeyForWorkingWithLastAccessDate();
		return PlayerPrefs.HasKey(key);
	}
	//Може замінити на "getKeyForLastAccessDate"?
	private string getKeyForWorkingWithLastAccessDate() {
		return "Last Access Date";
	}

	private bool playerEntersGameAfterRequiredTimeHasPassed() {
		var ba = atLeastDayHasPassed();
		var bb = atLeastYearHasPassed();
		return ba || bb;
	}

	private bool atLeastDayHasPassed() {
		int a = getDayFromLastAccessDate();
		int b = getDayFromCurrentMoment();
		return a < b;
	}

	private int getDayFromLastAccessDate() {
		var date = getDateOfLastAccess();
		return date.DayOfYear;
	}

	private System.DateTime getDateOfLastAccess() {
		var value = getStringRepresentationForLastAccessDate();
		return System.Convert.ToDateTime(value);
	}

	private string getStringRepresentationForLastAccessDate() {
		var key = getKeyForWorkingWithLastAccessDate();
		return PlayerPrefs.GetString(key);
	}

	private int getDayFromCurrentMoment() {
		var date = getDateFromCurrentMoment();
		return date.DayOfYear;
	}

	private System.DateTime getDateFromCurrentMoment() {
		return System.DateTime.Now;
	}

	private bool atLeastYearHasPassed() {
		int a = getYearFromLastAccessDate();
		int b = getYearFromCurrentMoment();
		return a < b;
	}

	private int getYearFromLastAccessDate() {
		var date = getDateOfLastAccess();
		return date.Year;
	}

	private int getYearFromCurrentMoment() {
		var date = getDateFromCurrentMoment();
		return date.Year;
	}

	private void giveDailyBonus() {		
		updateBalanceInCoins();
		updateLastAccessDate();
		showDailyBonusPopUpWindow();
	}

	private void updateBalanceInCoins() {
		var o = getPlayerProgressItself();
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		o.balanceInCoins += getCoinsForDailyBonus();
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private PlayerProgress getPlayerProgressItself() {
		
		var stream = getStreamForPlayerProgress();
		try {
			var serializer = getSerializerForPlayerProgress();
//			var stream = getStreamForPlayerProgress();
			var o = serializer.Deserialize(stream) as PlayerProgress;
//			stream.Close();
			return o;
		} catch(System.Exception e){
			return new PlayerProgress ();
		} finally {
			stream.Close();
		}
//		try
//		tryf

//		var serializer = getSerializerForPlayerProgress();
//		var stream = getStreamForPlayerProgress();
//		var o = serializer.Deserialize(stream) as PlayerProgress;
//		stream.Close();
//		return o;
	}

	private XmlSerializer getSerializerForPlayerProgress() {
		var type = getTypeForPlayerProgress();
		return new XmlSerializer(type);
	}

	private System.Type getTypeForPlayerProgress() {
		return typeof(PlayerProgress);
	}

	private Stream getStreamForPlayerProgress() {
		var path = getPathToPlayerProgress();
		var mode = getModeForPlayerProgress();
		return new FileStream(path, mode);
	}

	private string getPathToPlayerProgress() {
		var key = getKeyForPathToPlayerProgress();
		return PlayerPrefs.GetString(key);
	}

	private string getKeyForPathToPlayerProgress() {
		return getKeyForSettingUpPathToPlayerProgress();
	}

	private FileMode getModeForPlayerProgress() {
		return FileMode.OpenOrCreate;
	}

	private int getCoinsForDailyBonus() {
		return 200;
	}

	private void updateLastAccessDate() {
		//Може замінити на "getKeyForLastAccessDate()"?
		var key = getKeyForWorkingWithLastAccessDate();
		var value = getStringRepresentationForUpdatedLastAccessDate();
		PlayerPrefs.SetString(key, value);	
	}

	private string getStringRepresentationForUpdatedLastAccessDate() {
		var value = getDateFromCurrentMoment();
		return System.Convert.ToString(value);
	}

	private void showDailyBonusPopUpWindow() {
		var go = getGameObjectForDailyBonusPopUpWindow();
		var value = getValueForDailyBonusPopUpWindowToBeVisible();
		go.SetActive(value);
	}

	private GameObject getGameObjectForDailyBonusPopUpWindow() {
		var t = getTransformForDailyBonusPopUpWindow();
		return t.gameObject;	
	}

	private Transform getTransformForDailyBonusPopUpWindow() {
		var name = getNameForDailyBonusPopUpWindow();
		return transform.Find(name);
	}

	private string getNameForDailyBonusPopUpWindow() {
		return "Daily Bonus Pop-Up Window";
	}

	private bool getValueForDailyBonusPopUpWindowToBeVisible() {
		return true;
	}

	public void goToLevelSelect() {
		const string NAME = "LevelSelect";
		Application.LoadLevel(NAME);
	}

	public void goToLevelEditor() {
		enableLevelEditorMode();
		goToLevelSelect();
	}

	public void fillUpLevelsSetLocationPopUpWindowInputField() {
		transform.Find ("Levels Set Location Pop-Up Window")
			.GetComponentInChildren<InputField> ()
			.text = PlayerPrefs
				.GetString (getKeyForSettingUpPathToLevelsSetInLevelEditorMode ());
	}


	public

	//private 

	void enableLevelEditorMode() {
		const string KEY = "Level Editor Mode";
		const string VALUE = "Enabled";
		PlayerPrefs.SetString(KEY, VALUE);
	}

	public void goToStartScreen() {		
		var sceneName = getSceneNameForStartScreen();
		SceneManager.LoadScene(sceneName);
	}

	private string getSceneNameForStartScreen() {
		return "Start Screen";
	}

	public void goToCampaignsScreen() {		
		var sceneName = getSceneNameForCampaignsScreen();
		SceneManager.LoadScene(sceneName);
	}

	private string getSceneNameForCampaignsScreen() {
		return "Campaigns Screen";
	}

	public void goToStatusScreen() {		
		var sceneName = getSceneNameForStatusScreen();
		SceneManager.LoadScene(sceneName);
	}

	private string getSceneNameForStatusScreen() {
		return "Status Screen";
	}

	public void goToShopScreen() {		
		var sceneName = getSceneNameForShopScreen();
		SceneManager.LoadScene(sceneName);
	}

	private string getSceneNameForShopScreen() {
		return "Shop Screen";
	}

	public void quit() {
		//for tests only
		PlayerPrefs.DeleteAll();

		Application.Quit();
	}
}