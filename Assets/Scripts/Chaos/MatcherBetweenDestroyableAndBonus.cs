using UnityEngine;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class MatcherBetweenDestroyableAndBonus {
		private string destroyableName;
		private bool destroyableHasItsMatch;
		private int currentBonusPosition;

		public MatcherBetweenDestroyableAndBonus(string name) {
			initializeFieldsFrom(name);
			calculateDestroyableMatch();
		}

		private void initializeFieldsFrom(string name) {
			destroyableName = name;
			destroyableHasItsMatch = false;
			currentBonusPosition = 0;
		}

		private void calculateDestroyableMatch() {
			while(needInCalculationsIsPresent()) {
				analyzeIfThereIsAMatchBetweenDestroyableAndCurrentBonus();
			}
		}

		private bool needInCalculationsIsPresent() {
			var bc = getControllerOfBonuses();
			var l = bc.getBonusesList();
			int i = currentBonusPosition;
			int n = l.Count;
			bool ba = i < n;
			bool bb = !destroyableHasItsMatch; 
			return ba && bb;
		}
		
		private BonusesController getControllerOfBonuses() {
			const string TAG = "Level";
			var go = GameObject.FindGameObjectWithTag(TAG);
			return go.GetComponent<BonusesController>();
		}

		private void analyzeIfThereIsAMatchBetweenDestroyableAndCurrentBonus() {
			analyzeIfThereIsAMatchBetweenDestroyableAndBonus();
			moveOnToTheNextBonus();
		}

		private void analyzeIfThereIsAMatchBetweenDestroyableAndBonus() {
			const string KEY = "Bonus parent name";
			var bc = getControllerOfBonuses();
			var list = bc.getBonusesList();
			int i = currentBonusPosition;
			string value = destroyableName;
			var bonus = list[i];
			string bonusParentName = bonus[KEY]; 
			destroyableHasItsMatch = bonusParentName.Equals(value);			 
		}

		private void moveOnToTheNextBonus() {
			++currentBonusPosition;
		}

		public bool destroyableHasMatch() {
			return destroyableHasItsMatch;
		}
	}
}