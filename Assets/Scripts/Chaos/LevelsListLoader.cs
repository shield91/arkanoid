using UnityEngine;
using System;
using System.Xml;
using System.Text;

namespace AssemblyCSharp
{
	public class LevelsListLoader {
		private XmlDocument list;

		public LevelsListLoader() {
			initializeList();
			loadList();
		}

		private void initializeList() {
			list = new XmlDocument();
		}

		private void loadList() {
			try {
				load();
			} catch(Exception e) {
				handle(e);
			}
		}

		private void load() {
			if (levelEditorModeIsEnabled()) {
				loadInLevelEditorMode();
			} else {
				loadInPlayMode();
			}
		}
		
		private bool levelEditorModeIsEnabled() {
			const string KEY = "Level Editor Mode";
			string value = PlayerPrefs.GetString(KEY);
			return value == "Enabled";
		}
		
		private void loadInLevelEditorMode() {
			const string A = "\\Levels list.xml";
			string saveLocation = getSaveLocationFromPlayerPreferences();
			string filename = saveLocation + A;
			list.Load(filename);
		}
		
		private string getSaveLocationFromPlayerPreferences() {
			const string KEY = "Save location";
			const string DEFAULT_VALUE = "C:\\Arkanoid levels";
			return PlayerPrefs.GetString(KEY, DEFAULT_VALUE);
		}
		
		private void loadInPlayMode() {
			const string PATH = "Levels/Levels list";
			Type systemTypeInstance = typeof(TextAsset);
			UnityEngine.Object o = Resources.Load(PATH, systemTypeInstance);
			TextAsset ta = (TextAsset)o;
			string xml = ta.text;
			list.LoadXml(xml);
		}
		
		private void handle(Exception e) {
			createLevelsListFile();
			load();
			continueHandlingOf(e);
		}
		
		private void createLevelsListFile() {
			const string A = "\\Levels list.xml";
			string saveLocation = getSaveLocationFromPlayerPreferences();
			string filename = saveLocation + A;
			Encoding encoding = Encoding.UTF8;
			XmlTextWriter writer = new XmlTextWriter(filename, encoding);
			writer.Formatting = Formatting.Indented;
			writer.WriteStartElement("Levels");
			writer.WriteStartElement("Level");
			writer.WriteAttributeString("Number", "1");
			writer.WriteEndElement();
			writer.WriteStartElement("Level");
			writer.WriteAttributeString("Number", "2");
			writer.WriteEndElement();
			writer.WriteEndElement();
			writer.Close();
		}
		
		private void continueHandlingOf(Exception e) {
			string message = e.StackTrace;
			Debug.Log(message);
		}

		public XmlDocument getList() {
			return list;
		}
	}
}