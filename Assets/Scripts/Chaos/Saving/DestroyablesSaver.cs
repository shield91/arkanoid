using System.Xml;

namespace AssemblyCSharp
{
	public class DestroyablesSaver : IndependentItemsSaver {
		public DestroyablesSaver(XmlTextWriter chosenLevel) : base(chosenLevel) {
		}

		protected override string getItemsName() {
			return "Destroyables";
		}

		protected override string getIndependentItemTag() {
			return "Destroy";
		}

		protected override string getCurrentItemName () {
			return "Destroyable";
		}
	}
}