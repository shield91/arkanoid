using System.Xml;

namespace AssemblyCSharp
{
	public class UndestroyablesSaver : IndependentItemsSaver {
		public UndestroyablesSaver(XmlTextWriter chosenLevel) : base(chosenLevel) {
		}

		protected override string getItemsName() {
			return "Undestroyables";
		}

		protected override string getIndependentItemTag() {
			return "Undestroy";
		}

		protected override string getCurrentItemName () {
			return "Undestroyable";
		}
	}
}