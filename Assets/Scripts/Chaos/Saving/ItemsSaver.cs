using UnityEngine;
using System;
using System.Xml;
using System.Text;

namespace AssemblyCSharp
{
	public abstract class ItemsSaver {
		private XmlTextWriter chosenLevel;
		private int currentItemPosition;

		public ItemsSaver(XmlTextWriter level) {
			chosenLevel = level;
			currentItemPosition = 0;
		}

		public void save() {
			resetCurrentItemPosition();
			openNodeForItemsInFileForChosenLevel();
			saveItemsThemselves();
			closeNodeForItemsInFileForChosenLevel();
		}

		private void resetCurrentItemPosition() {
			currentItemPosition = 0;
		}

		private void openNodeForItemsInFileForChosenLevel() {
			string localName = getItemsName();
			chosenLevel.WriteStartElement(localName);
		}

		protected abstract string getItemsName();

		private void saveItemsThemselves() {
			while(thereAreItemsNeedToBeAnalyzed()) {
				saveCurrentItem();
			}
		}

		private bool thereAreItemsNeedToBeAnalyzed() {
			int i = currentItemPosition;
			int n = getNumberOfItems();
			return i < n;
		}

		protected abstract int getNumberOfItems();

		private void saveCurrentItem() {
			openNodeForCurrentItemInFileForChosenLevel();
			saveCurrentItemMainParameters();
			closeNodeForCurrentItemInFileForChosenLevel();
			moveOnToTheNextItem();
		}

		private void openNodeForCurrentItemInFileForChosenLevel() {
			string localName = getCurrentItemName();
			chosenLevel.WriteStartElement(localName);
		}

		protected abstract string getCurrentItemName();

		protected abstract void saveCurrentItemMainParameters();

		private void closeNodeForCurrentItemInFileForChosenLevel() {
			closeNodeInFileForChosenLevel();
		}

		protected void closeNodeInFileForChosenLevel() {
			chosenLevel.WriteEndElement();
		}

		private void moveOnToTheNextItem() {
			++currentItemPosition;
		}

		private void closeNodeForItemsInFileForChosenLevel() {
			closeNodeInFileForChosenLevel();
		}

		protected int getCurrentItemPosition() {
			return currentItemPosition;
		}

		protected XmlTextWriter getChosenLevel() {
			return chosenLevel;
		}
	}
}