using UnityEngine;
using System.Xml;

namespace AssemblyCSharp
{
	public abstract class IndependentItemsSaver : ItemsSaver {
		public IndependentItemsSaver(XmlTextWriter chosenLevel) : base(chosenLevel) {
		}

		protected override int getNumberOfItems() {
			GameObject[] go = getIndependentItemsThemselves();
			return go.Length;
		}

		private GameObject[] getIndependentItemsThemselves() {
			string TAG = getIndependentItemTag();
			return GameObject.FindGameObjectsWithTag(TAG);
		}

		protected abstract string getIndependentItemTag();

		protected override void saveCurrentItemMainParameters() {
			saveCurrentIndependentItemPosition();
			saveCurrentIndependentItemName();
		}

		private void saveCurrentIndependentItemPosition() {
			saveCurrentIndependentItemXPosition();
			saveCurrentIndependentItemYPosition();
		}

		private void saveCurrentIndependentItemXPosition() {
			const string LOCAL_NAME = "xPosition";
			Vector3 position = getPositionOfCurrentIndependentItem();
			XmlTextWriter level = getChosenLevel();
			float xPosition = position.x;
			string value = xPosition.ToString();
			level.WriteAttributeString(LOCAL_NAME, value);
		}

		private Vector3 getPositionOfCurrentIndependentItem() {
			int i = getCurrentItemPosition();
			GameObject[] independentItems = getIndependentItemsThemselves();
			GameObject independentItem = independentItems[i];		
			Transform t = independentItem.transform;
			return t.position;
		}

		private void saveCurrentIndependentItemYPosition() {
			const string LOCAL_NAME = "yPosition";
			Vector3 position = getPositionOfCurrentIndependentItem();
			XmlTextWriter level = getChosenLevel();
			float yPosition = position.y;
			string value = yPosition.ToString();
			level.WriteAttributeString(LOCAL_NAME, value);
		}

		private void saveCurrentIndependentItemName() {
			const string LOCAL_NAME = "Name";
			int i = getCurrentItemPosition();
			GameObject[] independentItems = getIndependentItemsThemselves();
			GameObject independentItem = independentItems[i];
			XmlTextWriter level = getChosenLevel();
			string value = independentItem.name;
			level.WriteAttributeString(LOCAL_NAME, value);
		}
	}
}