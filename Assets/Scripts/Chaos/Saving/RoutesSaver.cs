using UnityEngine;
using System;
using System.Xml;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class RoutesSaver : ItemsSaver {
		private int currentPointPosition;

		public RoutesSaver(XmlTextWriter chosenLevel) : base(chosenLevel) {
			currentPointPosition = 0;
		}

		protected override string getItemsName() {
			return "Routes";
		}

		protected override int getNumberOfItems() {
			int n = getNumberOfDestroyables();
			int m = getNumberOfUndestroyables();
			return n + m;
		}

		private int getNumberOfDestroyables() {
			var d = getArrayOfDestroyables();
			return d.Length;
		}

		private GameObject[] getArrayOfDestroyables() {
			var sf = getSharedFunctionsFromLevelObject();
			return sf.getDestroyables();
		}
		
		private SharedFunctions getSharedFunctionsFromLevelObject() {
			const string TAG = "Level";
			var go = GameObject.FindGameObjectWithTag(TAG);
			return go.GetComponent<SharedFunctions>();
		}

		private int getNumberOfUndestroyables() {
			var u = getArrayOfUndestroyables();
			return u.Length;
		}

		private GameObject[] getArrayOfUndestroyables() {
			var sf = getSharedFunctionsFromLevelObject();
			return sf.getUndestroyables();
		}

		protected override string getCurrentItemName() {
			return "Route";
		}

		protected override void saveCurrentItemMainParameters() {
			resetCurrentPointPosition();
			saveRoute();
		}

		private void resetCurrentPointPosition() {
			currentPointPosition = 0;
		}

		private void saveRoute() {
			while(thereAreRoutePointsNeedToBeAnalyzed()) {
				saveCurrentPoint();
			}
		}

		private bool thereAreRoutePointsNeedToBeAnalyzed() {
			var route = getRouteFromCurrentItem();
			int i = currentPointPosition;
			int n = route.Count;
			return i < n;
		}

		private List<Dictionary<string, string>> getRouteFromCurrentItem() {
			var item = getCurrentItemByItsPosition();
			var ii = item.GetComponent<IndependentItem>();
			return ii.getWaybillPoints();
		}

		private GameObject getCurrentItemByItsPosition() {
			try {
				return getCurrentUndestroyableByItsPosition();
			} catch (Exception e) {
				return getCurrentDestroyableBecauseOf(e);
			}
		}
		
		private GameObject getCurrentUndestroyableByItsPosition() {
			int i = getCurrentItemPosition();
			int n = getNumberOfDestroyables();
			int index = i - n;
			var u = getArrayOfUndestroyables();
			return u[index];
		}
		
		private GameObject getCurrentDestroyableBecauseOf(Exception e) {
			handle(e);
			return getCurrentDestroyableByItsPosition();
		}
		
		private void handle(Exception e) {
			string message = e.StackTrace;
			Debug.Log(message);
		}
		
		private GameObject getCurrentDestroyableByItsPosition() {
			int i = getCurrentItemPosition();
			int index = i;
			var d = getArrayOfDestroyables();
			return d[index];
		}

		private void saveCurrentPoint() {
			savePoint();
			moveOnToTheNextPoint();
		}

		private void savePoint() {
			openNodeForCurrentPointInFileForChosenLevel();
			savePointMainParameters();
			closeNodeForCurrentPointInFileForChosenLevel();
		}	

		private void openNodeForCurrentPointInFileForChosenLevel() {
			string LOCAL_NAME = "Point";
			XmlTextWriter level = getChosenLevel();
			level.WriteStartElement(LOCAL_NAME);
		}

		private void savePointMainParameters() {
			const string LOCAL_NAME_1 = "x";
			const string LOCAL_NAME_2 = "y";
			const string LOCAL_NAME_3 = "timeToNextPoint";
			const string KEY_1 = LOCAL_NAME_1;
			const string KEY_2 = LOCAL_NAME_2;
			const string KEY_3 = "Time to next point";
			var level = getChosenLevel();
			var route = getRouteFromCurrentItem();
			int index = currentPointPosition;
			var point = route[index];
			var valueOne = point[KEY_1];
			var valueTwo = point[KEY_2];
			var valueThree = point[KEY_3];
			level.WriteAttributeString(LOCAL_NAME_1, valueOne);
			level.WriteAttributeString(LOCAL_NAME_2, valueTwo);
			level.WriteAttributeString(LOCAL_NAME_3, valueThree);
		}

		private void closeNodeForCurrentPointInFileForChosenLevel() {
			closeNodeInFileForChosenLevel();
		}

		private void moveOnToTheNextPoint() {
			++currentPointPosition;
		}
	}
}