using UnityEngine;
using System;
using System.Xml;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class BonusesSaver : ItemsSaver {
		public BonusesSaver(XmlTextWriter chosenLevel) : base(chosenLevel) {
		}

		protected override string getItemsName() {
			return "Bonuses";
		}

		protected override int getNumberOfItems() {
			var list = getListOfBonuses();
			return list.Count;
		}

		private List<Dictionary<string, string>> getListOfBonuses() {
			const string TAG = "Level";
			var go = GameObject.FindGameObjectWithTag(TAG);
			var bc = go.GetComponent<BonusesController>();
			return bc.getBonusesList();
		}

		protected override string getCurrentItemName() {
			return "Bonus";
		}

		protected override void saveCurrentItemMainParameters() {
			saveCurrentBonusName();
			updateCurrentBonusParentNameIfBonusIsTheFixedOne();
			saveCurrentBonusParentName();
		}

		private void saveCurrentBonusName() {
			const string NAME = "Bonus name";
			const string LOCAL_NAME = "Name";
			XmlTextWriter level = getChosenLevel();
			string value = getValueFor(NAME);
			level.WriteAttributeString(LOCAL_NAME, value);
		}

		private string getValueFor(string name) {
			var currentBonus = getBonusAtCurrentItemPosition();
			return currentBonus[name];
		}

		private Dictionary<string, string> getBonusAtCurrentItemPosition() {
			int i = getCurrentItemPosition();
			var list = getListOfBonuses();
			return list[i];
		}

		private void updateCurrentBonusParentNameIfBonusIsTheFixedOne() {
			if (currentBonusIsTheFixedOne()) {
				updateCurrentBonusParentNameToAvoidMisconceptionsDuringLoadingLevel();
			}
		}

		private bool currentBonusIsTheFixedOne() {
			const string NAME = "Bonus parent name";
			string name = getValueFor(NAME);
			int length = name.Length;
			return length > 0;
		}

		private void updateCurrentBonusParentNameToAvoidMisconceptionsDuringLoadingLevel() {
			const string KEY = "Bonus parent name";
			const string TAG = "Destroy";
			const char SEPARATOR = ' ';
			var currentBonus = getBonusAtCurrentItemPosition();
			string name = currentBonus[KEY];
			var destroyables = GameObject.FindGameObjectsWithTag(TAG);
			var parent = GameObject.Find(name);
			int position = Array.IndexOf(destroyables, parent);
			var actualNameBuilder = new ActualNameBuilder(name);
			string actualName = actualNameBuilder.getActualName();
			int destroyableNumber = position + 1;
			string newName = actualName + SEPARATOR + destroyableNumber;
			currentBonus[KEY] = newName;
		}

		private void saveCurrentBonusParentName() {
			const string NAME = "Bonus parent name";
			const string LOCAL_NAME = "ParentName";
			XmlTextWriter level = getChosenLevel();
			string value = getValueFor(NAME);
			level.WriteAttributeString(LOCAL_NAME, value);
		}
	}
}