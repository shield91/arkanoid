﻿using UnityEngine;

public class MainCamera : MonoBehaviour {
	void Update() {
		if (levelEditorModeIsEnabled()) {
			performActionWithCameraIfSpecialKeyHasBeenPressed();
		}
	}

	private bool levelEditorModeIsEnabled() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var sf = go.GetComponent<SharedFunctions>();
		return sf.levelEditorModeIsEnabled();
	}

	private void performActionWithCameraIfSpecialKeyHasBeenPressed() {
		moveCameraHorizontallyIfHorizontalArrowKeyHasBeenPressed();
		moveCameraVerticallyIfVerticalArrowKeyHasBeenPressed();
		zoomCameraIfZoomKeyHasBeenPressed();
	}

	private void moveCameraHorizontallyIfHorizontalArrowKeyHasBeenPressed() {
		moveCameraToTheLeftIfLeftArrowKeyHasBeenPressed();
		moveCameraToTheRightIfRightArrowKeyHasBeenPressed();
	}

	private void moveCameraToTheLeftIfLeftArrowKeyHasBeenPressed() {
		if (leftArrowKeyHasBeenPressed()) {
			moveCameraToTheLeft();
		}
	}

	private bool leftArrowKeyHasBeenPressed() {
		const KeyCode KEY = KeyCode.LeftArrow;
		return Input.GetKey(KEY);
	}

	private void moveCameraToTheLeft() {
		var position = transform.position;
		float x = position.x - 0.1f;
		float y = position.y;
		float z = position.z;
		transform.position = new Vector3 (x, y, z);
	}

	private void moveCameraToTheRightIfRightArrowKeyHasBeenPressed() {
		if (rightArrowKayHasBeenPressed()) {
			moveCameraToTheRight();
		}
	}

	private bool rightArrowKayHasBeenPressed() {
		const KeyCode KEY = KeyCode.RightArrow;
		return Input.GetKey(KEY);
	}

	private void moveCameraToTheRight() {
		var position = transform.position;
		float x = position.x + 0.1f;
		float y = position.y;
		float z = position.z;
		transform.position = new Vector3 (x, y, z);
	}

	private void moveCameraVerticallyIfVerticalArrowKeyHasBeenPressed() {
		moveCameraDownIfDownArrowKeyHasBeenPressed();
		moveCameraUpIfUpArrowKeyHasBeenPressed();
	}

	private void moveCameraDownIfDownArrowKeyHasBeenPressed() {
		if (downArrowKeyHasBeenPressed()) {
			moveCameraDown();
		}
	}

	private bool downArrowKeyHasBeenPressed() {
		const KeyCode KEY = KeyCode.DownArrow;
		return Input.GetKey(KEY);
	}

	private void moveCameraDown() {
		var position = transform.position;
		float x = position.x;
		float y = position.y - 0.1f;
		float z = position.z;
		transform.position = new Vector3 (x, y, z);
	}

	private void moveCameraUpIfUpArrowKeyHasBeenPressed() {
		if (upArrowKeyHasBeenPressed()) {
			moveCameraUp();
		}
	}

	private bool upArrowKeyHasBeenPressed() {
		const KeyCode KEY = KeyCode.UpArrow;
		return Input.GetKey(KEY);
	}
	
	private void moveCameraUp() {
		var position = transform.position;
		float x = position.x;
		float y = position.y + 0.1f;
		float z = position.z;
		transform.position = new Vector3 (x, y, z);
	}
	
	private void zoomCameraIfZoomKeyHasBeenPressed() {
		zoomCameraOutIfMinusKeyHasBeenPressed();
		zoomCameraInIfPlusKeyHasBeenPressed();
	}

	private void zoomCameraOutIfMinusKeyHasBeenPressed() {
		if (minusKeyHasBeenPressed()) {
			zoomCameraOut();
		}
	}

	private bool minusKeyHasBeenPressed() {
		const KeyCode KEY = KeyCode.Minus;
		return Input.GetKey(KEY);
	}

	private void zoomCameraOut() {
		var c = GetComponent<Camera>();
		c.orthographicSize += 0.1f;
	}

	private void zoomCameraInIfPlusKeyHasBeenPressed() {
		if (plusKeyHasBeenPressed()) {
			zoomCameraIn();
		}
	}

	private bool plusKeyHasBeenPressed() {
		const KeyCode KEY = KeyCode.Equals;
		return Input.GetKey(KEY);
	}
	
	private void zoomCameraIn() {
		var c = GetComponent<Camera>();
		c.orthographicSize -= 0.1f;
	}
}