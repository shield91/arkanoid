using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class FixedBonusPositionRetriever : SpecialBonusPositionRetriever {
		private string bonusParentName;

		public FixedBonusPositionRetriever(string name) {
			bonusParentName = name;
		}

		protected override bool currentBonusIsTheRightOne() {
			string currentBonusParentName = getCurrentBonusParentName();
			return currentBonusParentName.Equals(bonusParentName);
		}
	}
}