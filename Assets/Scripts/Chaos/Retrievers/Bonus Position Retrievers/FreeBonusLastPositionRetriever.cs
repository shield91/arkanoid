namespace AssemblyCSharp
{
	public class FreeBonusLastPositionRetriever : SpecialBonusPositionRetriever	{
		protected override bool currentBonusIsTheRightOne() {
			string currentBonusParentName = getCurrentBonusParentName();
			int length = currentBonusParentName.Length;
			return length == 0;
		}
	}
}