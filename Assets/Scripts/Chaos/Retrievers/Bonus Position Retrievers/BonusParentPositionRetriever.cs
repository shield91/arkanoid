using UnityEngine;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class BonusParentPositionRetriever {
		private int bonusPosition;
		private int bonusParentPosition;

		public BonusParentPositionRetriever(int i) {
			bonusPosition = i;
			bonusParentPosition = 0;
		}

		public int getBonusParentPosition() {
			calculateBonusParentPosition();
			return getCalculatedBonusParentPosition();
		}

		private void calculateBonusParentPosition() {
			if (bonusHasBeenAttachedToOneOfDestroyables()) {
				retrieveBonusParentPositionFromBonusParentName();
			} else {
				retrieveBonusParentPositionRandomly();
			}
		}
		
		private bool bonusHasBeenAttachedToOneOfDestroyables() {
			const string KEY = "Bonus parent name";
			var list = getListOfBonuses();
			int i = bonusPosition;
			var bonus = list[i];
			string name = bonus[KEY];
			int n = name.Length;
			return n > 0;
		}

		private List<Dictionary<string, string>> getListOfBonuses() {
			const string TAG = "Level";
			var go = GameObject.FindGameObjectWithTag(TAG);
			var bc = go.GetComponent<BonusesController>();
			return bc.getBonusesList();
		}
		
		private void retrieveBonusParentPositionFromBonusParentName() {
			const string KEY = "Bonus parent name";
			const char SEPARATOR = ' ';
			var list = getListOfBonuses();
			int i = bonusPosition;
			var bonus = list[i];
			string name = bonus[KEY];
			var nameParts = name.Split(SEPARATOR);
			int destroyableNumberPosition = nameParts.Length - 1;
			string s = nameParts[destroyableNumberPosition];
			int destroyableNumber = int.Parse(s);
			bonusParentPosition = destroyableNumber - 1;
		}

		private void retrieveBonusParentPositionRandomly() {
			do {
				searchForBonusParentPositionAmongDestroyables();
			} while (parentDestroyableIsSupposedToHaveAnotherBonus());
		}

		private void searchForBonusParentPositionAmongDestroyables() {
			const string TAG = "Destroy";
			const int MIN = 0;
			var destroyables = GameObject.FindGameObjectsWithTag(TAG);
			int max = destroyables.Length;
			bonusParentPosition = Random.Range(MIN, max);
		}

		private bool parentDestroyableIsSupposedToHaveAnotherBonus() {
			const string TAG = "Destroy";
			var destroyables = GameObject.FindGameObjectsWithTag(TAG);
			int i = bonusParentPosition;
			var parentDestroyable = destroyables[i];
			var name = parentDestroyable.name;
			var matcher = new MatcherBetweenDestroyableAndBonus(name);
			var t = parentDestroyable.transform;
			int n = t.childCount;
			bool a = matcher.destroyableHasMatch();
			bool b = n > 0;
			return a || b;
		}

		private int getCalculatedBonusParentPosition() {
			return bonusParentPosition;
		}
	}
}