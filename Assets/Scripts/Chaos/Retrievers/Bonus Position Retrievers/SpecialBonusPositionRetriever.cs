using UnityEngine;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public abstract class SpecialBonusPositionRetriever	{
		private int specialBonusPosition;
		private int currentBonusPosition;

		public SpecialBonusPositionRetriever() {
			specialBonusPosition = 0;
			currentBonusPosition = 0;
		}

		public int getSpecialBonusPosition() {
			calculateSpecialBonusPosition();
			return getCalculatedSpecialBonusPosition();
		}

		private void calculateSpecialBonusPosition() {
			resetCurrentBonusPosition();
			calculateBonusPosition();
		}

		private void resetCurrentBonusPosition() {
			currentBonusPosition = 0;
		}

		private void calculateBonusPosition() {
			while (thereAreBonusesNeedToBeAnalyzed()) {
				analyzeIfCurrentBonusIsTheRightOne();
			}
		}

		private bool thereAreBonusesNeedToBeAnalyzed() {
			var list = getListOfBonuses();
			int i = currentBonusPosition;
			int n = list.Count;
			return i < n;
		}

		private List<Dictionary<string, string>> getListOfBonuses() {
			const string TAG = "Level";
			GameObject go = GameObject.FindGameObjectWithTag(TAG);
			var bc = go.GetComponent<BonusesController>();
			return bc.getBonusesList();
		}

		private void analyzeIfCurrentBonusIsTheRightOne() {
			analyzeIfBonusIsTheRightOne();
			moveOnToTheNextBonus();
		}

		private void analyzeIfBonusIsTheRightOne() {
			if (currentBonusIsTheRightOne()) {
				markCurrentBonusPositionAsPositionOfSpecialOne();
			}
		}

		protected abstract bool currentBonusIsTheRightOne();

		protected string getCurrentBonusParentName() {
			const string KEY = "Bonus parent name";
			int i = currentBonusPosition;
			var list = getListOfBonuses();
			var currentBonus = list[i];
			return currentBonus[KEY];
		}

		private void markCurrentBonusPositionAsPositionOfSpecialOne() {
			specialBonusPosition = currentBonusPosition;
		}

		private void moveOnToTheNextBonus() {
			++currentBonusPosition;
		}

		private int getCalculatedSpecialBonusPosition() {
			return specialBonusPosition;
		}
	}
}