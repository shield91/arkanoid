using System.Linq;
using System.Xml;
using System.Xml.XPath;

namespace AssemblyCSharp
{
	public class LevelsListSorter {
		private XmlDocument levelsList;
		private XPathNodeIterator levelsListIterator;
		private string[] sortedLevels;
		private int currentLevelPosition;

		public LevelsListSorter(XmlDocument levels) {
			levelsList = levels;
			levelsListIterator = getIteratorFromList();
			sortedLevels = getInitializedArrayFromIterator();
			currentLevelPosition = 0;
		}

		private XPathNodeIterator getIteratorFromList() {
			const string XPATH = "Levels/Level";
			const string EXPR = "@Number";
			const string LANG = "";
			XPathNavigator xpn = levelsList.CreateNavigator();
			XPathExpression xpe = xpn.Compile(XPATH);
			XmlSortOrder order = XmlSortOrder.Ascending;
			XmlCaseOrder caseOrder = XmlCaseOrder.None;
			XmlDataType dataType = XmlDataType.Number;
			xpe.AddSort(EXPR, order, caseOrder, LANG, dataType);
			return xpn.Select(xpe);
		}

		private string[] getInitializedArrayFromIterator() {
			int n = levelsListIterator.Count;
			return new string[n];
		}

		public void sortLevelsList() {
			moveSortedLevelsFromIteratorToArray();
			moveSortedLevelsFromArrayToFile();
		}

		private void moveSortedLevelsFromIteratorToArray() {
			resetCurrentLevelPosition();
			placeHeadAboveTheFirstLevelInIterator();
			moveLevelsFromIteratorToArray();
		}

		private void resetCurrentLevelPosition() {
			currentLevelPosition = 0;
		}

		private void placeHeadAboveTheFirstLevelInIterator() {
			moveHeadToTheNextLevelInIterator();
		}

		private void moveHeadToTheNextLevelInIterator() {
			levelsListIterator.MoveNext();
		}

		private void moveLevelsFromIteratorToArray() {
			while(thereAreLevelsNeedToBeAnalyzed()) {
				moveCurrentLevelFromIteratorToArray();
			}
		}

		private bool thereAreLevelsNeedToBeAnalyzed() {
			int n = sortedLevels.Count();
			int i = currentLevelPosition;
			return i < n;
		}

		private void moveCurrentLevelFromIteratorToArray() {
			moveLevelFromIteratorToArray();
			moveHeadToTheNextLevelInIterator();
			moveOnToTheNextLevel();
		}

		private void moveLevelFromIteratorToArray() {
			const string LOCAL_NAME = "Number";
			const string NAMESPACE_URI = "";
			XPathNavigator xpn = levelsListIterator.Current;
			int i = currentLevelPosition;
			sortedLevels[i] = xpn.GetAttribute(LOCAL_NAME, NAMESPACE_URI);
		}

		private void moveOnToTheNextLevel() {
			++currentLevelPosition;
		}

		private void moveSortedLevelsFromArrayToFile() {
			resetCurrentLevelPosition();
			moveLevelsFromArrayToFile();
		}

		private void moveLevelsFromArrayToFile() {
			while (thereAreLevelsNeedToBeAnalyzed()) {
				moveCurrentLevelFromArrayToFile();
			}
		}

		private void moveCurrentLevelFromArrayToFile() {
			moveLevelFromArrayToFile();
			moveOnToTheNextLevel();
		}

		private void moveLevelFromArrayToFile() {
			const string XPATH = "Levels/Level";
			const int NUMBER_ATTRIBUTE_POSITION = 0;
			int i = currentLevelPosition;
			XmlNodeList xnl = levelsList.SelectNodes(XPATH);
			XmlAttributeCollection xac = xnl[i].Attributes;
			XmlAttribute xa = xac[NUMBER_ATTRIBUTE_POSITION];
			xa.Value = sortedLevels[i];
		}
	}
}