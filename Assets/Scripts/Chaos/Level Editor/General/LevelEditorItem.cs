﻿using UnityEngine;
using AssemblyCSharp;
using System.Collections;

public abstract class LevelEditorItem : MonoBehaviour {
	private GameObject itemEnlargedCopy;

	private void Start() {
		resetItemEnlargedCopy();
	}

	private void resetItemEnlargedCopy() {
		var value = getValueForItemEnlargedCopyByDefault();
		setItemEnlargedCopy(value);
	}

	private GameObject getValueForItemEnlargedCopyByDefault() {
		return null;
	}

	private void setItemEnlargedCopy(GameObject value) {
		itemEnlargedCopy = value;
	}

	protected virtual void onInputDown() {
		updateItemEnlargedCopy();
		setUpNameForItemEnlargedCopy();
	}

	private void updateItemEnlargedCopy() {
		var value = getUpdatedValueForItemEnlargedCopy();
		setItemEnlargedCopy(value);
	}

	private GameObject getUpdatedValueForItemEnlargedCopy() {
		var o = getInstantiatedObjectForItemEnlargedCopy();
		return (GameObject)o;
	}

	private Object getInstantiatedObjectForItemEnlargedCopy() {
		var original = getOriginalForItemEnlargedCopy();
		var position = getPositionForItemEnlargedCopy();
		var rotation = getRotationForItemEnlargedCopy();
		return Instantiate(original, position, rotation);
	}

	private Object getOriginalForItemEnlargedCopy() {
		var path = getPathToItem();
		return Resources.Load(path);
	}

	private string getPathToItem() {
		var a = getPathToItemLocation();
		var b = getPathToResource();
		return a + b;
	}

	protected abstract string getPathToItemLocation();

	private string getPathToResource() {
		var c = getResourcePathCreatorForItem();
		return c.getPathToResource();
	}

	private ResourcePathCreator getResourcePathCreatorForItem() {
		var tag = getTagForResourcePathCreator();
		return new ResourcePathCreator(tag);
	}

	private string getTagForResourcePathCreator() {
		return name;
	}

	private Vector3 getPositionForItemEnlargedCopy() {
		var inputPosition = getInputPosition();
		return projectInput(inputPosition);
	}

	protected abstract Vector3 getInputPosition();

	private Vector3 projectInput(Vector3 inputPosition) {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var sf = go.GetComponent<SharedFunctions>();
		return sf.projectInput(inputPosition);
	}

	private Quaternion getRotationForItemEnlargedCopy() {
		return transform.rotation;
	}

	private void setUpNameForItemEnlargedCopy() {
		var c = getItemEnlargedCopy();
		var value = getValueForItemEnlargedCopyName();
		c.name = value;
	}

	protected GameObject getItemEnlargedCopy() {
		return itemEnlargedCopy;
	}

	private string getValueForItemEnlargedCopyName() {
		return name;
	}

	protected void onInputDrag() {
		if (itemHasBeenSelected()) {
			moveItemEnlargedCopyToInputPosition();
		}
	}

	private bool itemHasBeenSelected() {
		return getItemEnlargedCopy();
	}

	private void moveItemEnlargedCopyToInputPosition() {
		var value = getProjectedInputPositionForItemEnlargedCopy();
		var t = getTransformFromItemEnlargedCopy();
		t.position = value;
	}

	private Vector3 getProjectedInputPositionForItemEnlargedCopy() {
		var inputPosition = getInputPosition();
		return projectInput(inputPosition);
	}

	private Transform getTransformFromItemEnlargedCopy() {
		var c = getItemEnlargedCopy();
		return c.transform;
	}

	protected void onInputUp() {
		if (itemHasBeenSelected()) {
			performNecessaryActionsWithItemEnlargedCopy();
		}
	}

	private void performNecessaryActionsWithItemEnlargedCopy() {
		attachSpecialScriptToItemEnlargedCopy();
		performActionsWithItemEnlargedCopyIfItIsPossible();
	}

	protected abstract void attachSpecialScriptToItemEnlargedCopy();

	private void performActionsWithItemEnlargedCopyIfItIsPossible() {
		IEnumerator routine = performNecessaryActionsWithCopyIfItIsPossible();
		StartCoroutine(routine);
	}

	private IEnumerator performNecessaryActionsWithCopyIfItIsPossible() {
		const float SECONDS = 0.1f;
		yield return new WaitForSeconds(SECONDS);
		performActionsWithCopyIfItIsPossible();
	}
	
	private void performActionsWithCopyIfItIsPossible() {
		if (itemEnlargedCopyIsPresent()) {
			performActionsWithCopy();
		}
	}

	private bool itemEnlargedCopyIsPresent() {
		return getItemEnlargedCopy();
	}
	
	protected abstract void performActionsWithCopy();

	protected void deleteSpecialScriptAttachedToItem() {
		var obj = getSpecialScriptFromItemEnlargedCopy();
		Destroy(obj);
	}

	private LevelEditorSelectedItem getSpecialScriptFromItemEnlargedCopy() {
		var c = getItemEnlargedCopy();
		return c.GetComponent<LevelEditorSelectedItem>();
	}

	protected void deleteItemEnlargedCopy() {
		var obj = getItemEnlargedCopy();
		Destroy(obj);
	}
}