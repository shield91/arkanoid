﻿public class LevelEditorBrick : LevelEditorIndependentItem {
	protected override string getPathToItemLocation() {
		return "Order/Destroyables/";
	}

	protected override string getIndependentItemTag() {
		return "Undestroy";
	}

	protected override string getIndependentItemParentTag() {
		return "Undestroyables";
	}
}