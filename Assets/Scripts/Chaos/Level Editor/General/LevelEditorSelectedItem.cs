﻿using UnityEngine;

public abstract class LevelEditorSelectedItem : MonoBehaviour {
	private Collision2D collision;

	void Start() {
		collision = null;
	}

	void OnCollisionStay2D(Collision2D c) {
		initializeCollisionFrom(c);
		performNecessaryActionsIfCollisionIsOccurredWithSpecialElements();
	}

	private void initializeCollisionFrom(Collision2D c) {
		collision = c;
	}

	private void performNecessaryActionsIfCollisionIsOccurredWithSpecialElements() {
		if (collisionIsOccurredWithSpecialElements()) {
			performNecessaryActionsThatAreProperForItem();
		}
	}

	private bool collisionIsOccurredWithSpecialElements() {
		bool a = collisionIsOccurredWithDestroyables();
		bool b = collisionIsOccurredWithUndestroyables();
		bool c = collisionIsOccurredWithLevelEditorPanel();
		bool d = collisionIsOccurredWithAddNewBonusButton();
		return a || b || c || d;
	}

	protected bool collisionIsOccurredWithDestroyables() {
		Collider2D c = getColliderFromCollision();
		Destroyable d = c.GetComponent<Destroyable>();
		return d != null;
	}

	protected Collider2D getColliderFromCollision() {
		ContactPoint2D cp = collision.contacts[0];
		return cp.collider;
	}

	protected bool collisionIsOccurredWithUndestroyables() {
		Collider2D c = getColliderFromCollision();	
		Brick b = c.GetComponent<Brick>();
		return b != null;
	}
		
	private bool collisionIsOccurredWithLevelEditorPanel() {
		const string NAME = "Level Editor Panel";
		return collisionIsOccuredWith(NAME);
	}

	private bool collisionIsOccuredWith(string name) {
		Collider2D c = getColliderFromCollision();
		string tag = c.tag;
		return tag.Equals(name);
	}

	private bool collisionIsOccurredWithAddNewBonusButton() {
		const string NAME = "Add New Bonus";
		return collisionIsOccuredWith(NAME);
	}

	protected abstract void performNecessaryActionsThatAreProperForItem();

	protected void deleteLevelEditorSelectedItem() {
		Object o = gameObject;
		float t = 0.05f;
		Destroy(o, t);
	}

	public bool collisionHasBeenOccurred() {
		return collision != null;
	}

	protected Collision2D getCollision() {
		return collision;
	}
}