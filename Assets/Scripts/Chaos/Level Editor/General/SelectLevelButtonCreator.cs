using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace AssemblyCSharp
{
	public class SelectLevelButtonCreator {
		private GameObject button;

		public SelectLevelButtonCreator(string buttonName) {
			button = new GameObject(buttonName);
		}

		public void setUpButton() {
			changeButtonTag();
			changeButtonLayer();
			changeButtonParent();
			addRectTransformComponentToButton();
			addImageScriptComponentToButton();
			addButtonScriptComponentToButton();
			addTextToButton();
		}

		private void changeButtonTag() {
			const string TAG = "Go To Chosen Level Button";
			button.tag = TAG;
		}
		
		private void changeButtonLayer() {
			const int UI = 5;
			button.layer = UI;
		}
		
		private void changeButtonParent() {
			const string TAG = "Level Select Panel";
			GameObject child = button;
			GameObject parent = GameObject.FindGameObjectWithTag(TAG);
			Transform childTransform = child.transform;

			//Transform parentTransform = parent.transform;
			var parentTransform = parent.transform.Find("Scroll Panel/Buttons");

			childTransform.SetParent(parentTransform);
		}
		
		private void addRectTransformComponentToButton() {
			RectTransform rt = button.AddComponent<RectTransform>();

			//rt.anchoredPosition = getPositionForButton();

			rt.sizeDelta = getSizeForButton();
			rt.anchorMin = getAnchorForButton();
			rt.anchorMax = getAnchorForButton();
		}
		
		private Vector2 getPositionForButton() {
			if (buttonOutstepsLevelSelectPanelBoundaries()) {
				return getPositionForButtonInNewRow();
			} else {
				return getPositionForButtonInCurrentRow();
			}
		}
		
		private bool buttonOutstepsLevelSelectPanelBoundaries() {
			RectTransform rt = getRectTransformComponentFromPenultButton();
			Vector2 position = rt.anchoredPosition;
			Vector2 size = rt.sizeDelta;
			float xa = position.x;
			float xb = size.x;
			float xc = getSpaceBetweenButtons();
			float x = xa + xb + xc;
			float xd = getLevelSelectPanelWidth();
			return x + xb >= xd;
		}
		
		private RectTransform getRectTransformComponentFromPenultButton() {
			const string TAG = "Go To Chosen Level Button";
			GameObject[] go = GameObject.FindGameObjectsWithTag(TAG);
			int i = go.Length - 2;
			GameObject penultOne = go[i];
			return penultOne.GetComponent<RectTransform>();
		}
		
		private float getSpaceBetweenButtons() {
			return 10;
		}
		
		private float getLevelSelectPanelWidth() {
			const string TAG = "Level Select Panel";
			GameObject parent = GameObject.FindGameObjectWithTag(TAG);
			RectTransform rt = parent.GetComponent<RectTransform>();
			Vector2 size = rt.sizeDelta;
			return size.x;
		}
		
		private Vector2 getPositionForButtonInNewRow() {
			RectTransform rt = getRectTransformComponentFromPenultButton();
			Vector2 position = rt.anchoredPosition;
			Vector2 size = rt.sizeDelta;
			float ya = position.y;
			float yb = size.y;
			float yc = getSpaceBetweenButtons();
			float y = ya - yb - yc;
			float x = getPositionOfFirstButtonInRow();
			return new Vector2(x, y);
		}
		
		private float getPositionOfFirstButtonInRow() {
			const string TAG = "Go To Chosen Level Button";
			GameObject[] go = GameObject.FindGameObjectsWithTag(TAG);
			int i = 0;
			GameObject firstOne = go[i];
			RectTransform rt = firstOne.GetComponent<RectTransform>();
			Vector2 position = rt.anchoredPosition;
			return position.x;
		}
		
		private Vector2 getPositionForButtonInCurrentRow() {
			RectTransform rt = getRectTransformComponentFromPenultButton();
			Vector2 position = rt.anchoredPosition;
			Vector2 size = rt.sizeDelta;
			float xa = position.x;
			float xb = size.x;
			float xc = getSpaceBetweenButtons();
			float x = xa + xb + xc;
			float y = position.y;
			return new Vector2(x, y);
		}
		
		private Vector2 getSizeForButton() {
			const float X = 40;
			const float Y = 40;
			return new Vector2(X, Y);
		}
		
		private Vector2 getAnchorForButton() {
			const float X = 0;
			const float Y = 1;
			return new Vector2(X, Y);
		}
		
		private void addImageScriptComponentToButton() {
			const string PATH = "UISprite";
			Object original = Resources.Load(PATH);
			GameObject go = (GameObject)original;
			Image a = go.GetComponent<Image>();
			Image b = button.AddComponent<Image>();
			b.sprite = a.sprite;			
			b.type = Image.Type.Sliced;
		}
		
		private void addButtonScriptComponentToButton() {
			const string NAME = "Global";
			string s = getLevelNumberFromButtonName();
			int levelNumber = int.Parse(s);
			GameObject go = GameObject.Find(NAME);
			LevelSelect ls = go.GetComponent<LevelSelect>();
			UnityAction call = () => ls.startLevel(levelNumber);
			Button b = button.AddComponent<Button>();
			Button.ButtonClickedEvent bce = b.onClick;
			bce.AddListener(call);
		}
		
		private string getLevelNumberFromButtonName() {
			const char SEPARATOR = ' ';
			const int LEVEL_NUMBER_POSITION = 1;
			string name = button.name;
			string[] nameParts = name.Split(SEPARATOR);
			return nameParts[LEVEL_NUMBER_POSITION];
		}
		
		private void addTextToButton() {
			GameObject go = getGameObjectForText();
			Text t = go.AddComponent<Text>();
			RectTransform rt = go.GetComponent<RectTransform>();
			t.text = getLevelNumberFromButtonName();
			t.font = getFontForButtonText();			
			t.fontSize = 20;
			t.alignment = TextAnchor.MiddleCenter;
			t.color = getColorForButtonText();			
			rt.anchoredPosition = getZeroVectorForButtonText();
			rt.sizeDelta = getSizeOfButtonText();
			rt.anchorMin = getZeroVectorForButtonText();
			rt.anchorMax = getMaximumAnchorForButtonText();
		}

		private GameObject getGameObjectForText() {
			const string NAME = "Text";
			GameObject go = new GameObject(NAME);
			Transform childTransform = go.transform;
			Transform parentTransform = button.transform;
			childTransform.SetParent(parentTransform);
			return go;
		}
		
		private Font getFontForButtonText() {
			const string FONT_NAME = "Arial";
			const int SIZE = 20;
			return Font.CreateDynamicFontFromOSFont(FONT_NAME, SIZE);
		}
		
		private Color getColorForButtonText() {
			const float R = 0.196f;
			const float G = 0.196f;
			const float B = 0.196f;
			return new Color(R, G, B);
		}
		
		private Vector2 getSizeOfButtonText() {
			const float X = 40;
			const float Y = 40;
			return new Vector2(X, Y);
		}
		
		private Vector2 getZeroVectorForButtonText() {
			return new Vector2();
		}
		
		private Vector2 getMaximumAnchorForButtonText() {
			const float X = 1;
			const float Y = 1;
			return new Vector2(X, Y);
		}
	}
}