﻿using UnityEngine;
using AssemblyCSharp;

public class LevelEditorDeleteItem : LevelEditorItem {
	private void OnMouseDown() {
		onInputDown();
	}

	protected override void onInputDown() {
		callOnInputDownMethodFromSuperclass();
		addRigidBodyComponentToIndependentItems();
	}

	private void callOnInputDownMethodFromSuperclass() {
		base.onInputDown();
	}

	protected override string getPathToItemLocation() {
		return "Order/";
	}

	protected override Vector3 getInputPosition() {
		return Input.mousePosition;
	}

	private void addRigidBodyComponentToIndependentItems() {
		var a = new ItemsRigidBodyComponentAdder();
		a.adjustRigidBodyComponentForIndependentItems();
	}

	private void OnMouseDrag() {
		onInputDrag();
	}

	private void OnMouseUp() {
		onInputUp();
	}

	protected override void attachSpecialScriptToItemEnlargedCopy() {
		GameObject go = getItemEnlargedCopy();
		go.AddComponent<LevelEditorSelectedDeleteItem>();
	}

	protected override void performActionsWithCopy() {
		removeRigidBodyComponentFromIndependentItems();
		deleteItemEnlargedCopy();
	}

	private void removeRigidBodyComponentFromIndependentItems() {
		var d = new ItemsRigidBodyComponentDestroyer();
		d.adjustRigidBodyComponentForIndependentItems();
	}
}