using UnityEngine;

namespace AssemblyCSharp 
{
	public class ItemsRigidBodyComponentAdder : ItemsRigidBodyComponentAdjuster {
		protected override void adjustRigidBodyComponentForIndependentItem() {
			var item = getCurrentIndependentItemByItsPosition();
			var rb = item.AddComponent<Rigidbody2D>();
			rb.constraints = RigidbodyConstraints2D.FreezeAll;
		}
	}
}