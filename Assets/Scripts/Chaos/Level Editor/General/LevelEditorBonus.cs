﻿using UnityEngine;
using AssemblyCSharp;

public class LevelEditorBonus : LevelEditorItem {
	private void OnMouseDown() {
		onInputDown();
	}

	protected override void onInputDown() {
		callOnInputDownMethodFromSuperclass();
		addRigidBodyComponentToIndependentItems();
		setUpItemEnlargedCopy();
	}

	private void callOnInputDownMethodFromSuperclass() {
		base.onInputDown();
	}

	protected override string getPathToItemLocation() {
		return "Order/Bonuses/";
	}

	protected override Vector3 getInputPosition() {
		return Input.mousePosition;
	}

	private void addRigidBodyComponentToIndependentItems() {
		var a = new ItemsRigidBodyComponentAdder();
		a.adjustRigidBodyComponentForIndependentItems();
	}

	private void setUpItemEnlargedCopy() {
		showItemEnlargedCopy();
		enableItemEnlargedCopyCollider();
		setUpItemEnlargedCopyLocalScale();
	}

	private void showItemEnlargedCopy() {
		var go = getItemEnlargedCopy();
		var value = getValueForItemEnlargedCopyBeingVisible();
		go.SetActive(value);
	}

	private bool getValueForItemEnlargedCopyBeingVisible() {
		return true;
	}

	private void enableItemEnlargedCopyCollider() {
		var c = getColliderFromItemEnlargedCopy();
		var value = getValueForItemEnlargedCopyColliderBeingEnabled();
		c.enabled = value;
	}

	private Collider2D getColliderFromItemEnlargedCopy() {
		var go = getItemEnlargedCopy();
		return go.GetComponent<Collider2D>();
	}

	private bool getValueForItemEnlargedCopyColliderBeingEnabled() {
		return true;
	}

	private void setUpItemEnlargedCopyLocalScale() {
		var t = getTransformForItemEnlargedCopy();
		var value = getValueForItemEnlargedCopyNewLocalScale();
		t.localScale = value;
	}

	private Transform getTransformForItemEnlargedCopy() {
		var go = getItemEnlargedCopy();
		return go.transform;
	}

	private Vector3 getValueForItemEnlargedCopyNewLocalScale() {
		var n = getOldLocalScaleFromItemEnlargedCopy();
		var m = getValueForUpdatingItemEnlargedCopyLocalScale();
		return n / m;
	}

	private Vector3 getOldLocalScaleFromItemEnlargedCopy() {
		var t = getTransformForItemEnlargedCopy();
		return t.localScale;
	}

	private float getValueForUpdatingItemEnlargedCopyLocalScale() {
		return 8f;
	}

	private void OnMouseDrag() {
		onInputDrag();
	}

	private void OnMouseUp() {
		onInputUp();
	}

	protected override void attachSpecialScriptToItemEnlargedCopy() {
		GameObject go = getItemEnlargedCopy();
		go.AddComponent<LevelEditorSelectedBonus>();
	}
	
	protected override void performActionsWithCopy() {
		removeRigidBodyComponentFromIndependentItems();
		performOperationsWithCopy();
	}

	private void removeRigidBodyComponentFromIndependentItems() {
		var d = new ItemsRigidBodyComponentDestroyer();
		d.adjustRigidBodyComponentForIndependentItems();
	}

	private void performOperationsWithCopy() {
		if (collisionWithDestroyablesHasBeenOccurred()) {
			deleteSpecialScriptAttachedToItem();
		} else {
			deleteItemEnlargedCopy();
		}
	}

	private bool collisionWithDestroyablesHasBeenOccurred() {
		GameObject go = getItemEnlargedCopy();
		var lesi = go.GetComponent<LevelEditorSelectedItem>();
		return lesi.collisionHasBeenOccurred();
	}
}