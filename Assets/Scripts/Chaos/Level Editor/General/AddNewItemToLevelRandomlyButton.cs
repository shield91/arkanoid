﻿using UnityEngine;

public class AddNewItemToLevelRandomlyButton : MonoBehaviour {
	private Collision2D collision;

	void Start() {
		collision = null;
	}

	void OnCollisionEnter2D(Collision2D c) {
		initializeCollisionFrom(c);
		performRestOfOnCollisionEnter2DMethod();
	}

	private void initializeCollisionFrom(Collision2D c) {
		collision = c;
	}

	private void performRestOfOnCollisionEnter2DMethod() {
		if (collisionHasBeenOccurredWithBonus()) {
			performOnCollisionEnter2DMethod();
		}
	}

	private bool collisionHasBeenOccurredWithBonus() {
		const string VALUE = "Bonus";
		var cp = collision.contacts[0];
		var c = cp.collider;
		string name = c.name;
		return name.Contains(VALUE);
	}

	private void performOnCollisionEnter2DMethod() {
		toggleBonusesListVisibility();
		highlightButton();
	}

	private void toggleBonusesListVisibility() {
		if (bonusesListIsVisible()) {
			hideBonusesList();
		} else {
			showBonusesList();
		}
	}

	private bool bonusesListIsVisible() {
		var go = getListOfBonuses();
		return go.activeInHierarchy;
	}

	private GameObject getListOfBonuses() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var l = go.GetComponent<PlayScreen>();
		return l.getBonusesList();
	}

	private void hideBonusesList() {
		var go = getListOfBonuses();
		go.SetActive(false);
	}

	private void showBonusesList() {
		var go = getListOfBonuses();
		go.SetActive(true);
	}
		
	private void highlightButton() {
		const string PROPERTY_NAME = "_Color";
		const float R = 0f;
		const float G = 0f;
		const float B = 0f;
		var color = new Color(R, G, B);
		var r = GetComponent<Renderer>();
		var m = r.material;
		m.SetColor(PROPERTY_NAME, color);
	}

	void OnCollisionExit2D(Collision2D c) {
		unhighlightButton();
	}

	private void unhighlightButton() {
		const string PROPERTY_NAME = "_Color";
		const float R = 0f;
		const float G = 0.2f;
		const float B = 0f;
		var color = new Color(R, G, B);
		var r = GetComponent<Renderer>();
		var m = r.material;
		m.SetColor(PROPERTY_NAME, color);
	}

	void OnMouseDown() {
		toggleBonusesListVisibility();
		highlightButton();
	}

	void OnMouseUp() {
		unhighlightButton();
	}
}