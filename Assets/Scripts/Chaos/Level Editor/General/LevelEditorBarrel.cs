public class LevelEditorBarrel : LevelEditorIndependentItem {
	protected override string getPathToItemLocation() {
		return "Order/Destroyables/";
	}

	protected override string getIndependentItemTag() {
		return "Destroy";
	}
	
	protected override string getIndependentItemParentTag() {
		return "Destroyables";
	}
}