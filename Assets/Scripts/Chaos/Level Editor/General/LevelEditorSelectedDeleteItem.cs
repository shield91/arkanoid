using UnityEngine;
using AssemblyCSharp;
using System.Collections.Generic;

public class LevelEditorSelectedDeleteItem : LevelEditorSelectedItem {
	protected override void performNecessaryActionsThatAreProperForItem() {
		if (contactPointIsDeleteable()) {
			deleteContactPoint();
		}
	}

	private bool contactPointIsDeleteable() {
		bool a = collisionIsOccurredWithDestroyables();
		bool b = collisionIsOccurredWithUndestroyables();
		return a || b;
	}

	private void deleteContactPoint() {
		updateBonusesQuantityIfContactPointHasBonus();
		deletePoint();
	}

	private void updateBonusesQuantityIfContactPointHasBonus() {
		if (contactPointHasBonus()) {
			updateBonusesQuantityByDeletingFixedBonus();
		}
	}

	private bool contactPointHasBonus() {
		GameObject go = getGameObjectFromContactPoint();
		Transform t = go.transform;
		int n = t.childCount;
		return n == 1;
	}

	private GameObject getGameObjectFromContactPoint() {
		Collider2D c = getColliderFromCollision();
		return c.gameObject;
	}

	private void updateBonusesQuantityByDeletingFixedBonus() {
		deleteFixedBonus();
		decreaseBonusesQuantityByOne();
	}

	private void deleteFixedBonus() {
		var bc = getControllerOfBonuses();
		var l = bc.getBonusesList();
		int index = getFixedBonusPositionFromFixedBonusParentName();
		l.RemoveAt(index);
		bc.setBonusesList(l);
	}

	private BonusesController getControllerOfBonuses() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		return go.GetComponent<BonusesController>();
	}

	private int getFixedBonusPositionFromFixedBonusParentName() {
		string bonusParentName = getNameOfFixedBonusParent();
		var r = new FixedBonusPositionRetriever(bonusParentName);
		return r.getSpecialBonusPosition();
	}

	private string getNameOfFixedBonusParent() {
		GameObject parent = getGameObjectFromContactPoint();
		return parent.name;
	}

	private void decreaseBonusesQuantityByOne() {
		var go = getGameObjectForBonusesList();
		var parentTransform = go.transform;
		string name = getNameOfFixedBonus();
		var child = parentTransform.Find(name);
		var btli = child.GetComponent<BonusesTypesListItem>();
		btli.updateBonusesQuantity();
	}

	private GameObject getGameObjectForBonusesList() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var sf = go.GetComponent<SharedFunctions>();
		return sf.getBonusesList();
	}

	private string getNameOfFixedBonus() {
		const int INDEX = 0;
		var parent = getGameObjectFromContactPoint();
		var t = parent.transform;
		var childTransform = t.GetChild(INDEX);
		return childTransform.name;
	}

	private void deletePoint() {
		Collider2D c = getColliderFromCollision();
		GameObject go = c.gameObject;
		Destroy(go);
	}
}