﻿using UnityEngine;
using AssemblyCSharp;
using System.Collections.Generic;

public class LevelEditorSelectedBonus : LevelEditorSelectedItem {
	private int currentColliderPosition;

	void Start() {
		resetCurrentColliderPosition();
		removeRigidBodyComponentFromBonus();
	}

	private void resetCurrentColliderPosition() {
		currentColliderPosition = 0;
	}

	private void removeRigidBodyComponentFromBonus() {
		Rigidbody2D rb = GetComponent<Rigidbody2D>();
		Destroy(rb);
	}

	protected override void performNecessaryActionsThatAreProperForItem() {
		if (attachingBonusToOneOfDestroyablesIsPossible()) {
			attachBonusToOneOfDestroyables();
		} else {
			deleteLevelEditorSelectedItemProperly();
		}
	}

	private bool attachingBonusToOneOfDestroyablesIsPossible() {
		bool a = collisionIsOccurredWithDestroyables();
		bool b = collisionIsOccurredOnlyBetweenOneBonusAndOneDestroyable();
		bool c = destroyableDoesNotHaveAttachedBonus();
		bool d = numberOfBonusesWillNotExceedNumberOfDestroyables();
		return a && b && c && d;
	}

	private bool collisionIsOccurredOnlyBetweenOneBonusAndOneDestroyable() {
		int n = getNumberOfColliders();
		return n == 2;
	}

	private int getNumberOfColliders() {
		var c = getCollidersForSelectedBonus();
		return c.Length;
	}

	private Collider2D[] getCollidersForSelectedBonus() {
		CircleCollider2D bonusCollider = GetComponent<CircleCollider2D>();
		Vector2 point = transform.position;
		float radius = bonusCollider.radius;
		return Physics2D.OverlapCircleAll(point, radius);
	}

	private bool destroyableDoesNotHaveAttachedBonus() {
		GameObject go = getDestroyableFromCollision();
		Transform t = go.transform;
		int n = t.childCount;
		return n == 0;
	}

	private GameObject getDestroyableFromCollision() {
		Collider2D c = getColliderFromCollision();
		return c.gameObject;
	}

	private bool numberOfBonusesWillNotExceedNumberOfDestroyables() {
		var bc = getControllerOfBonuses();
		var l = bc.getBonusesList();
		int n = getNumberOfDestroyables();
		int m = l.Count;
		return m < n;
	}

	private BonusesController getControllerOfBonuses() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		return go.GetComponent<BonusesController>();
	}

	private int getNumberOfDestroyables() {
		const string TAG = "Destroy";
		var go = GameObject.FindGameObjectsWithTag(TAG);
		return go.Length;
	}

	private void attachBonusToOneOfDestroyables() {
		changeItemTag();
		makeItemToBeChildOfDestroyable();
		changeItemPosition();
		fillUpBonusesListWithOneMoreBonus();
	}

	private void changeItemTag() {
		const string TAG = "Untagged";
		tag = TAG;
	}

	private void makeItemToBeChildOfDestroyable() {
		GameObject parent = getDestroyableFromCollision();
		Transform parentTransform = parent.transform;
		transform.SetParent(parentTransform);
	}

	private void changeItemPosition() {
		GameObject destroyable = getDestroyableFromCollision();
		Transform destroyableTransform = destroyable.transform;
		Vector2 position = destroyableTransform.position;
		transform.position = position;
	}

	private void fillUpBonusesListWithOneMoreBonus() {
		fillUpBonusesListWithOneMoreItem();
		updateBonusesQuantity();
	}

	private void fillUpBonusesListWithOneMoreItem() {
		const string KEY_1 = "Bonus name";
		const string KEY_2 = "Bonus parent name";
		var bc = getControllerOfBonuses();
		var l = bc.getBonusesList();
		var d = new Dictionary<string, string>();
		var parent = transform.parent;
		d[KEY_1] = name;
		d[KEY_2] = parent.name;
		l.Add(d);
		bc.setBonusesList(l);
	}
	
	private void updateBonusesQuantity() {
		GameObject go = getGameObjectForBonusesList();
		Transform parentTransform = go.transform;
		Transform child = parentTransform.Find(name);
		var btli = child.GetComponent<BonusesTypesListItem>();
		btli.updateBonusesQuantity();
	}
	
	private GameObject getGameObjectForBonusesList() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var sf = go.GetComponent<SharedFunctions>();
		return sf.getBonusesList();
	}

	private void deleteLevelEditorSelectedItemProperly() {
		removeRigidBodyComponentFromIndependentItems();
		unhighlightIndependentItems();
		deleteLevelEditorSelectedItem();
	}
	
	private void removeRigidBodyComponentFromIndependentItems() {
		var d = new ItemsRigidBodyComponentDestroyer();
		d.adjustRigidBodyComponentForIndependentItems();
	}

	private void unhighlightIndependentItems() {
		resetCurrentColliderPosition();
		unhighlightIndependentObjects();
	}

	private void unhighlightIndependentObjects() {
		while (thereAreCollidersNeedToBeAnalyzed()) {
			unhighlightIndependentItemIfCurrentColliderBelongsToThatItem();
		}
	}

	private bool thereAreCollidersNeedToBeAnalyzed() {
		int i = currentColliderPosition;
		int n = getNumberOfColliders();
		return i < n;
	}

	private void unhighlightIndependentItemIfCurrentColliderBelongsToThatItem() {
		unhighlightItemIfCurrentColliderBelongsToThatItem();
		moveOnToTheNextCollider();
	}

	private void unhighlightItemIfCurrentColliderBelongsToThatItem() {
		if (currentColliderBelongsToIndependentItem()) {
			unhighlightIndependentItem();
		}
	}

	private bool currentColliderBelongsToIndependentItem() {
		var ii = getScriptForIndependentItemFromCurrentCollider();
		return ii != null;
	}

	private IndependentItem getScriptForIndependentItemFromCurrentCollider() {
		int index = currentColliderPosition;
		var colliders = getCollidersForSelectedBonus();
		var currentCollider = colliders[index];
		return currentCollider.GetComponent<IndependentItem>();
	}

	private void unhighlightIndependentItem() {
		var ii = getScriptForIndependentItemFromCurrentCollider();
		ii.unhighlightIndependentItem();
	}

	private void moveOnToTheNextCollider() {
		++currentColliderPosition;
	}
}