using UnityEngine;

namespace AssemblyCSharp
{
	public class RouteHider	{
		private int currentPointPosition;

		public RouteHider() {
			currentPointPosition = 0;
		}

		public void hideRoute() {
			while(thereArePointsNeedToBeAnalyzed()) {
				hideCurrentPoint();
			}
		}

		private bool thereArePointsNeedToBeAnalyzed() {
			Transform t = getParentForPoint();
			int i = currentPointPosition;
			int n = t.childCount;
			return i < n;
		}

		private Transform getParentForPoint() {
			const string TAG = "Clicked Item Route";
			var go = GameObject.FindGameObjectWithTag(TAG);
			return go.transform;
		}

		private void hideCurrentPoint() {
			hidePoint();
			moveOnToTheNextPoint();
		}

		private void hidePoint() {
			int index = currentPointPosition;
			var route = getParentForPoint();
			var point = route.GetChild(index);
			var go = point.gameObject;
			Object.Destroy(go);
		}

		private void moveOnToTheNextPoint() {
			++currentPointPosition;
		}
	}
}