﻿using UnityEngine;

public abstract class LevelEditorIndependentItem : LevelEditorItem {
	private void OnMouseDown() {
		onInputDown();
		makeItemEnlargedCopyLookLikeItIsSelected();
	}

	protected override Vector3 getInputPosition() {
		return Input.mousePosition;
	}

	private void makeItemEnlargedCopyLookLikeItIsSelected() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var sf = go.GetComponent<SharedFunctions>();
		var item = getItemEnlargedCopy();
		sf.makeItemLookLikeItIsSelected(item);
	}
	
	private void OnMouseDrag() {
		onInputDrag();
	}
	
	private void OnMouseUp() {
		onInputUp();
		makeItemEnlargedCopyLookLikeItIsDeselected();
	}

	protected override void attachSpecialScriptToItemEnlargedCopy() {
		GameObject go = getItemEnlargedCopy();
		go.AddComponent<LevelEditorSelectedIndependentItem>();
	}

	protected override void performActionsWithCopy() {
		changeItemTag();
		changeItemName();
		setParentForIndependentItem();
		deleteSpecialScriptAttachedToItem();
	}

	private void changeItemTag() {
		string tag = getIndependentItemTag();
		var go = getItemEnlargedCopy();
		go.tag = tag;
	}

	protected abstract string getIndependentItemTag();

	private void changeItemName() {
		string oldName = name;
		string number = getNumberOfIndependentItemAmongOthers();
		var go = getItemEnlargedCopy();
		string newName = oldName + number;
		go.name = newName;
	}

	private string getNumberOfIndependentItemAmongOthers() {
		string tag = getIndependentItemTag();
		var go = GameObject.FindGameObjectsWithTag(tag);
		int independentItemNumber = go.Length;
		return " " + independentItemNumber;
	}

	private void setParentForIndependentItem() {
		string tag = getIndependentItemParentTag();
		var child = getItemEnlargedCopy();
		var parent = GameObject.FindGameObjectWithTag(tag);
		var childTransform = child.transform;
		var parentTransform = parent.transform;
		childTransform.SetParent(parentTransform);
	}

	protected abstract string getIndependentItemParentTag();

	private void makeItemEnlargedCopyLookLikeItIsDeselected() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var sf = go.GetComponent<SharedFunctions>();
		var item = getItemEnlargedCopy();
		sf.makeItemLookLikeItIsDeselected(item);
	}
}