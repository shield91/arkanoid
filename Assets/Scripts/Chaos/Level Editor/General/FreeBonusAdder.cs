﻿using UnityEngine;

public class FreeBonusAdder : MonoBehaviour {
	private void OnMouseDown() {
		highlightButton();
		addFreeBonusToLevel();
	}

	private void highlightButton() {
		const string PROPERTY_NAME = "_Color";
		const float R = 0f;
		const float G = 0f;
		const float B = 0f;
		Color color = new Color(R, G, B);
		Renderer r = GetComponent<Renderer>();
		Material m = r.material;
		m.SetColor(PROPERTY_NAME, color);
	}

	private void addFreeBonusToLevel() {
		Transform parent = transform.parent;
		var btli = parent.GetComponent<BonusesTypesListItem>();
		btli.addBonusToLevelIfItIsPossible();
	}
	
	private void OnMouseUp() {
		unhighlightButton();
	}

	private void unhighlightButton() {
		const string PROPERTY_NAME = "_Color";
		const float R = 0f;
		const float G = 0.2f;
		const float B = 0f;
		Color color = new Color(R, G, B);
		Renderer r = GetComponent<Renderer>();
		Material m = r.material;
		m.SetColor(PROPERTY_NAME, color);
	}
}