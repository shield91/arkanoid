using UnityEngine;
using System;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class RouteViewer {
		private List<Dictionary<string, string>> route;
		private int routeCurrentPointPosition;

		public RouteViewer(List<Dictionary<string, string>> routePoints) {
			route = routePoints;
			routeCurrentPointPosition = 0;
		}

		public void showRoute() {
			resetRouteCurrentPointPosition();
			show();
		}

		private void resetRouteCurrentPointPosition() {
			routeCurrentPointPosition = 0;
		}

		private void show() {
			try {
				showWaybill();
			} catch (Exception e) {
				handle(e);
			}
		}

		private void showWaybill() {
			while (thereAreRoutePointsNeedToBeAnalyzed()) {
				showRouteCurrentPoint();
			}
		}

		private bool thereAreRoutePointsNeedToBeAnalyzed() {
			int i = routeCurrentPointPosition;
			int n = route.Count;
			return i < n;
		}

		private void showRouteCurrentPoint() {
			showCurrentPoint();
			moveOnToTheNextPoint();
		}

		private void showCurrentPoint() {
			

			const string PATH = "Order/Item Route Point";


//			const string PATH = "Item Route Point";


			var original = Resources.Load(PATH);
			var o = GameObject.Instantiate(original);
			var go = (GameObject)o;
			var t = go.transform;
			var parent = getParentForCurrentPoint();
			var tm = go.GetComponent<TextMesh>();
			var irp = go.GetComponent<ItemRoutePoint>();
			int number = routeCurrentPointPosition + 1;
			t.position = getPositionOfCurrentPoint();
			go.name = PATH + " " + number;
			tm.text = number + "";
			irp.resize();
			t.SetParent(parent);
		}

		private Transform getParentForCurrentPoint() {
			const string TAG = "Clicked Item Route";
			var go = GameObject.FindGameObjectWithTag(TAG);
			return go.transform;
		}

		private Vector3 getPositionOfCurrentPoint() {
			const string KEY_1 = "x";
			const string KEY_2 = "y";
			const float Z = -5f;
			var point = getCurrentPointByItsIndex();
			string valueOne = point[KEY_1];
			string valueTwo = point[KEY_2];
			float x = float.Parse(valueOne);
			float y = float.Parse(valueTwo);
			return new Vector3(x, y, Z);
		}

		private Dictionary<string, string> getCurrentPointByItsIndex() {
			int index = routeCurrentPointPosition;
			return route[index];
		}

		private void moveOnToTheNextPoint() {
			++routeCurrentPointPosition;
		}

		private void handle(Exception e) {
			string message = e.StackTrace;
			Debug.Log(message);
		}
	}
}