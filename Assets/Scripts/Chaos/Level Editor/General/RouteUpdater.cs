using UnityEngine;
using System;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class RouteUpdater {
		private int currentPointPosition;
		private bool invisiblePointIsFound;

		public RouteUpdater() {
			resetPrivateFields();
		}

		private void resetPrivateFields() {
			currentPointPosition = 0;
			invisiblePointIsFound = false;
		}

		public void updateRoute() {
			resetPrivateFields();
			updateRoutePoints();
		}

		private void updateRoutePoints() {
			while(thereArePointsNeedToBeAnalyzed()) {
				updateCurrentPointIfItIsPossible();
			}
		}

		private bool thereArePointsNeedToBeAnalyzed() {
			var t = getRouteForClickedItem();
			int i = currentPointPosition;
			int n = t.childCount;
			return i < n;
		}

		private Transform getRouteForClickedItem() {
			const string TAG = "Clicked Item Route";
			var go = GameObject.FindGameObjectWithTag(TAG);
			return go.transform;
		}

		private void updateCurrentPointIfItIsPossible() {
			updatePointIfItIsPossible();
			moveOnToTheNextPoint();
		}

		private void updatePointIfItIsPossible() {
			if (currentPointIsVisible()) {
				updatePoint();
			} else {
				markThatInvisiblePointIsFound();
			}
		}

		private bool currentPointIsVisible() {
			Transform t = getPointByItsPosition();
			GameObject go = t.gameObject;
			return go.activeInHierarchy;
		}

		private Transform getPointByItsPosition() {
			var t = getRouteForClickedItem();
			int i = currentPointPosition;
			return t.GetChild(i);
		}

		private void updatePoint() {
			const string HEADER = "Item Route Point ";
			var point = getPointByItsPosition();
			var tm = point.GetComponent<TextMesh>();
			var irp = point.GetComponent<ItemRoutePoint>();
			int number = getNumberForCurrentPoint();
			point.name = HEADER + number;
			tm.text = number + "";
			irp.resize();
		}

		private int getNumberForCurrentPoint() {
			int i = currentPointPosition;
			return invisiblePointIsFound ? i : i + 1;
		}

		private void markThatInvisiblePointIsFound() {
			invisiblePointIsFound = true;
		}

		private void moveOnToTheNextPoint() {
			++currentPointPosition;
		}
	}
}