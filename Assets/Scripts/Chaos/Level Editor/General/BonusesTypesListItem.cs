﻿using UnityEngine;
using System.Collections.Generic;

public abstract class BonusesTypesListItem : MonoBehaviour {	
	private int currentBonusIndex;
	private int specialTypeBonusesNumber;
	private bool freeBonusIsFound;

	private void Start() {
		if (levelEditorModeIsEnabled()) {
			performRestOfStartMethod();
		}
	}

	//Може просто вмикати цей скрипт у стані редактора рівнів?
	//Може перебудувати тіло?
	private bool levelEditorModeIsEnabled() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var sf = go.GetComponent<SharedFunctions>();
		return sf.levelEditorModeIsEnabled();
	}

	private void performRestOfStartMethod() {
		resetCurrentBonusIndex();
		resetSpecialTypeBonusesNumber();
		resetInformationAboutFreeBonus();
		updateBonusesQuantity();
	}

	private void resetCurrentBonusIndex() {
		int value = getValueForCurrentBonusIndexByDefault();
		setCurrentBonusIndex(value);
	}

	private int getValueForCurrentBonusIndexByDefault() {
		return 0;
	}

	private void setCurrentBonusIndex(int value) {
		currentBonusIndex = value;
	}
		
	private void resetSpecialTypeBonusesNumber() {
		int value = getValueForSpecialTypeBonusesNumberByDefault();
		setSpecialTypeBonusesNumber(value);
	}		

	private int getValueForSpecialTypeBonusesNumberByDefault() {
		return 0;
	}

	private void setSpecialTypeBonusesNumber(int value) {
		specialTypeBonusesNumber = value;
	}		

	//Може перейменувати в "resetDataAboutFreeBonus()"?
	private void resetInformationAboutFreeBonus() {
		var value = getValueForDataAboutFreeBonusByDefault();
		setDataAboutFreeBonus(value);
	}

	private bool getValueForDataAboutFreeBonusByDefault() {
		return false;		
	}

	private void setDataAboutFreeBonus(bool value) {
		freeBonusIsFound = value;
	}

	public void updateBonusesQuantity() {
		resetCurrentBonusIndex();
		resetSpecialTypeBonusesNumber();
		analyzeAllBonuses();
		updateQuantityOfBonuses();
	}

	private void analyzeAllBonuses() {
		while (thereAreBonusesNeedToBeAnalyzed()) {
			analyzeCurrentBonus();
		}
	}

	private bool thereAreBonusesNeedToBeAnalyzed() {
		int i = getCurrentBonusIndex();
		int n = getNumberOfBonuses();
		return i < n;
	}

	private int getCurrentBonusIndex() {	
		return currentBonusIndex; 
	}

	private int getNumberOfBonuses() {	 
		var list = getListOfBonuses();
		return list.Count;
	}

	private List<Dictionary<string, string>> getListOfBonuses() {		
		var controller = getControllerOfBonuses();
		return controller.getBonusesList();
	}

	//Може перебудувати тіло?
	private BonusesController getControllerOfBonuses() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		return go.GetComponent<BonusesController>();
	}

	//Може перейменувати в "processCurrentBonus()"?
	private void analyzeCurrentBonus() {
		increaseSpecialTypeBonusesNumberIfItIsPossible();
		moveOnToTheNextBonus();
	}

	//Може перейменувати в "increaseSpecialTypeBonusesNumberAtFirstOpportunity()"?
	private void increaseSpecialTypeBonusesNumberIfItIsPossible() {
		if (currentBonusTypeCorrelatesWithBonusesListType()) {
			increaseSpecialTypeBonusesNumberByOne();
		}
	}

	private bool currentBonusTypeCorrelatesWithBonusesListType() {
		var bonusTypeName = getNameOfBonusType();
		var value = getBonusName();
		return bonusTypeName.Equals(value);
	}		

	private string getNameOfBonusType() {
		var bonus = getCurrentBonusByItsIndex();
		var index = getIndexForBonusTypeName();
		return bonus[index];
	}

	private Dictionary<string, string> getCurrentBonusByItsIndex() {
		var list = getListOfBonuses();
		var index = getCurrentBonusIndex();
		return list[index];
	}

	private string getIndexForBonusTypeName() {
		return "Bonus name";
	}		

	//Може просто звертатись до name у предметі (GameObject), до якого прикріплений цей скрипт?
	protected abstract string getBonusName();

	private void increaseSpecialTypeBonusesNumberByOne() {
		int value = getValueForIncreasedSpecialTypeBonusesNumber();
		setSpecialTypeBonusesNumber(value);
	}		

	private int getValueForIncreasedSpecialTypeBonusesNumber() {
		int number = getSpecialTypeBonusesNumber();
		int increment = getIncrementForSpecialTypeBonusesNumber();
		return number + increment;
	}

	private int getSpecialTypeBonusesNumber() {
		return specialTypeBonusesNumber;
	}

	private int getIncrementForSpecialTypeBonusesNumber() {
		return 1;
	}

	private void moveOnToTheNextBonus() {
		int value = getValueForNextBonusIndex();
		setCurrentBonusIndex(value);
	}		

	private int getValueForNextBonusIndex() {	
		int index = getCurrentBonusIndex();
		int increment = getValueForMovingOnToTheNextBonusIndex();
		return index + increment;
	}

	private int getValueForMovingOnToTheNextBonusIndex() {
		return 1;
	}

	private void updateQuantityOfBonuses() {
		var tm = getTextMeshForBonusQuantity();
		var value = getQuantityOfBonuses();
		tm.text = value;
	}

	private TextMesh getTextMeshForBonusQuantity() {
		var t = getTransformForBonusQuantity();
		return t.GetComponent<TextMesh>();
	}

	private Transform getTransformForBonusQuantity() {
		var name = getNameForBonusQuantity();
		return transform.Find(name);
	}

	private string getNameForBonusQuantity() {
		return "Bonus Quantity";
	}

	private string getQuantityOfBonuses() {
		int n = getSpecialTypeBonusesNumber();
		return n.ToString();
	}

	public void addBonusToLevelIfItIsPossible() {
		if (bonusAddingToLevelIsPossible()) {
			addBonusToLevel();
		}
	}

	//Може перебудувати тіло?
	private bool bonusAddingToLevelIsPossible() {
		var bc = getControllerOfBonuses();
		var l = bc.getBonusesList();
		int n = getNumberOfDestroyables();
		int m = l.Count;
		return m < n;
	}

	//Може перебудувати тіло?
	private int getNumberOfDestroyables() {
		const string TAG = "Destroy";
		var go = GameObject.FindGameObjectsWithTag(TAG);
		return go.Length;
	}

	private void addBonusToLevel() {
		addInformationAboutNewBonusToBonusesList();
		updateBonusesQuantity();
	}

	//Може перейменувати в "addDataAboutNewBonusToBonusesList()"?
	//Може перебудувати тіло?
	private void addInformationAboutNewBonusToBonusesList() {
		const string KEY_1 = "Bonus name";
		const string KEY_2 = "Bonus parent name";
		var bc = getControllerOfBonuses();
		var list = bc.getBonusesList();
		var d = new Dictionary<string, string>();
		d[KEY_1] = getBonusName();
		d[KEY_2] = "";
		list.Add(d);
		bc.setBonusesList(list);
	}

	public void subtractBonusFromLevelIfItIsPossible() {
		resetCurrentBonusIndex();
		resetInformationAboutFreeBonus();
		analyzeIfItIsPossibleToSubtractBonusFromLevel();
		updateBonusesQuantity();
	}

	//Може перейменувати в "subtractBonusFromLevelAtFirstOpportunity()"?
	private void analyzeIfItIsPossibleToSubtractBonusFromLevel() {
		while(needInAnalyzingIsStillPresent()) {
			analyzeIfCurrentBonusIsTheRightOne();
		}
	}

	//Може перейменувати в "bonusCanBeSubtracted()"?
	private bool needInAnalyzingIsStillPresent() {
		var a = thereAreBonusesNeedToBeAnalyzed();
		var b = freeBonusIsWanted();
		return a && b;
	}		

	private bool freeBonusIsWanted() {
		var b = freeBonusIsSpotted();
		return !b;
	}

	private bool freeBonusIsSpotted() {
		return freeBonusIsFound;
	}

	private void analyzeIfCurrentBonusIsTheRightOne() {
		if (currentBonusIsTheRightOne()) {
			deleteCurrentBonus();
		} else {
			moveOnToTheNextBonus();
		}
	}
		
	private bool currentBonusIsTheRightOne() {
		var a = currentBonusIsTheExpectedOne();
		var b = currentBonusIsParentless();
		return a && b;
	}		

	private bool currentBonusIsTheExpectedOne() {
		var bonusName = getNameOfCurrentBonus();
		var value = getNameOfExpectedBonus();
		return bonusName.Equals(value);
	}

	private string getNameOfCurrentBonus() {
		return getNameOfBonusType();
	}

	private string getNameOfExpectedBonus() {
		return getBonusName();
	}

	private bool currentBonusIsParentless() {
		int length = getLengthOfCurrentBonusParentName();
		int value = getValueForCurrentBonusBeingParentless();
		return length == value;
	}

	private int getLengthOfCurrentBonusParentName() {
		var parentName = getNameOfCurrentBonusParent();
		return parentName.Length;
	}

	private string getNameOfCurrentBonusParent() {	
		var bonus = getCurrentBonusByItsIndex();
		var index = getIndexForBonusParentName();
		return bonus[index];
	}

	private string getIndexForBonusParentName() {
		return "Bonus parent name";
	}

	private int getValueForCurrentBonusBeingParentless() {
		return 0;
	}

	//Може перебудувати тіло?
	private void deleteCurrentBonus() {
		var bc = getControllerOfBonuses();
		var list = bc.getBonusesList();
		int i = currentBonusIndex;
		freeBonusIsFound = true;
		list.RemoveAt(i);
		bc.setBonusesList(list);
	}
}