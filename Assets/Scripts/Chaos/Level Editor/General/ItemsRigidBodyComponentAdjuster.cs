using UnityEngine;

namespace AssemblyCSharp
{
	public abstract class ItemsRigidBodyComponentAdjuster {
		private int currentIndependentItemPosition;

		public ItemsRigidBodyComponentAdjuster() {
			currentIndependentItemPosition = 0;
		}
		
		public void adjustRigidBodyComponentForIndependentItems() {
			resetCurrentIndependentItemPosition();
			adjustRigidBodyComponentForItems();
		}
		
		private void resetCurrentIndependentItemPosition() {
			currentIndependentItemPosition = 0;
		}
		
		private void adjustRigidBodyComponentForItems() {
			while (thereAreIndependentItemsNeedToBeAnalyzed()) {
				adjustRigidBodyComponentForCurrentIndependentItem();
			}
		}
		
		private bool thereAreIndependentItemsNeedToBeAnalyzed() {
			int i = currentIndependentItemPosition;
			int n = getNumberOfDestroyables();
			int m = getNumberOfUndestroyables();
			return i < n + m;
		}
		
		private int getNumberOfDestroyables() {
			var go = getArrayOfDestroyables();
			return go.Length;
		}
		
		private GameObject[] getArrayOfDestroyables() {
			var sf = getSharedFunctionsFromLevelObject();
			return sf.getDestroyables();
		}
		
		private SharedFunctions getSharedFunctionsFromLevelObject() {
			const string TAG = "Level";
			var go = GameObject.FindGameObjectWithTag(TAG);
			return go.GetComponent<SharedFunctions>();
		}
		
		private int getNumberOfUndestroyables() {
			var go = getArrayOfUndestroyables();
			return go.Length;
		}
		
		private GameObject[] getArrayOfUndestroyables() {
			var sf = getSharedFunctionsFromLevelObject();
			return sf.getUndestroyables();
		}
		
		private void adjustRigidBodyComponentForCurrentIndependentItem() {
			adjustRigidBodyComponentForIndependentItem();
			moveOnToTheNextIndependentItem();
		}

		protected abstract void adjustRigidBodyComponentForIndependentItem();
		
		protected GameObject getCurrentIndependentItemByItsPosition() {
			int i = currentIndependentItemPosition;
			int n = getNumberOfDestroyables();
			var d = getArrayOfDestroyables();
			var u = getArrayOfUndestroyables();
			return i < n ? d [i] : u [i - n];
		}
		
		private void moveOnToTheNextIndependentItem() {
			++currentIndependentItemPosition;
		}
	}
}