using UnityEngine;

namespace AssemblyCSharp
{
	public class ItemsRigidBodyComponentDestroyer : ItemsRigidBodyComponentAdjuster {
		protected override void adjustRigidBodyComponentForIndependentItem() {
			var item = getCurrentIndependentItemByItsPosition();
			var rb = item.GetComponent<Rigidbody2D>();
			Object.Destroy(rb);
		}
	}
}