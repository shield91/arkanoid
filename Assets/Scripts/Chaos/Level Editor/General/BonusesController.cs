﻿using UnityEngine;
using AssemblyCSharp;
using System.Xml;
using System.Collections.Generic;

using System.Xml.Serialization;
using System.IO;
using System;

public class BonusesController : MonoBehaviour {
	private List<Dictionary<string, string>> actualBonuses;
	private Dictionary<string, string> currentBonus;
	private XmlNodeList savedBonuses;
	private string lastFreeBonusName;
	private int currentBonusIndex;

	private void Awake() {
		initializeFields();
		loadActualBonuses();
	}

	private void initializeFields() {
		actualBonuses = new List<Dictionary<string, string>>();


		//savedBonuses = getBonusesFromLevel();

		lastFreeBonusName = "";
		currentBonusIndex = 0;
	}

	private XmlNodeList getBonusesFromLevel() {
		const string XPATH = "Level/Bonuses/Bonus";
		XmlDocument level = getLoadedLevel();
		return level.SelectNodes(XPATH);
	}

	private XmlDocument getLoadedLevel() {
		LevelLoader l = new LevelLoader ();
		return l.getLoadedLevel();
	}

	private void loadActualBonuses() {		
		resetCurrentBonusIndex();
		loadBonuses();
	}

	private void resetCurrentBonusIndex() {
		int value = getValueForCurrentBonusIndexByDefault();
		setCurrentBonusIndex(value);
	}

	private int getValueForCurrentBonusIndexByDefault() {
		return 0;
	}

	private void setCurrentBonusIndex(int value) {
		currentBonusIndex = value;
	}

	private void loadBonuses() {
		while(thereAreBonusesNeedToBeLoaded()) {
			fillUpActualBonusesWithCurrentOne();
		}			
	}

	private bool thereAreBonusesNeedToBeLoaded() {
		int i = getCurrentBonusIndex();
		int n = getNumberOfBonuses();
		return i < n;
	}

	private int getCurrentBonusIndex() {
		return currentBonusIndex;
	}

	private int getNumberOfBonuses() {
		var bonuses = getBonusesFromLevelsSet();
		return bonuses.Count;
	}

	private List<Gift> getBonusesFromLevelsSet() {
		var l = getChosenLevelFromLevelsSet();
		return l.bonuses;
	}

	private Level getChosenLevelFromLevelsSet() {
		if (campaignModeIsOn()) {
			return getChosenCampaignLevelFromLevelsSet();
		} else {
			return getChosenFreeLevelFromLevelsSet();
		}
	}

	private bool campaignModeIsOn() {
		var key = getKeyForChosenCampaignIndex();
		return PlayerPrefs.HasKey(key);
	}

	private string getKeyForChosenCampaignIndex() {
		return "Chosen Campaign Index";
	}

	private Level getChosenCampaignLevelFromLevelsSet() {
		var levels = getChosenCampaignLevelsFromLevelsSet();
		var index = getIndexOfChosenLevel();
		return levels[index];
	}

	private List<Level> getChosenCampaignLevelsFromLevelsSet() {
		var c = getChosenCampaignFromLevelsSet();
		return c.levels;
	}

	private Campaign getChosenCampaignFromLevelsSet() {
		var campaigns = getCampaignsFromLevelsSet();
		var index = getIndexOfChosenCampaign();
		return campaigns[index];
	}

	private List<Campaign> getCampaignsFromLevelsSet() {
		var s = getSetOfLevels();
		return s.campaigns;
	}

	private Levels getSetOfLevels() {		
		
		//У скрипті MainMenu.cs провернути щось подібне:
		//https://stackoverflow.com/questions/2407302/convert-xmldocument-to-string
		//(це аби можна було запихувати "Levels Set.xml" в string навіть в стані (режимі) редактора рівнів).
		//		if (levelEditorModeIsOn()) {
		//			return getSetOfLevelsInLevelEditorMode();
		//		} else {
		//			return getSetOfLevelsInPlayMode();
		//		}
		//levelEditorModeIsOn
		if (PlayerPrefs.HasKey ("Level Editor Mode")) {
			//getSetOfLevelsInLevelEditorMode
			var serializer = getSerializerForLevelsSet();
			var stream = new FileStream (PlayerPrefs.GetString ("Path To Levels"), FileMode.OpenOrCreate);
			var o = serializer.Deserialize (stream) as Levels;
			stream.Close();
			return o;

			//float
		} else {
			//getSetOfLevelsInPlayMode
			var serializer = getSerializerForLevelsSet();
			var textReader = getTextReaderForLevelsSet();
			return serializer.Deserialize(textReader) as Levels;	
		}

//		var serializer = getSerializerForLevelsSet();
//		var textReader = getTextReaderForLevelsSet();
//		return serializer.Deserialize(textReader) as Levels;
	}

	private XmlSerializer getSerializerForLevelsSet() {
		var type = getTypeForLevelsSet();
		return new XmlSerializer(type);
	}

	private Type getTypeForLevelsSet() {
		return typeof(Levels);
	}

	private TextReader getTextReaderForLevelsSet() {
		var s = getStringForLevelsSet();
		return new StringReader(s);
	}

	private string getStringForLevelsSet() {
		var key = getKeyForLevelsSet();
		return PlayerPrefs.GetString(key);
	}

	private string getKeyForLevelsSet() {
		return "Levels";
	}

	private int getIndexOfChosenCampaign() {
		var key = getKeyForChosenCampaignIndex();
		return PlayerPrefs.GetInt(key);	
	}

	private int getIndexOfChosenLevel() {
		var key = getKeyForChosenLevelIndex();
		return PlayerPrefs.GetInt(key);
	}

	private string getKeyForChosenLevelIndex() {
		return "Chosen Level Index";
	}

	private Level getChosenFreeLevelFromLevelsSet() {
		var levels = getFreeLevelsFromLevelsSet();
		var index = getIndexOfChosenLevel();
		return levels[index];
	}

	private List<Level> getFreeLevelsFromLevelsSet() {
		var s = getSetOfLevels();
		return s.freeLevels;
	}

	private void fillUpActualBonusesWithCurrentOne() {
		addCurrentBonusToListOfActualOnes();
		moveOnToTheNextBonus();
	}

	private void addCurrentBonusToListOfActualOnes() {
		updateItemForCurrentBonus();
		setUpItemForCurrentBonus();
		addItemForCurrentBonusToListOfActualOnes();
	}

	private void updateItemForCurrentBonus() {
		var value = getValueForCurrentBonusByDefault();
		setCurrentBonus(value);
	}

	private Dictionary<string, string> getValueForCurrentBonusByDefault() {
		return new Dictionary<string, string>();
	}

	private void setCurrentBonus(Dictionary<string, string> value) {
		currentBonus = value;
	}

	private void setUpItemForCurrentBonus() {
		setUpCurrentBonusName();
		setUpCurrentBonusParentName();
	}

	private void setUpCurrentBonusName() {
		var item = getCurrentBonus();
		var key = getKeyForBonusName();
		var value = getValueForBonusNameFromLevelsSet();
		item[key] = value;
	}

	private Dictionary<string, string> getCurrentBonus() {
		return currentBonus;
	}

	private string getKeyForBonusName() {
		return "Bonus name";
	}

	private string getValueForBonusNameFromLevelsSet() {
		var b = getCurrentBonusFromLevelsSet();
		return b.name;
	}

	private Gift getCurrentBonusFromLevelsSet() {
		var bonuses = getBonusesFromLevelsSet();
		var index = getCurrentBonusIndex();
		return bonuses[index];
	}

	private void setUpCurrentBonusParentName() {
		var item = getCurrentBonus();
		var key = getKeyForBonusParentName();
		var value = getValueForCurrentBonusParentName();
		item[key] = value;
	}

	private string getKeyForBonusParentName() {
		return "Bonus parent name";
	}

	private string getValueForCurrentBonusParentName() {
		var b = getCurrentBonusFromLevelsSet();
		return b.parentName;
	}

	private void addItemForCurrentBonusToListOfActualOnes() {
		var bonuses = getBonusesList();
		var item = getCurrentBonus();
		bonuses.Add(item);
	}

	private void moveOnToTheNextBonus() {
		int value = getValueForCurrentBonusIndex();
		setCurrentBonusIndex(value);
	}

	private int getValueForCurrentBonusIndex() {
		int increment = getValueForMovingOnTheNextBonus();
		int index = getCurrentBonusIndex();
		return index + increment;
	}

	private int getValueForMovingOnTheNextBonus() {
		return 1;
	}


	private void resetCurrentBonusPosition() {
		currentBonusIndex = 0;
	}









	private string getNameOfSavedBonus() {
		const string NAME = "Name";
		return getValueOfAttribute(NAME);
	}

	private string getValueOfAttribute(string name) {
		int i = currentBonusIndex;
		var currentBonus = savedBonuses[i];
		var xac = currentBonus.Attributes;
		var xa = xac[name];
		return xa.Value;
	}

	private string getParentNameOfSavedBonus() {
		const string NAME = "ParentName";
		return getValueOfAttribute(NAME);
	}


	//TODO

	private List<BonusData> l = new List<BonusData>();

	[System.Serializable]
	public class BonusData {
		private Dictionary<string, string> values;
		private string name;
		private string parentName;

		public BonusData(Dictionary<string, string> data) {
			name = data["name"];
			parentName = data["parent name"];
			values = data;
		}

		public string getName() {
			return name;
			//return values ["name"];
		}

		public string getParentName() {
			return parentName;
			//return values ["parent name"];
		}

		public void setName(string value){
			name = value;
		}

		public void setParentName(string value) {
			parentName = value;
		}
	}

	private List<DictionaryStringString> ll = new List<DictionaryStringString>();
	[System.Serializable]
	public class DictionaryStringString : Dictionary<string, string> {}

	void Update() {
//		Debug.Log("Update: " + l.Count);
		if (l.Count == 0) {
			var d = new Dictionary<string, string>();

			d["name"] = "1";
			d["parent name"] = "2";
			var bd = new BonusData(d);
			//bd.setName(d["name"]);
			//bd.setParentName(d["parent name"]);

			//l.Add(bd);
			l.Add(bd);
			Debug.Log("Update: " + l.Count + bd.getName() + bd.getParentName());
		}
		//Debug.Log("Update 1: "+ l[0].getName()+ "|"+ l[0].getParentName());
		if (ll.Count == 0) {
			var d = new DictionaryStringString();
			d["name"] = "3";
			d["parent name"] = "4";

			ll.Add(d);
			ll.Add(d);
			ll.Add(d);
			ll.Add(d);
			ll.Add(d);
			Debug.Log("Update: " + ll.Count + d["name"] + d["parent name"]);
		}


		if (levelEditorModeIsEnabled()) {
			updateBonusesQuantityIfItIsNecessary();
		}
	}

	private bool levelEditorModeIsEnabled() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var sf = GetComponent<SharedFunctions>();
		return sf.levelEditorModeIsEnabled();
	}
	
	private void updateBonusesQuantityIfItIsNecessary() {
		if (bonusesAreExceedingDestroyables()) {
			updateBonusesQuantityByDeletingLastFreeBonus();
		}
	}
	
	private bool bonusesAreExceedingDestroyables() {
		const string TAG = "Destroy";
		var go = GameObject.FindGameObjectsWithTag(TAG);
		var bc = GetComponent<BonusesController>();
		var list = bc.getBonusesList();
		int m = list.Count;
		int n = go.Length;
		return m > n;
	}
	
	private void updateBonusesQuantityByDeletingLastFreeBonus() {
		deleteLastFreeBonus();
		decreaseBonusesQuantityByOne();
	}
	
	private void deleteLastFreeBonus() {
		const string KEY = "Bonus name";
		var list = actualBonuses;
		int index = getLastPositionOfFreeBonus();
		var bonus = list[index];
		lastFreeBonusName = bonus[KEY];
		list.RemoveAt(index);
	}
	
	private int getLastPositionOfFreeBonus() {
		var r = new FreeBonusLastPositionRetriever();
		return r.getSpecialBonusPosition();
	}
	
	private void decreaseBonusesQuantityByOne() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var l = go.GetComponent<PlayScreen>();
		var bonusesList = l.getBonusesList();
		var parentTransform = bonusesList.transform;
		string name = lastFreeBonusName;
		var child = parentTransform.Find(name);
		var btli = child.GetComponent<BonusesTypesListItem>();
		btli.updateBonusesQuantity();
	}
	//TODO
	
	public List<Dictionary<string, string>> getBonusesList() {
		return actualBonuses;
	}

	public void setBonusesList(List<Dictionary<string, string>> list) {
		actualBonuses = list;
	}
}