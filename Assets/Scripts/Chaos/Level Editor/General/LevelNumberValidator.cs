using UnityEngine;
using System;
using System.Xml;

namespace AssemblyCSharp
{
	public class LevelNumberValidator {
		private int chosenLevelNumber;
		private int currentLevelPosition;
		private bool levelNumberIsMissingInLevelsList;

		public LevelNumberValidator(int levelNumber) {
			initializeFieldsFrom(levelNumber);
			checkIfLevelNumberIsAbsentInLevelsList();
		}

		private void initializeFieldsFrom(int levelNumber) {
			chosenLevelNumber = levelNumber;
			currentLevelPosition = 0;
			levelNumberIsMissingInLevelsList = true;
		}

		private void checkIfLevelNumberIsAbsentInLevelsList() {
			resetCurrentLevelPosition();
			continueCheckingIfLevelNumberIsAbsentInLevelsList();
		}

		private void resetCurrentLevelPosition() {
			currentLevelPosition = 0;
		}

		private void continueCheckingIfLevelNumberIsAbsentInLevelsList() {
			while (thereAreLevelsNeedToBeAnalyzed()) {
				checkIfChosenLevelNumberIsAtCurrentLevelPosition();
			}
		}

		private bool thereAreLevelsNeedToBeAnalyzed() {
			const string XPATH = "Levels/Level";
			XmlDocument d = getLoadedLevelsList();
			XmlNodeList l = d.SelectNodes(XPATH);
			int i = currentLevelPosition;
			int n = l.Count;
			return i < n;
		}

		private XmlDocument getLoadedLevelsList() {
			const string A = "\\Levels list.xml";
			string saveLocation = getSaveLocationFromPlayerPreferences();
			string filename = saveLocation + A;
			XmlDocument d = new XmlDocument();
			d.Load(filename);
			return d;
		}

		private string getSaveLocationFromPlayerPreferences() {
			const string KEY = "Save location";
			const string DEFAULT_VALUE = "C:\\Arkanoid levels";
			return PlayerPrefs.GetString(KEY, DEFAULT_VALUE);
		}

		private void checkIfChosenLevelNumberIsAtCurrentLevelPosition() {
			const string XPATH = "Levels/Level";
			const int LEVEL_NUMBER_POSITION = 0;
			XmlDocument d = getLoadedLevelsList();
			XmlNodeList l = d.SelectNodes(XPATH);
			XmlNode n = l[currentLevelPosition];
			XmlAttributeCollection c = n.Attributes;
			XmlAttribute a = c[LEVEL_NUMBER_POSITION];
			string s = a.Value;
			int number = int.Parse(s);
			levelNumberIsMissingInLevelsList &= number != chosenLevelNumber;
			++currentLevelPosition;
		}

		public bool levelNumberIsPositiveNumber() {
			return chosenLevelNumber > 0;
		}

		public bool levelNumberIsAbsentInLevelsList() {
			return levelNumberIsMissingInLevelsList;
		}
	}
}