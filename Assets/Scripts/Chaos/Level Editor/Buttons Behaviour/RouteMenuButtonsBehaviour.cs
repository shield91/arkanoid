﻿using AssemblyCSharp;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class RouteMenuButtonsBehaviour : MonoBehaviour {
	private List<Dictionary<string, string>> route;


	private GameObject selectedItem;
	
	void Start() {
		route = new List<Dictionary<string, string>>();
	}


	public void setSelectedItem(GameObject value) {

		selectedItem = value;

	}

	public void addPoint() {
		createPoint();
		loadPointData();
		highlightPoint();
	}

	private void createPoint() {
		
		const string PATH = "Order/Item Route Point";

//		const string PATH = "Item Route Point";

		var original = Resources.Load(PATH);
		var o = GameObject.Instantiate(original);
		var go = (GameObject)o;
		var t = go.transform;
		var parent = getParentForPoint();
		var tm = go.GetComponent<TextMesh>();
		var irp = go.GetComponent<ItemRoutePoint>();
		string number = getNumberForPoint();
		t.position = getPositionOfPoint();
		go.name = PATH + " " + number;
		tm.text = number;
		irp.resize();
		t.SetParent(parent);
	}

	private Transform getParentForPoint() {
		const string TAG = "Clicked Item Route";
		var go = GameObject.FindGameObjectWithTag(TAG);
		return go.transform;
	}

	private string getNumberForPoint() {
		int i = route.Count + 1;
		return i + "";
	}

	private Vector2 getPositionOfPoint() {
		const float X = 0f;
		const float Y = 0f;
		return new Vector2(X, Y);
	}

	private void loadPointData() {
		const string KEY_1 = "x";
		const string KEY_2 = "y";
		const string KEY_3 = "Time to next point";
		var point = new Dictionary<string, string>();
		point[KEY_1] = "0";
		point[KEY_2] = "0";
		point[KEY_3] = "0";
		route.Add(point);
	}

	private void highlightPoint() {
		Transform point = getNewPointFromRoute();
		ItemRoutePoint irp = point.GetComponent<ItemRoutePoint>();
		irp.highlight();
	}

	private Transform getNewPointFromRoute() {
		Transform routePoints = getParentForPoint();
		int index = route.Count - 1;
		return routePoints.GetChild(index);
	}

	public void deletePoint() {

		// deletedPointIsNotTheFirstOne
		if (getScriptFromPoint ().getPointIndex () > 0) {

			deletePointFromList ();
			deletePointItself ();
			updateRoute ();

		}

	}

	private void deletePointFromList() {
		ItemRoutePoint irp = getScriptFromPoint();
		int i = irp.getPointIndex();
		route.RemoveAt(i);
	}

	private ItemRoutePoint getScriptFromPoint() {
		GameObject go = getPointFromRoute();
		return go.GetComponent<ItemRoutePoint>();
	}
	
	private GameObject getPointFromRoute() {
		const string TAG = "Item Route Clicked Point";
		return GameObject.FindGameObjectWithTag(TAG);
	}

	private void deletePointItself() {
		GameObject go = getPointFromRoute();
		ItemRoutePoint irp = getScriptFromPoint();
		irp.unhighlight();
		go.SetActive(false);
		Destroy(go);
	}

	private void updateRoute() {
		RouteUpdater u = new RouteUpdater();
		u.updateRoute();
	}

	public void saveRoute() {
		tryToSaveHighlightedPointData();
		saveRouteForClickedItem();
		hideRoute();
		hideRouteMenu();
		markClickedItemAsOrdinaryOne();
	}

	private void tryToSaveHighlightedPointData() {
		try {
			saveHighlightedPointData();
		} catch (Exception e) {
			handle(e);
		}
	}

	private void saveHighlightedPointData() {
		const string KEY_1 = "x";
		const string KEY_2 = "y";
		const string KEY_3 = "Time to next point";
		const string TAG = "Level";
		const string NAME = "Time To Next Point Field";
		var go = getPointFromRoute();
		var t = go.transform;
		var position = t.position;
		var l = GameObject.FindGameObjectWithTag(TAG);
		var sf = l.GetComponent<SharedFunctions>();
		var f = sf.getInputFieldComponentFrom(NAME);
		var irp = getScriptFromPoint();
		int i = irp.getPointIndex();
		var point = route[i];
		point[KEY_1] = position.x + "";
		point[KEY_2] = position.y + "";
		point[KEY_3] = f.text;
	}

	private void handle(Exception e) {
		string message = e.StackTrace;
		Debug.Log(message);
	}

	private void saveRouteForClickedItem() {
		IndependentItem ii = getScriptFromClickedItem();
		ii.setRoutePoints(route);
	}

	private IndependentItem getScriptFromClickedItem() {
		const string TAG = "Clicked Item";
		var go = GameObject.FindGameObjectWithTag(TAG);
		return go.GetComponent<IndependentItem>();
	}

	private void hideRoute() {
		RouteHider h = new RouteHider();
		h.hideRoute();
	}

	private void hideRouteMenu() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var l = go.GetComponent<PlayScreen>();
		var menu = l.getItemRouteMenu();
		menu.SetActive(false);
	}

	private void markClickedItemAsOrdinaryOne() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var sf = go.GetComponent<SharedFunctions>();
		sf.markClickedIndependentItemAsOrdinaryOne();		
	}

	public void exitRouteMenu() {
		hideRoute();
		hideRouteMenu();
		markClickedItemAsOrdinaryOne();
	}

	public List<Dictionary<string, string>> getRoute() {
		return route;
	}

	public void setRoute(List<Dictionary<string, string>> routePoints) {
		route = routePoints;
	}


	public GameObject getSelectedItem() {

		return selectedItem;

	}

}