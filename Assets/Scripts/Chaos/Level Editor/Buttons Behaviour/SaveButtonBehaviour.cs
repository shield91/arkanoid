﻿using AssemblyCSharp;
using System;
using System.Xml;
using System.Text;
using UnityEngine;
using UnityEngine.UI;


using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;

public class SaveButtonBehaviour : MonoBehaviour {
	private GameObject saveDialogWindow;
	private XmlDocument levelsList;
	private XmlTextWriter chosenLevel;
	private string saveLocation;
	private int levelNumber;
	
	void Start() {
		initializeFields();
		fillUpFieldsInSaveDialogWindow();
		hideSaveDialogWindow();
		loadLevelsListIfLevelEditorModeIsEnabled();
	}
	
	private void initializeFields() {
		saveDialogWindow = GameObject.FindGameObjectWithTag("Save Dialog Window");
		levelsList = new XmlDocument();
		chosenLevel = null;
		saveLocation = getSaveLocationFromPlayerPreferences();
		levelNumber = 0;
	}
	
	private string getSaveLocationFromPlayerPreferences() {
		const string KEY = "Save location";
		const string DEFAULT_VALUE = "C:\\Arkanoid levels";
		return PlayerPrefs.GetString(KEY, DEFAULT_VALUE);
	}
	
	private void fillUpFieldsInSaveDialogWindow() {
		fillUpLevelNumberField();
		fillUpSaveLocationField();
	}
	
	private void fillUpLevelNumberField() {
		const string KEY = "Loaded level number";
		const string NAME = "Level Number Field";
		string text = PlayerPrefs.GetInt(KEY) + "";
		InputField f = getInputFieldComponentFrom(NAME);
		f.text = text;
	}
	
	private InputField getInputFieldComponentFrom(string name) {
		GameObject go = GameObject.FindGameObjectWithTag(name);
		return go.GetComponent<InputField>();
	}
	
	private void fillUpSaveLocationField() {
		const string NAME = "Save Location Field";
		InputField f = getInputFieldComponentFrom(NAME);
		f.text = saveLocation;
	}
	
	public void hideSaveDialogWindow() {
		saveDialogWindow.SetActive(false);
	}
	
	private void loadLevelsListIfLevelEditorModeIsEnabled() {
		if (levelEditorModeIsEnabled()) {
			loadLevelsList();
		}
	}

	private bool levelEditorModeIsEnabled() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var sf = go.GetComponent<SharedFunctions>();
		return sf.levelEditorModeIsEnabled();
	}
	
	private void loadLevelsList() {
		const string A = "\\Levels list.xml";
		string filename = saveLocation + A;
		levelsList.Load(filename);
	}
	
	public void showSaveDialogWindow() {
		saveDialogWindow.SetActive(true);
	}
	
	public void save() {
		
//		var serializer = getSerializerForLevelsSet();

		disableLevelEditorButtonsToAvoidDataCorruptionDuringSaving ();

		var serializer = new XmlSerializer(typeof(Levels));
		var stream = new FileStream (PlayerPrefs.GetString ("Path To Levels"), FileMode.OpenOrCreate);
		var levelsSet = serializer.Deserialize (stream) as Levels;
		stream.Close();

//		var chosenLevel;
		Level chosenLevel;

		//campaignModeIsOn
		if (PlayerPrefs.HasKey("Chosen Campaign Index")) {
			chosenLevel = levelsSet
				.campaigns[PlayerPrefs.GetInt("Chosen Campaign Index")]
				.levels[PlayerPrefs.GetInt("Chosen Level Index")];
		} else {

//			"
			chosenLevel = levelsSet.freeLevels[PlayerPrefs.GetInt("Chosen Level Index")];
//			c
		}
//		.
		//saveDestroyables
		chosenLevel.destroyables = new List<DestroyableItem>();
		for (int i = 0; i < transform.parent.Find ("Destroyables").childCount; ++i) {
			var item = new DestroyableItem();
			item.name = transform.parent.Find ("Destroyables").GetChild (i).name;
			item.x = transform.parent.Find ("Destroyables").GetChild (i).position.x;
			item.y = transform.parent.Find ("Destroyables").GetChild (i).position.y;
			chosenLevel.destroyables.Add (item);
		}

		//saveUndestroyables
		chosenLevel.undestroyables=new List<UndestroyableItem>();
		var undestroyables = transform.parent.Find ("Undestroyables");
		for (int i = 0; i < undestroyables.childCount; ++i) {
			var item = new UndestroyableItem ();
			item.name = undestroyables.GetChild (i).name;
			item.x = undestroyables.GetChild (i).position.x;
			item.y = undestroyables.GetChild (i).position.y;
			chosenLevel.undestroyables.Add (item);
		}

		//saveBonuses
		chosenLevel.bonuses = new List<Gift>();
		var bonuses = GetComponent<BonusesController> ().getBonusesList();
		for (int i = 0; i < bonuses.Count; ++i) {
			var item = new Gift ();
			item.name = bonuses [i] ["Bonus name"];
			item.parentName = bonuses [i] ["Bonus parent name"];
			chosenLevel.bonuses.Add (item);
		}

		//saveRoutes
		chosenLevel.routes = new List<Route>();
		for (int i = 0; i < transform.parent.Find ("Destroyables").childCount; ++i) {
			var destroyables = transform.parent.Find ("Destroyables");
			var routePoints = destroyables.GetChild (i)
				.GetComponent<IndependentItem> ().getWaybillPoints ();
			var item = new Route ();
			item.points = new List<Point> ();
//			item.points = new List<Points> ();
			for (int j = 0; j < routePoints.Count;

//				destroyables.childCount;

				++j) {
				var point = new Point ();
				point.x = float.Parse (routePoints [j] ["x"]);
//				Float.Parse(routePoints [j] ["x"]);
				point.y = float.Parse(routePoints [j] ["y"]);
				point.timeToNextPoint = int.Parse(routePoints [j] ["Time to next point"]);
				item.points.Add (point);
			}
			chosenLevel.routes.Add (item);
		}

		for (int i = 0; i < undestroyables.childCount; ++i) {
			var routePoints = undestroyables.GetChild (i)
				.GetComponent<IndependentItem> ().getWaybillPoints ();
			var item = new Route ();
			item.points = new List<Point> ();
//			item.points = new List<Points> ();
			for (int j = 0; j < routePoints.Count;
//				j<roup
//				j<undestroyables.childCount;
				++j) {
				var point = new Point ();
				point.x = float.Parse(routePoints [j] ["x"]);
				point.y = float.Parse (routePoints [j] ["y"]);
//				19routePoints [j] ["y"];
				point.timeToNextPoint = int.Parse(routePoints [j] ["Time to next point"]);
//				19routePoints [j] ["Time to next point"];
				item.points.Add (point);
			}
			chosenLevel.routes.Add (item);
		}

		stream = new FileStream (PlayerPrefs.GetString ("Path To Levels"), FileMode.OpenOrCreate);
		serializer.Serialize (stream, levelsSet);
		stream.Close ();

//		cho
//		_}P,
//				PR- k-

//			R
//				:
//
//		for (int i =0; 
//			i<transform.parent.Find ("Destroyables").childCount+undestroyables.childCount;
//			++i) {
//			var item = new Route ();
////			item.points = new List<Point> ();
////			item.
//			try {
//				for (int j =0; j<undestroyables.childCount;++j){
//					item.points = new List<Point> ();
//					var routePoints = undestroyables.GetChild(j)
//						.GetComponent<IndependentItem>().getRoutePoints();
//					for (int k=0; k<routePoints.Count;++k) {
//						var point = new Point();
//						point.x = routePoints[k]["x"];
//						point.y = routePoints[k]["y"];
//						point.timeToNextPoint = routePoints[k]["Time to next point"];
//						item.points.Add(point);
//					}
//					chosenLevel.routes.Add(item);
//				}
//			}catch (Exception e){
//				
////					GetHashCode,_
////					var point = new Point();
////					point.x=undestroyables.GetChild(j)
////						.GetComponent<IndependentItem>().getRoutePointsr
//
//				item.
//			-
//			_
//			-
//
//			if<
//
//			if<undestroyables.childCount;r

		enableLevelEditorButtons ();

//		try {
//			saveLevelIfLevelNumberIsValid();
//		} catch (Exception e) {
//			handle(e);		
//		}
	}
	
	private void saveLevelIfLevelNumberIsValid() {
		if (levelNumberIsValid()) {
			saveLevel();
		}
	}
	
	private bool levelNumberIsValid() {
		retrieveLevelNumberFromLevelNumberField();
		return levelNumberHasPassedValidation();			
	}
	
	private void retrieveLevelNumberFromLevelNumberField() {
		const string NAME = "Level Number Field";
		InputField f = getInputFieldComponentFrom(NAME);
		string s = f.text;
		levelNumber = int.Parse(s);
	}
	
	private bool levelNumberHasPassedValidation() {
		LevelNumberValidator v = new LevelNumberValidator(levelNumber);
		return v.levelNumberIsPositiveNumber();
	}
	
	private void saveLevel() {
		updateSaveLocation();
		hideSaveDialogWindow();
		disableLevelEditorButtonsToAvoidDataCorruptionDuringSaving();
		addLevelToLevelsListIfLevelNumberIsAbsentInLevelsList();
		sortLevelsList();
		createFileForChosenLevel();
		saveDestroyables();
		saveUndestroyables();
		saveBonuses();
		saveRoutes();
		saveLevelsList();
		saveFileForChosenLevel();
		updateSaveLocationInPlayerPreferences();
		enableLevelEditorButtons();
	}
	
	private void updateSaveLocation() {
		const string NAME = "Save Location Field";
		InputField f = getInputFieldComponentFrom(NAME);
		saveLocation = f.text;
	}
	
	private void disableLevelEditorButtonsToAvoidDataCorruptionDuringSaving() {
		disableSaveButton();
		disableMainMenuButton();
	}
	
	private void disableSaveButton() {
		const string NAME = "Save Button";
		disable(NAME);
	}
	
	private void disable(string name) {
		Button b = getButtonComponentFrom(name);
		b.interactable = false;
	}
	
	private Button getButtonComponentFrom(string name) {
		GameObject go = GameObject.FindGameObjectWithTag(name);
		return go.GetComponent<Button>();
	}
	
	private void disableMainMenuButton() {
		const string NAME = "Main Menu Button";
		disable(NAME);
	}
	
	private void addLevelToLevelsListIfLevelNumberIsAbsentInLevelsList() {
		if (levelNumberIsAbsentInLevelsList()) {
			addLevelToLevelsList();
		}
	}
	
	private bool levelNumberIsAbsentInLevelsList() {
		LevelNumberValidator v = new LevelNumberValidator(levelNumber);
		return v.levelNumberIsAbsentInLevelsList();
	}
	
	private void addLevelToLevelsList() {
		const string NAME = "Level";
		const string ATTRIBUTE_NAME = "Number";
		string attributeValue = levelNumber + "";
		XmlNode newChild = levelsList.CreateElement(NAME);
		XmlElement root = levelsList.DocumentElement;
		XmlElement child = (XmlElement) newChild;
		child.SetAttribute(ATTRIBUTE_NAME, attributeValue);
		root.AppendChild(newChild);
	}
	
	private void sortLevelsList() {
		LevelsListSorter s = new LevelsListSorter(levelsList);
		s.sortLevelsList();
	}
	
	private void createFileForChosenLevel() {
		const string A = "\\Level ";
		const string B = ".xml";
		const string LOCAL_NAME = "Level";
		string filename = saveLocation + A + levelNumber + B;
		Encoding encoding = Encoding.UTF8;
		chosenLevel = new XmlTextWriter(filename, encoding);
		chosenLevel.Formatting = Formatting.Indented;
		chosenLevel.WriteStartElement(LOCAL_NAME);
	}
	
	private void saveDestroyables() {
		ItemsSaver s = new DestroyablesSaver(chosenLevel);
		s.save();
	}
	
	private void saveUndestroyables() {
		ItemsSaver s = new UndestroyablesSaver(chosenLevel);
		s.save();
	}
	
	private void saveBonuses() {
		ItemsSaver s = new BonusesSaver(chosenLevel);
		s.save();
	}

	private void saveRoutes() {
		ItemsSaver s = new RoutesSaver(chosenLevel);
		s.save();
	}
	
	private void saveLevelsList() {
		const string A = "\\Levels list.xml";
		string filename = saveLocation + A;
		levelsList.Save(filename);
	}
	
	private void saveFileForChosenLevel() {
		chosenLevel.WriteEndElement();
		chosenLevel.Close();
	}
	
	private void updateSaveLocationInPlayerPreferences() {
		const string KEY = "Save location";
		string value = saveLocation;
		PlayerPrefs.SetString(KEY, value);
	}
	
	private void enableLevelEditorButtons() {
		enableSaveButton();
		enableMainMenuButton();
	}
	
	private void enableSaveButton() {
		const string NAME = "Save Button";
		enable(NAME);
	}
	
	private void enable(string name) {
		Button b = getButtonComponentFrom(name);
		b.interactable = true;
	}
	
	private void enableMainMenuButton() {
		const string NAME = "Main Menu Button";
		enable(NAME);
	}
	
	private void handle(Exception e) {
		string message = e.StackTrace;
		Debug.Log(message);
	}
}