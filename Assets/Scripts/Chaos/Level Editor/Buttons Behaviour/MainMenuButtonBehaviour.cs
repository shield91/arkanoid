﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButtonBehaviour : MonoBehaviour {
	public void goToMainMenu() {
		goToPreviousScreen();
		updatePlayerProgress();
	}

	private void goToPreviousScreen() {
		if (playerIsInSurvivalMode()) {
			goToStartScreen();
		} else {
			goToLevelsScreen();
		}
	}

	private bool playerIsInSurvivalMode() {
		var key = getKeyForSurvivalMode();
		return PlayerPrefs.HasKey(key);
	}

	private string getKeyForSurvivalMode() {
		return "Survival Mode Is On";
	}

	private void goToStartScreen() {
		deleteDataAboutSurvivalMode();
		loadStartScreen();
	}

	private void deleteDataAboutSurvivalMode() {
		var key = getKeyForSurvivalMode();
		PlayerPrefs.DeleteKey(key);
	}

	private void loadStartScreen() {
		var sceneName = getSceneNameForStartScreen();
		SceneManager.LoadScene(sceneName);
	}

	private string getSceneNameForStartScreen() {
		return "Start Screen";
	}

	private void goToLevelsScreen() {
		var sceneName = getSceneNameForLevelsScreen();
		SceneManager.LoadScene(sceneName);
	}

	private string getSceneNameForLevelsScreen() {
		return "LevelSelect";
	}

	private void updatePlayerProgress() {
		var ps = getPlayScreenComponentFromGameObjectForLevel();
		ps.updatePlayerProgress();
	}

	private PlayScreen getPlayScreenComponentFromGameObjectForLevel() {
		var go = getGameObjectForLevel();
		return go.GetComponent<PlayScreen>();
	}

	private GameObject getGameObjectForLevel() {
		var tag = getTagForLevel();
		return GameObject.FindWithTag(tag);
	}

	private string getTagForLevel() {
		return "Level";
	}
}