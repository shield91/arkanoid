﻿using AssemblyCSharp;
using UnityEngine;
using System.Collections.Generic;

public class IndependentItemMenuButtonsBehaviour : MonoBehaviour {
	public void createRouteForClickedItem() {
		showClickedItemRouteMenu();
		putRouteInRouteMenu();
		showRouteForClickedItem();
		highlightFirstPointOfTheRoute();
	}

	private void showClickedItemRouteMenu() {
		GameObject menu = getRouteMenuForClickedItem();
		menu.SetActive(true);
	}

	private GameObject getRouteMenuForClickedItem() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var l = go.GetComponent<PlayScreen>();
		return l.getItemRouteMenu();
	}

	private void putRouteInRouteMenu() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var menu = go.GetComponent<RouteMenuButtonsBehaviour>();
		var route = getRouteForClickedItem();
		menu.setRoute(route);
	}

	private List<Dictionary<string, string>> getRouteForClickedItem() {
		const string TAG = "Clicked Item";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var ii = go.GetComponent<IndependentItem>();
		return ii.getWaybillPoints();
	}

	private void showRouteForClickedItem() {
		List<Dictionary<string, string>> points = getRouteForClickedItem();
		RouteViewer v = new RouteViewer(points);
		v.showRoute();
	}

	private void highlightFirstPointOfTheRoute() {
		const string TAG = "Clicked Item Route";
		const int INDEX = 0;
		var go = GameObject.FindGameObjectWithTag(TAG);
		var route = go.transform;
		var point = route.GetChild(INDEX);
		var irp = point.GetComponent<ItemRoutePoint>();
		irp.highlight();
	}
}