﻿using UnityEngine;

public class HelpButtonBehaviour : MonoBehaviour {
	private GameObject helpWindow;

	void Start() {
		initializeHelpWindow();
		hideHelpWindow();
	}

	private void initializeHelpWindow() {
		const string TAG = "Help Window";
		helpWindow = GameObject.FindGameObjectWithTag(TAG);
	}

	public void hideHelpWindow() {
		helpWindow.SetActive(false);
	}

	public void showHelpWindow() {
		helpWindow.SetActive(true);
	}
}