﻿using UnityEngine;
using System.Collections.Generic;

public class DrawingRegion : MonoBehaviour {
	public EdgeCollider2D trailCollider;
	public LineRenderer trailRenderer;
	private List<Vector2> path;
	private Vector2 firstPoint;
	private Vector2 lastPoint;
	private float trailMaximalLengthMultiplier;
	private float trailMinimalLength;
	private float trailMaximalLength;
	private bool trailIsConstructed;
	private int currentPointPosition;

	private void Start() {		
		resetPrivateFields();
	}

	private void resetPrivateFields() {
		resetPath();
		resetFirstPoint();
		resetLastPoint();
		resetTrailMaximalLengthMultiplier();
		resetTrailMinimalLength();
		resetTrailMaximalLength();
		resetDataAboutTrailConstruction();
		resetCurrentPointPosition();
	}

	private void resetPath() {
		var value = getValueForPathByDefault();
		setPath(value);
	}

	private List<Vector2> getValueForPathByDefault() {
		return new List<Vector2>();
	}

	private void setPath(List<Vector2> value) {
		path = value;
	}

	private void resetFirstPoint() {
		var value = getValueForFirstPointByDefault();
		setFirstPoint(value);
	}

	private Vector2 getValueForFirstPointByDefault() {
		return new Vector2();
	}

	private void setFirstPoint(Vector2 value) {
		firstPoint = value;
	}

	private void resetLastPoint() {
		var value = getValueForLastPointByDefault();
		setLastPoint(value);
	}

	private Vector2 getValueForLastPointByDefault() {
		return getValueForFirstPointByDefault();
	}

	private void setLastPoint(Vector2 value) {
		lastPoint = value;
	}

	private void resetTrailMaximalLengthMultiplier() {
		var value = getValueForTrailMaximalLengthMultiplierByDefault();
		setTrailMaximalLengthMultiplier(value);
	}

	private float getValueForTrailMaximalLengthMultiplierByDefault() {
		return 1f;
	}

	public void setTrailMaximalLengthMultiplier(float value) {
		trailMaximalLengthMultiplier = value;
	}

	private void resetTrailMinimalLength() {
		var value = getValueForTrailMinimalLengthByDefault();
		setTrailMinimalLength(value);
	}

	private float getValueForTrailMinimalLengthByDefault() {
		return 2f;
	}

	private void setTrailMinimalLength(float value) {
		trailMinimalLength = value;
	}

	private void resetTrailMaximalLength() {
		var value = getValueForTrailMaximalLengthByDefault();
		setTrailMaximalLength(value);
	}

	private float getValueForTrailMaximalLengthByDefault() {
		return 4f;
	}

	private void setTrailMaximalLength(float value) {
		trailMaximalLength = value;
	}

	private void resetDataAboutTrailConstruction() {
		var value = getValueForDataAboutTrailConstructionByDefault();
		setDataAboutTrailConstruction(value);
	}

	private bool getValueForDataAboutTrailConstructionByDefault() {
		return false;
	}

	private void setDataAboutTrailConstruction(bool value) {
		trailIsConstructed = value;
	}
	
	private void Update() {
		setVertexCountForTrailRenderer();
		updateTrailRenderer();
	}

	private void setVertexCountForTrailRenderer() {
		int count = getNumberOfPoints();
		trailRenderer.SetVertexCount(count);
	}

	private int getNumberOfPoints() {
		var points = trailCollider.points;
		return points.Length;
	}

	private void updateTrailRenderer() {
		resetCurrentPointPosition();
		updateRenderer();
	}

	private void resetCurrentPointPosition() {
		currentPointPosition = 0;
	}

	private void updateRenderer() {
		while(thereArePointsNeedToBeAnalyzed()) {
			updateCurrentPoint();
		}
	}

	private bool thereArePointsNeedToBeAnalyzed() {
		int i = currentPointPosition;
		int n = getNumberOfPoints();
		return i < n;
	}

	private void updateCurrentPoint() {
		updatePoint();
		moveOnToTheNextPoint();
	}

	private void updatePoint() {
		const float Z = -2f;
		int index = currentPointPosition;
		var points = trailCollider.points;
		var point = points[index];
		var x = point.x;
		var y = point.y;
		var position = new Vector3(x, y, Z);
		trailRenderer.SetPosition(index, position);
	}

	private void moveOnToTheNextPoint() {
		++currentPointPosition;
	}

	public void startTrail(Vector2 point) {
		clearThePath();
		retrieveFirstPointFrom(point);
		retrieveLastPointFrom(firstPoint);
		markThatTrailIsWaitingForItsCreation();
		startThePath();
		endThePath();

		convertPathIntoTrail(); //comment
//		b=true; //decomment
	}

	/*private bool b = false;

	private void FixedUpdate() {
		if (b) {
			convertPathIntoTrail ();
			b = false;
		}
	} decomment*/

	private void clearThePath() {
		path.Clear();
	}

	private void retrieveFirstPointFrom(Vector2 point) {
		firstPoint = getAdjustedPointFrom(point);
	}

	private Vector2 getAdjustedPointFrom(Vector2 point) {
		float fa = point.x;
		float fb = getRightBoundOfDrawingRegion();
		float fc = getLeftBoundOfDrawingRegion();
		float fd = Mathf.Min(fa, fb);
		float x = Mathf.Max(fc, fd);
		float fe = point.y;
		float ff = getUpperBoundOfDrawingRegion();
		float y = Mathf.Min(fe, ff);
		return new Vector2(x, y);
	}

	private float getRightBoundOfDrawingRegion() {
		const float DELTA = 0.2f;
		var upperRightBound = getUpperRightBoundOfDrawingRegion();
		return upperRightBound.x - DELTA;
	}

	private Vector3 getUpperRightBoundOfDrawingRegion() {
		var bounds = getBoundsOfDrawingRegion();
		return bounds.max;
	}

	private Bounds getBoundsOfDrawingRegion() {
		var bc = GetComponent<BoxCollider2D>();
		return bc.bounds;
	}

	private float getLeftBoundOfDrawingRegion() {
		const float DELTA = 0.2f;
		var bounds = getBoundsOfDrawingRegion();
		var leftLowerBound = bounds.min;
		return leftLowerBound.x + DELTA;
	}

	private float getUpperBoundOfDrawingRegion() {
		const float DELTA = 0.2f;
		var upperRightBound = getUpperRightBoundOfDrawingRegion();
		return upperRightBound.y - DELTA;
	}

	private void retrieveLastPointFrom(Vector2 point) {
		lastPoint = point;
	}

	private void markThatTrailIsWaitingForItsCreation() {
		trailIsConstructed = false;
	}

	private void startThePath() {
		path.Add(firstPoint);
	}

	private void endThePath() {
		var point = getAdjustedPointFrom(lastPoint);
		path.Add(point);
	}

	private void convertPathIntoTrail() {
		trailCollider.points = path.ToArray();
	}

	public void endTrail(Vector2 point) {
		addToTrail(point);
	}

	public void addToTrail(Vector2 point) {
		retrieveLastPointFrom(point);
		updateTrailIfItIsPossible();
	}

	private void updateTrailIfItIsPossible() {
		if (lastPointIsInDrawingRegion()) {
			updateTrail();
		}
	}

	private bool lastPointIsInDrawingRegion() {
		var bc = GetComponent<BoxCollider2D>();
		return bc.OverlapPoint(lastPoint);
	}

	private void updateTrail() {
		
//		clearThePath();
//		startThePath();

		completeThePath();
		updatePathIfItIsNecessary();

		convertPathIntoTrail();//comment
//		b=true; //decomment
	}

	private void completeThePath() {
		if (trailHasBeenCreatedBefore()) {
			endPathForTrailRecreation();
		} else if (trailIsGoingToBeCreated()) {
			endPathForTrailCreation();
		} else {
			endThePath();
		}
	}

	private bool trailHasBeenCreatedBefore() {
		return trailIsConstructed;
	}

	private void endPathForTrailRecreation() {
		
		var hitIsFound = false;
		var colliders = Physics2D.OverlapAreaAll (path[0], path[path.Count-1]);
		for (int i = 0; i < colliders.Length; ++i) {
			if (colliders [i].tag.Equals ("Ball")) {
				hitIsFound = true;

			}
		}

		if (hitIsFound) {
		} else {

			if (pathCompletionIsPossible ()) {
				//		var hits = Physics2D.GetRayIntersectionAll (new Ray (firstPoint, point - firstPoint),
				//			Vector2.Distance (firstPoint, point));



				//		for (int i = 0; i < hits.Length; ++i) {
				//			if (hits [i].transform.tag.Equals ("Ball")) {
				//				//Debug.Log ("Collider: " + hits [i].transform.tag + " " + hits.Length);
				//				hitIsFound = true;
				//				hitIndex = i;
				//			}
				//		}

				//		var colliders = Physics2D.OverlapAreaAll (firstPoint, point);
				//		for (int i = 0; i < colliders.Length; ++i) {
				//			if (colliders [i].tag.Equals ("Ball")) {
				//				Debug.Log ("Collider: " + colliders [i].tag + " " + colliders.Length);
				//
				//				colliders[i].transform.position.
				//			}
				//		}

				//		if (hitIsFound) {
				//			lastPoint = hits [hitIndex].point;
				//		} else {
				//			lastPoint = point;
				//		}

				var hitIndex = 0;
				colliders = Physics2D.OverlapAreaAll (firstPoint, getAdjustedPointFrom (lastPoint));
				for (int i = 0; i < colliders.Length; ++i) {
					if (colliders [i].tag.Equals ("Ball")) {
						hitIsFound = true;

					}
				}

				if (hitIsFound) {
					////			var hits = Physics2D.GetRayIntersectionAll (new Ray (firstPoint, point - firstPoint),
					////				           Vector2.Distance (firstPoint, point));
					//			var direction = point - firstPoint;
					//			var size = direction / 2f;
					//			var origin = firstPoint + size;
					//			var hits = Physics2D.BoxCastAll (origin, size, 0f, direction);
					//
					//			var hitIndex = 0;
					//			for (int i = 0; i < hits.Length; ++i) {
					//				if (hits [i].transform.tag.Equals ("Ball")) {
					//					hitIndex = i;
					//				}
					//			}
					//
					//			lastPoint = hits [hitIndex].point;
					//lastPoint = colliders[hitIndex].transform.position;
				} else {
					//lastPoint = point;
					clearThePath ();
					startThePath ();
					endThePath ();
				}

				//endThePath();
			} else if (futureTrailWillBeOutOfBounds ()) {
				//		var hits = Physics2D.GetRayIntersectionAll (new Ray (firstPoint, point - firstPoint),
				//			Vector2.Distance (firstPoint, point));


				//		for (int i = 0; i < hits.Length; ++i) {
				//			if (hits [i].transform.tag.Equals ("Ball")) {
				//				//Debug.Log ("Collider: " + hits [i].transform.tag + " " + hits.Length);
				//				hitIsFound = true;
				//				hitIndex = i;
				//			}
				//		}

				//		var colliders = Physics2D.OverlapAreaAll (firstPoint, point);
				//		for (int i = 0; i < colliders.Length; ++i) {
				//			if (colliders [i].tag.Equals ("Ball")) {
				//				Debug.Log ("Collider: " + colliders [i].tag + " " + colliders.Length);
				//
				//				colliders[i].transform.position.
				//			}
				//		}

				//		if (hitIsFound) {
				//			lastPoint = hits [hitIndex].point;
				//		} else {
				//			lastPoint = point;
				//		}

				var x = getHorizontalPositionForPassableTrailEnd ();
				var y = getVerticalPositionForPassableTrailEnd ();
				var trailEnd = new Vector2 (x, y);
				var adjustedTrailEnd = getAdjustedPointFrom (trailEnd);

				var hitIndex = 0;
				colliders = Physics2D.OverlapAreaAll (firstPoint, adjustedTrailEnd);
				for (int i = 0; i < colliders.Length; ++i) {
					if (colliders [i].tag.Equals ("Ball")) {
						hitIsFound = true;

					}
				}

				if (hitIsFound) {
					////			var hits = Physics2D.GetRayIntersectionAll (new Ray (firstPoint, point - firstPoint),
					////				           Vector2.Distance (firstPoint, point));
					//			var direction = point - firstPoint;
					//			var size = direction / 2f;
					//			var origin = firstPoint + size;
					//			var hits = Physics2D.BoxCastAll (origin, size, 0f, direction);
					//
					//			var hitIndex = 0;
					//			for (int i = 0; i < hits.Length; ++i) {
					//				if (hits [i].transform.tag.Equals ("Ball")) {
					//					hitIndex = i;
					//				}
					//			}
					//
					//			lastPoint = hits [hitIndex].point;
					//lastPoint = colliders[hitIndex].transform.position;
				} else {
					//lastPoint = point;
					clearThePath ();
					startThePath ();
					completePathWithPassableBounds ();
				}

//			completePathWithPassableBounds();
			}
		}

//		if (pathCompletionIsPossible()) {			
//			endThePath();
//		} else if (futureTrailWillBeOutOfBounds()) {			
//			completePathWithPassableBounds();
//		}		
	}

	private bool pathCompletionIsPossible() {
		bool ba = trailLengthIsAboveItsMinimum();
		bool bb = trailLengthIsBelowItsMaximum();
		return ba && bb;
	}

	private bool trailLengthIsAboveItsMinimum() {
		float length = getLengthOfTrail();
		return length > trailMinimalLength;
	}

	private float getLengthOfTrail() {
		Vector2 a = firstPoint;
		Vector2 b = lastPoint;
		return Vector2.Distance(a, b);
	}

	private bool trailLengthIsBelowItsMaximum() {
		var length = getLengthOfTrail();
		var maximum = getTrailLengthMaximum();
		return length < maximum;
	}

	private float getTrailLengthMaximum() {
		var maximalLength = getTrailMaximalLength();
		var multiplier = getTrailMaximalLengthMultiplier();
		return maximalLength * multiplier;
	}

	private float getTrailMaximalLength() {
		return trailMaximalLength;
	}

	private float getTrailMaximalLengthMultiplier() {
		return trailMaximalLengthMultiplier;
	}

	private bool futureTrailWillBeOutOfBounds() {
		bool ba = trailLengthHasReachedItsMinimum();
		bool bb = trailLengthHasReachedItsMaximum();
		return ba || bb;
	}
	
	private bool trailLengthHasReachedItsMinimum() {
		return !trailLengthIsAboveItsMinimum();
	}
	
	private bool trailLengthHasReachedItsMaximum() {
		return !trailLengthIsBelowItsMaximum();
	}

	private void completePathWithPassableBounds() {
		var x = getHorizontalPositionForPassableTrailEnd();
		var y = getVerticalPositionForPassableTrailEnd();
		var trailEnd = new Vector2(x, y);
		var adjustedTrailEnd = getAdjustedPointFrom(trailEnd);
		path.Add(adjustedTrailEnd);
	}

	private float getHorizontalPositionForPassableTrailEnd() {
		var r = getLengthForFutureTrail();
		var cos = getCosineOfAngleBetweenFutureTrailAndTrailToTheRight();
		return r * cos + firstPoint.x;
	}
	
	private float getLengthForFutureTrail() {		
		var b = trailLengthHasReachedItsMinimum();
		var minimum = getTrailMinimalLength();
		var maximum = getTrailLengthMaximum();
		return b ? minimum : maximum;
	}

	private float getTrailMinimalLength() {
		return trailMinimalLength;
	}

	private float getCosineOfAngleBetweenFutureTrailAndTrailToTheRight() {
		float alpha = getAngleBetweenFutureTrailAndTrailToTheRight();
		return Mathf.Cos(alpha);
	}

	private float getAngleBetweenFutureTrailAndTrailToTheRight() {
		var from = lastPoint - firstPoint;
		var to = Vector2.right;
		var angle = Vector2.Angle(from, to);
		return angle * Mathf.Deg2Rad;
	}

	private float getVerticalPositionForPassableTrailEnd() {
		var r = getLengthForFutureTrail();
		var sin = getSineOfAngleBetweenFutureTrailAndTrailToTheRight();
		return r * sin + firstPoint.y;
	}

	private float getSineOfAngleBetweenFutureTrailAndTrailToTheRight() {
		float alpha = getAngleBetweenFutureTrailAndTrailToTheRight();
		float sine = Mathf.Sin(alpha);
		float lastPointY = lastPoint.y;
		float firstPointY = firstPoint.y;
		float f = lastPointY - firstPointY;
		float sineSign = Mathf.Sign(f);
		return sineSign * sine;
	}

	private bool trailIsGoingToBeCreated() {
		lastPoint = getAdjustedPointFrom(lastPoint);
		return firstPoint != lastPoint;
	}

	private void endPathForTrailCreation() {
		endPathForTrailRecreation();
		markThatTrailIsCreated();
	}

	private void markThatTrailIsCreated() {
		trailIsConstructed = true;
	}

	private void updatePathIfItIsNecessary() {
		if (futureTrailWillBumpIntoTheItemWithinDrawingRegion()) {
			updatePath();
		}
	}

	private bool futureTrailWillBumpIntoTheItemWithinDrawingRegion() {
		var h = getHitBetweenFutureTrailAndItemWithinDrawingRegion();
		return h.collider != null;
	}

	private RaycastHit2D getHitBetweenFutureTrailAndItemWithinDrawingRegion() {
		const string LAYER_NAMES = "Items";
		const int FIRST_POINT_INDEX = 0;
		int lastPointIndex = path.Count - 1;
		var start = path[FIRST_POINT_INDEX];
		var end = path[lastPointIndex];
		var layerMask = LayerMask.GetMask(LAYER_NAMES);
		return Physics2D.Linecast(start, end, layerMask);
	}
	
	private void updatePath() {
		makeHitPointToBecomeLastPointOfThePath();
		clearThePath();
		startThePath();
		endThePath();
	}

	private void makeHitPointToBecomeLastPointOfThePath() {
		var h = getHitBetweenFutureTrailAndItemWithinDrawingRegion();
		lastPoint = h.point;
	}
}