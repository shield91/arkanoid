﻿using UnityEngine;

public abstract class UserInput : MonoBehaviour {
	public DrawingRegion region;

	protected void onInputDown() {
		if (playModeIsEnabled()) {
			startTrail();
		}
	}

	private bool playModeIsEnabled() {
		var sf = getSharedFunctionsScriptFromLevelObject();
		return sf.playModeIsEnabled();
	}

	private SharedFunctions getSharedFunctionsScriptFromLevelObject() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		return go.GetComponent<SharedFunctions>();
	}

	private void startTrail() {
		var position = getInputPosition();
		var worldPosition = projectInput(position);
		var zone = getDrawingRegion();
		zone.startTrail(worldPosition);
	}

	protected abstract Vector3 getInputPosition();

	private Vector2 projectInput(Vector3 inputPosition) {
		var sf = getSharedFunctionsScriptFromLevelObject();
		return sf.projectInput(inputPosition);
	}

	private DrawingRegion getDrawingRegion() {
		return region;
	}

	protected void onInputDrag() {
		if (playModeIsEnabled()) {
			continueTrail();
		}
	}

	private void continueTrail() {
		var inputPosition = getInputPosition();
		var point = projectInput(inputPosition);
		var zone = getDrawingRegion();
		zone.addToTrail(point);
	}

	protected void onInputUp() {
		if (playModeIsEnabled()) {
			endTrail();
		}
	}

	private void endTrail() {
		var inputPosition = getInputPosition();
		var point = projectInput(inputPosition);
		var zone = getDrawingRegion();
		zone.addToTrail(point);
	}
}