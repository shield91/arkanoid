using UnityEngine;
using System.Collections.Generic;

public class ItemMover : MonoBehaviour {
	private List<Dictionary<string, string>> route;
	private int currentMomentInTime;
	private int currentPointPosition;

	private void Start() {
		route = getRouteFromIndependentItemScript();
		currentMomentInTime = 0;
		currentPointPosition = 0;
	}

	private List<Dictionary<string, string>> getRouteFromIndependentItemScript() {
		var ii = GetComponent<IndependentItem>();
		return ii.getWaybillPoints();
	}

	private void Update() {
		if (itemIsInRest()) {
			leaveItemInRest();
		} else {
			leaveItemInMotion();
		}
	}

	private bool itemIsInRest() {
		const string KEY = "Time to next point";
		var i = currentMomentInTime;
		var j = currentPointPosition;
		var currentPoint = route[j];
		var s = currentPoint[KEY];
		var n = int.Parse(s);
		return i < n;
	}

	private void leaveItemInRest() {
		++currentMomentInTime;
	}

	private void leaveItemInMotion() {
		if (itemIsOnTheWayToItsDestination()) {
			continueItemMotion();
		} else {
			putItemInRest();
		}
	}

	private bool itemIsOnTheWayToItsDestination() {
		float fa = getDistanceBetweenCurrentPointAndItem();
		float fb = getDistanceBetweenCurrentAndNextPoints();
		return fa <= fb;
	}

	private float getDistanceBetweenCurrentPointAndItem() {
		var a = getPositionOfCurrentPoint();
		var b = getPositionOfItem();
		return Vector2.Distance(a, b);
	}

	private Vector2 getPositionOfCurrentPoint() {
		float x = getHorizontalPositionOfCurrentPoint();
		float y = getVerticalPositionOfCurrentPoint();
		return new Vector2(x, y);
	}

	private float getHorizontalPositionOfCurrentPoint() {
		int currentIndex = currentPointPosition;
		return getHorizontalPositionOfPoint(currentIndex);
	}
	
	private float getHorizontalPositionOfPoint(int index) {
		const string KEY = "x";
		var currentPoint = route[index];
		var s = currentPoint[KEY];
		return float.Parse(s);
	}

	private float getVerticalPositionOfCurrentPoint() {
		int currentIndex = currentPointPosition;
		return getVerticalPositionOfPoint(currentIndex);
	}
	
	private float getVerticalPositionOfPoint(int index) {
		const string KEY = "y";
		var currentPoint = route[index];
		var s = currentPoint[KEY];
		return float.Parse(s);
	}

	private Vector2 getPositionOfItem() {
		float x = getHorizontalPositionOfItem();
		float y = getVerticalPositionOfItem();
		return new Vector2(x, y);
	}

	private float getHorizontalPositionOfItem() {
		Vector2 position = transform.position;
		return position.x;
	}
	
	private float getVerticalPositionOfItem() {
		Vector2 position = transform.position;
		return position.y;
	}

	private float getDistanceBetweenCurrentAndNextPoints() {
		var a = getPositionOfCurrentPoint();
		var b = getPositionOfNextPoint();
		return Vector2.Distance(a, b);
	}

	private Vector2 getPositionOfNextPoint() {
		float x = getHorizontalPositionOfNextPoint();
		float y = getVerticalPositionOfNextPoint();
		return new Vector2(x, y);
	}

	private float getHorizontalPositionOfNextPoint() {
		int nextIndex = getNextPointPosition();
		return getHorizontalPositionOfPoint(nextIndex);
	}

	private int getNextPointPosition() {
		int i = currentPointPosition;
		int j = i + 1;
		int n = route.Count;
		return j % n;
	}

	private float getVerticalPositionOfNextPoint() {
		int nextIndex = getNextPointPosition();
		return getVerticalPositionOfPoint(nextIndex);
	}

	private void continueItemMotion() {
		const float STEPS = 100f;
		var currentX = getHorizontalPositionOfCurrentPoint();
		var nextX = getHorizontalPositionOfNextPoint();
		var currentY = getVerticalPositionOfCurrentPoint();
		var nextY = getVerticalPositionOfNextPoint();
		var oldPosition = transform.position;
		var oldX = oldPosition.x;
		var oldY = oldPosition.y;
		var newX = oldX + (nextX - currentX) / STEPS;
		var newY = oldY + (nextY - currentY) / STEPS;
		var newPosition = new Vector3(newX, newY);
		transform.position = newPosition;
	}

	private void putItemInRest() {
		putItemInLocationOfNextPoint();
		resetCurrentMomentInTime();
		setNewCurrentPointPosition();
	}

	private void putItemInLocationOfNextPoint() {
		float nextX = getHorizontalPositionOfNextPoint();
		float nextY = getVerticalPositionOfNextPoint();
		Vector3 newPosition = new Vector3(nextX, nextY);
		transform.position = newPosition;
	}

	private void resetCurrentMomentInTime() {
		currentMomentInTime = 0;
	}

	private void setNewCurrentPointPosition() {
		int i = getNextPointPosition();
		currentPointPosition = i;
	}
}