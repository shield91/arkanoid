﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemRoutePoint : MonoBehaviour {
	private bool dragModeIsEnabled;

	private void Awake() {
		disableDragMode();
	}

	private void disableDragMode() {
		var value = getValueForDragModeBeingDisabled();
		setDragModeBeingEnabled(value);
	}

	private bool getValueForDragModeBeingDisabled() {
		return false;
	}

	private void setDragModeBeingEnabled(bool value) {
		dragModeIsEnabled = value;
	}

	public void resize() {
		deleteOldBoxCollider();
		resizePoint();
		deleteBoxColliderIfItIsNecessary();
	}

	private void deleteOldBoxCollider() {
		BoxCollider2D obj = GetComponent<BoxCollider2D>();
		Destroy(obj);
	}

	private void resizePoint() {
		BoxCollider2D bc = gameObject.AddComponent<BoxCollider2D>();
		Transform background = getBackgroundForPoint();
		background.localPosition = bc.offset;
		background.localScale = bc.size;
	}

	private Transform getBackgroundForPoint() {
		const int INDEX = 0;
		return transform.GetChild(INDEX);
	}

	private void deleteBoxColliderIfItIsNecessary() {
		if (pointIsAnItemPosition()) {
			deleteOldBoxCollider();
		}
	}

	private bool pointIsAnItemPosition() {
		int i = getPointIndex();
		return i == 0;
	}

	public int getPointIndex() {
		TextMesh tm = GetComponent<TextMesh>();
		string s = tm.text;
		int i = int.Parse(s);
		return i - 1;
	}

	private void OnMouseDown() {

		  
//		enableDragMode();
		highlight();

	}

	private void enableDragMode() {
		var value = getValueForDragModeBeingEnabled();
		setDragModeBeingEnabled(value);
	}

	private bool getValueForDragModeBeingEnabled() {
		return true;
	}

	public void highlight() {
		if (pointIsUnhighlighted()) {
			highlightPoint();
		}
	}

	private bool pointIsUnhighlighted() {
		return tag != "Item Route Clicked Point";
	}

	private void highlightPoint() {
		makePointLookLikeItIsSelected();
		unhighlightAnotherClickedPoint();
		highlightThisPoint();
		loadPointData();

		  
		getInputFieldComponentFrom("Clicked Point Horizontal Position Field").
		onValueChanged.AddListener(delegate {changePositionOnX();});

		getInputFieldComponentFrom("Clicked Point Vertical Position Field").
		onValueChanged.AddListener(delegate {changePositionOnY();});

//		 getInputFieldComponentFrom ( ) . onValueChanged . 

	}

	private void makePointLookLikeItIsSelected() {
		const float Z = -6f;
		var position = transform.position;
		var x = position.x;
		var y = position.y;
		transform.position = new Vector3(x, y, Z);
	}

	private void unhighlightAnotherClickedPoint() {
		try {
			unhighlightAnotherPoint();
		} catch (Exception e) {
			handle(e);
		}
	}

	private void unhighlightAnotherPoint() {
		const string TAG = "Item Route Clicked Point";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var p = go.GetComponent<ItemRoutePoint>();
		p.unhighlight();
	}

	public void unhighlight() {
		tryToSavePointData();
		unhighlightPoint();
		makePointLookLikeItIsDeselected();
		disableDragMode();

		  
//		getInputFieldComponentFrom("Clicked Point Horizontal Position Field").
//		onValueChanged.RemoveListener(delegate {changePositionOnX();});
//
//		getInputFieldComponentFrom("Clicked Point Vertical Position Field").
//		onValueChanged.RemoveListener(delegate {changePositionOnY();});

		getInputFieldComponentFrom("Clicked Point Horizontal Position Field").
		onValueChanged.RemoveAllListeners();
//	 } ) 

		getInputFieldComponentFrom("Clicked Point Vertical Position Field").
		onValueChanged.RemoveAllListeners();

	}

	private void tryToSavePointData() {
		try {
			savePointData();
		} catch (Exception e) {
			handle(e);
		}
	}

	private void savePointData() {
		const string NAME_1 = "Clicked Point Horizontal Position Field";
		const string NAME_2 = "Clicked Point Vertical Position Field";
		const string NAME_3 = "Time To Next Point Field";
		const string KEY_1 = "x";
		const string KEY_2 = "y";
		const string KEY_3 = "Time to next point";
		int i = getPointIndex();
		var route = getRouteForClickedItem();
		var point = route[i];
		var ifa = getInputFieldComponentFrom(NAME_1);
		var ifb = getInputFieldComponentFrom(NAME_2);
		var ifc = getInputFieldComponentFrom(NAME_3);
		var b = getScriptForRouteMenu();
		point[KEY_1] = ifa.text;
		point[KEY_2] = ifb.text;
		point[KEY_3] = ifc.text;
		b.setRoute(route);
	}

	private List<Dictionary<string, string>> getRouteForClickedItem() {
		RouteMenuButtonsBehaviour b = getScriptForRouteMenu();
		return b.getRoute();
	}

	private RouteMenuButtonsBehaviour getScriptForRouteMenu() {
		var sf = getSharedFunctionsFromLevelObject();
		return sf.getRouteMenuButtonsBehaviourScript();
	}

	private SharedFunctions getSharedFunctionsFromLevelObject() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		return go.GetComponent<SharedFunctions>();
	}

	private InputField getInputFieldComponentFrom(string name) {
		var sf = getSharedFunctionsFromLevelObject();
		return sf.getInputFieldComponentFrom(name);
	}

	private void handle(Exception e) {
		string message = e.StackTrace;
		Debug.Log(message);
	}

	private void unhighlightPoint() {
		const string PROPERTY_NAME = "_Color";
		const float R = 139f / 255f;
		const float G = 0;
		const float B = 255f / 255f;
		Color color = new Color(R, G, B);
		Renderer r = GetComponent<Renderer>();
		Material m = r.material;
		tag = "Untagged";
		m.SetColor(PROPERTY_NAME, color);
	}

	private void makePointLookLikeItIsDeselected() {
		const float Z = -5f;
		var position = transform.position;
		var x = position.x;
		var y = position.y;
		transform.position = new Vector3(x, y, Z);
	}

	private void highlightThisPoint() {
		const string PROPERTY_NAME = "_Color";
		const float R = 19f / 255f;
		const float G = 69f / 255f;
		const float B = 130f / 255f;
		Color color = new Color(R, G, B);
		Renderer r = GetComponent<Renderer>();
		Material m = r.material;
		tag = "Item Route Clicked Point";
		m.SetColor(PROPERTY_NAME, color);
	}

	private void loadPointData() {
		loadPointDataAboutPositionOnX();
		loadPointDataAboutPositionOnY();
		loadPointDataAboutTimeToNextPoint();
	}

	private void loadPointDataAboutPositionOnX() {
		const string NAME = "Clicked Point Horizontal Position Field";
		const string HEADER = "x";
		var f = getInputFieldComponentFrom(NAME);
		f.text = getDataFor(HEADER);
	}

	private string getDataFor(string header) {
		Dictionary<string, string> data = getDataAboutPoint();
		return data[header];
	}

	private Dictionary<string, string> getDataAboutPoint() {
		List<Dictionary<string, string>> points = getRouteForClickedItem();
		int i = getPointIndex();
		return points[i];
	}

	private void loadPointDataAboutPositionOnY() {
		const string NAME = "Clicked Point Vertical Position Field";
		const string HEADER = "y";
		var f = getInputFieldComponentFrom(NAME);
		f.text = getDataFor(HEADER);
	}

	private void loadPointDataAboutTimeToNextPoint() {
		const string NAME = "Time To Next Point Field";
		const string HEADER = "Time to next point";
		var f = getInputFieldComponentFrom(NAME);
		f.text = getDataFor(HEADER);
	}

	private void OnMouseDrag() {
		if (pointDraggingIsPossible()) {
			changePointPosition();
		}
	}

	private bool pointDraggingIsPossible() {
		return dragModeIsEnabled;
	}

	private void changePointPosition() {
		movePointToInputPosition();
		putNewPositionInMenu();
	}

	private void movePointToInputPosition() {
		Vector3 position = Input.mousePosition;
		transform.position = projectInput(position);
	}

	private Vector3 projectInput(Vector3 inputPosition) {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var sf = go.GetComponent<SharedFunctions>();
		return sf.projectInput(inputPosition);
	}

	private void putNewPositionInMenu() {
		putNewHorizontalPositionInMenu();
		putNewVerticalPositionInMenu();
	}

	private void putNewHorizontalPositionInMenu() {
		const string NAME = "Clicked Point Horizontal Position Field";
		var f = getInputFieldComponentFrom(NAME);
		var position = transform.position;
		f.text = position.x + "";
	}

	private void putNewVerticalPositionInMenu() {
		const string NAME = "Clicked Point Vertical Position Field";
		var f = getInputFieldComponentFrom(NAME);
		var position = transform.position;
		f.text = position.y + "";
	}

	private void OnMouseUp() {

		enableDragMode();

//		disableDragMode();

	}

	  
	public void changePositionOnX() {

//		transform.position.x = float.Parse (
//			getInputFieldComponentFrom ("Clicked Point Horizontal Position Field").
//			text);

		try {

			if (pointIsAnItemPosition()) {

				var sit = GameObject.FindWithTag("Level").
					GetComponent<RouteMenuButtonsBehaviour>().
					getSelectedItem().transform;

				sit.position = new Vector3(float.Parse(
					getInputFieldComponentFrom(
						"Clicked Point Horizontal Position Field").text),
					sit.position.y, sit.position.z);

			}

			transform.position = new Vector3(float.Parse(
				getInputFieldComponentFrom(
					"Clicked Point Horizontal Position Field").text),
				transform.position.y, transform.position.z);

		} catch ( Exception e ) {

		}

//		transform.position = new Vector3 (float.Parse(
//			getInputFieldComponentFrom("Clicked Point Horizontal Position Field").
//			text), transform.position.y, transform.position.z);
		// ) new Vector2 ( float . Parse ( ) )

	}

	  
	public void changePositionOnY() {

		try {

			if (pointIsAnItemPosition()) {

				var sit = GameObject.FindWithTag("Level").
					GetComponent<RouteMenuButtonsBehaviour>().
					getSelectedItem().transform;

				sit.position = new Vector3(sit.position.x, float.Parse(
					getInputFieldComponentFrom(
						"Clicked Point Vertical Position Field").text),
					sit.position.z);

			}

			transform.position = new Vector3(transform.position.x, float.
				Parse(getInputFieldComponentFrom(
					"Clicked Point Vertical Position Field").text),
				transform.position.z);

		} catch (Exception e) {

		} // v 

//		transform.position = new Vector3(transform.position.x,
//			float.Parse(getInputFieldComponentFrom(
//				"Clicked Point Vertical Position Field").text),
//			transform.position.z);
//		getInputFieldComponentFrom ( " Clicked Point Vertical Position Field " ) )

	}

}