﻿using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour {
	public ParticleSystem sparks;
	public Vector2 v;
	public float velocity;
	private Collision2D collision;
	private int ballHitPower;

	private void Start() {
		resetPrivateFields();
		adjustRigidBodyComponent();
		setUpEnability();
	}

	private void resetPrivateFields() {
		resetCollision();
		resetBallHitPower();
	}

	private void resetCollision() {
		var value = getValueForCollisionByDefault();
		setCollision(value);
	}

	private Collision2D getValueForCollisionByDefault() {
		return null;
	}

	private void setCollision (Collision2D value) {
		collision = value;
	}

	private void resetBallHitPower() {
		int value = getValueForBallHitPowerByDefault();
		setBallHitPower(value);
	}

	private int getValueForBallHitPowerByDefault() {
		return 1;
	}

	public void setBallHitPower(int value) {
		ballHitPower = value;
	}

	private void adjustRigidBodyComponent() {
		if (levelEditorModeIsEnabled()) {
			deleteRigidBodyComponent();
		}
	}

	private bool levelEditorModeIsEnabled() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var sf = go.GetComponent<SharedFunctions>();
		return sf.levelEditorModeIsEnabled();
	}

	private void deleteRigidBodyComponent() {
		var obj = getRigidBodyComponentFromBall();
		Destroy(obj);
	}

	private Rigidbody2D getRigidBodyComponentFromBall() {
		return GetComponent<Rigidbody2D>();
	}

	private void setUpEnability() {
		var value = getValueForEnability();
		enabled = value;
	}

	private bool getValueForEnability() {
		return levelEditorModeIsEnabled();
	}

	private void Update() {
		if (playModeIsEnabled()) {
			doWorksWithBall();
		}
	}

	private bool playModeIsEnabled() {
		return !levelEditorModeIsEnabled();
	}

	private void doWorksWithBall() {
		updateVelocity();
		determineBallFateIfItHasGoneBelowDrawingRegion();
	}

	private void updateVelocity() {
		modifyVelocity();
		updateVectorV();
		adjustVelocity();
	}

	private void modifyVelocity() {
		var rb = getRigidBodyComponentFromBall();
		var value = getNewVelocityForBall();
		rb.velocity = value;
	}

	private Vector2 getNewVelocityForBall() {
		var v = getNormalizedOldVelocityFromBall();
		var f = getVelocity();
		return v * f;
	}

	private Vector2 getNormalizedOldVelocityFromBall() {
		var v = getOldVelocityFromBall();
		return v.normalized;
	}

	private Vector2 getOldVelocityFromBall() {
		var rb = getRigidBodyComponentFromBall();
		return rb.velocity;
	}

	private float getVelocity() {
		return velocity;
	}

	private void updateVectorV() {
		var value = getValueForVectorV();
		setV(value);
	}

	private Vector2 getValueForVectorV() {
		return getOldVelocityFromBall();
	}

	private void setV(Vector2 value) {
		v = value;
	}

	private void adjustVelocity() {
		if (ballIsGoingToStop()) {
			resetBallMovement();
		}
	}

	private bool ballIsGoingToStop() {
		var magnitude = getMagnitudeFromOldVelocityOfBall();
		var value = getValueForBallGoingToStop();
		return  magnitude <= value;
	}

	private float getMagnitudeFromOldVelocityOfBall() {
		var v = getOldVelocityFromBall();
		return v.magnitude;
	}

	private float getValueForBallGoingToStop() {
		return 0.1f;
	}

	private void resetBallMovement() {
		launch();
	}

	public void launch() {
		var rb = getRigidBodyComponentFromBall();
		var value = getInitialVelocityForBall();
		rb.velocity = value;
	}

	private Vector2 getInitialVelocityForBall() {
		var x = getInitialHorizontalVelocityForBall();
		var y = getInitialVerticalVelocityForBall();
		return new Vector2(x, y);
	}

	private float getInitialHorizontalVelocityForBall() {
		var v = getVelocity();
		var d = getDirectionOfInitialHorizontalVelocityForBall();
		return v * d;
	}

	private float getDirectionOfInitialHorizontalVelocityForBall() {
		var f = getValueForDirectionOfInitialHorizontalVelocityForBall();
		return Mathf.Sign(f);
	}

	private float getValueForDirectionOfInitialHorizontalVelocityForBall() {
		int min = getMinimumForDirectionOfInitialHorizontalVelocityForBall();
		int max = getMaximumForDirectionOfInitialHorizontalVelocityForBall();
		return Random.Range(min, max);
	}

	private int getMinimumForDirectionOfInitialHorizontalVelocityForBall() {
		return -2;
	}

	private int getMaximumForDirectionOfInitialHorizontalVelocityForBall() {
		return 2;
	}

	private float getInitialVerticalVelocityForBall() {
		return getInitialHorizontalVelocityForBall();
	}

	private void determineBallFateIfItHasGoneBelowDrawingRegion() {
		if (ballHasGoneBelowDrawingRegion()) {
			determineBallFate();
		}
	}

	private bool ballHasGoneBelowDrawingRegion() {
		var y = getVerticalPositionFromBall();
		var value = getValueForDrawingRegionLowerLimit();
		return y <= value;
	}

	private float getVerticalPositionFromBall() {
		var p = getPositionFromBall();
		return p.y;
	}

	private Vector3 getPositionFromBall() {
		return transform.position;
	}

	private float getValueForDrawingRegionLowerLimit() {
		return -5.4f;
	}

	private void determineBallFate() {
		if (itIsLastBallOnStage()) {
			determineLastBallOnStageFate();
		} else {
			destroyBall();
		}
	}

	private bool itIsLastBallOnStage() {
		int n = getQuantityOfBallsOnStage();
		int m = getValueForBallBeingLastOnStage();
		return n == m;
	}

	private int getQuantityOfBallsOnStage() {
		var b = getAllBallsOnStage();
		return b.Length;
	}

	private GameObject[] getAllBallsOnStage() {
		var sf = getSharedFunctionsComponentFromLevelTemplate();
		return sf.getBalls();
	}

	private SharedFunctions getSharedFunctionsComponentFromLevelTemplate() {
		var levelTemplate = getLevelTemplateForThisScreen();
		return levelTemplate.GetComponentInChildren<SharedFunctions>();
	}

	private Transform getLevelTemplateForThisScreen() {
		var level = getTransformForLevel();
		return level.parent;
	}

	private Transform getTransformForLevel() {
		var level = getGameObjectForLevel();
		return level.transform;
	}

	private GameObject getGameObjectForLevel() {
		var tag = getTagForLevel();
		return GameObject.FindWithTag(tag);
	}

	private string getTagForLevel() {
		return "Level";
	}

	private int getValueForBallBeingLastOnStage() {
		return 1;
	}

	private void determineLastBallOnStageFate() {
		if (itIsLastBallFromReserve()) {
			destroyBall();
		} else {
			determineBallFromReserveFate();
		}
	}

	private bool itIsLastBallFromReserve() {
		int n = getQuantityOfBallsInReserve();
		int m = getValueForBallBeingLastInReserve();
		return n == m;
	}

	private int getQuantityOfBallsInReserve() {
		var s = getQuantityOfBallsInReserveStringRepresentation();
		return int.Parse(s);
	}

	private string getQuantityOfBallsInReserveStringRepresentation() {
		var t = getTextComponentForBallsCounter();
		return t.text;
	}

	private Text getTextComponentForBallsCounter() {
		var ballsCounter = getBallsCounterOnThisScreen();
		return ballsCounter.GetComponent<Text>();
	}

	private Transform getBallsCounterOnThisScreen() {
		var name = getNameForBallsCounter();
		var levelTemplate = getLevelTemplateForThisScreen();
		return levelTemplate.Find(name);
	}

	private string getNameForBallsCounter() {
		return "Canvas/Balls/Value";
	}

	private int getValueForBallBeingLastInReserve() {
		return 1;
	}

	public void destroyBall() {
		changeBallTag();
		checkIfThereAreOtherBallsLeftOnLevel();
		destroyBallItself();
	}

	private void changeBallTag() {
		tag = "Untagged";
	}

	private void checkIfThereAreOtherBallsLeftOnLevel() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var ps = go.GetComponent<PlayScreen>();
		ps.onBallDestroyed();
	}

	private void destroyBallItself() {
		var obj = gameObject;
		Destroy(obj);
	}

	private void determineBallFromReserveFate() {
		updateQuantityOfBallsInReserve();
		putBallAtInitialPosition();
		launch();
	}

	private void updateQuantityOfBallsInReserve() {
		var t = getTextComponentForBallsCounter();
		t.text = getUpdatedValueForQuantityOfBallsInReserve();
	}

	private string getUpdatedValueForQuantityOfBallsInReserve() {
		int value = getIntegerRepresentationOfUpdatedValueForQuantityOfBallsInReserve();
		return value.ToString();
	}

	private int getIntegerRepresentationOfUpdatedValueForQuantityOfBallsInReserve() {
		int value = getQuantityOfBallsInReserve();
		int decrement = getDecrementForUpdatingQuantityOfBallsInReserve();
		return value - decrement;
	}

	private int getDecrementForUpdatingQuantityOfBallsInReserve() {
		return 1;
	}

	private void putBallAtInitialPosition() {
		var value = getValueForInitialPosition();
		transform.localPosition = value;
	}

	private Vector3 getValueForInitialPosition() {
		return Vector3.zero;
	}

	private void OnCollisionEnter2D(Collision2D c) {
		initializeCollisionFrom(c);
		createSparksIfItIsPossible();
		takeMeasuresIfBallHasGoneOutOfStage();
		processCollision();
	}

	private void initializeCollisionFrom(Collision2D c) {
		var value = c;
		setCollision(value);
	}

	private void createSparksIfItIsPossible() {
		if (sparksCreatingIsPossible()) {
			createSparks();
		}
	}

	private bool sparksCreatingIsPossible() {
		return true;//collisionIsOccurredWithDestroyable();
	}

	private bool collisionIsOccurredWithDestroyable() {
		var d = getDestroyableComponentFromCollision();
		var value = getValueForDestroyableComponentBeingAbsent();
		return d != value;
	}

	private Destroyable getDestroyableComponentFromCollision() {
		var c = getOtherColliderFromCollision();
		return c.GetComponent<Destroyable>();
	}

	private Collider2D getOtherColliderFromCollision() {
		var c = getFirstContactFromCollision();
		return c.otherCollider;
	}

	private ContactPoint2D getFirstContactFromCollision() {
		var c = getContactsFromCollision();
		var index = getIndexOfFirstContact();
		return c[index];
	}

	private ContactPoint2D[] getContactsFromCollision() {
		var c = getCollision();
		return c.contacts;
	}

	private Collision2D getCollision() {
		return collision;
	}

	private int getIndexOfFirstContact() {
		return 0;
	}

	private Destroyable getValueForDestroyableComponentBeingAbsent() {
		return null;
	}

	private void createSparks() {
		var obj = getInstantiatedSparks();
		var t = getTimeForSparks();
		Destroy(obj, t);
	}

	private Object getInstantiatedSparks() {
		var original = getOriginalForSparks();
		var position = getPositionForSparks();
		var rotation = getRotationForSparks();
		return Instantiate(original, position, rotation);
	}

	private Object getOriginalForSparks() {
		return getSparks();
	}

	private ParticleSystem getSparks() {
		return sparks;
	}

	private Vector3 getPositionForSparks() {
		var x = getPositionForSparksOnX();
		var y = getPositionForSparksOnY();
		var z = getPositionForSparksOnZ();
		return new Vector3(x, y, z);
	}

	private float getPositionForSparksOnX() {
		var p = getPointFromCollision();
		return p.x;
	}

	private Vector2 getPointFromCollision() {
		var c = getFirstContactFromCollision();
		return c.point;
	}

	private float getPositionForSparksOnY() {
		var p = getPointFromCollision();
		return p.y;
	}

	private float getPositionForSparksOnZ() {
		return 0;
	}

	private Quaternion getRotationForSparks() {
		return Quaternion.identity;
	}

	private float getTimeForSparks() {
		return 0.1f;
	}

	private void takeMeasuresIfBallHasGoneOutOfStage() {
		if (ballHasGoneOutOfStage()) {
			returnBallInsideStage();
		}
	}

	private bool ballHasGoneOutOfStage() {
		const string VALUE = "Stage";
		var c = collision.collider;
		string label = c.tag;
		bool ba = label.Equals(VALUE);
		bool bb = ballHasGoneThroughTheLeftWall();
		bool bc = ballHasGoneThroughTheRightWall();
		bool bd = ballHasGoneThroughTheCeiling();
		return ba && (bb || bc || bd);
	}

	private bool ballHasGoneThroughTheLeftWall() {
		const float LEFT_WALL = -4.8f;
		var position = transform.position;
		float x = position.x;
		return x <= LEFT_WALL;
	}

	private bool ballHasGoneThroughTheRightWall() {
		const float RIGHT_WALL = 4.9f;
		var position = transform.position;
		float x = position.x;
		return x >= RIGHT_WALL;
	}

	private bool ballHasGoneThroughTheCeiling() {
		const float CEILING = 4.9f;
		var position = transform.position;
		float y = position.y;
		return y >= CEILING;
	}

	private void returnBallInsideStage() {
		putBallInsideStage();
		launch();
	}

	private void putBallInsideStage() {
		if (ballHasGoneThroughTheLeftWall()) {
			putBallAtTheLeftWallInsideStage();
		} else if (ballHasGoneThroughTheRightWall()) {
			putBallAtTheRightWallInsideStage();
		} else {
			putBallAtTheCeilingInsideStage();
		}
	}

	private void putBallAtTheLeftWallInsideStage() {
		const float X = -4.7f;
		var position = transform.position;
		float y = position.y;
		transform.position = new Vector2(X, y);
	}

	private void putBallAtTheRightWallInsideStage() {
		const float X = 4.8f;
		var position = transform.position;
		float y = position.y;
		transform.position = new Vector2(X, y);
	}

	private void putBallAtTheCeilingInsideStage() {
		const float Y = 4.8f;
		var position = transform.position;
		float x = position.x;
		transform.position = new Vector2(x, Y);
	}

	private void processCollision() {
		if (ballIsGoingToBounceOffTheStageInHorizontalDirection()) {
			adjustBallVelocity();
		}
	}

	private bool ballIsGoingToBounceOffTheStageInHorizontalDirection() {
		var ba = ballIsGoingToBounceOffTheStage();
		var bb = ballIsGoingToBounceOffInHorizontalDirection();
		return ba && bb;
	}

	private bool ballIsGoingToBounceOffTheStage() {
		var name = getNameFromCollision();
		var value = getNameForStage();
		return name.Equals(value);
	}

	private string getNameFromCollision() {
		var c = getColliderFromCollision();
		return c.name;
	}

	private Collider2D getColliderFromCollision() {
		var c = getCollision();
		return c.collider;
	}

	private string getNameForStage() {
		return "Stage";
	}

	private bool ballIsGoingToBounceOffInHorizontalDirection() {
		var f = getAbsoluteValueOfBallVelocityOnY();
		var value = getAbsoluteValueOfHorizontalThreshold();
		return f < value;
	}

	private float getAbsoluteValueOfBallVelocityOnY() {
		var f = getVelocityOfBallOnY();
		return Mathf.Abs(f);
	}

	private float getVelocityOfBallOnY() {
		var v = getVelocityOfBall();
		return v.y;
	}

	private Vector2 getVelocityOfBall() {
		var rb = getRigidBodyComponentFromBall();
		return rb.velocity;
	}

	private float getAbsoluteValueOfHorizontalThreshold() {
		var f = getThresholdOfHorizontalMovement();
		return Mathf.Abs(f);
	}

	private float getThresholdOfHorizontalMovement() {
		var f = getAngleForHorizontalThresholdInRadians();
		return Mathf.Sin(f);
	}

	private float getAngleForHorizontalThresholdInRadians() {
		var fa = getConstantOfDegreesToRadiansConversion();
		var fb = getAngleForHorizontalThresholdInDegrees();
		return fa * fb;
	}

	private float getConstantOfDegreesToRadiansConversion() {
		return Mathf.Deg2Rad;
	}

	private float getAngleForHorizontalThresholdInDegrees() {
		return 5f;
	}

	private void adjustBallVelocity() {
		var rb = getRigidBodyComponentFromBall();
		var value = getAdjustedVelocityForBall();
		rb.velocity = value;
	}

	private Vector2 getAdjustedVelocityForBall() {
		var x = getAdjustedVelocityForBallOnX();
		var y = getAdjustedVelocityForBallOnY();
		return new Vector2(x, y);
	}

	private float getAdjustedVelocityForBallOnX() {
		var v = getVelocityOfBall();
		return v.x;
	}

	private float getAdjustedVelocityForBallOnY() {
		var fa = getThresholdOfHorizontalMovement();
		var fb = getDirectionOfVerticalVelocityForBall();
		return fa * fb;
	}

	private float getDirectionOfVerticalVelocityForBall() {
		var f = getVelocityOfBallOnY();
		return Mathf.Sign(f);
	}
	
	public int getBallHitPower() {
		return ballHitPower;
	}
}