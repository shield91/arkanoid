using UnityEngine;
using System;
using System.Xml;

namespace AssemblyCSharp 
{
	public class LevelLoader {
		public XmlDocument getLoadedLevel() {
			if (levelEditorModeIsEnabled()) {
				return getLevelForLevelEditorMode();
			} else {
				return getLevelForPlayMode();
			}
		}

		private bool levelEditorModeIsEnabled() {
			const string TAG = "Level";
			var go = GameObject.FindGameObjectWithTag(TAG);
			var sf = go.GetComponent<SharedFunctions>();
			return sf.levelEditorModeIsEnabled();
		}

		private XmlDocument getLevelForLevelEditorMode() {
			try {
				return getSavedLevelForLevelEditorMode();
			} catch (Exception e) {
				return getEmptyLevelForLevelEditorModeBecauseOf(e);
			}
		}

		private XmlDocument getSavedLevelForLevelEditorMode() {
			const string KEY = "Loaded level number";
			const string A = "\\Level ";
			const string B = ".xml";
			string saveLocation = getSaveLocationFromPlayerPreferences();
			int levelNumber = PlayerPrefs.GetInt(KEY);
			string filename = saveLocation + A + levelNumber + B;
			XmlDocument level = new XmlDocument();
			level.Load(filename);
			return level;
		}
		
		private string getSaveLocationFromPlayerPreferences() {
			const string KEY = "Save location";
			const string DEFAULT_VALUE = "C:\\Arkanoid levels";
			return PlayerPrefs.GetString(KEY, DEFAULT_VALUE);
		}

		private XmlDocument getEmptyLevelForLevelEditorModeBecauseOf(Exception e) {
			string message = e.StackTrace;
			Debug.Log(message);
			return new XmlDocument();
		}
		
		private XmlDocument getLevelForPlayMode() {
			const string KEY = "Loaded level number";
			const string A = "Levels/Level ";
			int levelNumber = PlayerPrefs.GetInt(KEY);
			string path = A + levelNumber;
			Type systemTypeInstance = typeof(TextAsset);
			UnityEngine.Object o = Resources.Load(path, systemTypeInstance);
			TextAsset ta = (TextAsset)o;
			XmlDocument level = new XmlDocument();
			string xml = ta.text;
			level.LoadXml(xml);
			return level;
		}
	}
}