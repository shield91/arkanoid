﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class Campaign {
		[XmlAttribute("Name")]
		public string name {get; set;}
		[XmlArray("Levels")]
		[XmlArrayItem("Level")]
		public List<Level> levels = new List<Level>();
	}
}