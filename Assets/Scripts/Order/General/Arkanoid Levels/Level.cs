﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class Level {
		[XmlAttribute("Number")]
		public int number {get; set;}
		[XmlAttribute("Cost")]
		public int cost {get; set;}
		[XmlArray("DestroyableItems")]
		[XmlArrayItem("DestroyableItem")]
		public List<DestroyableItem> destroyables = new List<DestroyableItem>();
		[XmlArray("UndestroyableItems")]
		[XmlArrayItem("UndestroyableItem")]
		public List<UndestroyableItem> undestroyables = new List<UndestroyableItem>();
		[XmlArray("Bonuses")]
		[XmlArrayItem("Gift")]
		public List<Gift> bonuses = new List<Gift>();
		[XmlArray("Routes")]
		[XmlArrayItem("Route")]
		public List<Route> routes = new List<Route>();
	}
}