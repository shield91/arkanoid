﻿using System.Xml.Serialization;

namespace AssemblyCSharp
{
	public class DestroyableItem {
		
		[XmlAttribute("Name")]
		public string name {get; set;}
		[XmlAttribute("X")]
		public float x {get; set;}
		[XmlAttribute("Y")]
		public float y {get; set;}
	}
}