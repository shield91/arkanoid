﻿using System.Xml.Serialization;

namespace AssemblyCSharp
{
	public class Gift {
		[XmlAttribute("Name")]
		public string name {get; set;}
		[XmlAttribute("ParentName")]
		public string parentName {get; set;}
	}
}