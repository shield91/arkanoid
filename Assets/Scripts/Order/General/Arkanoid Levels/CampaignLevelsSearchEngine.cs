﻿using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class CampaignLevelsSearchEngine {
		
		private bool chosenCampaignIsPresentInPlayerProgress;
		private int chosenCampaignIndex;
		//		з

		private bool levelIsPaid;
		private int wantedLevelNumber;
		private int currentCampaignIndex;
		private int currentLevelIndex;

		public CampaignLevelsSearchEngine(int targetLevelNumber) {
			setCalculationDataFrom(targetLevelNumber);
			findCampaignChosenLevel();
		}

		private void setCalculationDataFrom(int targetLevelNumber) {
			setWantedLevelNumber(targetLevelNumber);
			resetCalculationData();
		}

		private void setWantedLevelNumber(int targetLevelNumber) {
			wantedLevelNumber = targetLevelNumber;
		}

		private void resetCalculationData() {
			
			chosenCampaignIsPresentInPlayerProgress = false;
			chosenCampaignIndex = 0;

			resetDataAboutPaidLevel();
			resetCurrentCampaignIndex();
			resetCurrentLevelIndex();
		}

		private void resetDataAboutPaidLevel() {
			var value = getValueForUnpaidLevel();
			setDataAboutPaidLevel(value);
		}

		private bool getValueForUnpaidLevel() {
			return false;
		}

		private void setDataAboutPaidLevel(bool value) {
			levelIsPaid = value;
		}

		private void resetCurrentCampaignIndex() {
			int value = getValueForCurrentCampaignIndexByDefault();
			setCurrentCampaignIndex(value);
		}

		private int getValueForCurrentCampaignIndexByDefault() {
			return 0;
		}

		private void setCurrentCampaignIndex(int value) {
			currentCampaignIndex = value;
		}

		private void resetCurrentLevelIndex() {			
			int value = getValueForCurrentLevelIndexByDefault();
			setCurrentLevelIndex(value);
		}

		private int getValueForCurrentLevelIndexByDefault() {
			return 0;
		}

		private void setCurrentLevelIndex(int value) {
			currentLevelIndex = value;
		}

		private void findCampaignChosenLevel() {
			while (needInSearchIsStillPresent()) {
				continueSearchAmongCampaigns();
			}
		}

		private bool needInSearchIsStillPresent() {
			var a = thereAreCampaignsNeedToBeAnalyzed();
			var b = levelIsWanted();
			return a && b;
		}

		public bool thereAreCampaignsNeedToBeAnalyzed() {
			int i = getCurrentCampaignIndex();
			int n = getNumberOfCampaigns();
			return i < n;
		}

		private int getCurrentCampaignIndex() {
			return currentCampaignIndex;
		}

		private int getNumberOfCampaigns() {
			var campaigns = getCampaignsFromPlayerProgress();
			return campaigns.Count;
		}

		private List<PlayerProgressCampaign> getCampaignsFromPlayerProgress() {
			var playerProgress = getProgressOfPlayer();
			return playerProgress.campaigns;
		}

		public PlayerProgress getProgressOfPlayer() {
			var serializer = getSerializerForPlayerProgress();
			var stream = getStreamForPlayerProgress();
			var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
			stream.Close();
			return playerProgress;
		}

		public XmlSerializer getSerializerForPlayerProgress() {
			var type = typeof(PlayerProgress);
			return new XmlSerializer(type);
		}

		public Stream getStreamForPlayerProgress() {
			var path = getPathToPlayerProgress();
			var mode = getFileModeForStream();
			return new FileStream(path, mode);
		}

		private string getPathToPlayerProgress() {
			const string KEY = "Path To Player Progress";
			return PlayerPrefs.GetString(KEY);
		}

		private FileMode getFileModeForStream() {
			return FileMode.OpenOrCreate;
		}

		private bool levelIsWanted() {
			return levelIsUnpaid();
		}

		private bool levelIsUnpaid() {
			return !levelIsPurchased();
		}

		public bool levelIsPurchased() {
			return levelIsPaid;
		}

		private void continueSearchAmongCampaigns() {
			processCurrentCampaign();
			moveOnToTheNextCampaign();
		}

		private void processCurrentCampaign() {
			if (currentCampaignIsTheChosenOne()) {
				searchAmongLevels();
			}
		}

		private bool currentCampaignIsTheChosenOne() {
			var name = getCurrentCampaignNameFromPlayerProgress();
			var value = getCurrentCampaignNameFromLevelsSet();
			return name.Equals(value);
		}

		private string getCurrentCampaignNameFromPlayerProgress() {
			var campaign = getCurrentCampaignFromPlayerProgress();
			return campaign.name;
		}

		private PlayerProgressCampaign getCurrentCampaignFromPlayerProgress() {
			var campaigns = getCampaignsFromPlayerProgress();
			var index = getCurrentCampaignIndex();
			return campaigns[index];
		}

		public string getCurrentCampaignNameFromLevelsSet() {
			var campaign = getChosenCampaignFromLevelsSet();
			return campaign.name;
		}

		private Campaign getChosenCampaignFromLevelsSet() {
			var campaigns = getCampaignsFromLevelsSet();
			var index = getIndexOfChosenCampaign();
			return campaigns[index];
		}

		private List<Campaign> getCampaignsFromLevelsSet() {
			var levels = getSetOfLevels();
			return levels.campaigns;
		}

		private Levels getSetOfLevels() {
			var serializer = getSerializerForLevelsSet();
			var textReader = getTextReaderForLevelsSet();
			return serializer.Deserialize(textReader) as Levels;
		}

		private XmlSerializer getSerializerForLevelsSet() {
			var type = typeof(Levels);
			return new XmlSerializer(type);
		}

		private TextReader getTextReaderForLevelsSet() {
			var s = getStringForLevelsSet();
			return new StringReader(s);
		}

		private string getStringForLevelsSet() {
			const string KEY = "Levels";
			return PlayerPrefs.GetString(KEY);
		}

		private int getIndexOfChosenCampaign() {
			const string KEY = "Chosen Campaign Index";
			return PlayerPrefs.GetInt(KEY);
		}

		private void searchAmongLevels() {
			
			chosenCampaignIsPresentInPlayerProgress=true;
			chosenCampaignIndex = currentCampaignIndex;
			//ge
				//..

			while (needInSearchAmongLevelsIsStillPresent()) {
				continueSearchAmongLevels();
			}
		}

		private bool needInSearchAmongLevelsIsStillPresent() {
			var a = thereAreLevelsNeedToBeAnalyzed();
			var b = levelIsWanted();
			return a && b;
		}

		private bool thereAreLevelsNeedToBeAnalyzed() {
			int i = getCurrentLevelIndex();
			int n = getNumberOfLevels();
			return i < n;
		}

		private int getCurrentLevelIndex() {
			return currentLevelIndex;
		}

		private int getNumberOfLevels() {
			var levels = getLevelsOfCurrentCampaign();
			return levels.Count;
		}

		private List<PlayerProgressLevel> getLevelsOfCurrentCampaign() {
			var campaign = getCurrentCampaignFromPlayerProgress();
			return campaign.levels;
		}

		private void continueSearchAmongLevels() {
			updateDataAboutPaidLevel();
			moveOnToTheNextLevel();
		}

		private void updateDataAboutPaidLevel() {
			var value = getValueForUpdatingDataAboutPaidLevel();
			setDataAboutPaidLevel(value);
		}

		private bool getValueForUpdatingDataAboutPaidLevel() {
			int targetLevelNumber = getWantedLevelNumber();
			int currentLevelNumber = getNumberOfCurrentLevel();
			return targetLevelNumber == currentLevelNumber;
		}

		private int getWantedLevelNumber() {
			return wantedLevelNumber;
		}

		private int getNumberOfCurrentLevel() {
			var level = getCurrentLevelByItsIndex();
			return level.number;
		}

		private PlayerProgressLevel getCurrentLevelByItsIndex() {
			var levels = getLevelsOfCurrentCampaign();
			var index = getCurrentLevelIndex();
			return levels[index];
		}

		private void moveOnToTheNextLevel() {
			int value = getValueForMovingOnToTheNextLevel();
			setCurrentLevelIndex(value);
		}

		private int getValueForMovingOnToTheNextLevel() {
			int index = getCurrentLevelIndex();
			int increment = getIncrementForMovingOnToTheNextLevel();
			return index + increment;
		}

		private int getIncrementForMovingOnToTheNextLevel() {
			return 1;
		}

		private void moveOnToTheNextCampaign() {
			int value = getValueForMovingOnToTheNextCampaign();
			setCurrentCampaignIndex(value);
		}

		private  int getValueForMovingOnToTheNextCampaign() {
			int index = getCurrentCampaignIndex();
			int increment = getIncrementForMovingOnToTheNextCampaign();
			return index + increment;
		}

		private int getIncrementForMovingOnToTheNextCampaign() {
			return getIncrementForMovingOnToTheNextLevel();
		}

		public int getChosenCampaignIndex() {
			
			return chosenCampaignIndex;

//			int index = getCurrentCampaignIndex();
//			int decrement = getDecrementForRetrievingChosenCampaignIndexInPlayerProgress();
//			return index - decrement;

		}

		private int getDecrementForRetrievingChosenCampaignIndexInPlayerProgress() {
			return 1;
		}

		public int getChosenLevelIndex() {
			int index = getCurrentLevelIndex();
			int decrement = getDecrementForRetrievingChosenCampaignLevelIndexInPlayerProgress();
			return index - decrement;
		}

		private int getDecrementForRetrievingChosenCampaignLevelIndexInPlayerProgress() {
			return getDecrementForRetrievingChosenCampaignIndexInPlayerProgress();
		}


		public bool chosenCampaignIsAbsentInPlayerProgress() {
			return !chosenCampaignIsPresentInPlayerProgress;
		}
	}
}