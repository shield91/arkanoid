﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	[XmlRoot("Levels")]
	public class Levels	{
		[XmlArray("FreeLevels")]
		[XmlArrayItem("Level")]
		public List<Level> freeLevels = new List<Level>();
		[XmlArray("Campaigns")]
		[XmlArrayItem("Campaign")]
		public List<Campaign> campaigns = new List<Campaign>();
	}
}