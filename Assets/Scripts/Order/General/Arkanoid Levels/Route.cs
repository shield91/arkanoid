﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class Route {
		[XmlArray("Points")]
		[XmlArrayItem("Point")]
		public List<Point> points = new List<Point>();
	}
}