﻿using System.Xml.Serialization;

namespace AssemblyCSharp
{
	public class Point {
		//TODO to knowledge base
		[XmlAttribute("X")]
		public float x {get; set;}
		[XmlAttribute("Y")]
		public float y {get; set;}
		[XmlAttribute("TimeToNextPoint")]
		public int timeToNextPoint {get; set;}		
	}
}