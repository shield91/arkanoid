﻿using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class FreeLevelsSearchEngine	{
		private bool levelIsOpen;
		private int wantedLevelNumber;
		private int currentLevelIndex;

		public FreeLevelsSearchEngine(int targetLevelNumber) {
			setCalculationDataFrom(targetLevelNumber);
			findNextFreeLevel();
		}

		private void setCalculationDataFrom(int targetLevelNumber) {
			setWantedLevelNumber(targetLevelNumber);
			resetCalculationData();
		}

		private void setWantedLevelNumber(int targetLevelNumber) {
			wantedLevelNumber = targetLevelNumber;
		}

		private void resetCalculationData() {
			resetDataAboutOpenedLevel();
			resetCurrentLevelIndex();
		}

		private void resetDataAboutOpenedLevel() {
			var value = getValueForClosedLevel();
			setDataAboutOpenedLevel(value);
		}

		private bool getValueForClosedLevel() {
			return false;
		}

		private void setDataAboutOpenedLevel(bool value) {
			levelIsOpen = value;
		}

		private void resetCurrentLevelIndex() {
			int value = getValueForCurrentLevelIndexByDefault();
			setCurrentLevelIndex(value);
		}

		private int getValueForCurrentLevelIndexByDefault() {
			return 0;
		}

		private void setCurrentLevelIndex(int value) {
			currentLevelIndex = value;
		}

		private void findNextFreeLevel() {
			while(needInSearchIsStillPresent()) {
				continueSearch();
			}
		}

		private bool needInSearchIsStillPresent() {
			var a = thereAreLevelsNeedToBeAnalyzed();
			var b = levelIsWanted();
			return a && b;
		}

		private bool thereAreLevelsNeedToBeAnalyzed() {
			int i = getCurrentLevelIndex();
			int n = getNumberOfLevels();
			return i < n;
		}

		private int getCurrentLevelIndex() {
			return currentLevelIndex;
		}

		private int getNumberOfLevels() {
			var levels = getFreeLevelsFromPlayerProgress();
			return levels.Count;
		}

		private List<PlayerProgressLevel> getFreeLevelsFromPlayerProgress() {
			var playerProgress = getProgressOfPlayer();
			return playerProgress.freeLevels;
		}

		private PlayerProgress getProgressOfPlayer() {
			var serializer = getSerializerForPlayerProgress();
			var stream = getStreamForPlayerProgress();
			var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
			stream.Close();
			return playerProgress;
		}

		private XmlSerializer getSerializerForPlayerProgress() {
			var type = typeof(PlayerProgress);
			return new XmlSerializer(type);
		}

		private Stream getStreamForPlayerProgress() {
			var path = getPathToPlayerProgress();
			var mode = getFileModeForStream();
			return new FileStream(path, mode);
		}

		private string getPathToPlayerProgress() {
			const string KEY = "Path To Player Progress";
			return PlayerPrefs.GetString(KEY);
		}

		private FileMode getFileModeForStream() {
			return FileMode.OpenOrCreate;
		}

		private bool levelIsWanted() {
			return levelIsClosed();
		}

		private bool levelIsClosed() {
			return !levelIsOpened();
		}

		public bool levelIsOpened() {
			return levelIsOpen;
		}

		private void continueSearch() {
			updateDataAboutOpenedLevel();
			moveOnToTheNextLevel();
		}

		private void updateDataAboutOpenedLevel() {
			var value = getValueForUpdatingDataAboutOpenedLevel();
			setDataAboutOpenedLevel(value);
		}

		private bool getValueForUpdatingDataAboutOpenedLevel() {
			int targetLevelNumber = getWantedLevelNumber();
			int currentLevelNumber = getNumberOfCurrentLevel();
			return targetLevelNumber == currentLevelNumber;
		}

		private int getWantedLevelNumber() {
			return wantedLevelNumber;
		}

		private int getNumberOfCurrentLevel() {
			var level = getCurrentLevelByItsIndex();
			return level.number;
		}

		private PlayerProgressLevel getCurrentLevelByItsIndex() {
			var levels = getFreeLevelsFromPlayerProgress();
			var index = getCurrentLevelIndex();
			return levels[index];
		}

		private void moveOnToTheNextLevel() {
			int value = getValueForMovingOnToTheNextLevel();
			setCurrentLevelIndex(value);
		}

		private int getValueForMovingOnToTheNextLevel() {
			int index = getCurrentLevelIndex();
			int increment = getIncrementForMovingOnToTheNextLevel();
			return index + increment;
		}

		private int getIncrementForMovingOnToTheNextLevel() {
			return 1;
		}

		public int getChosenLevelIndex() {
			int index = getCurrentLevelIndex();
			int decrement = getDecrementForRetrievingChosenFreeLevelIndexInPlayerProgress();
			return index - decrement;
		}

		private int getDecrementForRetrievingChosenFreeLevelIndexInPlayerProgress() {
			return getIncrementForMovingOnToTheNextLevel();
		}
	}
}