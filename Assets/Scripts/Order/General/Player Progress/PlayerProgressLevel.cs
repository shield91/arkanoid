﻿using System.Xml.Serialization;

namespace AssemblyCSharp {
	public class PlayerProgressLevel {
		[XmlAttribute("Number")]
		public int number {get; set;}
		[XmlAttribute("TimesPlayed")]
		public int timesPlayed {get; set;}
	}
}