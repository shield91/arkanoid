﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	[XmlRoot("PlayerProgress")]
	public class PlayerProgress	{
		[XmlAttribute("BalanceInCoins")]
		public int balanceInCoins {get; set;}
		[XmlAttribute("GameTimeInSeconds")]
		public int gameTimeInSeconds {get; set;}
		[XmlAttribute("CoinsEarned")]
		public int coinsEarned {get; set;}
		[XmlArray("FreeLevels")]
		[XmlArrayItem("Level")]
		public List<PlayerProgressLevel> freeLevels {get; set;}
		[XmlElement("Survival")]
		public Survival survival = new Survival();
		[XmlArray("Campaigns")]
		[XmlArrayItem("Campaign")]
		public List<PlayerProgressCampaign> campaigns {get; set;}
	}
}