﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace AssemblyCSharp {
	public class PlayerProgressCampaign	{
		[XmlAttribute("Name")]
		public string name {get; set;}
		[XmlArray("Levels")]
		[XmlArrayItem("Level")]
		public List<PlayerProgressLevel> levels = new List<PlayerProgressLevel>();
	}
}