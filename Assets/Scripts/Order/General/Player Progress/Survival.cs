﻿using System.Xml.Serialization;

namespace AssemblyCSharp
{
	public class Survival {
		[XmlAttribute("TimesPlayed")]
		public int timesPlayed {get; set;}
	}
}