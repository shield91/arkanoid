﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CampaignButton : MonoBehaviour {
	public void goToCampaign() {
		saveCampaignIndex();
		loadLevelsScreen();
	}

	private void saveCampaignIndex() {
		var key = getKeyForSavingCampaignIndex();
		var value = getValueForSavingCampaignIndex();
		PlayerPrefs.SetInt(key, value);
	}

	private string getKeyForSavingCampaignIndex() {
		return "Chosen Campaign Index";
	}

	private int getValueForSavingCampaignIndex() {
		return transform.GetSiblingIndex();
	}

	private void loadLevelsScreen() {
		var sceneName = getSceneNameForLevelsScreen();
		SceneManager.LoadScene(sceneName);
	}

	private string getSceneNameForLevelsScreen() {
		return "LevelSelect";
	}
}