﻿using AssemblyCSharp;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

public class CampaignsScreen : MonoBehaviour {
	private GameObject currentCampaign;
	private int currentCampaignIndex;

	private void Start() {
		resetCalculationData();
		loadCampaigns();


		//setUpAddNewCampaignButtonVisibility
		transform.Find("Add New Campaign").gameObject
			.SetActive(PlayerPrefs.HasKey("Level Editor Mode"));
//		Chosen Cap

	}

	private void resetCalculationData() {
		resetCurrentCampaign();
		resetCurrentCampaignIndex();
	}

	private void resetCurrentCampaign() {
		var value = getDefaultValueForCurrentCampaign();
		setCurrentCampaign(value);
	}

	private GameObject getDefaultValueForCurrentCampaign() {
		return null;
	}

	private void setCurrentCampaign(GameObject value) {
		currentCampaign = value;
	}

	private void resetCurrentCampaignIndex() {
		int value = getDefaultValueForCurrentCampaignIndex();
		setCurrentCampaignIndex(value);
	}

	private int getDefaultValueForCurrentCampaignIndex() {
		return 0;
	}

	private void setCurrentCampaignIndex(int value) {
		currentCampaignIndex = value;
	}

	private void loadCampaigns() {
		while (thereAreCampaignsNeedToBeLoaded()) {
			processCurrentCampaign();
		}
	}

	private bool thereAreCampaignsNeedToBeLoaded() {
		int i = getCurrentCampaignIndex();
		int n = getNumberOfCampaigns();
		return i < n;
	}

	private int getCurrentCampaignIndex() {
		return currentCampaignIndex;
	}

	private int getNumberOfCampaigns() {
		var campaigns = getCampaignsFromLevelsSet();
		return campaigns.Count;
	}

	private List<Campaign> getCampaignsFromLevelsSet() {
		var levels = getSetOfLevels();
		return levels.campaigns;
	}

	private Levels getSetOfLevels() {
		
		//У скрипті MainMenu.cs провернути щось подібне: 
		//https://stackoverflow.com/questions/2407302/convert-xmldocument-to-string
		//(це аби можна було запихувати "Levels Set.xml" в string навіть в стані (режимі) редактора рівнів).
		// Подумати над онолв
//			..
		// подумати над оновленням "Levels Set.xml" лише в рядку (string) без потреби викликати
		// stream.
		// ||\\ююg
		//		if (levelEditorModeIsOn()) {
		//			return getSetOfLevelsInLevelEditorMode();
		//		} else {
		//			return getSetOfLevelsInPlayMode();
		//		}
		//levelEditorModeIsOn
		if (PlayerPrefs.HasKey ("Level Editor Mode")) {
			//getSetOfLevelsInLevelEditorMode
			var serializer = getSerializerForLevelsSet();
			var stream = new FileStream (PlayerPrefs.GetString ("Path To Levels"), FileMode.OpenOrCreate);
			var o = serializer.Deserialize (stream) as Levels;
			stream.Close();
			return o;

			//float
		} else {
			//getSetOfLevelsInPlayMode
			var serializer = getSerializerForLevelsSet();
			var textReader = getTextReaderForLevelsSet();
			return serializer.Deserialize(textReader) as Levels;	
		}

		//		var serializer = getSerializerForLevelsSet();
//		var textReader = getTextReaderForLevelsSet();
//		return serializer.Deserialize(textReader) as Levels;
	}

	private XmlSerializer getSerializerForLevelsSet() {
		var type = getTypeForLevelsSet();
		return new XmlSerializer(type);
	}

	private Type getTypeForLevelsSet() {
		return typeof(Levels);
	}

	private TextReader getTextReaderForLevelsSet() {
		var s = getStringForLevelsSet();
		return new StringReader(s);
	}

	private string getStringForLevelsSet() {
		var key = getKeyForLevelsSet();
		return PlayerPrefs.GetString(key);
	}

	private string getKeyForLevelsSet() {
		return "Levels";
	}

	private void processCurrentCampaign() {
		loadCurrentCampaign();
		moveOnToTheNextCampaign();
	}

	private void loadCurrentCampaign() {
		updateCurrentCampaign();
		setUpNameForUpdatedCurrentCampaign();
		setUpTextForUpdatedCurrentCampaign();
	}

	private void updateCurrentCampaign() {
		var value = getValueForUpdatingCurrentCampaign();
		setCurrentCampaign(value);
	}

	private GameObject getValueForUpdatingCurrentCampaign() {
		var original = getOriginalForUpdatingCurrentCampaign();
		var parent = getParentForUpdatingCurrentCampaign();
		return Instantiate(original, parent);
	}

	private GameObject getOriginalForUpdatingCurrentCampaign() {
		var o = getObjectForUpdatingCurrentCampaign();
		return (GameObject) o;
	}

	private UnityEngine.Object getObjectForUpdatingCurrentCampaign() {
		var path = getPathForUpdatingCurrentCampaign();
		return Resources.Load(path);
	}

	private string getPathForUpdatingCurrentCampaign() {
		return "Order/Campaign Button";
	}

	private Transform getParentForUpdatingCurrentCampaign() {
		var name = getParentNameForUpdatingCurrentCampaign();
		return transform.Find(name);
	}

	private string getParentNameForUpdatingCurrentCampaign() {
		return "Scroll Panel/Buttons";
	}

	private void setUpNameForUpdatedCurrentCampaign() {
		var go = getCurrentCampaign();
		var value = getNameForCurrentCampaign();
		go.name = value;
	}

	private GameObject getCurrentCampaign() {
		return currentCampaign;
	}

	private string getNameForCurrentCampaign() {
		
		try {
					var campaign = getCurrentCampaignFromLevelsSet();
					return campaign.name;
		} catch(Exception e) {
			return "Campaign " + getCurrentCampaign ().transform.parent.childCount;
//				getButtonForCurrentLevel ().transform.parent.childCount;
//			return "Campaign " + getCurrentCampaignIndex();
		}

		//		var campaign = getCurrentCampaignFromLevelsSet();
//		return campaign.name;
	}

	private Campaign getCurrentCampaignFromLevelsSet() {
		var campaigns = getCampaignsFromLevelsSet();
		var index = getIndexForCurrentCampaignFromLevelsSet();
		return campaigns[index];
	}

	private int getIndexForCurrentCampaignFromLevelsSet() {
		var t = getTransformFromCurrentCampaign();
		return t.GetSiblingIndex();
	}

	private Transform getTransformFromCurrentCampaign() {
		var go = getCurrentCampaign();
		return go.transform;
	}

	private void setUpTextForUpdatedCurrentCampaign() {
		var t = getTextComponentFromCurrentCampaign();
		var value = getValueForUpdatedCurrentCampaignText();
		t.text = value;
	}

	private Text getTextComponentFromCurrentCampaign() {
		var t = getTransformFromCurrentCampaign();
		return t.GetComponentInChildren<Text>();
	}

	private string getValueForUpdatedCurrentCampaignText() {
		var go = getCurrentCampaign();
		return go.name;
	}

	private void moveOnToTheNextCampaign() {
		int value = getValueForMovingOnToTheNextCampaign();
		setCurrentCampaignIndex(value);
	}

	private int getValueForMovingOnToTheNextCampaign() {
		int index = getCurrentCampaignIndex();
		int increment = getIncrementForMovingOnToTheNextCampaign();
		return index + increment;
	}

	private int getIncrementForMovingOnToTheNextCampaign() {
		return 1;
	}

	public void goToPreviousScreen() {
		var sceneName = getSceneNameForPreviousScreen();
		SceneManager.LoadScene(sceneName);
	}

	private string getSceneNameForPreviousScreen() {
		return "MainMenu";
	}


	public void addNewCampaign() {
		//loadNewCampaign
		setCurrentCampaignIndex(getNumberOfCampaigns());
		loadCurrentCampaign ();

		var serializer = getSerializerForLevelsSet();
		var levels = getSetOfLevels();
		var stream = new FileStream (PlayerPrefs.GetString ("Path To Levels"), FileMode.OpenOrCreate);
		var item = new Campaign ();
		item.name = getNameForCurrentCampaign ();
//			"Campaign " + getCurrentCampaignIndex();
		levels.campaigns.Add (item);
		serializer.Serialize (stream, levels);
		stream.Close ();
//		і

		//		..

//		є
		
	}
}