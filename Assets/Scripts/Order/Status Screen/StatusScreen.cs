﻿using UnityEngine;

using UnityEngine.UI;
using UnityEngine.SceneManagement;
using AssemblyCSharp;
using System;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

public class StatusScreen : MonoBehaviour {
	private int openedLevelsNumber;
	private int currentCampaignIndex;
	private int availableLevelsNumber;
	private PlayerProgressLevel mostPlayedLevel;
	private int currentLevelIndex;

	// Може прибрати блок try-catch? Щось подібне вже є у MainMenu.cs.
	private void Start() {

		// TODO code cleaning
//		showGameTime();
//		showEarnedCoins();
//		showProgressInCampaign();
//		showMostPlayedLevel();


		PlayerProgress playerProgress;
		XmlSerializer serializer;
		var stream = new FileStream(PlayerPrefs.GetString("Path To Player Progress"),
			FileMode.OpenOrCreate);
		try {
			serializer = new XmlSerializer(typeof(PlayerProgress));
			playerProgress =  serializer.Deserialize(stream) as PlayerProgress;
			stream.Close();
		} catch (Exception e) {
			serializer = new XmlSerializer(typeof(PlayerProgress));
			var o = new PlayerProgress();
			o.balanceInCoins = 1000;
			serializer.Serialize(stream, o);
			stream.Close();
			playerProgress = o;
		}

		//showGameTime
		var timeInHours = playerProgress.gameTimeInSeconds / 3600;
		var timeInMinutes = playerProgress.gameTimeInSeconds % 3600 / 60;
		var timeInSeconds = playerProgress.gameTimeInSeconds % 60;

		transform.Find ("Game's Time/Value").GetComponent<Text> ().text = 
			timeInHours + ":"
			+ (timeInMinutes < 10 ? "0" + timeInMinutes : timeInMinutes.ToString()) + ":"
			+ (timeInSeconds < 10 ? "0" + timeInSeconds : timeInSeconds.ToString());

		//showEarnedCoins
		transform.Find("Coins Earned/Value").GetComponent<Text>().text= 
			playerProgress.coinsEarned.ToString();

		//showProgressInCampaign
		int openedLevels = 0;
		for (int i = 0; i < playerProgress.campaigns.Count; ++i) {
			openedLevels += playerProgress.campaigns [i].levels.Count;
		}

		serializer = new XmlSerializer (typeof(Levels));
//		stream = new FileStream (PlayerPrefs.GetString ("Path To Levels"), FileMode.OpenOrCreate);
//		var levels = serializer.Deserialize (stream) as Levels;
		//TODO
		var levels = serializer.Deserialize(new StringReader(PlayerPrefs.GetString("Levels"))) as Levels;
//		stream.Close();
		int availableLevels = 0;
		for (int i = 0; i < levels.campaigns.Count; ++i) {
			availableLevels += levels.campaigns [i].levels.Count;
		}
		transform.Find ("Progress In Campaign/Value").GetComponent<Text> ().text = 
			openedLevels + "/" + availableLevels;

		//showMostPlayedLevel
		var mostPlayedLevel = new PlayerProgressLevel();//playerProgress.freeLevels[0];
		//calculateMostPlayedFreeLevel
		for (int i = 0; i < playerProgress.freeLevels.Count; ++i) {
			if (playerProgress.freeLevels [i].timesPlayed > mostPlayedLevel.timesPlayed) {
				mostPlayedLevel = playerProgress.freeLevels [i];
			}
		}

		//calculateMostPlayedCampaignLevel
		for (int i = 0; i < playerProgress.campaigns.Count; ++i) {
			for (int j = 0; j < playerProgress.campaigns [i].levels.Count; ++j) {
				if (playerProgress.campaigns [i].levels [j].timesPlayed >
				    mostPlayedLevel.timesPlayed) {
					mostPlayedLevel = playerProgress.campaigns [i].levels [j];
				}
			}
		}

		var mostPlayedLevelName = "Level " + mostPlayedLevel.number;
		if (playerProgress.survival.timesPlayed > mostPlayedLevel.timesPlayed) {
			mostPlayedLevelName = "Survival";
		}
		transform.Find ("Most Played Level/Value").GetComponent<Text> ().text = mostPlayedLevelName;
	}

	// TODO code cleaning (start)

//	private void showGameTime() {
//		var t = getTextComponentFromGameTimeValue();
//		var value = getValueForGameTime();
//		t.text = value;
//	}
//
//	private Text getTextComponentFromGameTimeValue() {
//		var t = getTransformForGameTimeValue();
//		return t.GetComponent<Text>();
//	}
//
//	private Transform getTransformForGameTimeValue() {
//		var name = getNameForGameTimeValue();
//		return transform.Find(name);
//	}
//
//	private string getNameForGameTimeValue() {
//		return "Game's Time/Value";
//	}
//
//	private string getValueForGameTime() {
//		var h = getTimeInHours();
//		var s = getTimeInMinutesAndSecondsWithSeparator();
//		return h + s;
//	}
//
//	private int getTimeInHours() {
//		var t = getGameTimeInSecondsFromPlayerProgress();
//		var n = getDividerForGameTimeInHours();
//		return t / n;
//	}
//
//	private int getGameTimeInSecondsFromPlayerProgress() {
//		var p = getProgressOfPlayer();
//		return p.gameTimeInSeconds;
//	}
//
//	private PlayerProgress getProgressOfPlayer() {
//		var serializer = getSerializerForPlayerProgress();
//		var stream = getStreamForPlayerProgress();
//		var p = serializer.Deserialize(stream) as PlayerProgress;
//		stream.Close();
//		return p;
//	}
//	
//	private XmlSerializer getSerializerForPlayerProgress() {
//		var type = getTypeForPlayerProgress();
//		return new XmlSerializer(type);
//	}
//
//	private Type getTypeForPlayerProgress() {
//		return typeof(PlayerProgress);
//	}
//
//	private Stream getStreamForPlayerProgress() {
//		var path = getPathForPlayerProgress();
//		var mode = getModeForPlayerProgress();
//		return new FileStream(path, mode);
//	}
//
//	private string getPathForPlayerProgress() {
//		var key = getKeyForPlayerProgress();
//		return PlayerPrefs.GetString(key);
//	}
//
//	private string getKeyForPlayerProgress() {
//		return "Path To Player Progress";
//	}
//
//	private FileMode getModeForPlayerProgress() {
//		return FileMode.OpenOrCreate;
//	}
//
//	private int getDividerForGameTimeInHours() {
//		return 3600;
//	}
//
//	private string getTimeInMinutesAndSecondsWithSeparator() {
//		var s = getSeparatorForGameTime();
//		var t = getTimeInMinutesAndSeconds();
//		return s + t;
//	}
//
//	private string getSeparatorForGameTime() {
//		return ":";
//	}
//
//	private string getTimeInMinutesAndSeconds() {
//		var m = getTimeInMinutes();
//		var s = getTimeInSecondsWithSeparator();
//		return m + s;
//	}
//
//	private string getTimeInMinutes() {
//		var value = getGameTimeInMinutes();
//		return getProcessed(value);
//	}
//
//	private int getGameTimeInMinutes() {
//		var s = getGameTimeInSecondsFromPlayerProgress();
//		var d = getDividerForGameTimeInHours();
//		var dd = getDividerForGameTimeInMinutes();
//		return s % d / dd;
//	}
//
//	private int getDividerForGameTimeInMinutes() {
//		return 60;
//	}
//
//	private string getProcessed(int value) {
//		if (numberHasOnlyOneDigit(value)) {
//			return getAdjusted(value);
//		} else {
//			return getUnadjusted(value);
//		}
//	}
//
//	private bool numberHasOnlyOneDigit(int value) {
//		int n = getNumberForOneDigitLimit();
//		return value < n;
//	}
//
//	private int getNumberForOneDigitLimit() {
//		return 10;
//	}
//
//	private string getAdjusted(int value) {
//		var s = getStringForAdjustingNumber();
//		return s + value;
//	}
//
//	private string getStringForAdjustingNumber() {
//		return "0";
//	}
//
//	private string getUnadjusted(int value) {
//		return value.ToString();
//	}
//
//	private string getTimeInSecondsWithSeparator() {
//		var s = getSeparatorForGameTime();
//		var n = getTimeInSeconds();
//		return s + n;
//	}
//
//	private string getTimeInSeconds() {
//		var value = getGameTimeInSeconds();
//		return getProcessed(value);
//	}
//
//	private int getGameTimeInSeconds() {
//		int s = getGameTimeInSecondsFromPlayerProgress();
//		int d = getDividerForGameTimeInMinutes();
//		return s % d;
//	}
//
//	private void showEarnedCoins() {
//		var t = getTextComponentFromCoinsEarnedValue();
//		var value = getValueForEarnedCoins();
//		t.text = value;
//	}
//
//	private Text getTextComponentFromCoinsEarnedValue() {
//		var t = getTransformForCoinsEarnedValue();
//		return t.GetComponent<Text>();
//	}
//
//	private Transform getTransformForCoinsEarnedValue() {
//		var name = getNameForCoinsEarnedValue();
//		return transform.Find(name);
//	}
//
//	private string getNameForCoinsEarnedValue() {
//		return "Coins Earned/Value";
//	}
//
//	private string getValueForEarnedCoins() {
//		var c = getCoinsEarnedFromPlayerProgress();
//		return c.ToString();
//	}
//
//	private int getCoinsEarnedFromPlayerProgress() {
//		var p = getProgressOfPlayer();
//		return p.coinsEarned;
//	}
//
//	private void showProgressInCampaign() {
//		var t = getTextComponentFromProgressInCampaignValue();
//		var value = getValueForProgressInCampaign();
//		t.text = value;
//	}
//
//	private Text getTextComponentFromProgressInCampaignValue() {
//		var t = getTransformForProgressInCampaignValue();
//		return t.GetComponent<Text>();
//	}
//
//	private Transform getTransformForProgressInCampaignValue() {
//		var name = getNameForProgressInCampaignValue();
//		return transform.Find(name);
//	}
//
//	private string getNameForProgressInCampaignValue() {
//		return "Progress In Campaign/Value";
//	}
//
//	private string getValueForProgressInCampaign() {
//		var n = getNumberOfOpenedLevels();
//		var s = getAvailableLevelsNumberWithSeparator();
//		return n + s;
//	}
//
//	private int getNumberOfOpenedLevels() {
//		resetOpenedLevelsNumber();
//		resetCurrentCampaignIndex();
//		calculateOpenedLevelsNumber();
//		return getCalculatedOpenedLevelsNumber();
//	}
//
//	private void resetOpenedLevelsNumber() {
//		int value = getValueForOpenedLevelsNumberByDefault();
//		setOpenedLevelsNumber(value);
//	}
//
//	private int getValueForOpenedLevelsNumberByDefault() {
//		return 0;
//	}
//
//	private void setOpenedLevelsNumber(int value) {	
//		openedLevelsNumber = value;	
//	}
//
//	private void resetCurrentCampaignIndex() {
//		int value = getValueForCurrentCampaignIndexByDefault();
//		setCurrentCampaignIndex(value);
//	}
//
//	private int getValueForCurrentCampaignIndexByDefault() {
//		return 0;
//	}
//
//	private void setCurrentCampaignIndex(int value) {
//		currentCampaignIndex = value;
//	}
//
//	private void calculateOpenedLevelsNumber() {
//		while (thereAreCampaignsNeedToBeProcessed()) {
//			processCurrentCampaign();
//		}
//	}
//
//	private bool thereAreCampaignsNeedToBeProcessed() {
//		int i = getCurrentCampaignIndex();
//		int n = getNumberOfCampaigns();
//		return i < n;
//	}
//
//	private int getCurrentCampaignIndex() {
//		return currentCampaignIndex;
//	}
//
//	private int getNumberOfCampaigns() {
//		var c = getCampaignsFromPlayerProgress();
//		return c.Count;
//	}
//
//	private List<PlayerProgressCampaign> getCampaignsFromPlayerProgress() {
//		var p = getProgressOfPlayer();
//		return p.campaigns;
//	}
//
//	private void processCurrentCampaign() {
//		updateOpenedLevelsNumber();
//		moveOnToTheNextCampaign();
//	}
//
//	private void updateOpenedLevelsNumber() {
//		int value = getValueForUpdatedOpenedLevelsNumber();
//		setOpenedLevelsNumber(value);
//	}
//
//	private int getValueForUpdatedOpenedLevelsNumber() {
//		int n = getOpenedLevelsNumber();
//		int m = getNumberOfCampaignLevelsFromPlayerProgress();
//		return n + m;
//	}
//
//	private int getOpenedLevelsNumber() {
//		return openedLevelsNumber;
//	}
//
//	private int getNumberOfCampaignLevelsFromPlayerProgress() {
//		var l = getCampaignLevelsFromPlayerProgress();
//		return l.Count;
//	}
//
//	private List<PlayerProgressLevel> getCampaignLevelsFromPlayerProgress() {
//		var c = getCurrentCampaignFromPlayerProgress();
//		return c.levels;
//	}
//
//	private PlayerProgressCampaign getCurrentCampaignFromPlayerProgress() {
//		var c = getCampaignsFromPlayerProgress();
//		var index = getCurrentCampaignIndex();
//		return c[index];
//	}
//
//	private void moveOnToTheNextCampaign() {
//		int value = getValueForMovingOnToTheNextCampaign();
//		setCurrentCampaignIndex(value);
//	}
//
//	private int getValueForMovingOnToTheNextCampaign() {
//		int i = getCurrentCampaignIndex();
//		int increment = getIncrementForMovingOnToTheNextCampaign();
//		return i + increment;
//	}
//
//	private int getIncrementForMovingOnToTheNextCampaign() {
//		return 1;
//	}
//
//	private int getCalculatedOpenedLevelsNumber() {
//		return getOpenedLevelsNumber();
//	}
//
//	private string getAvailableLevelsNumberWithSeparator() {
//		var s = getSeparatorForProgressInCampaign();
//		var n = getNumberOfAvailableLevels();
//		return s + n;
//	}
//
//	private string getSeparatorForProgressInCampaign() {
//		return "/";
//	}
//
//	private int getNumberOfAvailableLevels() {
//		resetAvailableLevelsNumber();
//		resetCurrentCampaignIndex();
//		calculateAvailableLevelsNumber();
//		return getCalculatedAvailableLevelsNumber();
//	}
//
//	private void resetAvailableLevelsNumber() {
//		int value = getValueForAvailableLevelsNumberByDefault();
//		setAvailableLevelsNumber(value);
//	}
//
//	private int getValueForAvailableLevelsNumberByDefault() {
//		return 0;
//	}
//
//	private void setAvailableLevelsNumber(int value) {
//		availableLevelsNumber = value;
//	}
//
//	private void calculateAvailableLevelsNumber() {
//		while (thereAreLevelsSetCampaignsNeedToBeProcessed()) {
//			processCurrentLevelsSetCampaign();
//		}
//	}
//
//	private bool thereAreLevelsSetCampaignsNeedToBeProcessed() {
//		int i = getCurrentCampaignIndex();
//		int n = getNumberOfLevelsSetCampaigns();
//		return i < n;
//	}
//
//	private int getNumberOfLevelsSetCampaigns() {
//		var c = getCampaignsFromLevelsSet();
//		return c.Count;
//	}
//
//	private List<Campaign> getCampaignsFromLevelsSet() {
//		var s = getSetOfLevels();
//		return s.campaigns;
//	}
//
//	private Levels getSetOfLevels() {
//		var serializer = getSerializerForLevelsSet();
//		var textReader = getTextReaderForLevelsSet();
//		return serializer.Deserialize(textReader) as Levels;	
//	}
//
//	private XmlSerializer getSerializerForLevelsSet() {
//		var type = getTypeForLevelsSet();
//		return new XmlSerializer(type);
//	}
//
//	private Type getTypeForLevelsSet() {
//		return typeof(Levels);
//	}
//
//	private TextReader getTextReaderForLevelsSet() {
//		var s = getStringForLevelsSet();
//		return new StringReader(s);
//	}
//
//	private string getStringForLevelsSet() {
//		var key = getKeyForLevelsSet();
//		return PlayerPrefs.GetString(key);
//	}
//
//	private string getKeyForLevelsSet() {
//		return "Levels";
//	}
//
//	private void processCurrentLevelsSetCampaign() {
//		updateAvailableLevelsNumber();
//		moveOnToTheNextCampaign();
//	}
//
//	private void updateAvailableLevelsNumber() {
//		int value = getValueForUpdatedAvailableLevelsNumber();
//		setAvailableLevelsNumber(value);
//	}
//
//	private int getValueForUpdatedAvailableLevelsNumber() {
//		int n = getAvailableLevelsNumber();
//		int m = getNumberOfCampaignLevelsFromLevelsSet();
//		return n + m;
//	}
//
//	private int getAvailableLevelsNumber() {
//		return availableLevelsNumber;
//	}
//
//	private int getNumberOfCampaignLevelsFromLevelsSet() {	
//		var l = getCampaignLevelsFromLevelsSet();
//		return l.Count;
//	}
//
//	private List<Level> getCampaignLevelsFromLevelsSet() {
//		var c = getCurrentCampaignFromLevelsSet();
//		return c.levels;
//	}
//
//	private Campaign getCurrentCampaignFromLevelsSet() {
//		var c = getCampaignsFromLevelsSet();
//		var index = getCurrentCampaignIndex();
//		return c[index];
//	}
//
//	private int getCalculatedAvailableLevelsNumber() {
//		return getAvailableLevelsNumber();
//	}
//
//	private void showMostPlayedLevel() {
//
//		var t = getTextComponentFromMostPlayedLevelValue();
//		var value = getValueForMostPlayedLevel();
//		t.text = value;
//
////		var t = getTextComponentFromProgressInCampaignValue();
////		var value = getValueForProgressInCampaign();
////		t.text = value;
//
//		//showMostPlayedLevel
//		var mostPlayedLevel = new PlayerProgressLevel();//playerProgress.freeLevels[0];
//		//calculateMostPlayedFreeLevel
//		for (int i = 0; i < playerProgress.freeLevels.Count; ++i) {
//			if (playerProgress.freeLevels [i].timesPlayed > mostPlayedLevel.timesPlayed) {
//				mostPlayedLevel = playerProgress.freeLevels [i];
//			}
//		}
//
//		//calculateMostPlayedCampaignLevel
//		for (int i = 0; i < playerProgress.campaigns.Count; ++i) {
//			for (int j = 0; j < playerProgress.campaigns [i].levels.Count; ++j) {
//				if (playerProgress.campaigns [i].levels [j].timesPlayed >
//					mostPlayedLevel.timesPlayed) {
//					mostPlayedLevel = playerProgress.campaigns [i].levels [j];
//				}
//			}
//		}
//
//		var mostPlayedLevelName = "Level " + mostPlayedLevel.number;
//		if (playerProgress.survival.timesPlayed > mostPlayedLevel.timesPlayed) {
//			mostPlayedLevelName = "Survival";
//		}
//		transform.Find ("Most Played Level/Value").GetComponent<Text> ().text = mostPlayedLevelName;
//
//
//	}
//
//	private Text getTextComponentFromMostPlayedLevelValue() {
//		var t = getTransformForMostPlayedLevelValue();
//		return t.GetComponent<Text>();
//	}
//
//	private Transform getTransformForMostPlayedLevelValue() {
//		var name = getNameForMostPlayedLevelValue();
//		return transform.Find(name);
//	}
//
//	private string getNameForMostPlayedLevelValue() {
//		return "Most Played Level/Value";
//	}
//
//	private string getValueForMostPlayedLevel() {
//	
//		resetMostPlayedLevel();
//		calculateMostPlayedFreeLevel();
//		calculateMostPlayedCampaignLevel();
//		return getCalculatedMostPlayedLevelName();
////		u 0
//
//		//showMostPlayedLevel
//		var mostPlayedLevel = new PlayerProgressLevel();//playerProgress.freeLevels[0];
//		//calculateMostPlayedFreeLevel
//		for (int i = 0; i < playerProgress.freeLevels.Count; ++i) {
//			if (playerProgress.freeLevels [i].timesPlayed > mostPlayedLevel.timesPlayed) {
//				mostPlayedLevel = playerProgress.freeLevels [i];
//			}
//		}
//
//		//calculateMostPlayedCampaignLevel
//		for (int i = 0; i < playerProgress.campaigns.Count; ++i) {
//			for (int j = 0; j < playerProgress.campaigns [i].levels.Count; ++j) {
//				if (playerProgress.campaigns [i].levels [j].timesPlayed >
//					mostPlayedLevel.timesPlayed) {
//					mostPlayedLevel = playerProgress.campaigns [i].levels [j];
//				}
//			}
//		}
//
//		var mostPlayedLevelName = "Level " + mostPlayedLevel.number;
//		if (playerProgress.survival.timesPlayed > mostPlayedLevel.timesPlayed) {
//			mostPlayedLevelName = "Survival";
//		}
//
//	}
//
//	private void resetMostPlayedLevel() {
//		var value = getValueForMostPlayedLevelByDefault();
//		setMostPlayedLevel(value);
//	}
//
//	private PlayerProgressLevel getValueForMostPlayedLevelByDefault() {
//		return new PlayerProgressLevel();
//	}
//
//	private void setMostPlayedLevel (PlayerProgressLevel value) {		
//		mostPlayedLevel = value;
//	}
//
//	private void calculateMostPlayedFreeLevel() {
//
//		resetCurrentLevelIndex();
//		calculateFreeLevel();
//
//		//calculateMostPlayedFreeLevel
//		for (int i = 0; i < playerProgress.freeLevels.Count; ++i) {
//			if (playerProgress.freeLevels [i].timesPlayed > mostPlayedLevel.timesPlayed) {
//				mostPlayedLevel = playerProgress.freeLevels [i];
//			}
//		}
//
//	}
//
//	private void resetCurrentLevelIndex() {
//		int value = getValueForCurrentLevelIndexByDefault();
//		setCurrentLevelIndex(value);
//	}
//
//	private int getValueForCurrentLevelIndexByDefault() {
//		return 0;
//	}
//
//	private void setCurrentLevelIndex(int value) {
//		currentLevelIndex = value;
//	}
//
//	private void calculateFreeLevel() {
//
//		while (thereAreFreeLevelsNeedToBeProcessed()) {
//			processCurrentFreeLevel();
//		}
//
////		thereAreCampaignsNeedToBeProcessed
//
//		//calculateMostPlayedFreeLevel
//		for (int i = 0; i < playerProgress.freeLevels.Count; ++i) {
//			if (playerProgress.freeLevels [i].timesPlayed > mostPlayedLevel.timesPlayed) {
//				mostPlayedLevel = playerProgress.freeLevels [i];
//			}
//		}
//
//	}
//
//	private bool thereAreFreeLevelsNeedToBeProcessed() {
//		int i = getCurrentLevelIndex();
//		int n = getNumberOfFreeLevels();
//		return i < n;
//	}
//
//	private int getCurrentLevelIndex() {
//		return currentLevelIndex;
//	}
//
//	private int getNumberOfFreeLevels() {
//		var l = getFreeLevelsFromPlayerProgress();
//		return l.Count;
//	}

	// TODO code cleaning (end)

	public void goToPreviousScreen() {
		SceneManager.LoadScene ("MainMenu");
	}
}