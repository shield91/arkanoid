﻿using AssemblyCSharp;
using System;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShopScreen : MonoBehaviour {
	private void Start() {
		loadPlayerCoins();
		setUpNoAdsButtonVisibility();
	}

	private void loadPlayerCoins() {
		var t = getTextComponentFromValueForCoinsField();
		var value = getValueForBalanceInCoinsFromPlayerProgress();
		t.text = value;
	}

	private Text getTextComponentFromValueForCoinsField() {
		var t = getTransformForValueInCoinsField();
		return t.GetComponent<Text>();
	}

	private Transform getTransformForValueInCoinsField() {
		var name = getNameForValueInCoinsField();
		return transform.Find(name);
	}

	private string getNameForValueInCoinsField() {
		return "Coins/Value";
	}

	private string getValueForBalanceInCoinsFromPlayerProgress() {
		int balance = getBalanceInCoinsFromPlayerProgress();
		return balance.ToString();
	}

	private int getBalanceInCoinsFromPlayerProgress() {
		var pp = getProgressOfPlayer();
		return pp.balanceInCoins;
	}

	private PlayerProgress getProgressOfPlayer() {
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
		stream.Close();
		return playerProgress;
	}

	private XmlSerializer getSerializerForPlayerProgress() {
		var type = getTypeForPlayerProgress();
		return new XmlSerializer(type);
	}

	private Type getTypeForPlayerProgress() {
		return typeof(PlayerProgress);
	}

	private Stream getStreamForPlayerProgress() {
		var path = getPathToPlayerProgress();
		var mode = getFileModeForStream();
		return new FileStream(path, mode);
	}

	private string getPathToPlayerProgress() {
		var key = getKeyForPathToPlayerProgress();
		return PlayerPrefs.GetString(key);
	}

	private string getKeyForPathToPlayerProgress() {
		return "Path To Player Progress";
	}

	private FileMode getFileModeForStream() {
		return FileMode.OpenOrCreate;
	}

	private void setUpNoAdsButtonVisibility() {
		var go = getGameObjectForNoAdsButton();
		var value = getValueForNoAdsButtonVisibility();
		go.SetActive(value);
	}

	private GameObject getGameObjectForNoAdsButton() {
		var t = getTransformForNoAdsButton();
		return t.gameObject;
	}

	private Transform getTransformForNoAdsButton() {
		var name = getNameForNoAdsButton();
		return transform.Find(name);
	}

	private string getNameForNoAdsButton() {
		return "No Ads    0,5$";
	}

	private bool getValueForNoAdsButtonVisibility() {
		var b = getValueForAdsBeingTurnedOff();
		return !b;
	}

	private bool getValueForAdsBeingTurnedOff() {
		var key = getKeyForAdsBeingTurnedOff();
		return PlayerPrefs.HasKey(key);
	}

	private string getKeyForAdsBeingTurnedOff() {
		return "no_add";
	}

	public void updateBalanceByAdding(int coinsQuantity) {
		updateBalanceInPlayerProgressByAdding(coinsQuantity);
		loadPlayerCoins();
	}

	private void updateBalanceInPlayerProgressByAdding(int coinsQuantity) {
		var serializer = getSerializerForPlayerProgress();
		var o = getProgressOfPlayer();
		var stream = getStreamForPlayerProgress();
		o.balanceInCoins += coinsQuantity;
		serializer.Serialize(stream, o);
		stream.Close();
	}

	public void goToPreviousScreen() {
		var sceneName = getSceneNameForPreviousScreen();
		SceneManager.LoadScene(sceneName);
	}

	private string getSceneNameForPreviousScreen() {
		return "MainMenu";
	}
}