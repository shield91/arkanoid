﻿using AssemblyCSharp;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.UI;

public class CampaignUnpaidLevelButton : MonoBehaviour {
	private GameObject replacement;

	private void Start() {
		resetReplacement();
	}

	private void resetReplacement() {
		var value = getValueForReplacementByDefault();
		setReplacement(value);
	}

	private GameObject getValueForReplacementByDefault() {
		return null;
	}

	private void setReplacement(GameObject value) {
		replacement = value;
	}

	public void processClick() {
		if (levelEditorModeIsEnabled()) {
			processClickInLevelEditorMode();
		} else {
			processClickInPlayMode();
		}
	}

	private bool levelEditorModeIsEnabled() {
		var key = getKeyForLevelEditorMode();
		return PlayerPrefs.HasKey(key);
	}

	private string getKeyForLevelEditorMode() {
		return "Level Editor Mode";
	}

	private void processClickInLevelEditorMode () {
		saveChosenLevelIndex();
		showCostPopUpWindow();
	}

	private void saveChosenLevelIndex() {
		var key = getKeyForChosenLevelIndex();
		var value = getValueForChosenLevelIndex();
		PlayerPrefs.SetInt(key, value);
	}

	private string getKeyForChosenLevelIndex() {
		return "Chosen Level Index";
	}

	private int getValueForChosenLevelIndex() {
		return transform.GetSiblingIndex();
	}

	private void showCostPopUpWindow() {
		var go = getGameObjectForCostPopUpWindow();
		go.SetActive(true);
	}

	private GameObject getGameObjectForCostPopUpWindow() {
		var t = getTransformForCostPopUpWindow();
		return t.gameObject;
	}

	private Transform getTransformForCostPopUpWindow() {
		var t = getTransformForCanvas();
		var name = getNameForCostPopUpWindow();
		return t.Find(name);
	}

	private Transform getTransformForCanvas() {
		return transform.root;
	}

	private string getNameForCostPopUpWindow() {
		return "Cost Pop-Up Window";
	}

	private void processClickInPlayMode() {
		if (levelCanBePurchased()) {
			purchaseLevel();
		}
	}

	private bool levelCanBePurchased() {
		int balance = getBalanceFromLevelsScreen();
		int cost = getCostOfCampaignLevel();
		return cost <= balance;
	}

	private int getBalanceFromLevelsScreen() {
		var s = getStringRepresentationOfBalanceFromLevelsScreen();
		return int.Parse(s);
	}

	private string getStringRepresentationOfBalanceFromLevelsScreen() {
		var t = getTextComponentFromCoinsValueOnLevelsScreen();
		return t.text;
	}

	private Text getTextComponentFromCoinsValueOnLevelsScreen() {
		var coins = getCoinsValueFromCanvasOnLevelsScreen();
		return coins.GetComponent<Text>();
	}

	private Transform getCoinsValueFromCanvasOnLevelsScreen() {
		var t = getCanvasOnLevelsScreen ();
		var name = getNameForCoinsValueOnLevelsScreen();
		return t.Find(name);
	}

	private Transform getCanvasOnLevelsScreen() {
		var ls = GetComponentInParent<LevelSelect>();
		return ls.transform;
	}

	private string getNameForCoinsValueOnLevelsScreen() {
		return "Coins/Value";
	}

	private int getCostOfCampaignLevel() {
		var s = getStringRepresentationOfCampaignLevelCost();
		return int.Parse(s);
	}

	private string getStringRepresentationOfCampaignLevelCost() {
		var t = getTextComponentFromCost();
		return t.text;
	}

	private Text getTextComponentFromCost() {
		var t = getTransformForCost();
		return t.GetComponent<Text>();
	}

	private Transform getTransformForCost() {
		var name = getNameForCost();
		return transform.Find(name);
	}

	private string getNameForCost() {
		return "Cost";
	}

	private void purchaseLevel() {
		updateBalance();
		instantiateCampaignOrdinaryLevelButton();
		updatePlayerProgress();
		updateBalanceOnScreen();
		destroyThisButton();
	}

	private void updateBalance() {
		var t = getTextComponentFromCoinsValueOnLevelsScreen();
		var value = getValueForUpdatingBalance();
		t.text = value;
	}

	private string getValueForUpdatingBalance() {
		int value = getIntegerRepresentationOfValueForUpdatingBalance();
		return value.ToString();
	}

	private int getIntegerRepresentationOfValueForUpdatingBalance() {
		int balance = getBalanceFromLevelsScreen();
		int cost = getCostOfCampaignLevel();
		return balance - cost;
	}

	private void instantiateCampaignOrdinaryLevelButton() {
		resetReplacement();
		instantiateReplacement();
		setUpReplacement();
	}

	private void instantiateReplacement() {
		var value = getInstantiatedReplacement();
		setReplacement(value);
	}

	private GameObject getInstantiatedReplacement() {
		var original = getOriginalForInstantiatingReplacement();
		var parent = getParentForInstantiatingReplacement();
		return Instantiate(original, parent);
	}

	private GameObject getOriginalForInstantiatingReplacement() {
		var o = getLoadedOriginalForInstantiatingReplacement();
		return (GameObject) o;
	}

	private Object getLoadedOriginalForInstantiatingReplacement() {
		var path = getPathForInstantiatingReplacement();
		return Resources.Load(path);
	}

	private string getPathForInstantiatingReplacement() {
		return "Order/Ordinary Level Button";
	}

	private Transform getParentForInstantiatingReplacement() {
		return transform.parent;
	}

	private void setUpReplacement() {
		setUpTextForReplacement();
		setUpNameForReplacement();
		setUpIndexForReplacement();
	}

	private void setUpTextForReplacement() {
		var t = getTextComponentFromReplacement();
		var value = getValueForTextForReplacement();
		t.text = value;
	}

	private Text getTextComponentFromReplacement() {
		var go = getReplacement();
		return go.GetComponentInChildren<Text>();
	}

	private GameObject getReplacement() {
		return replacement;
	}

	private string getValueForTextForReplacement() {
		var t = getTextComponentFromNumber();
		return t.text;
	}

	private Text getTextComponentFromNumber() {
		var t = getTransformForNumber();
		return t.GetComponent<Text>();
	}

	private Transform getTransformForNumber() {
		var name = getNameForNumber();
		return transform.Find(name);
	}

	private string getNameForNumber() {
		return "Number";
	}

	private void setUpNameForReplacement() {
		var go = getReplacement();
		var value = getNameForReplacement();
		go.name = value;
	}

	private string getNameForReplacement() {
		var header = getHeaderForNameForReplacement();
		var value = getValueForNameForReplacement();
		return header + value;
	}

	private string getHeaderForNameForReplacement() {
		return "Level ";
	}

	private string getValueForNameForReplacement() {
		var t = getTextComponentFromNumber();
		return t.text;
	}

	private void setUpIndexForReplacement() {
		var t = getTransformForReplacement();
		var index = getIndexForReplacement();
		t.SetSiblingIndex(index);
	}

	private Transform getTransformForReplacement() {
		var go = getReplacement();
		return go.transform;
	}

	private int getIndexForReplacement() {
		return transform.GetSiblingIndex();
	}

	private void updatePlayerProgress() {
		updatePlayerProgressByAddingNewCampaignIfItIsNecessary();
		updatePlayerProgressByPerformingOtherStuff();
	}

	private void updatePlayerProgressByAddingNewCampaignIfItIsNecessary() {
		if (chosenCampaignIsAbsentInPlayerProgress()) {
			updatePlayerProgressByAddingNewCampaign();
		}
	}

	private bool chosenCampaignIsAbsentInPlayerProgress() {
		var engine = getSearchEngineForCampaignLevels();
		return engine.chosenCampaignIsAbsentInPlayerProgress();
	}

	private CampaignLevelsSearchEngine getSearchEngineForCampaignLevels() {
		int targetLevelNumber = getTargetLevelNumberForSearchEngine();
		return new CampaignLevelsSearchEngine(targetLevelNumber);
	}

	private int getTargetLevelNumberForSearchEngine() {
		return getIndexForReplacement();
	}

	private void updatePlayerProgressByAddingNewCampaign() {
		var serializer = getSerializerForPlayerProgress();
		var o = getProgressOfPlayer();
		var playerProgressCampaigns = o.campaigns;
		var item = getCampaignForChosenLevel();
		var stream = getStreamForPlayerProgress();
		playerProgressCampaigns.Add(item);
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private XmlSerializer getSerializerForPlayerProgress() {
		var engine = getSearchEngineForCampaignLevels();
		return engine.getSerializerForPlayerProgress();
	}

	private PlayerProgress getProgressOfPlayer() {
		var engine = getSearchEngineForCampaignLevels();
		return engine.getProgressOfPlayer();
	}

	private Stream getStreamForPlayerProgress() {
		var engine = getSearchEngineForCampaignLevels();
		return engine.getStreamForPlayerProgress();
	}

	private PlayerProgressCampaign getCampaignForChosenLevel() {
		var item = getCampaignForChosenLevelByDefault();
		var value = getValueForCampaignName();
		item.name = value;
		return item;
	}

	private PlayerProgressCampaign getCampaignForChosenLevelByDefault() {
		return new PlayerProgressCampaign();
	}

	private string getValueForCampaignName() {
		var engine = getSearchEngineForCampaignLevels();
		return engine.getCurrentCampaignNameFromLevelsSet();
	}

	private void updatePlayerProgressByPerformingOtherStuff() {
		var serializer = getSerializerForPlayerProgress();
		var o = getProgressOfPlayer();
		var campaigns = o.campaigns;
		var index = getIndexOfChosenCampaignInPlayerProgress();
		var campaign = campaigns[index];
		var campaignLevels = campaign.levels;
		var item = getPurchasedLevel();
		var stream = getStreamForPlayerProgress();
		campaignLevels.Add(item);
		o.balanceInCoins = getUpdatedBalanceInCoins();
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private int getIndexOfChosenCampaignInPlayerProgress() {
		var engine = getSearchEngineForCampaignLevels();
		return engine.getChosenCampaignIndex();
	}

	private PlayerProgressLevel getPurchasedLevel() {
		var item = getPlayerProgressLevelByDefault();
		var value = getNumberForPurchasedLevel();
		item.number = value;
		return item;
	}

	private PlayerProgressLevel getPlayerProgressLevelByDefault() {
		return new PlayerProgressLevel();
	}

	private int getNumberForPurchasedLevel() {
		var s = getValueForTextForReplacement();
		return int.Parse(s);
	}

	private int getUpdatedBalanceInCoins() {
		return getBalanceFromLevelsScreen();
	}

	private void updateBalanceOnScreen() {
		var ls = GetComponentInParent<LevelSelect>();
		ls.loadPlayerCoins();
	}

	private void destroyThisButton() {
		var obj = getObjectForRemovingItFromScene();
		Destroy(obj);
	}

	private Object getObjectForRemovingItFromScene() {
		return gameObject;
	}
}