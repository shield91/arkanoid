﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class OrdinaryLevelButton : MonoBehaviour {
	public void processClick() {
		saveChosenLevelIndex();
		loadPlayScreen();
	}

	private void saveChosenLevelIndex() {
		var key = getKeyForSavingChosenLevel();
		var value = getValueForSavingChosenLevel();
		PlayerPrefs.SetInt(key, value);
	}

	private string getKeyForSavingChosenLevel() {
		return "Chosen Level Index";
	}

	private int getValueForSavingChosenLevel() {
		return transform.GetSiblingIndex();
	}

	public void loadPlayScreen() {
		var sceneName = getSceneNameForPlayScreen();
		SceneManager.LoadScene(sceneName);
	}

	private string getSceneNameForPlayScreen() {
		return "Level Base";
	}
}