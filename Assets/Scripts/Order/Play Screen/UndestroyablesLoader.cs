using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class UndestroyablesLoader {
		private int currentUndestroyableIndex;

		public UndestroyablesLoader() {
			resetCurrentUndestroyableIndex();
		}

		private void resetCurrentUndestroyableIndex() {
			int value = getValueForCurrentUndestroyableIndexByDefault();
			setCurrentUndestroyableIndex(value);
		}

		private int getValueForCurrentUndestroyableIndexByDefault() {
			return 0;
		}

		private void setCurrentUndestroyableIndex(int value) {
			currentUndestroyableIndex = value;
		}

		public void load() {
			resetCurrentUndestroyableIndex();
			loadUndestroyables();
		}

		private void loadUndestroyables() {
			while (thereAreUndestroyablesNeedToBeLoaded()) {
				loadCurrentUndestroyable();
			}
		}

		private bool thereAreUndestroyablesNeedToBeLoaded() {
			int i = getCurrentUndestroyableIndex();
			int n = getNumberOfUndestroyables();
			return i < n;
		}

		private int getCurrentUndestroyableIndex() {
			return currentUndestroyableIndex;
		}

		private int getNumberOfUndestroyables() {
			var undestroyables = getUndestroyablesFromChosenLevel();
			return undestroyables.Count;
		}

		private List<UndestroyableItem> getUndestroyablesFromChosenLevel() {
			var level = getChosenLevelFromLevelsSet();
			return level.undestroyables;
		}

		private Level getChosenLevelFromLevelsSet() {
			if (playerIsInCampaignMode()) {
				return getChosenCampaignLevelFromLevelsSet();
			} else {
				return getChosenFreeLevelFromLevelsSet();
			}
		}

		private bool playerIsInCampaignMode() {
			var key = getKeyForChosenCampaignIndex();
			return PlayerPrefs.HasKey(key);
		}

		private string getKeyForChosenCampaignIndex() {
			return "Chosen Campaign Index";
		}

		private Level getChosenCampaignLevelFromLevelsSet() {
			var levels = getLevelsFromChosenCampaign();
			var index = getIndexOfChosenLevel();
			return levels[index];
		}

		private List<Level> getLevelsFromChosenCampaign() {
			var campaign = getChosenCampaignFromLevelsSet();
			return campaign.levels;
		}

		private Campaign getChosenCampaignFromLevelsSet() {
			var campaigns = getCampaignsFromLevelsSet();
			var index = getIndexOfChosenCampaign();
			return campaigns[index];
		}

		private List<Campaign> getCampaignsFromLevelsSet() {
			var levels = getSetOfLevels();
			return levels.campaigns;
		}

		private Levels getSetOfLevels() {
			
			//У скрипті MainMenu.cs провернути щось подібне:
			//https://stackoverflow.com/questions/2407302/convert-xmldocument-to-string
			//(це аби можна було запихувати "Levels Set.xml" в string навіть в стані (режимі) редактора рівнів).
			//		if (levelEditorModeIsOn()) {
			//			return getSetOfLevelsInLevelEditorMode();
			//		} else {
			//			return getSetOfLevelsInPlayMode();
			//		}
			//levelEditorModeIsOn
			if (PlayerPrefs.HasKey ("Level Editor Mode")) {
				//getSetOfLevelsInLevelEditorMode
				var serializer = getSerializerForLevelsSet();
				var stream = new FileStream (PlayerPrefs.GetString ("Path To Levels"), FileMode.OpenOrCreate);
				var o = serializer.Deserialize (stream) as Levels;
				stream.Close();
				return o;

				//float
			} else {
				//getSetOfLevelsInPlayMode
				var serializer = getSerializerForLevelsSet();
				var textReader = getTextReaderForLevelsSet();
				return serializer.Deserialize(textReader) as Levels;	
			}

//			var serializer = getSerializerForLevelsSet();
//			var textReader = getTextReaderForLevelsSet();
//			return serializer.Deserialize(textReader) as Levels;
		}

		private XmlSerializer getSerializerForLevelsSet() {
			var type = typeof(Levels);
			return new XmlSerializer(type);
		}

		private TextReader getTextReaderForLevelsSet() {
			var s = getStringForLevelsSet();
			return new StringReader(s);
		}

		private string getStringForLevelsSet() {
			var key = getKeyForLevelsSet();
			return PlayerPrefs.GetString(key);
		}

		private string getKeyForLevelsSet() {
			return "Levels";
		}

		private int getIndexOfChosenCampaign() {
			var key = getKeyForChosenCampaignIndex();
			return PlayerPrefs.GetInt(key);
		}

		private int getIndexOfChosenLevel() {
			var key = getKeyForChosenLevelIndex();
			return PlayerPrefs.GetInt(key);
		}

		private string getKeyForChosenLevelIndex() {
			return "Chosen Level Index";
		}

		private Level getChosenFreeLevelFromLevelsSet() {
			var levels = getFreeLevelsFromLevelsSet();
			var index = getIndexOfChosenLevel();
			return levels[index];
		}

		private List<Level> getFreeLevelsFromLevelsSet() {
			var levelsSet = getSetOfLevels();
			return levelsSet.freeLevels;
		}

		private void loadCurrentUndestroyable() {
			loadUndestroyable();
			moveOnToTheNextUndestroyable();
		}

		private void loadUndestroyable() {
			var undestroyable = getCurrentUndestroyableFromChosenLevel();
			undestroyable.name = getNameForCurrentUndestroyable();
		}

		private GameObject getCurrentUndestroyableFromChosenLevel() {
			var o = getObjectForCurrentUndestroyable();
			return (GameObject) o;
		}

		private Object getObjectForCurrentUndestroyable() {
			var original = getOriginalForCurrentUndestroyable();
			var position = getPositionForCurrentUndestroyable();
			var rotation = getRotationForCurrentUndestroyable();
			var parent = getParentForCurrentUndestroyable();
			return GameObject.Instantiate(original, position, rotation, parent);
		}

		private Object getOriginalForCurrentUndestroyable() {
			var path = getPathForOriginal();
			return Resources.Load(path);
		}

		private string getPathForOriginal() {
			var header = getHeaderForPathToOriginal();
			var name = getActualNameForPathToOriginal();
			return header + name;
		}

		private string getHeaderForPathToOriginal() {
			return "Order/Destroyables/";
		}

		private string getActualNameForPathToOriginal() {
			var builder = getActualNameBuilderForPathToOriginal();
			return builder.getActualName();
		}

		private ActualNameBuilder getActualNameBuilderForPathToOriginal() {
			var name = getNameForPathToOriginal();
			return new ActualNameBuilder(name);
		}

		private string getNameForPathToOriginal() {
			var undestroyable = getCurrentUndestroyableByItsIndex();
			return undestroyable.name;
		}

		private DestroyableItem getCurrentUndestroyableByItsIndex() {
			var undestroyables = getUndestroyablesFromChosenLevel();
			var index = getCurrentUndestroyableIndex();
			return undestroyables[index];
		}

		private Vector3 getPositionForCurrentUndestroyable() {
			var x = getHorizontalPositionForCurrentUndestroyable();
			var y = getVerticalPositionForCurrentUndestroyable();
			return new Vector3(x, y);
		}

		private float getHorizontalPositionForCurrentUndestroyable() {
			var undestroyable = getCurrentUndestroyableByItsIndex();
			return undestroyable.x;
		}

		private float getVerticalPositionForCurrentUndestroyable() {
			var undestroyable = getCurrentUndestroyableByItsIndex();
			return undestroyable.y;
		}

		private Quaternion getRotationForCurrentUndestroyable() {
			return Quaternion.identity;
		}

		private Transform getParentForCurrentUndestroyable() {
			var go = getGameObjectForParentOfCurrentUndestroyable();
			return go.transform;
		}

		private GameObject getGameObjectForParentOfCurrentUndestroyable() {
			var tag = getTagForParentOfCurrentUndestroyable();
			return GameObject.FindWithTag(tag);
		}

		private string getTagForParentOfCurrentUndestroyable() {
			return "Undestroyables";
		}

		private string getNameForCurrentUndestroyable() {
			
			return getNameForPathToOriginal();

//			return getActualNameForPathToOriginal();
		}

		private void moveOnToTheNextUndestroyable() {
			int value = getValueForNextUndestroyableIndex();
			setCurrentUndestroyableIndex(value);
		}

		private int getValueForNextUndestroyableIndex() {
			int index = getCurrentUndestroyableIndex();
			int increment = getValueForMovingOnToTheNextUndestroyable();
			return index + increment;
		}

		private int getValueForMovingOnToTheNextUndestroyable() {
			return 1;
		}
	}
}