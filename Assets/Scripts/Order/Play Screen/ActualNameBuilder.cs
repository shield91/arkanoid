namespace AssemblyCSharp
{
	public class ActualNameBuilder {
		private string fullName;
		private string actualName;
		private int currentNamePartPosition;

		public ActualNameBuilder(string name) {
			fullName = name;
			actualName = "";
			currentNamePartPosition = 0;
		}

		public string getActualName() {
			calculateActualName();
			return getCalculatedActualName();
		}

		private void calculateActualName() {
			resetCurrentNamePartPosition();
			continueActualNameCalculating();
		}

		private void resetCurrentNamePartPosition() {
			currentNamePartPosition = 0;
		}

		private void continueActualNameCalculating() {
			while(fullNameNumberPartIsNotReached()) {
				addCurrentNamePartToActualName();
			}
		}

		private bool fullNameNumberPartIsNotReached() {
			const char SEPARATOR = ' ';
			string[] nameParts = fullName.Split(SEPARATOR);
			int n = nameParts.Length - 1;
			int i = currentNamePartPosition;
			return i < n;
		}

		private void addCurrentNamePartToActualName() {
			addCurrentPartToActualName();
			moveOnToTheNextNamePart();
		}

		private void addCurrentPartToActualName() {
			const char SEPARATOR = ' ';
			string[] nameParts = fullName.Split(SEPARATOR);
			int i = currentNamePartPosition;
			bool b = nextNamePartIsNumberPart();
			string conjuction = b ? "" : " ";
			actualName += nameParts[i] + conjuction;
		}

		private bool nextNamePartIsNumberPart() {
			const char SEPARATOR = ' ';
			string[] nameParts = fullName.Split(SEPARATOR);
			int n = nameParts.Length - 1;
			int i = currentNamePartPosition;
			return i + 1 == n;
		}

		private void moveOnToTheNextNamePart() {
			++currentNamePartPosition;
		}

		private string getCalculatedActualName() {
			return actualName;
		}
	}
}