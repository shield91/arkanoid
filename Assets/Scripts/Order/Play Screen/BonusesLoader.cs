using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class BonusesLoader {
		private GameObject currentBonus;
		private Transform possibleParent;
		private int bonusesNumber;
		private int currentBonusIndex;

		public BonusesLoader() {
			resetCurrentBonus();
			resetPossibleParent();
			resetBonusesNumber();
			resetCurrentBonusIndex();
		}

		private void resetCurrentBonus() {
			var value = getValueForCurrentBonusByDefault();
			setCurrentBonus(value);
		}

		private GameObject getValueForCurrentBonusByDefault() {
			return null;
		}

		private void setCurrentBonus(GameObject value) {
			currentBonus = value;
		}

		private void resetPossibleParent() {
			var value = getValueForPossibleParentByDefault();
			setPossibleParent(value);
		}

		private Transform getValueForPossibleParentByDefault() {
			return null;
		}

		private void setPossibleParent(Transform value) {
			possibleParent = value;
		}

		private void resetBonusesNumber() {
			int value = getValueForBonusesNumber();
			setBonusesNumber(value);
		}

		private int getValueForBonusesNumber() {
			if (survivalModeIsOn()) {
				return getValueForBonusesNumberInSurvivalMode();
			} else {
				return getValueForBonusesNumberInOrdinaryMode();
			}
		}

		private bool survivalModeIsOn() {
			var key = getKeyForSurvivalModeIsOn();
			return PlayerPrefs.HasKey(key);
		}

		private string getKeyForSurvivalModeIsOn() {
			return "Survival Mode Is On";
		}

		private int getValueForBonusesNumberInSurvivalMode() {
			int min = getMinimumForNumberOfBonusesInSurvivalMode();
			int max = getMaximumForNumberOfBonusesInSurvivalMode();
			return Random.Range(min, max);
		}

		private int getMinimumForNumberOfBonusesInSurvivalMode() {
			return 0;
		}

		private int getMaximumForNumberOfBonusesInSurvivalMode() {
			return 4;
		}

		private int getValueForBonusesNumberInOrdinaryMode() {
			var bonuses = getBonusesFromChosenLevel();
			return bonuses.Count;
		}

		private List<Gift> getBonusesFromChosenLevel() {
			var level = getChosenLevelFromLevelsSet();
			return level.bonuses;
		}

		private Level getChosenLevelFromLevelsSet() {
			if (playerIsInCampaignMode()) {
				return getChosenCampaignLevelFromLevelsSet();
			} else {
				return getChosenFreeLevelFromLevelsSet();
			}
		}

		private bool playerIsInCampaignMode() {
			var key = getKeyForChosenCampaignIndex();
			return PlayerPrefs.HasKey(key);
		}

		private string getKeyForChosenCampaignIndex() {
			return "Chosen Campaign Index";
		}

		private Level getChosenCampaignLevelFromLevelsSet() {
			var levels = getLevelsFromChosenCampaign();
			var index = getIndexOfChosenLevel();
			return levels[index];
		}

		private List<Level> getLevelsFromChosenCampaign() {
			var campaign = getChosenCampaignFromLevelsSet();
			return campaign.levels;
		}

		private Campaign getChosenCampaignFromLevelsSet() {
			var campaigns = getCampaignsFromLevelsSet();
			var index = getIndexOfChosenCampaign();
			return campaigns[index];
		}

		private List<Campaign> getCampaignsFromLevelsSet() {
			var levels = getSetOfLevels();
			return levels.campaigns;
		}

		private Levels getSetOfLevels() {
			
			//У скрипті MainMenu.cs провернути щось подібне:
			//https://stackoverflow.com/questions/2407302/convert-xmldocument-to-string
			//(це аби можна було запихувати "Levels Set.xml" в string навіть в стані (режимі) редактора рівнів).
			//		if (levelEditorModeIsOn()) {
			//			return getSetOfLevelsInLevelEditorMode();
			//		} else {
			//			return getSetOfLevelsInPlayMode();
			//		}
			//levelEditorModeIsOn
			if (PlayerPrefs.HasKey ("Level Editor Mode")) {
				//getSetOfLevelsInLevelEditorMode
				var serializer = getSerializerForLevelsSet();
				var stream = new FileStream (PlayerPrefs.GetString ("Path To Levels"), FileMode.OpenOrCreate);
				var o = serializer.Deserialize (stream) as Levels;
				stream.Close();
				return o;

				//float
			} else {
				//getSetOfLevelsInPlayMode
				var serializer = getSerializerForLevelsSet();
				var textReader = getTextReaderForLevelsSet();
				return serializer.Deserialize(textReader) as Levels;	
			}


//			var serializer = getSerializerForLevelsSet();
//			var textReader = getTextReaderForLevelsSet();
//			return serializer.Deserialize(textReader) as Levels;
		}

		private XmlSerializer getSerializerForLevelsSet() {
			var type = typeof(Levels);
			return new XmlSerializer(type);
		}

		private TextReader getTextReaderForLevelsSet() {
			var s = getStringForLevelsSet();
			return new StringReader(s);
		}

		private string getStringForLevelsSet() {
			var key = getKeyForLevelsSet();
			return PlayerPrefs.GetString(key);
		}

		private string getKeyForLevelsSet() {
			return "Levels";
		}

		private int getIndexOfChosenCampaign() {
			var key = getKeyForChosenCampaignIndex();
			return PlayerPrefs.GetInt(key);
		}

		private int getIndexOfChosenLevel() {
			var key = getKeyForChosenLevelIndex();
			return PlayerPrefs.GetInt(key);
		}

		private string getKeyForChosenLevelIndex() {
			return "Chosen Level Index";
		}

		private Level getChosenFreeLevelFromLevelsSet() {
			var levels = getFreeLevelsFromLevelsSet();
			var index = getIndexOfChosenLevel();
			return levels[index];
		}

		private List<Level> getFreeLevelsFromLevelsSet() {
			var levelsSet = getSetOfLevels();
			return levelsSet.freeLevels;
		}

		private void setBonusesNumber(int value) {
			bonusesNumber = value;
		}

		private void resetCurrentBonusIndex() {
			int value = getValueForCurrentBonusIndexByDefault();
			setCurrentBonusIndex(value);
		}

		private int getValueForCurrentBonusIndexByDefault() {
			return 0;
		}

		private void setCurrentBonusIndex(int value) {
			currentBonusIndex = value;
		}

		public void load() {
			resetCurrentBonusIndex();
			loadBonuses();
		}

		private void loadBonuses() {
			while (thereAreBonusesNeedToBeLoaded()) {
				loadCurrentBonus();
			}
		}

		private bool thereAreBonusesNeedToBeLoaded() {
			int i = getCurrentBonusIndex();
			int n = getBonusesNumber();
			return i < n;
		}

		private int getCurrentBonusIndex() {
			return currentBonusIndex;
		}

		private int getBonusesNumber() {
			return bonusesNumber;
		}

		private void loadCurrentBonus() {
			loadBonus();
			moveOnToTheNextBonus();
		}

		private void loadBonus() {

			if (survivalModeIsOn()) {
				// loadBonusInSurvivalMode
				loadBonusInLevelEditorMode();
			} else {
				// loadBonusInOrdinaryMode
				if (bonusHasBeenAttachedToOneOfDestroyablesInLevelEditorMode()) {
					loadBonusInLevelEditorMode();
				} else if ( /* playModeIsOn */ !levelEditorModeIsOn() ) {
					// loadBonusInPlayMode
					loadBonusInLevelEditorMode ();
				}
			}

		}

		private bool bonusHasBeenAttachedToOneOfDestroyablesInLevelEditorMode() {
			var ba = levelEditorModeIsOn();
			var bb = bonusHasBeenAttachedToOneOfDestroyables();
			return ba && bb;
		}

		private bool levelEditorModeIsOn() {
			var key = getKeyForLevelEditorModeIsOn();
			return PlayerPrefs.HasKey(key);
		}

		private string getKeyForLevelEditorModeIsOn() {
			return "Level Editor Mode";
		}

		private bool bonusHasBeenAttachedToOneOfDestroyables() {
			int n = getParentNameLengthFromCurrentBonus();
			int m = getValueForBonusBeingParentless();
			return n > m;
		}

		private int getParentNameLengthFromCurrentBonus() {
			var s = getParentNameFromCurrentBonus();
			return s.Length;
		}

		private string getParentNameFromCurrentBonus() {
			var b = getCurrentBonusByItsIndex();
			return b.parentName;
		}

		private Gift getCurrentBonusByItsIndex() {
			var bonuses = getBonusesFromChosenLevel();
			var index = getCurrentBonusIndex();
			return bonuses[index];
		}

		private int getValueForBonusBeingParentless() {
			return 0;
		}

		private void loadBonusInLevelEditorMode() {
			loadBonusItself();
			setUpBonusName();
			setUpBonusSize();
			setUpBonusVisibility();
		}

		private void loadBonusItself() {		
			var value = getCurrentBonusFromChosenLevel();
			setCurrentBonus(value);
		}

		private GameObject getCurrentBonusFromChosenLevel() {
			var o = getObjectForCurrentBonus();
			return (GameObject) o;
		}

		private Object getObjectForCurrentBonus() {
			var original = getOriginalForCurrentBonus();
			var rotation = getRotationForCurrentBonus();
			var parent = getParentForCurrentBonus();
			var position = parent.position;
			return GameObject.Instantiate(original, position, rotation, parent);
		}

		private Object getOriginalForCurrentBonus() {
			if (survivalModeIsOn()) {
				return getOriginalForCurrentBonusInSurvivalMode();
			} else {
				return getOriginalForCurrentBonusInOrdinaryMode();
			}
		}

		private Object getOriginalForCurrentBonusInSurvivalMode() {
			var originals = getOriginalsForCurrentBonusInSurvivalMode();
			var index = getIndexOfOriginalForCurrentBonusInSurvivalMode();
			return originals[index];
		}

		private Object[] getOriginalsForCurrentBonusInSurvivalMode() {
			var path = getPathForOriginals();
			return Resources.LoadAll(path);
		}

		private string getPathForOriginals() {
			return "Order/Bonuses";
		}

		private int getIndexOfOriginalForCurrentBonusInSurvivalMode() {
			int min = getMinimumForIndexOfOriginalForCurrentBonusInSurvivalMode();
			int max = getMaximumForIndexOfOriginalForCurrentBonusInSurvivalMode();
			return Random.Range(min, max);
		}

		private int getMinimumForIndexOfOriginalForCurrentBonusInSurvivalMode() {
			return 0;
		}

		private int getMaximumForIndexOfOriginalForCurrentBonusInSurvivalMode() {
			var originals = getOriginalsForCurrentBonusInSurvivalMode();
			return originals.Length;
		}

		private Object getOriginalForCurrentBonusInOrdinaryMode() {
			var path = getPathForOriginal();
			return Resources.Load(path);
		}

		private string getPathForOriginal() {
			var header = getHeaderForPathToOriginal();
			var name = getActualNameForPathToOriginal();
			return header + name;
		}

		private string getHeaderForPathToOriginal() {
			return "Order/Bonuses/";
		}

		private string getActualNameForPathToOriginal() {
			return getNameForPathToOriginal();
		}

		private string getNameForPathToOriginal() {
			var bonus = getCurrentBonusByItsIndex();
			return bonus.name;
		}

		private Quaternion getRotationForCurrentBonus() {
			return Quaternion.identity;
		}

		private Transform getParentForCurrentBonus() {
			if (survivalModeIsOn()) {
				return getParentForCurrentBonusInSurvivalMode();
			} else {
				return getParentForCurrentBonusInOrdinaryMode();
			}
		}

		private Transform getParentForCurrentBonusInSurvivalMode() {
			setParentForCurrentBonusInSurvivalMode();
			return getPossibleParent();
		}

		private void setParentForCurrentBonusInSurvivalMode() {
			do {
				setPossibleParentForCurrentBonus();
			} while (possibleParentHasAnotherChild());
		}

		private void setPossibleParentForCurrentBonus() {
			var value = getValueForPossibleParentInSurvivalMode();
			setPossibleParent(value);
		}

		private Transform getValueForPossibleParentInSurvivalMode() {
			var go = getGameObjectForPossibleParentRandomly();
			return go.transform;
		}

		private GameObject getGameObjectForPossibleParentRandomly() {
			var destroyables = getDestroyablesOnPlayScreen();
			var index = getIndexForPossibleParentRandomly();
			return destroyables[index];
		}

		private GameObject[] getDestroyablesOnPlayScreen() {
			var tag = getTagForDestroyablesOnPlayScreen();
			return GameObject.FindGameObjectsWithTag(tag);
		}

		private string getTagForDestroyablesOnPlayScreen() {
			return "Destroy";
		}

		private int getIndexForPossibleParentRandomly() {
			int min = getMinimumForIndexOfPossibleParent();
			int max = getMaximumForIndexOfPossibleParent();
			return Random.Range(min, max);
		}

		private int getMinimumForIndexOfPossibleParent() {
			return 0;
		}

		private int getMaximumForIndexOfPossibleParent() {
			var destroyables = getDestroyablesOnPlayScreen();
			return destroyables.Length;
		}

		private bool possibleParentHasAnotherChild() {
			int number = getNumberOfPossibleParentChildren();
			int value = getValueForParentBeingChildfree();
			return number > value;
		}

		private int getNumberOfPossibleParentChildren() {
			var parent = getPossibleParent();
			return parent.childCount;
		}

		private Transform getPossibleParent() {
			return possibleParent;
		}

		private int getValueForParentBeingChildfree() {
			return 0;
		}

		private Transform getParentForCurrentBonusInOrdinaryMode() {
			var go = getGameObjectForParentOfCurrentBonus();
			return go.transform;
		}

		private GameObject getGameObjectForParentOfCurrentBonus() {
			var destroyables = getDestroyablesOnPlayScreen();
			var index = getIndexForParentOfCurrentBonus();
			return destroyables[index];
		}

		private int getIndexForParentOfCurrentBonus() {
			var retriever = getRetrieverForRetrievingIndexForParentOfCurrentBonus();
			return retriever.getBonusParentPosition();
		}

		private BonusParentPositionRetriever getRetrieverForRetrievingIndexForParentOfCurrentBonus() {
			int i = getCurrentBonusIndex();
			return new BonusParentPositionRetriever(i);
		}

		private void setUpBonusName() {
			var b = getCurrentBonus();
			b.name = getNameForCurrentBonus();
		}

		private GameObject getCurrentBonus() {
			return currentBonus;
		}

		private string getNameForCurrentBonus() {
			if (survivalModeIsOn()) {
				return getNameForCurrentBonusInSurvivalMode();
			} else {
				return getNameForCurrentBonusInOrdinaryMode();
			}
		}

		private string getNameForCurrentBonusInSurvivalMode() {
			return "Bonus";
		}

		private string getNameForCurrentBonusInOrdinaryMode() {
			return getActualNameForPathToOriginal();
		}

		private void setUpBonusSize() {
			var t = getTransformFromCurrentBonus();
			var value = getValueForScaleDividing();
			t.localScale /= value;
		}

		private Transform getTransformFromCurrentBonus() {
			var b = getCurrentBonus();
			return b.transform;
		}

		private float getValueForScaleDividing() {
			return 2f;
		}

		private void setUpBonusVisibility() {
			var b = getCurrentBonus();
			var value = getValueForBonusVisibility();
			b.SetActive(value);
		}

		private bool getValueForBonusVisibility() {
			return levelEditorModeIsOn();
		}

		private void moveOnToTheNextBonus() {
			int value = getValueForNextBonusIndex();
			setCurrentBonusIndex(value);
		}

		private int getValueForNextBonusIndex() {
			int index = getCurrentBonusIndex();
			int increment = getValueForMovingOnToTheNextBonus();
			return index + increment;
		}

		private int getValueForMovingOnToTheNextBonus() {
			return 1;
		}
	}
}