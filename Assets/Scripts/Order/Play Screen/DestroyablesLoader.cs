using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class DestroyablesLoader {
		private GameObject currentDestroyable;
		private int destroyablesNumber;
		private int currentDestroyableIndex;

		public DestroyablesLoader() {
			resetCalculationData();
		}

		private void resetCalculationData() {
			resetCurrentDestroyable();
			resetDestroyablesNumber();
			resetCurrentDestroyableIndex();
		}

		private void resetCurrentDestroyable() {
			var value = getValueForCurrentDestroyableByDefault();
			setCurrentDestroyable(value);
		}

		private GameObject getValueForCurrentDestroyableByDefault() {
			return null;
		}

		private void setCurrentDestroyable(GameObject value) {
			currentDestroyable = value;
		}

		private void resetDestroyablesNumber() {
			int value = getValueForDestroyablesNumber();
			setDestroyablesNumber(value);
		}

		private int getValueForDestroyablesNumber() {
			if (survivalModeIsOn()) {
				return getValueForDestroyablesNumberInSurvivalMode();
			} else {
				return getValueForDestroyablesNumberInOrdinaryMode();
			}
		}

		private bool survivalModeIsOn() {
			var key = getKeyForSurvivalModeIsOn();
			return PlayerPrefs.HasKey(key);
		}

		private string getKeyForSurvivalModeIsOn() {
			return "Survival Mode Is On";
		}

		private int getValueForDestroyablesNumberInSurvivalMode() {
			int min = getMinimumForNumberOfDestroyablesInSurvivalMode();
			int max = getMaximumForNumberOfDestroyablesInSurvivalMode();
			return Random.Range(min, max);
		}

		private int getMinimumForNumberOfDestroyablesInSurvivalMode() {
			return 8;
		}

		private int getMaximumForNumberOfDestroyablesInSurvivalMode() {
			return 41;
		}

		private int getValueForDestroyablesNumberInOrdinaryMode() {
			var destroyables = getDestroyablesFromChosenLevel();
			return destroyables.Count;
		}

		private List<DestroyableItem> getDestroyablesFromChosenLevel() {
			var level = getChosenLevelFromLevelsSet();
			return level.destroyables;
		}

		private Level getChosenLevelFromLevelsSet() {
			if (playerIsInCampaignMode()) {
				return getChosenCampaignLevelFromLevelsSet();
			} else {
				return getChosenFreeLevelFromLevelsSet();
			}
		}

		private bool playerIsInCampaignMode() {
			var key = getKeyForChosenCampaignIndex();
			return PlayerPrefs.HasKey(key);
		}

		private string getKeyForChosenCampaignIndex() {
			return "Chosen Campaign Index";
		}

		private Level getChosenCampaignLevelFromLevelsSet() {
			var levels = getLevelsFromChosenCampaign();
			var index = getIndexOfChosenLevel();
			return levels[index];
		}

		private List<Level> getLevelsFromChosenCampaign() {
			var campaign = getChosenCampaignFromLevelsSet();
			return campaign.levels;
		}

		private Campaign getChosenCampaignFromLevelsSet() {
			var campaigns = getCampaignsFromLevelsSet();
			var index = getIndexOfChosenCampaign();
			return campaigns[index];
		}

		private List<Campaign> getCampaignsFromLevelsSet() {
			var levels = getSetOfLevels();
			return levels.campaigns;
		}

		private Levels getSetOfLevels() {
			
			//У скрипті MainMenu.cs провернути щось подібне:
			//https://stackoverflow.com/questions/2407302/convert-xmldocument-to-string
			//(це аби можна було запихувати "Levels Set.xml" в string навіть в стані (режимі) редактора рівнів).
			//		if (levelEditorModeIsOn()) {
			//			return getSetOfLevelsInLevelEditorMode();
			//		} else {
			//			return getSetOfLevelsInPlayMode();
			//		}
			//levelEditorModeIsOn
			if (PlayerPrefs.HasKey ("Level Editor Mode")) {
				//getSetOfLevelsInLevelEditorMode
				var serializer = getSerializerForLevelsSet();
				var stream = new FileStream (PlayerPrefs.GetString ("Path To Levels"), FileMode.OpenOrCreate);
				var o = serializer.Deserialize (stream) as Levels;
				stream.Close();
				return o;

				//float
			} else {
				//getSetOfLevelsInPlayMode
				var serializer = getSerializerForLevelsSet();
				var textReader = getTextReaderForLevelsSet();
				return serializer.Deserialize(textReader) as Levels;	
			}
			
//			var serializer = getSerializerForLevelsSet();
//			var textReader = getTextReaderForLevelsSet();
//			return serializer.Deserialize(textReader) as Levels;
		}

		private XmlSerializer getSerializerForLevelsSet() {
			var type = typeof(Levels);
			return new XmlSerializer(type);
		}

		private TextReader getTextReaderForLevelsSet() {
			var s = getStringForLevelsSet();
			return new StringReader(s);
		}

		private string getStringForLevelsSet() {
			var key = getKeyForLevelsSet();
			return PlayerPrefs.GetString(key);
		}

		private string getKeyForLevelsSet() {
			return "Levels";
		}

		private int getIndexOfChosenCampaign() {
			var key = getKeyForChosenCampaignIndex();
			return PlayerPrefs.GetInt(key);
		}

		private int getIndexOfChosenLevel() {
			var key = getKeyForChosenLevelIndex();
			return PlayerPrefs.GetInt(key);
		}

		private string getKeyForChosenLevelIndex() {
			return "Chosen Level Index";
		}

		private Level getChosenFreeLevelFromLevelsSet() {
			var levels = getFreeLevelsFromLevelsSet();
			var index = getIndexOfChosenLevel();
			return levels[index];
		}

		private List<Level> getFreeLevelsFromLevelsSet() {
			var levelsSet = getSetOfLevels();
			return levelsSet.freeLevels;
		}

		private void setDestroyablesNumber(int value) {
			destroyablesNumber = value;
		}

		private void resetCurrentDestroyableIndex() {
			int value = getValueForCurrentDestroyableIndexByDefault();
			setCurrentDestroyableIndex(value);
		}

		private int getValueForCurrentDestroyableIndexByDefault() {
			return 0;
		}

		private void setCurrentDestroyableIndex(int value) {
			currentDestroyableIndex = value;
		}

		public void load() {
			resetCurrentDestroyableIndex();
			loadDestroyables();
		}

		private void loadDestroyables() {
			while (thereAreDestroyablesNeedToBeLoaded()) {
				loadCurrentDestroyable();
			}
		}

		private bool thereAreDestroyablesNeedToBeLoaded() {
			int i = getCurrentDestroyableIndex();
			int n = getDestroyablesNumber();
			return i < n;
		}

		private int getCurrentDestroyableIndex() {
			return currentDestroyableIndex;
		}

		private int getDestroyablesNumber() {
			return destroyablesNumber;
		}

		private void loadCurrentDestroyable() {
			loadDestroyable();
			processCurrentDestroyablePosition();
			moveOnToTheNextDestroyable();
		}

		private void loadDestroyable() {
			var destroyable = getGameObjectForCurrentDestroyable();
			destroyable.name = getNameForCurrentDestroyable();
			setCurrentDestroyable(destroyable);
		}

		private GameObject getGameObjectForCurrentDestroyable() {
			var o = getObjectForCurrentDestroyable();
			return (GameObject) o;
		}

		private Object getObjectForCurrentDestroyable() {
			var original = getOriginalForCurrentDestroyable();
			var position = getPositionForCurrentDestroyable();
			var rotation = getRotationForCurrentDestroyable();
			var parent = getParentForCurrentDestroyable();
			return GameObject.Instantiate(original, position, rotation, parent);
		}

		private Object getOriginalForCurrentDestroyable() {
			if (survivalModeIsOn()) {
				return getOriginalForCurrentDestroyableInSurvivalMode();
			} else {
				return getOriginalForCurrentDestroyableInOrdinaryMode();
			}
		}

		private Object getOriginalForCurrentDestroyableInSurvivalMode() {
			var originals = getOriginalsForCurrentDestroyableInSurvivalMode();
			var index = getIndexOfOriginalForCurrentDestroyableInSurvivalMode();
			return originals[index];
		}

		private Object[] getOriginalsForCurrentDestroyableInSurvivalMode() {
			var path = getPathForOriginals();
			return Resources.LoadAll(path);
		}

		private string getPathForOriginals() {
			return "Order/Destroyables";
		}

		private int getIndexOfOriginalForCurrentDestroyableInSurvivalMode() {
			int min = getMinimumForIndexOfOriginalForCurrentDestroyableInSurvivalMode();
			int max = getMaximumForIndexOfOriginalForCurrentDestroyableInSurvivalMode();
			return Random.Range(min, max);
		}

		private int getMinimumForIndexOfOriginalForCurrentDestroyableInSurvivalMode() {
			return 0;
		}

		private int getMaximumForIndexOfOriginalForCurrentDestroyableInSurvivalMode() {
			return 2;
		}

		private Object getOriginalForCurrentDestroyableInOrdinaryMode() {
			var path = getPathForOriginal();
			return Resources.Load(path);
		}

		private string getPathForOriginal() {
			var header = getHeaderForPathToOriginal();
			var name = getActualNameForPathToOriginal();
			return header + name;
		}

		private string getHeaderForPathToOriginal() {
			return "Order/Destroyables/";
		}

		private string getActualNameForPathToOriginal() {
			var builder = getActualNameBuilderForPathToOriginal();
			return builder.getActualName();
		}

		private ActualNameBuilder getActualNameBuilderForPathToOriginal() {
			var name = getNameForPathToOriginal();
			return new ActualNameBuilder(name);
		}

		private string getNameForPathToOriginal() {
			var destroyable = getCurrentDestroyableByItsIndex();
			return destroyable.name;
		}

		private DestroyableItem getCurrentDestroyableByItsIndex() {
			var destroyables = getDestroyablesFromChosenLevel();
			var index = getCurrentDestroyableIndex();
			return destroyables[index];
		}

		private Vector3 getPositionForCurrentDestroyable() {
			if (survivalModeIsOn()) {
				return getPositionForCurrentDestroyableInSurvivalMode();
			} else {
				return getPositionForCurrentDestroyableInOrdinaryMode();
			}
		}

		private Vector3 getPositionForCurrentDestroyableInSurvivalMode() {
			return Vector3.zero;
		}

		private Vector3 getPositionForCurrentDestroyableInOrdinaryMode() {
			var x = getHorizontalPositionForCurrentDestroyable();
			var y = getVerticalPositionForCurrentDestroyable();
			return new Vector3(x, y);
		}

		private float getHorizontalPositionForCurrentDestroyable() {
			var destroyable = getCurrentDestroyableByItsIndex();
			return destroyable.x;
		}

		private float getVerticalPositionForCurrentDestroyable() {
			var destroyable = getCurrentDestroyableByItsIndex();
			return destroyable.y;
		}

		private Quaternion getRotationForCurrentDestroyable() {
			var transform = getTransformOfOriginalForCurrentDestroyable();
			return transform.rotation;
		}

		private Transform getTransformOfOriginalForCurrentDestroyable() {
			var go = getGameObjectForOriginalOfCurrentDestroyable();
			return go.transform;
		}

		private GameObject getGameObjectForOriginalOfCurrentDestroyable() {
			var original = getOriginalForCurrentDestroyable();
			return (GameObject) original;
		}

		private Transform getParentForCurrentDestroyable() {
			var go = getGameObjectForParentOfCurrentDestroyable();
			return go.transform;
		}

		private GameObject getGameObjectForParentOfCurrentDestroyable() {
			var tag = getTagForParentOfCurrentDestroyable();
			return GameObject.FindWithTag(tag);
		}

		private string getTagForParentOfCurrentDestroyable() {
			return "Destroyables";
		}

		private string getNameForCurrentDestroyable() {
			if (survivalModeIsOn()) {
				return getNameForCurrentDestroyableInSurvivalMode();
			} else {
				return getNameForCurrentDestroyableInOrdinaryMode();
			}
		}

		private string getNameForCurrentDestroyableInSurvivalMode() {
			return "Destroyable";
		}

		private string getNameForCurrentDestroyableInOrdinaryMode() {
			
			return getNameForPathToOriginal();

//			return getActualNameForPathToOriginal();
		}

		private void processCurrentDestroyablePosition() {
			if (survivalModeIsOn()) {
				adjustCurrentDestroyablePosition();
			}
		}

		private void adjustCurrentDestroyablePosition() {
			do {
				alterCurrentDestroyablePosition();
			} while (currentDestroyableIsPlacedInOccupiedSpace());
		}

		private void alterCurrentDestroyablePosition() {
			var destroyable = getTransformOfCurrentDestroyable();
			var value = getAlteredPositionForCurrentDestroyable();
			destroyable.position = value;
		}

		private Transform getTransformOfCurrentDestroyable() {
			var destroyable = getCurrentDestroyable();
			return destroyable.transform;
		}

		private GameObject getCurrentDestroyable() {
			return currentDestroyable;
		}

		private Vector3 getAlteredPositionForCurrentDestroyable() {
			var x = getAlteredHorizontalPositionForCurrentDestroyable();
			var y = getAlteredVerticalPositionForCurrentDestroyable();
			return new Vector3(x, y);
		}

		private float getAlteredHorizontalPositionForCurrentDestroyable() {
			var min = getMinimumValueForAlteredHorizontalPositionForCurrentDestroyable();
			var max = getMaximumValueForAlteredHorizontalPositionForCurrentDestroyable();
			return Random.Range(min, max);
		}

		private float getMinimumValueForAlteredHorizontalPositionForCurrentDestroyable() {
			var stageBound = getLeftBoundOfStage();
			var offset = getOffsetForAlteredHorizontalPositionForCurrentDestroyable();
			return stageBound + offset;
		}

		private float getLeftBoundOfStage() {
			var bound = getLowerLeftBoundOfStage();
			return bound.x;
		}

		private Vector3 getLowerLeftBoundOfStage() {
			var bounds = getBoundsOfStage();
			return bounds.min;
		}

		private Bounds getBoundsOfStage() {
			var collider = getEdgeColliderFromLevelTemplate();
			return collider.bounds;
		}

		private EdgeCollider2D getEdgeColliderFromLevelTemplate() {
			var template = getLevelTemplateFromParentForCurrentDestroyable();
			return template.GetComponentInChildren<EdgeCollider2D>();
		}

		private Transform getLevelTemplateFromParentForCurrentDestroyable() {
			var parent = getParentForCurrentDestroyable();
			return parent.parent;
		}

		private float getOffsetForAlteredHorizontalPositionForCurrentDestroyable() {
			var width = getWidthOfCurrentDestroyable();
			var multiplier = getMultiplierForOffset();
			return width * multiplier;
		}

		private float getWidthOfCurrentDestroyable() {
			var size = getSizeOfCurrentDestroyable();
			return size.x;
		}

		private Vector3 getSizeOfCurrentDestroyable() {
			var bounds = getBoundsOfCurrentDestroyable();
			return bounds.size;
		}

		private Bounds getBoundsOfCurrentDestroyable() {
			var collider = getColliderFromCurrentDestroyable();
			return collider.bounds;
		}

		private Collider2D getColliderFromCurrentDestroyable() {
			var destroyable = getCurrentDestroyable();
			return destroyable.GetComponent<Collider2D>();
		}

		private float getMultiplierForOffset() {
			return 0.5f;
		}

		private float getMaximumValueForAlteredHorizontalPositionForCurrentDestroyable() {
			var stageBound = getRightBoundOfStage();
			var offset = getOffsetForAlteredHorizontalPositionForCurrentDestroyable();
			return stageBound - offset;
		}

		private float getRightBoundOfStage() {
			var bound = getUpperRightBoundOfStage();
			return bound.x;
		}

		private Vector3 getUpperRightBoundOfStage() {
			var bounds = getBoundsOfStage();
			return bounds.max;
		}

		private float getAlteredVerticalPositionForCurrentDestroyable() {
			var min = getMinimumValueForAlteredVerticalPositionForCurrentDestroyable();
			var max = getMaximumValueForAlteredVerticalPositionForCurrentDestroyable();
			return Random.Range(min, max);
		}

		private float getMinimumValueForAlteredVerticalPositionForCurrentDestroyable() {
			var drawingRegionBound = getUpperBoundOfDrawingRegion();
			var offset = getOffsetForAlteredVerticalPositionForCurrentDestroyable();
			return drawingRegionBound + offset;
		}

		private float getUpperBoundOfDrawingRegion() {
			var bound = getUpperRightBoundOfDrawingRegion();
			return bound.y;
		}

		private Vector3 getUpperRightBoundOfDrawingRegion() {
			var bounds = getBoundsOfDrawingRegion();
			return bounds.max;
		}

		private Bounds getBoundsOfDrawingRegion() {
			var collider = getColliderFromDrawingRegion();
			return collider.bounds;
		}

		private Collider2D getColliderFromDrawingRegion() {
			var region = getDrawingRegionFromLevelTemplate();
			return region.GetComponent<Collider2D>();
		}

		private Transform getDrawingRegionFromLevelTemplate() {
			var template = getLevelTemplateFromParentForCurrentDestroyable();
			var name = getNameForDrawingRegion();
			return template.Find(name);
		}

		private string getNameForDrawingRegion() {
			return "DrawRegion";
		}

		private float getOffsetForAlteredVerticalPositionForCurrentDestroyable() {
			var height = getHeightOfCurrentDestroyable();
			var multiplier = getMultiplierForOffset();
			return height * multiplier;
		}

		private float getHeightOfCurrentDestroyable() {
			var size = getSizeOfCurrentDestroyable();
			return size.y;
		}

		private float getMaximumValueForAlteredVerticalPositionForCurrentDestroyable() {
			var stageBound = getUpperBoundOfStage();
			var offset = getOffsetForAlteredVerticalPositionForCurrentDestroyable();
			return stageBound - offset;
		}

		private float getUpperBoundOfStage() {
			var bound = getUpperRightBoundOfStage();
			return bound.y;
		}

		private bool currentDestroyableIsPlacedInOccupiedSpace() {
			int collidersNumber = getNumberOfCollidersOverlappingCurrentDestroyable();
			int value = getValueForCurrentDestroyableBeingPlacedInFreeSpace();
			return collidersNumber > value;
		}

		private int getNumberOfCollidersOverlappingCurrentDestroyable() {
			var collider = getColliderFromCurrentDestroyable();
			var contactFilter = getContactFilterForSpottingCollidersOverlappingCurrentDestroyable();
			var results = getStoreForCollidersOverlappingCurrentDestroyable();
			return collider.OverlapCollider(contactFilter, results);
		}
		// Може перейменувати в "getContactFilterByDefault()"?
//		,
		private ContactFilter2D getContactFilterForSpottingCollidersOverlappingCurrentDestroyable() {
			return new ContactFilter2D();
		}

		private Collider2D[] getStoreForCollidersOverlappingCurrentDestroyable() {
			int rank = getValueForStoreVolume();
			return new Collider2D[rank];
		}

		private int getValueForStoreVolume() {
			return 2;
		}

		private int getValueForCurrentDestroyableBeingPlacedInFreeSpace() {
			return 0;
		}

		private void moveOnToTheNextDestroyable() {
			int value = getValueForNextDestroyableIndex();
			setCurrentDestroyableIndex(value);
		}

		private int getValueForNextDestroyableIndex() {
			int index = getCurrentDestroyableIndex();
			int increment = getValueForMovingOnToTheNextDestroyable();
			return index + increment;
		}

		private int getValueForMovingOnToTheNextDestroyable() {
			return 1;
		}
	}
}