using AssemblyCSharp;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

public class PlayScreen : MonoBehaviour {
	public Text message;
	public GameObject messagePanel;
	private GameObject independentItemMenu;
	private GameObject itemRouteMenu;
	private GameObject bonusesList;

	private void Start() {
		initializePrivateFields();
		hideMessagePanel();
		loadItems();
		prepareLevel();
		hideIndependentItemMenu();
		hideItemRouteMenu();
		hideBonusesList();
		updateTimesPlayedInPlayerProgress();
		unfreezeTime();


//		enabled=playM
		enabled=!levelEditorModeIsEnabled();
//		isen
	}

	private void initializePrivateFields() {
		initializeIndependentItemMenu();
		initializeRouteMenu();
		initializeBonusesList();
	}

	private void initializeIndependentItemMenu() {
		var value = getInitializedIndependentItemMenu();
		setIndependentItemMenu(value);
	}

	private GameObject getInitializedIndependentItemMenu() {
		const string TAG = "Item Menu";
		return GameObject.FindGameObjectWithTag(TAG);
	}

	private void setIndependentItemMenu(GameObject value) {
		independentItemMenu = value;
	}

	private void initializeRouteMenu() {
		var value = getInitializedItemRouteMenu();
		setItemRouteMenu(value);
	}

	private GameObject getInitializedItemRouteMenu() {
		const string TAG = "Route Menu";
		return GameObject.FindGameObjectWithTag(TAG);
	}

	private void setItemRouteMenu(GameObject value) {
		itemRouteMenu = value;
	}

	private void initializeBonusesList() {
		var value = getInitializedBonusesList();
		setBonusesList(value);
	}

	private GameObject getInitializedBonusesList() {
		const string TAG = "Bonuses List";
		return GameObject.FindGameObjectWithTag(TAG);
	}

	private void setBonusesList(GameObject value) {
		bonusesList = value;
	}

	private void hideMessagePanel() {
		var panel = getMessagePanel();
		var value = getValueForMessagePanelBeingInvisible();
		panel.SetActive(value);
	}

	private GameObject getMessagePanel() {
		return messagePanel;
	}

	private bool getValueForMessagePanelBeingInvisible() {
		return false;
	}

	private void loadItems() {
		if (survivalModeIsOn()) {
			loadItemsForSurvival();
		} else {
			loadItemsForChosenLevel();
		}
	}

	private bool survivalModeIsOn() {
		const string KEY = "Survival Mode Is On";
		return PlayerPrefs.HasKey(KEY);
	}

	private void loadItemsForSurvival() {
		loadDestroyables();
		loadBonuses();
	}

	private void loadDestroyables() {
		var loader = getLoaderForDestroyables();
		loader.load();
	}

	private DestroyablesLoader getLoaderForDestroyables() {
		return new DestroyablesLoader();
	}

	private void loadBonuses() {
		var loader = getLoaderForBonuses();
		loader.load();
	}

	private BonusesLoader getLoaderForBonuses() {
		return new BonusesLoader();
	}

	private void loadItemsForChosenLevel() {
		loadDestroyables();
		loadUndestroyables();
		loadBonuses();
	}

	private void loadUndestroyables() {
		var loader = getLoaderForUndestroyables();
		loader.load();
	}

	private UndestroyablesLoader getLoaderForUndestroyables() {
		return new UndestroyablesLoader();
	}

	private void prepareLevel() {
		if (levelEditorModeIsEnabled()) {
			loadLevelInLevelEditorMode();
		} else {
			loadLevelInPlayMode();
		}
	}

	private bool levelEditorModeIsEnabled() {
		var sf = GetComponent<SharedFunctions>();
		return sf.levelEditorModeIsEnabled();
	}

	private void loadLevelInLevelEditorMode() {
		disableDrawingRegionCollisionWithOthers();
		hideStartButton();
		hideTrail();
	}

	private void disableDrawingRegionCollisionWithOthers() {
		const string TAG = "Drawing Region";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var c = go.GetComponent<BoxCollider2D>();
		c.enabled = false;
	}

	private void hideStartButton() {
		const string NAME = "Start Button";
		hide(NAME);
	}

	private void hide(string name) {
		GameObject go = GameObject.FindGameObjectWithTag(name);
		go.SetActive(false);
	}

	private void hideTrail() {
		const string NAME = "Trail";
		hide(NAME);
	}

	private void loadLevelInPlayMode() {
		hideSaveButton();
		hideHelpButton();
		hideLevelEditorPanel();
	}

	private void hideSaveButton() {
		const string NAME = "Save Button";
		hide(NAME);
	}

	private void hideHelpButton() {
		const string NAME = "Help Button";
		hide(NAME);
	}

	private void hideLevelEditorPanel() {
		const string NAME = "Level Editor Panel";
		hide(NAME);
	}

	private void hideIndependentItemMenu() {
		const string NAME = "Item Menu";
		hide(NAME);
	}

	private void hideItemRouteMenu() {
		const string NAME = "Route Menu";
		hide(NAME);
	}

	private void hideBonusesList() {
		const string NAME = "Bonuses List";
		hide(NAME);
	}

	private void updateTimesPlayedInPlayerProgress() {
		if (survivalModeIsOn()) {
			updateTimesPlayedForSurvival();
		} else if (campaignModeIsOn()) {
			updateTimesPlayedForCampaignLevel();
		} else {
			updateTimesPlayedForFreeLevel();
		}
	}		

	private void updateTimesPlayedForSurvival() {
		var playerProgress = getPlayerProgress();
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var survival = playerProgress.survival;
		++survival.timesPlayed;
		serializer.Serialize(stream, playerProgress);
		stream.Close();
	}

	private PlayerProgress getPlayerProgress() {
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
		stream.Close();
		return playerProgress;
	}

	private XmlSerializer getSerializerForPlayerProgress() {
		var type = typeof(PlayerProgress);
		return new XmlSerializer(type);
	}

	private Stream getStreamForPlayerProgress() {
		var path = getPathToPlayerProgress();
		var mode = getFileModeForPlayerProgress();
		return new FileStream(path, mode);
	}

	private string getPathToPlayerProgress() {
		const string KEY = "Path To Player Progress";
		return PlayerPrefs.GetString(KEY);
	}

	private FileMode getFileModeForPlayerProgress() {
		return FileMode.OpenOrCreate;
	}

	private bool campaignModeIsOn() {
		const string KEY = "Chosen Campaign Index";
		return PlayerPrefs.HasKey(KEY);
	}

	private void updateTimesPlayedForCampaignLevel() {
		var playerProgress = getPlayerProgress();
		var serializer = getSerializerForPlayerProgress();
		var campaignIndex = getIndexOfChosenCampaignInPlayerProgress();
		var levelIndex = getIndexOfChosenCampaignLevelInPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var campaigns = playerProgress.campaigns;
		var chosenCampaign = campaigns[campaignIndex];
		var levels = chosenCampaign.levels;
		var chosenLevel = levels[levelIndex];
		++chosenLevel.timesPlayed;
		serializer.Serialize(stream, playerProgress);
		stream.Close();
	}

	private int getIndexOfChosenCampaignInPlayerProgress() {
		var engine = getSearchEngineForCampaignLevels();
		return engine.getChosenCampaignIndex();
	}

	private CampaignLevelsSearchEngine getSearchEngineForCampaignLevels() {
		int targetLevelNumber = getNumberOfTargetCampaignLevel();
		return new CampaignLevelsSearchEngine(targetLevelNumber);
	}

	private int getNumberOfTargetCampaignLevel() {
		var level = getChosenLevelInCampaign();
		return level.number;
	}

	private Level getChosenLevelInCampaign() {
		var levels = getLevelsOfChosenCampaign();
		var index = getIndexOfChosenLevel();
		return levels[index];
	}

	private List<Level> getLevelsOfChosenCampaign() {
		var campaign = getChosenCampaignFromLevelsSet();
		return campaign.levels;
	}

	private Campaign getChosenCampaignFromLevelsSet() {
		var campaigns = getCampaignsFromLevelsSet();
		var index = getIndexOfChosenCampaign();
		return campaigns[index];
	}		

	private List<Campaign> getCampaignsFromLevelsSet() {
		var levels = getSetOfLevels();
		return levels.campaigns;
	}

	private Levels getSetOfLevels() {
		
		//У скрипті MainMenu.cs провернути щось подібне:
		//https://stackoverflow.com/questions/2407302/convert-xmldocument-to-string
		//(це аби можна було запихувати "Levels Set.xml" в string навіть в стані (режимі) редактора рівнів).
		//		if (levelEditorModeIsOn()) {
		//			return getSetOfLevelsInLevelEditorMode();
		//		} else {
		//			return getSetOfLevelsInPlayMode();
		//		}
		//levelEditorModeIsOn
		if (PlayerPrefs.HasKey ("Level Editor Mode")) {
			//getSetOfLevelsInLevelEditorMode
			var serializer = getSerializerForLevelsSet();
			var stream = new FileStream (PlayerPrefs.GetString ("Path To Levels"), FileMode.OpenOrCreate);
			var o = serializer.Deserialize (stream) as Levels;
			stream.Close();
			return o;

			//float
		} else {
			//getSetOfLevelsInPlayMode
			var serializer = getSerializerForLevelsSet();
			var textReader = getTextReaderForLevelsSet();
			return serializer.Deserialize(textReader) as Levels;	
		}

//		var serializer = getSerializerForLevelsSet();
//		var textReader = getTextReaderForLevelsSet();
//		return serializer.Deserialize(textReader) as Levels;
	}

	private XmlSerializer getSerializerForLevelsSet() {
		var type = typeof(Levels);
		return new XmlSerializer(type);
	}

	private TextReader getTextReaderForLevelsSet() {
		var s = getStringForLevelsSet();
		return new StringReader(s);
	}

	private string getStringForLevelsSet() {
		var key = getKeyForLevelsSet();
		return PlayerPrefs.GetString(key);
	}

	private string getKeyForLevelsSet() {
		return "Levels";
	}

	private int getIndexOfChosenCampaign() {
		var key = getKeyForChosenCampaignIndex();
		return PlayerPrefs.GetInt(key);
	}

	private string getKeyForChosenCampaignIndex() {
		return "Chosen Campaign Index";
	}

	private int getIndexOfChosenLevel() {
		var key = getKeyForChosenLevelIndex();
		return PlayerPrefs.GetInt(key);
	}

	private string getKeyForChosenLevelIndex() {
		return "Chosen Level Index";
	}

	private int getIndexOfChosenCampaignLevelInPlayerProgress() {
		var engine = getSearchEngineForCampaignLevels();
		return engine.getChosenLevelIndex();
	}

	private void updateTimesPlayedForFreeLevel() {
		var playerProgress = getPlayerProgress();
		var serializer = getSerializerForPlayerProgress();
		var index = getIndexOfChosenFreeLevel();
		var stream = getStreamForPlayerProgress();
		var freeLevels = playerProgress.freeLevels;
		var chosenLevel = freeLevels[index];
		++chosenLevel.timesPlayed;
		serializer.Serialize(stream, playerProgress);
		stream.Dispose();
	}

	private int getIndexOfChosenFreeLevel() {
		var engine = getSearchEngineForFreeLevels();
		return engine.getChosenLevelIndex();
	}

	private FreeLevelsSearchEngine getSearchEngineForFreeLevels() {
		int targetLevelNumber = getNumberOfTargetFreeLevel();
		return new FreeLevelsSearchEngine(targetLevelNumber);
	}

	private int getNumberOfTargetFreeLevel() {
		var level = getTargetFreeLevelFromLevelsSet();
		return level.number;
	}

	private Level getTargetFreeLevelFromLevelsSet() {
		var levels = getFreeLevelsFromLevelsSet();
		var index = getIndexOfChosenLevel();
		return levels[index];
	}

	private List<Level> getFreeLevelsFromLevelsSet() {
		var levelsSet = getSetOfLevels();
		return levelsSet.freeLevels;
	}

	private void unfreezeTime() {
		var value = getValueForUnfreezingTime();
		Time.timeScale = value;
	}

	private float getValueForUnfreezingTime() {
		return 1f;
	}

	public void onBlockDestroyed() {
		if (allDestroyablesAreGone()) {
			processDestroyingOfAllDestroyables();
		}
	}

	private bool allDestroyablesAreGone() {
		var sf = GetComponent<SharedFunctions>();
		var go = sf.getDestroyables();
		int n = go.Length;
		return n == 0;
	}

	private void processDestroyingOfAllDestroyables() {
		giveCoinsToPlayer();
		chooseBetweenLoadingItemsAndEndingTheGame();
	}

	private void giveCoinsToPlayer() {
		var balance = getBalanceOfCoinsOnPlayScreen();
		var value = getValueForUpdatingBalanceOfCoinsOnPlayScreen();
		balance.text = value;
	}

	private Text getBalanceOfCoinsOnPlayScreen() {
		var balanceTransform = getTransformForBalanceOfCoinsOnPlayScreen();
		return balanceTransform.GetComponent<Text>();
	}

	private Transform getTransformForBalanceOfCoinsOnPlayScreen() {
		var levelTemplate = getTemplateOfLevel();
		var name = getNameForTransformForBalanceOfCoinsOnPlayScreen();
		return levelTemplate.Find(name);
	}

	private Transform getTemplateOfLevel() {
		return transform.parent;
	}

	private string getNameForTransformForBalanceOfCoinsOnPlayScreen() {
		return "Canvas/Coins/Value";
	}

	private string getValueForUpdatingBalanceOfCoinsOnPlayScreen() {
		var value = getIntegerRepresentationOfUpdatedBalanceOfCoinsOnPlayScreen();
		return value.ToString();
	}

	private int getIntegerRepresentationOfUpdatedBalanceOfCoinsOnPlayScreen() {
		int balance = getOldBalanceOfCoinsOnPlayScreen();
		int increment = getIncrementForUpdatingBalanceOfCoinsOnPlayScreen();
		return balance + increment;
	}

	private int getOldBalanceOfCoinsOnPlayScreen() {
		var s = getStringRepresentationOfOldBalanceOfCoinsOnPlayScreen();
		return int.Parse(s);
	}

	private string getStringRepresentationOfOldBalanceOfCoinsOnPlayScreen() {
		var balance = getBalanceOfCoinsOnPlayScreen();
		return balance.text;
	}

	private int getIncrementForUpdatingBalanceOfCoinsOnPlayScreen() {
		return 50;
	}

	private void chooseBetweenLoadingItemsAndEndingTheGame() {
		if (survivalModeIsOn()) {
			loadItemsForSurvival();
		} else {
			informPlayerAboutSuccess();
		}
	}

	private void informPlayerAboutSuccess() {		
		var value = getTextForInformingPlayerAboutSuccess();
		var content = getMessage();
		var panel = getMessagePanel();
		content.text = value;
		panel.SetActive(true);
	}

	private string getTextForInformingPlayerAboutSuccess() {
		return "You have finished this level.";
	}

	private Text getMessage() {
		return message;
	}

	public void onBallDestroyed() {
		if (allFailureConditionsHaveBeenPassed()) {
			informPlayerAboutFailure();
		}
	}

	public bool allFailureConditionsHaveBeenPassed() {
		bool ba = allBallsAreGone();
		bool bb = thereAreSomeDestroyablesLeft();
		return ba && bb;
	}

	private bool allBallsAreGone() {
		var sf = GetComponent<SharedFunctions>();
		var go = sf.getBalls();
		int n = go.Length;
		return n == 0;
	}

	private bool thereAreSomeDestroyablesLeft() {
		return !allDestroyablesAreGone();
	}

	private void informPlayerAboutFailure() {
		var value = getTextForInformingPlayerAboutFailure();
		var content = getMessage();
		var panel = getMessagePanel();
		content.text = value;
		panel.SetActive(true);
	}

	private string getTextForInformingPlayerAboutFailure() {
		return "Level failed.";
	}

	private void Update() {
		updateTime();
	}

	private void updateTime() {
		var time = getTextComponentFromTransformOfTimeValue();
		var value = getValueForUpdatingTime();
		time.text = value;
	}

	private Text getTextComponentFromTransformOfTimeValue() {
		var timeValue = getTransformOfTimeValue();
		return timeValue.GetComponent<Text>();
	}

	private Transform getTransformOfTimeValue() {
		var levelTemplate = getTemplateOfLevel();
		var name = getNameForTransformOfTimeValue();
		return levelTemplate.Find(name);
	}

	private string getNameForTransformOfTimeValue() {
		return "Canvas/Time/Value";
	}

	private string getValueForUpdatingTime() {
		var a = getProcessedTimeInMinutesWithSeparator();
		var b = getProcessedTimeInSeconds();
		return a + b;
	}

	private string getProcessedTimeInMinutesWithSeparator() {
		var a = getProcessedNumberForTimeInMinutes();
		var b = getSeparatorForTime();
		return a + b;
	}

	private string getProcessedNumberForTimeInMinutes() {
		int number = getNumberForTimeInMinutes();
		return getProcessed(number);
	}

	private int getNumberForTimeInMinutes() {
		int time = getFloorOfTimeSinceLevelLoad();
		int divider = getDividerForTimeInMinutes();
		return time / divider;
	}

	private int getFloorOfTimeSinceLevelLoad() {
		var f = getTimeSinceLevelLoadForPlayScreen();
		return Mathf.FloorToInt(f);
	}

	private float getTimeSinceLevelLoadForPlayScreen() {
		return Time.timeSinceLevelLoad;
	}

	private int getDividerForTimeInMinutes() {
		return 60;
	}
	//getAdjusted
	private string getProcessed(int number) {
		if (thereIsOnlyOneDigitInThe(number)) {
			       //getProcessed
			return getAdjusted(number);
		} else {
			return getUnprocessed(number);
		}
	}

	private bool thereIsOnlyOneDigitInThe(int number) {
		int value = getValueForNumberHavingOnlyOneDigit();
		return number < value;
	}

	private int getValueForNumberHavingOnlyOneDigit() {
		return 10;
	}

	private string getAdjusted(int number) {
		var value = getValueForAdjustingTheNumber();
		return value + number;
	}

	private string getValueForAdjustingTheNumber() {
		return "0";
	}

	private string getUnprocessed(int number) {
		return number.ToString();
	}

	private string getSeparatorForTime() {
		return ":";
	}

	private string getProcessedTimeInSeconds() {
		int number = getNumberForTimeInSeconds();
		return getProcessed(number);
	}

	private int getNumberForTimeInSeconds() {
		int time = getFloorOfTimeSinceLevelLoad();
		int divider = getDividerForTimeInSeconds();
		return time % divider;
	}

	private int getDividerForTimeInSeconds() {
		return getDividerForTimeInMinutes();
	}

	public void updatePlayerProgress() {
		var playerProgress = getPlayerProgress();
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		playerProgress.gameTimeInSeconds += getIncrementForGameTimeInSeconds();
		playerProgress.coinsEarned += getIncrementForCoinsEarned();
		playerProgress.balanceInCoins += getIncrementForBalanceInCoins();
		serializer.Serialize(stream, playerProgress);
		stream.Close();
	}

	private int getIncrementForGameTimeInSeconds() {
		return getFloorOfTimeSinceLevelLoad();
	}

	private int getIncrementForCoinsEarned() {
		return getOldBalanceOfCoinsOnPlayScreen();
	}

	private int getIncrementForBalanceInCoins() {
		return getIncrementForCoinsEarned();
	}

	public GameObject getIndependentItemMenu() {
		return independentItemMenu;
	}

	public GameObject getItemRouteMenu() {
		return itemRouteMenu;
	}

	public GameObject getBonusesList() {
		return bonusesList;
	}
}