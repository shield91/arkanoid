using UnityEngine;

namespace AssemblyCSharp
{
	public class FireballsTransformer : BallsAdjuster {
		protected override void adjustBall() {
			makeCurrentBallToLookLikeFireball();
			increaseCurrentBallHitPower();
		}

		private void makeCurrentBallToLookLikeFireball() {
			const string PROPERTY_NAME = "_Color";
			const float R = 1f;
			const float G = 0.5f;
			const float B = 0f;
			var ball = getCurrentBallByItsPosition();
			var color = new Color(R, G, B);
			var r = ball.GetComponent<Renderer>();
			var m = r.material;
			m.SetColor(PROPERTY_NAME, color);
		}

		private void increaseCurrentBallHitPower() {
			var b = getBallScriptFromCurrentBall();
			int oldHitPower = b.getBallHitPower();
			int newHitPower = oldHitPower + 1;
			b.setBallHitPower(newHitPower);
		}
	}
}