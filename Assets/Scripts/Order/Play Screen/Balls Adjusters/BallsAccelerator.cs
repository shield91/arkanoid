namespace AssemblyCSharp
{
	public class BallsAccelerator : BallsSpeedAdjuster {
		private float maximum;

		public BallsAccelerator(float top = 40f, float increment = 10f) : base(increment) {
			var value = top;
			setMaximum(value);
		}

		private void setMaximum(float value) {
			maximum = value;
		}

		protected override void adjustBall() {
			var ball = getBallScriptFromCurrentBall();
			bool b = currentBallSpeedHasReachedItsMaximum();
			float oldVelocity = ball.velocity;
			float delta = getDelta();
			var limit = getMaximum();
			float newVelocity = b ? limit : oldVelocity + delta;
			ball.velocity = newVelocity;
		}

		private bool currentBallSpeedHasReachedItsMaximum() {
			var b = getBallScriptFromCurrentBall();
			float velocity = b.velocity;
			float delta = getDelta();
			var limit = getMaximum();
			return velocity + delta >= limit;
		}

		private float getMaximum() {
			return maximum;
		}
	}
}