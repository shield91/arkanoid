using UnityEngine;

namespace AssemblyCSharp
{
	public abstract class BallsAdjuster {
		private int currentBallPosition;

		public BallsAdjuster() {			
			resetCurrentBallPosition();
		}

		private void resetCurrentBallPosition() {			
			int value = getValueForCurrentBallPositionByDefault();
			setCurrentBallPosition(value);
		}

		private int getValueForCurrentBallPositionByDefault() {
			return 0;
		}

		private void setCurrentBallPosition(int value) {
			currentBallPosition = value;
		}

		public void adjustBallsOnStage() {
			resetCurrentBallPosition();
			adjustBalls();
		}			

		private void adjustBalls() {
			while (thereAreBallsNeedToBeAdjusted()) {
				adjustCurrentBall();
			}
		}

		private bool thereAreBallsNeedToBeAdjusted() {			
			int i = getCurrentBallPosition();
			int n = getNumberOfBalls();
			return i < n;
		}

		private int getCurrentBallPosition() {
			return currentBallPosition;
		}

		private int getNumberOfBalls() {
			var balls = getBallsThemselves();
			return balls.Length;
		}
		
		protected virtual GameObject[] getBallsThemselves() {
			const string TAG = "Level";
			var go = GameObject.FindGameObjectWithTag(TAG);
			var sf = go.GetComponent<SharedFunctions>();
			return sf.getBalls();
		}

		private void adjustCurrentBall() {
			adjustBall();
			moveOnToTheNextBall();
		}

		protected abstract void adjustBall();

		protected Ball getBallScriptFromCurrentBall() {
			GameObject go = getCurrentBallByItsPosition();
			return go.GetComponent<Ball>();
		}

		protected GameObject getCurrentBallByItsPosition() {			
			var index = getCurrentBallPosition();
			var balls = getBallsThemselves();
			return balls[index];
		}

		private void moveOnToTheNextBall() {
			int value = getValueForMovingOnToTheNextBall();
			setCurrentBallPosition(value);
		}

		private int getValueForMovingOnToTheNextBall() {
			int index = getCurrentBallPosition();
			int increment = getIncrementForMovingOnToTheNextPosition();
			return index + increment;
		}

		private int getIncrementForMovingOnToTheNextPosition() {
			return 1;
		}
	}
}