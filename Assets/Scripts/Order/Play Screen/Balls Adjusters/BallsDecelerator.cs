namespace AssemblyCSharp
{
	public class BallsDecelerator : BallsSpeedAdjuster {
		private float minimum;

		public BallsDecelerator(float bottom = 5f, float increment = 10f) : base(increment) {
			var value = bottom;
			setMinimum(value);
		}

		private void setMinimum(float value) {
			minimum = value;
		}

		protected override void adjustBall() {
			var ball = getBallScriptFromCurrentBall();
			bool b = currentBallSpeedHasReachedItsMinimum();
			float oldVelocity = ball.velocity;
			float delta = getDelta();
			var limit = getMinimum();
			float newVelocity = b ? limit : oldVelocity - delta;
			ball.velocity = newVelocity;
		}			

		private bool currentBallSpeedHasReachedItsMinimum() {
			var b = getBallScriptFromCurrentBall();
			float velocity = b.velocity;
			float delta = getDelta();
			var limit = getMinimum();
			return velocity - delta <= limit;
		}

		private float getMinimum() {
			return minimum;
		}
	}
}