﻿using UnityEngine;

namespace AssemblyCSharp {
	public class BallsSizeTransformer : BallsAdjuster {
		private float volume;

		public BallsSizeTransformer(float size = 0.4f) {
			var value = size;
			setVolume(value);
		}

		private void setVolume(float value) {
			volume = value;
		}

		protected override void adjustBall() {
			var transform = getTransformFromCurrentBall();
			var value = getValueForAdjustingLocalScale();
			transform.localScale = value;
		}

		private Transform getTransformFromCurrentBall() {
			var go = getCurrentBallByItsPosition();
			return go.transform;
		}

		private Vector3 getValueForAdjustingLocalScale() {
			var scale = getDefaultValueForLocalScale();
			var size = getVolume();
			return scale * size;
		}

		private Vector3 getDefaultValueForLocalScale() {
			return Vector3.one;
		}

		private float getVolume() {
			return volume;
		}
	}
}