namespace AssemblyCSharp
{
	public abstract class BallsSpeedAdjuster : BallsAdjuster {
		private float delta;

		public BallsSpeedAdjuster(float increment = 10f) {
			var value = increment;
			setDelta(value);
		}

		private void setDelta(float value) {
			delta = value;
		}

		protected float getDelta() {
			return delta;
		}
	}
}