using UnityEngine;

namespace AssemblyCSharp
{
	public class OrdinaryBallsTransformer : BallsAdjuster {
		protected override void adjustBall() {
			makeCurrentBallToLookLikeOrdinaryBall();
			decreaseCurrentBallHitPower();
		}
		
		private void makeCurrentBallToLookLikeOrdinaryBall() {
			const string PROPERTY_NAME = "_Color";
			const float R = 1f;
			const float G = 1f;
			const float B = 1f;
			var ball = getCurrentBallByItsPosition();
			var color = new Color(R, G, B);
			var r = ball.GetComponent<Renderer>();
			var m = r.material;
			m.SetColor(PROPERTY_NAME, color);
		}
		
		private void decreaseCurrentBallHitPower() {
			var b = getBallScriptFromCurrentBall();
			int oldHitPower = b.getBallHitPower();
			int newHitPower = oldHitPower - 1;
			b.setBallHitPower(newHitPower);
		}
	}
}