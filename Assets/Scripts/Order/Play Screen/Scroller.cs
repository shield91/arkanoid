﻿using UnityEngine;

public class Scroller : MonoBehaviour {
	private int currentBonusIndex;

	private void Start() {
		resetCurrentBonusIndex();
	}

	private void resetCurrentBonusIndex() {
		if (previousButtonIsPressed()) {
			resetCurrentBonusIndexInCaseOfPreviousButtonBeingPressed();
		} else if (nextButtonIsPressed()) {
			resetCurrentBonusIndexInCaseOfNextButtonBeingPressed();
		}
	}

	private bool previousButtonIsPressed() {
		var value = getNameForPreviousButton();
		return name.Equals(value);
	}

	private string getNameForPreviousButton() {
		return "Previous";
	}

	private void resetCurrentBonusIndexInCaseOfPreviousButtonBeingPressed() {
		int value = getCurrentBonusIndexFromNextButton();
		setCurrentBonusIndex(value);
	}

	private int getCurrentBonusIndexFromNextButton() {
		var s = getScrollerComponentFromNextButton();
		return s.getCurrentBonusIndex();
	}

	private Scroller getScrollerComponentFromNextButton() {
		var t = getTransformForNextButton();
		return t.GetComponent<Scroller>();
	}

	private Transform getTransformForNextButton() {
		var t = getTransformForScrollPanel();
		var name = getNameForNextButton();
		return t.Find(name);
	}

	private Transform getTransformForScrollPanel() {
		return transform.parent;
	}

	private string getNameForNextButton() {
		return "Next";
	}

	private int getCurrentBonusIndex() {
		return currentBonusIndex;
	}

	private void setCurrentBonusIndex (int value) {
		currentBonusIndex = value;
	}

	private bool nextButtonIsPressed() {
		var value = getNameForNextButton();
		return name.Equals(value);
	}

	private void resetCurrentBonusIndexInCaseOfNextButtonBeingPressed() {
		int value = getValueForCurrentBonusIndexByDefault();
		setCurrentBonusIndex(value);
	}

	private int getValueForCurrentBonusIndexByDefault() {
		return 1;
	}

	private void OnMouseDown() {
		highlightButton();
		scroll();
	}

	private void highlightButton() {
		var value = getValueForHighlightedButtonColor();
		var sr = getSpriteRendererComponent();
		sr.color = value;
	}

	private Color getValueForHighlightedButtonColor() {
		var r = getRedComponentForHighlightedButtonColor();
		var g = getGreenComponentForHighlightedButtonColor();
		var b = getBlueComponentForHighlightedButtonColor();
		return new Color(r, g, b);
	}

	private float getRedComponentForHighlightedButtonColor() {
		var n = getCurrentValueForHighlightedButtonColorRedComponent();
		var m = getMaximumForHighlightedButtonColorRedComponent();
		return n / m;
	}

	private float getCurrentValueForHighlightedButtonColorRedComponent() {
		return 255f;
	}

	private float getMaximumForHighlightedButtonColorRedComponent() {
		return 255f;
	}

	private float getGreenComponentForHighlightedButtonColor() {
		var n = getCurrentValueForHighlightedButtonColorGreenComponent();
		var m = getMaximumForHighlightedButtonColorGreenComponent();
		return n / m;
	}

	private float getCurrentValueForHighlightedButtonColorGreenComponent() {
		return 128f;
	}

	private float getMaximumForHighlightedButtonColorGreenComponent() {
		return 255f;
	}

	private float getBlueComponentForHighlightedButtonColor() {
		var n = getCurrentValueForHighlightedButtonColorBlueComponent();
		var m = getMaximumForHighlightedButtonColorBlueComponent();
		return n / m;
	}

	private float getCurrentValueForHighlightedButtonColorBlueComponent() {
		return 0f;
	}

	private float getMaximumForHighlightedButtonColorBlueComponent() {
		return 255f;
	}

	private SpriteRenderer getSpriteRendererComponent() {
		return GetComponent<SpriteRenderer>();
	}

	private void scroll() {
		if (previousButtonIsPressed()) {
			scrollBackward();
		} else if (nextButtonIsPressed()) {
			scrollForward();
		}
	}

	private void scrollBackward() {
		hideThirdVisibleBonus();
		shiftFirstVisibleBonusInDirectionOfThirdOne();
		shiftSecondVisibleBonusInDirectionOfThirdOne();
		showFirstVisibleBonus();
		moveOnToThePreviousBonus();
		setUpNextButtonVisibility();
	}

	private void hideThirdVisibleBonus() {
		var go = getGameObjectForThirdVisibleBonusInCaseOfScrollingBackward();
		var value = getValueForThirdVisibleBonusBeingInvisible();
		go.SetActive(value);
	}

	private GameObject getGameObjectForThirdVisibleBonusInCaseOfScrollingBackward() {
		var t = getTransformForThirdVisibleBonus();
		return t.gameObject;
	}

	private Transform getTransformForThirdVisibleBonus() {
		var t = getTransformForScrollPanel();
		var index = getIndexOfThirdVisibleBonusInCaseOfScrollingBackward();
		return t.GetChild(index);
	}

	private int getIndexOfThirdVisibleBonusInCaseOfScrollingBackward() {
		int index = getCurrentBonusIndex();
		int value = getValueForAccessingThirdVisibleBonusInCaseOfScrollingBackward();
		return index + value;
	}

	private int getValueForAccessingThirdVisibleBonusInCaseOfScrollingBackward() {
		return 2;
	}

	private bool getValueForThirdVisibleBonusBeingInvisible() {
		return false;
	}

	private void shiftFirstVisibleBonusInDirectionOfThirdOne() {
		var rb = getRigidbodyComponentFromFirstVisibleBonus();
		var value = getPositionOfSecondVisibleBonus();
		rb.position = value;
	}

	private Rigidbody2D getRigidbodyComponentFromFirstVisibleBonus() {
		var t = getTransformForFirstVisibleBonus();
		return t.GetComponent<Rigidbody2D>();
	}

	private Transform getTransformForFirstVisibleBonus() {
		var t = getTransformForScrollPanel();
		var index = getCurrentBonusIndex();
		return t.GetChild(index);
	}

	private Vector3 getPositionOfSecondVisibleBonus() {
		var t = getTransformForSecondVisibleBonus();
		return t.position;
	}

	private Transform getTransformForSecondVisibleBonus() {
		var t = getTransformForScrollPanel();
		var index = getIndexOfSecondVisibleBonus();
		return t.GetChild(index);
	}

	private int getIndexOfSecondVisibleBonus() {
		int index = getCurrentBonusIndex();
		int value = getValueForAccessingSecondVisibleBonus();
		return index + value;
	}

	private int getValueForAccessingSecondVisibleBonus() {
		return 1;
	}

	private void shiftSecondVisibleBonusInDirectionOfThirdOne() {
		var rb = getRigidbodyComponentFromSecondVisibleBonus();
		var value = getPositionOfThirdVisibleBonus();
		rb.position = value;
	}

	private Rigidbody2D getRigidbodyComponentFromSecondVisibleBonus() {
		var t = getTransformForSecondVisibleBonus();
		return t.GetComponent<Rigidbody2D>();
	}

	private Vector3 getPositionOfThirdVisibleBonus() {
		var t = getTransformForThirdVisibleBonus();
		return t.position;
	}

	private void showFirstVisibleBonus() {
		var go = getGameObjectForFirstVisibleBonus();
		var value = getValueForFirstVisibleBonusBeingVisible();
		go.SetActive(value);
	}

	private GameObject getGameObjectForFirstVisibleBonus() {
		var t = getTransformForUpdatedFirstVisibleBonus();
		return t.gameObject;
	}

	private Transform getTransformForUpdatedFirstVisibleBonus() {
		var t = getTransformForScrollPanel();
		var index = getIndexOfUpdatedFirstVisibleBonus();
		return t.GetChild(index);
	}

	private int getIndexOfUpdatedFirstVisibleBonus() {
		int index = getCurrentBonusIndex();
		int value = getValueForAccessingUpdatedFirstVisibleBonus();
		return index - value;
	}

	private int getValueForAccessingUpdatedFirstVisibleBonus() {
		return 1;
	}

	private bool getValueForFirstVisibleBonusBeingVisible() {
		return true;
	}

	private void moveOnToThePreviousBonus() {
		rollbackCurrentBonusIndex();
		setCurrentBonusIndexForNextButton();
	}

	private void rollbackCurrentBonusIndex() {
		int value = getValueForRollbackedCurrentBonusIndex();
		setCurrentBonusIndex(value);
	}

	private int getValueForRollbackedCurrentBonusIndex() {
		int index = getCurrentBonusIndex();
		int decrement = getDecrementForCurrentBonusIndex();
		return index - decrement;
	}

	private int getDecrementForCurrentBonusIndex() {
		return 1;
	}

	private void setCurrentBonusIndexForNextButton() {
		var s = getScrollerComponentFromNextButton();
		var value = getCurrentBonusIndex();
		s.setCurrentBonusIndex(value);
	}

	private void setUpNextButtonVisibility() {
		var go = getGameObjectForNextButton();
		var value = getValueForNextButtonVisibility();
		go.SetActive(value);
	}

	private GameObject getGameObjectForNextButton() {
		var t = getTransformForNextButton();
		return t.gameObject;
	}

	private bool getValueForNextButtonVisibility() {
		int index = getCurrentBonusIndex();
		int value = getValueForDefiningNextButtonVisibility();
		return index < value;
	}

	private int getValueForDefiningNextButtonVisibility() {
		int n = getNumberOfItemsOnScrollPanel();
		int m = getValueForNextButtonVisibilityLimit();
		return n - m;
	}

	private int getNumberOfItemsOnScrollPanel() {
		var t = getTransformForScrollPanel();
		return t.childCount;
	}

	private int getValueForNextButtonVisibilityLimit() {
		return 4;
	}

	private void scrollForward() {
		hideFirstVisibleBonus();
		shiftThirdVisibleBonusInDirectionOfFirstOne();
		shiftSecondVisibleBonusInDirectionOfFirstOne();
		showThirdVisibleBonus();
		moveOnToTheNextBonus();
		setUpPreviousButtonVisibility();
	}

	private void hideFirstVisibleBonus() {
		var go = getGameObjectForFirstVisibleBonusInCaseOfScrollingForward();
		var value = getValueForFirstVisibleBonusBeingInvisible();
		go.SetActive(value);
	}

	private GameObject getGameObjectForFirstVisibleBonusInCaseOfScrollingForward() {
		var t = getTransformForFirstVisibleBonus();
		return t.gameObject;
	}

	private bool getValueForFirstVisibleBonusBeingInvisible() {
		return false;
	}

	private void shiftThirdVisibleBonusInDirectionOfFirstOne() {
		var rb = getRigidbodyComponentFromThirdVisibleBonus();
		var value = getPositionOfSecondVisibleBonus();
		rb.position = value;
	}

	private Rigidbody2D getRigidbodyComponentFromThirdVisibleBonus() {
		var t = getTransformForThirdVisibleBonus();
		return t.GetComponent<Rigidbody2D>();
	}

	private void shiftSecondVisibleBonusInDirectionOfFirstOne() {
		var rb = getRigidbodyComponentFromSecondVisibleBonus();
		var value = getPositionOfFirstVisibleBonus();
		rb.position = value;
	}

	private Vector3 getPositionOfFirstVisibleBonus() {
		var t = getTransformForFirstVisibleBonus();
		return t.position;
	}

	private void showThirdVisibleBonus() {
		var go = getGameObjectForThirdVisibleBonusInCaseOfScrollingForward();
		var value = getValueForThirdVisibleBonusBeingVisible();
		go.SetActive(value);
	}

	private GameObject getGameObjectForThirdVisibleBonusInCaseOfScrollingForward() {
		var t = getTransformForThirdVisibleBonusInCaseOfScrollingForward();
		return t.gameObject;
	}

	private Transform getTransformForThirdVisibleBonusInCaseOfScrollingForward() {
		var t = getTransformForScrollPanel();
		var index = getIndexOfThirdVisibleBonusInCaseOfScrollingForward();
		return t.GetChild(index);
	}

	private int getIndexOfThirdVisibleBonusInCaseOfScrollingForward() {
		int index = getCurrentBonusIndex();
		int value = getValueForAccessingThirdVisibleBonusInCaseOfScrollingForward();
		return index + value;
	}

	private int getValueForAccessingThirdVisibleBonusInCaseOfScrollingForward() {
		return 3;
	}

	private bool getValueForThirdVisibleBonusBeingVisible() {
		return true;
	}

	private void moveOnToTheNextBonus() {
		updateCurrentBonusIndex();
		setCurrentBonusIndexForPreviousButton();
	}

	private void updateCurrentBonusIndex() {
		int value = getValueForUpdatedCurrentBonusIndex();
		setCurrentBonusIndex(value);
	}

	private int getValueForUpdatedCurrentBonusIndex() {
		int index = getCurrentBonusIndex();
		int increment = getIncrementForCurrentBonusIndex();
		return index + increment;
	}

	private int getIncrementForCurrentBonusIndex() {
		return 1;
	}

	private void setCurrentBonusIndexForPreviousButton() {
		var s = getScrollerComponentFromPreviousButton();
		var value = getCurrentBonusIndex();
		s.setCurrentBonusIndex(value);
	}

	private Scroller getScrollerComponentFromPreviousButton() {
		var t = getTransformForPreviousButton();
		return t.GetComponent<Scroller>();
	}

	private Transform getTransformForPreviousButton() {
		var t = getTransformForScrollPanel();
		var name = getNameForPreviousButton();
		return t.Find(name);
	}

	private void setUpPreviousButtonVisibility() {
		var go = getGameObjectForPreviousButton();
		var value = getValueForPreviousButtonVisibility();
		go.SetActive(value);
	}

	private GameObject getGameObjectForPreviousButton() {
		var t = getTransformForPreviousButton();
		return t.gameObject;
	}

	private bool getValueForPreviousButtonVisibility() {
		int index = getCurrentBonusIndex();
		int value = getValueForDefiningPreviousButtonVisibility();
		return index > value;
	}

	private int getValueForDefiningPreviousButtonVisibility() {
		return 1;
	}

	private void OnMouseUp() {
		unhighlightButton();
		setUpButtonVisibility();
	}

	private void unhighlightButton() {
		var value = getValueForUnhighlightedButtonColor();
		var sr = getSpriteRendererComponent();
		sr.color = value;
	}

	private Color getValueForUnhighlightedButtonColor() {
		var r = getRedComponentForUnhighlightedButtonColor();
		var g = getGreenComponentForUnhighlightedButtonColor();
		var b = getBlueComponentForUnhighlightedButtonColor();
		return new Color(r, g, b);
	}

	private float getRedComponentForUnhighlightedButtonColor() {
		var n = getCurrentValueForUnhighlightedButtonColorRedComponent();
		var m = getMaximumForUnhighlightedButtonColorRedComponent();
		return n / m;
	}

	private float getCurrentValueForUnhighlightedButtonColorRedComponent() {
		return 0f;
	}

	private float getMaximumForUnhighlightedButtonColorRedComponent() {
		return 255f;
	}

	private float getGreenComponentForUnhighlightedButtonColor() {
		var n = getCurrentValueForUnhighlightedButtonColorGreenComponent();
		var m = getMaximumForUnhighlightedButtonColorGreenComponent();
		return n / m;
	}

	private float getCurrentValueForUnhighlightedButtonColorGreenComponent() {
		return 82f;
	}

	private float getMaximumForUnhighlightedButtonColorGreenComponent() {
		return 255f;
	}

	private float getBlueComponentForUnhighlightedButtonColor() {
		var n = getCurrentValueForUnhighlightedButtonColorBlueComponent();
		var m = getMaximumForUnhighlightedButtonColorBlueComponent();
		return n / m;
	}

	private float getCurrentValueForUnhighlightedButtonColorBlueComponent() {
		return 255f;
	}

	private float getMaximumForUnhighlightedButtonColorBlueComponent() {
		return 255f;
	}

	private void setUpButtonVisibility() {
		if (previousButtonIsPressed()) {
			setUpPreviousButtonVisibility();
		} else if (nextButtonIsPressed()) {
			setUpNextButtonVisibility();
		}
	}
}