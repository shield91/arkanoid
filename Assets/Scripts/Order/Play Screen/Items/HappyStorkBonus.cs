﻿using UnityEngine;
using UnityEngine.UI;

public class HappyStorkBonus : Bonus {
	protected override void apply() {
		increaseBallsInReserve();
	}

	private void increaseBallsInReserve() {
		var t = getTextComponentForNumberOfBalls();
		var value = getValueForUpdatedNumberOfBalls();
		t.text = value;
	}

	private Text getTextComponentForNumberOfBalls() {
		var t = getTransformForNumberOfBalls();
		return t.GetComponent<Text>();
	}

	private Transform getTransformForNumberOfBalls() {
		var template = getTemplateOfLevel();
		var name = getNameForNumberOfBalls();
		return template.Find(name);
	}

	private Transform getTemplateOfLevel() {
		var t = getTransformFromLevel();
		return t.parent;
	}

	private Transform getTransformFromLevel() {
		var go = getGameObjectForLevel();
		return go.transform;
	}

	private GameObject getGameObjectForLevel() {
		var tag = getTagForLevel();
		return GameObject.FindWithTag(tag);
	}

	private string getTagForLevel() {
		return "Level";
	}

	private string getNameForNumberOfBalls() {
		return "Canvas/Balls/Value";
	}

	private string getValueForUpdatedNumberOfBalls() {
		int value = getIntegerRepresentationOfValueForUpdatedNumberOfBalls();
		return value.ToString();
	}

	private int getIntegerRepresentationOfValueForUpdatedNumberOfBalls() {
		int a = getUpdatedNumberOfBalls();
		int b = getMaximumForUpdatedNumberOfBalls();
		return Mathf.Min(a, b);
	}

	private int getUpdatedNumberOfBalls() {
		int number = getNumberOfBalls();
		int increment = getValueForUpdatingNumberOfBalls();
		return number + increment;
	}

	private int getNumberOfBalls() {
		var s = getStringRepresentationOfNumberOfBalls();
		return int.Parse(s);
	}

	private string getStringRepresentationOfNumberOfBalls() {
		var t = getTextComponentForNumberOfBalls();
		return t.text;
	}

	private int getValueForUpdatingNumberOfBalls() {
		return 1;
	}

	private int getMaximumForUpdatedNumberOfBalls() {
		return 5;
	}
}