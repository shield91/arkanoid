﻿using System;
using System.Collections;
using UnityEngine;

public class Destroyable : IndependentItem {
	public int hitPoints = 1;
	private Collision2D collision;
	private Collider2D catalyst;

	private void Start() {
		collision = null;
	}


	private void OnTriggerEnter2D(Collider2D c) {
		this.catalyst = c;
		performRestOfOnCollisionEnter2DMethod();
		
	}
	
    private void OnCollisionEnter2D(Collision2D c) {
		initializeCollisionFrom(c);
		performRestOfOnCollisionEnter2DMethod();
    }

	private void initializeCollisionFrom(Collision2D c) {
		collision = c;
	}

	private void performRestOfOnCollisionEnter2DMethod() {
		if (levelEditorModeIsEnabled()) {
			highlightIndependentItem();
		} else {
			inflictDamageToDestroyable();
		}
	}

	protected override void highlightIndependentItem() {
		const string NAME = "Sprites/Default";
		setShader(NAME);
	}

	private void setShader(string name) {
		Renderer r = GetComponent<Renderer>();
		Material m = r.material;
		Shader s = Shader.Find(name);
		m.shader = s;
	}

    private void inflictDamageToDestroyable() {
		
		try {
			subtractHitPoints();
			analyzeDamage();
		} catch (Exception e) {
			handle(e);
		}

//		subtractHitPoints();
//		analyzeDamage();
    }

	private void subtractHitPoints() {
		
		deductHitPoints();

//		try {
//			deductHitPoints();
//		} catch (Exception e) {
//			handle(e);
//		}	
	}

	private void deductHitPoints() {
		
		deductPoints();

//		try {
//			deductPoints();
//		} catch (Exception e) {
//			handle(e);
//		}		
	}

	private void deductPoints() {
		int value = getValueForUpdatedHitPoints();
		setHitPoints(value);
	}

	private int getValueForUpdatedHitPoints() {
		int points = getHitPoints();
		int decrement = getDecrementForUpdatingHitPoints();
		return points - decrement;
	}

	private int getHitPoints() {
		return hitPoints;
	}

	private int getDecrementForUpdatingHitPoints() {
		var b = getBallComponentFromActivator();
		return b.getBallHitPower();
	}

	private Ball getBallComponentFromActivator() {
		var go = getGameObjectForActivator();
		return go.GetComponent<Ball>();
	}

	private GameObject getGameObjectForActivator() {
		var c = getColliderForActivator();
		return c.gameObject;
	}

	private Collider2D getColliderForActivator() {
		try {
			return getColliderForPill();
		} catch (Exception e) {
			return getColliderInCaseOf(e);
		}				
	}

	private Collider2D getColliderForPill() {
		var c = getCollision();
		return c.collider;
	}

	private Collision2D getCollision() {
		return collision;
	}

	private Collider2D getColliderInCaseOf(Exception e) {
		handle(e);
		return getColliderInCaseOfException();
	}

	private void handle (Exception e) {
		e.GetType();
	}

	private Collider2D getColliderInCaseOfException() {
		return getCatalyst();
	}

	private Collider2D getCatalyst() {
		return catalyst;
	}

	private void setHitPoints(int value) {
		hitPoints = value;
	}

	private void analyzeDamage() {
		if (damageReachedItsCriticalLimit()) {
			makeDestroyableDisappear();
		} else {
			playHitAnimation();
		}
	}

	private bool damageReachedItsCriticalLimit() {
		return hitPoints <= 0;
	}

    private void makeDestroyableDisappear() {
		playDieAnimation();
		disableDestroyableCircleCollider2D();
		untagDestroyable();
		checkIfThereAreOtherDestroyablesLeftOnLevel();
		disableCollisionWithOthers();
		launchBonusIfItHasBeenAttachedToDestroyable();
		destroyDestroyableItself();
    }

	private void playDieAnimation() {
		const string STATE_NAME = "Die";
		play(STATE_NAME);
	}

	private void play(string stateName) {
		Animator a = GetComponent<Animator>();
		a.Play(stateName);
	}
	
	private void disableDestroyableCircleCollider2D() {
		CircleCollider2D c = GetComponent<CircleCollider2D>();
		c.enabled = false;
	}

	private void untagDestroyable() {
		tag = "Untagged";
	}

	private void checkIfThereAreOtherDestroyablesLeftOnLevel() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		var l = go.GetComponent<PlayScreen>();
		l.onBlockDestroyed();
	}

	private void disableCollisionWithOthers() {
		var c = GetComponent<CircleCollider2D>();
		c.isTrigger = true;
	}

	private void launchBonusIfItHasBeenAttachedToDestroyable() {
		if (bonusHasBeenAttachedToDestroyable()) {
			launchBonus();
		}
	}

	private bool bonusHasBeenAttachedToDestroyable() {
		int n = transform.childCount;
		return n == 1;
	}

	private void launchBonus() {		
		makeBonusVisible();
		updateVelocityForBonus();

		//makeFinalPreparations()
			//allowPlayerToPickUpBonusAfterSomeTime();
		StartCoroutine(launchGift());

//		setUpColliderForBonus();
//		detachBonusFromDestroyable();

	}

	private IEnumerator launchGift() {
		yield return new WaitForSeconds(0.2f);
		Debug.Log ("Activated");

		//allowPlayerToPickUpBonus
		setUpColliderForBonus();
		detachBonusFromDestroyable();

	}

	private void makeBonusVisible() {
		var go = getGameObjectForBonus();
		var value = getValueForBonusBeingVisible();
		go.SetActive(value);
	}

	private GameObject getGameObjectForBonus() {
		var t = getTransformForBonus();
		return t.gameObject;
	}

	private Transform getTransformForBonus() {
		int index = getIndexForBonusPosition();
		return transform.GetChild(index);
	}

	private int getIndexForBonusPosition() {
		return 0;
	}

	private bool getValueForBonusBeingVisible() {
		return true;
	}

	private void updateVelocityForBonus() {
		var rb = getRigidBodyForBonus();
		rb.velocity = getVelocityForBonus();
	}

	private Rigidbody2D getRigidBodyForBonus() {
		var t = getTransformForBonus();
		return t.GetComponent<Rigidbody2D>();
	}

	private void setUpColliderForBonus() {
		var c = getColliderForBonus();
		c.enabled = getValueForColliderBeingEnabled();
		c.isTrigger = getValueForTriggerBeingEnabled();
	}

	private Collider2D getColliderForBonus() {
		var t = getTransformForBonus();
		return t.GetComponent<Collider2D>();
	}

	private bool getValueForColliderBeingEnabled() {
		return getValueForBonusBeingVisible();
	}

	private bool getValueForTriggerBeingEnabled() {
		return getValueForColliderBeingEnabled();
	}

	private void detachBonusFromDestroyable() {
		var t = getTransformForDestroyable();
		t.DetachChildren();
	}

	private Transform getTransformForDestroyable() {
		return transform;
	}

	private Vector2 getVelocityForBonus() {
		const float X = 0;
		const float Y = -2;
		return new Vector2(X, Y);
	}

	private void destroyDestroyableItself() {
		const float T = 1f;
		var obj = gameObject;
		Destroy(obj, T);
	}

	private void playHitAnimation() {
		const string STATE_NAME = "Hit";
		play(STATE_NAME);
	}
	
	public override void unhighlightIndependentItem() {
		const string NAME = "Standard";
		setShader(NAME);
	}

	public override string getItemOrdinaryTag() {
		return "Destroy";
	}
}