﻿using UnityEngine;
using AssemblyCSharp;
using System.Collections;

public class SlowDownBallBonus : Bonus {
	protected override void apply() {
		IEnumerator routine = slowDownBallsTemporarily();
		StartCoroutine(routine);
	}

	private IEnumerator slowDownBallsTemporarily() {
		slowDownBallsOnStage();
		const float SECONDS = 10f;
		yield return new WaitForSeconds(SECONDS);
		speedUpBallsOnStage();
		deleteBonusItem();
	}

	private void slowDownBallsOnStage() {
		var decelerator = new BallsDecelerator(1f, 1.2f);
		decelerator.adjustBallsOnStage();
	}

	private void speedUpBallsOnStage() {
		var accelerator = new BallsAccelerator(5f,1.2f);
		accelerator.adjustBallsOnStage();
	}
}