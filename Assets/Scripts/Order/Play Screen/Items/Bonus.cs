﻿using UnityEngine;

public abstract class Bonus : MonoBehaviour {
	private Collider2D collisionInitiator;

	private void Start() {		
		resetCollisionInitiator();
	}

	private void resetCollisionInitiator() {
		var value = getValueForCollisionInitiatorByDefault();
		setCollisionInitiator(value);
	}

	private Collider2D getValueForCollisionInitiatorByDefault() {
		return null;
	}

	private void setCollisionInitiator(Collider2D value) {
		collisionInitiator = value;
	}

	private void OnTriggerEnter2D(Collider2D other) {
		initializeCollisionInitiatorFrom(other);
		applyBonusIfItHasCollidedWithItsActivator();
	}

	private void initializeCollisionInitiatorFrom(Collider2D c) {		
		var value = getValueForCollisionInitiatorFrom(c);
		setCollisionInitiator(value);
	}

	private Collider2D getValueForCollisionInitiatorFrom(Collider2D c) {
		return c;
	}

	private void applyBonusIfItHasCollidedWithItsActivator() {
		if (bonusHasCollidedWithItsActivator()) {
			applyBonus();
		}
	}

	private bool bonusHasCollidedWithItsActivator() {
		var ball = getBallComponentFromCollisionInitiator();
		var value = getValueForBallComponentBeingAbsent();
		return ball != value;
	}

	private Ball getBallComponentFromCollisionInitiator() {
		var initiator = getCollisionInitiator();
		return initiator.GetComponent<Ball>();
	}

	protected Collider2D getCollisionInitiator() {
		return collisionInitiator;
	}

	private Ball getValueForBallComponentBeingAbsent() {
		return null;
	}

	private void applyBonus() {
		disableMeshRenderer();
		apply();
		disableCircleCollider2D();
	}

	private void disableMeshRenderer() {
		MeshRenderer mr = GetComponent<MeshRenderer>();
		mr.enabled = false;
	}		

	protected abstract void apply();

	private void disableCircleCollider2D() {
		CircleCollider2D c = GetComponent<CircleCollider2D>();
		c.enabled = false;
	}

	private void OnTriggerExit2D(Collider2D other) {
		initializeCollisionInitiatorFrom(other);
		deleteBonusIfItHasGoneOutOfStage();
	}

	private void deleteBonusIfItHasGoneOutOfStage() {
		if (bonusHasGoneOutOfStage()) {
			deleteBonusItem();
		}
	}

	private bool bonusHasGoneOutOfStage() {		
		var collisionInitiatorTag = getTagFromCollisionInitiator();
		var value = getValueForBonusHavingGoneOutOfStage();
		return collisionInitiatorTag.Equals(value);
	}

	private string getTagFromCollisionInitiator() {
		var initiator = getCollisionInitiator();
		return initiator.tag;
	}

	private string getValueForBonusHavingGoneOutOfStage() {
		return "Stage";
	}

	protected void deleteBonusItem() {
		Object obj = gameObject;
		Destroy(obj);
	}
}