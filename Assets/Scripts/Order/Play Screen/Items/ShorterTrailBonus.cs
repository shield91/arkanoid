﻿using UnityEngine;
using System.Collections;

public class ShorterTrailBonus : Bonus {
	protected override void apply() {
		IEnumerator routine = makeTrailShorterTemporarily();
		StartCoroutine(routine);
	}

	private IEnumerator makeTrailShorterTemporarily() {
		makeTrailShorter();
		tagBonusItemAsAnActiveOne();
		yield return waitForSomeTime();
		makeTrailLongerAtTheFirstOpportunity();
		deleteBonusItem();
	}

	private void makeTrailShorter() {
		var value = getValueForTrailShrinking();
		setTrailMaximalLengthMultiplier(value);
	}

	private float getValueForTrailShrinking() {
		const float A = 2f;
		const float B = 3f;
		return A / B;
	}

	private void setTrailMaximalLengthMultiplier(float value) {
		var drawingRegion = getDrawingRegionComponentFromDrawingRegion();
		drawingRegion.setTrailMaximalLengthMultiplier(value);
	}

	private DrawingRegion getDrawingRegionComponentFromDrawingRegion() {
		var drawingRegion = getDrawingRegionItself();
		return drawingRegion.GetComponent<DrawingRegion>();
	}

	private GameObject getDrawingRegionItself() {
		var tag = getTagForDrawingRegion();
		return GameObject.FindWithTag(tag);
	}

	private string getTagForDrawingRegion() {
		return "Drawing Region";
	}

	private void tagBonusItemAsAnActiveOne() {
		tag = getTagForActiveBonusItem();
	}

	private string getTagForActiveBonusItem() {
		return "Active Shorter Trail Bonus";
	}

	private WaitForSeconds waitForSomeTime() {
		var seconds = getTimeForWaitingInSeconds();
		return new WaitForSeconds(seconds);
	}

	private float getTimeForWaitingInSeconds() {
		return 10f;
	}

	private void makeTrailLongerAtTheFirstOpportunity() {
		if (activeBonusItemIsTheLastOne()) {
			makeTrailLonger();
		}
	}

	private bool activeBonusItemIsTheLastOne() {
		int number = getNumberOfActiveBonusItems();
		int value = getValueForActiveBonusItemBeingTheLastOne();
		return number <= value;
	}

	private int getNumberOfActiveBonusItems() {
		var items = getActiveBonusItemsThemselves();
		return items.Length;
	}

	private GameObject[] getActiveBonusItemsThemselves() {
		return GameObject.FindGameObjectsWithTag(tag);
	}

	private int getValueForActiveBonusItemBeingTheLastOne() {
		return 1;
	}

	private void makeTrailLonger() {
		var value = getValueForTrailEnlarging();
		setTrailMaximalLengthMultiplier(value);
	}

	private float getValueForTrailEnlarging() {
		return 1f;
	}
}