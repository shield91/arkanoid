﻿using UnityEngine;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class DestroyablesTransformer {
		private int currentDestroyableIndex;

		public DestroyablesTransformer() {
			resetCurrentDestroyableIndex();
		}

		private void resetCurrentDestroyableIndex() {
			const int VALUE = 0;
			setCurrentDestroyableIndex(VALUE);
		}

		private void setCurrentDestroyableIndex(int value) {
			currentDestroyableIndex = value;
		}

		public void turnOrdinaryDestroyablesIntoPenetrableOnes() {
			resetCurrentDestroyableIndex();
			turnDestroyablesIntoPenetrableOnes();
		}

		private void turnDestroyablesIntoPenetrableOnes() {
			while (thereAreDestroyablesNeedToBeProcessed()) {
				turnCurrentDestroyableIntoPenetrableOne();
			}
		}

		private bool thereAreDestroyablesNeedToBeProcessed() {
			int i = getCurrentDestroyableIndex();
			int n = getDestroyablesNumber();
			return i < n;
		}

		private int getCurrentDestroyableIndex() {
			return currentDestroyableIndex;
		}

		private int getDestroyablesNumber() {
			var destroyables = getDestroyablesThemselves();
			return destroyables.Length;
		}

		private GameObject[] getDestroyablesThemselves() {
			const string TAG = "Destroy";
			return GameObject.FindGameObjectsWithTag(TAG);
		}

		private void turnCurrentDestroyableIntoPenetrableOne() {
			turnDestroyableIntoPenetrableOne();
			moveOnToTheNextDestroyable();
		}

		private void turnDestroyableIntoPenetrableOne() {
			var collider = getColliderForCurrentDestroyable();
			var value = getValueForDestroyableBeingTurnedIntoPenetrableOne();
			collider.isTrigger = value;
		}

		private Collider2D getColliderForCurrentDestroyable() {
			var destroyable = getCurrentDestroyableByItsIndex();
			return destroyable.GetComponent<Collider2D>();
		}

		private GameObject getCurrentDestroyableByItsIndex() {
			var destroyables = getDestroyablesThemselves();
			var index = getCurrentDestroyableIndex();
			return destroyables[index];
		}

		private bool getValueForDestroyableBeingTurnedIntoPenetrableOne() {
			int number = getRoutePointsNumberForCurrentDestroyable();
			int value = getValueForDestroyableBeingAllowedToTurnIntoPenetrableOne();
			return number == value;
		}

		private int getRoutePointsNumberForCurrentDestroyable() {
			var routePoints = getRoutePointsForCurrentDestroyable();
			return routePoints.Count;
		}

		private List<Dictionary<string, string>> getRoutePointsForCurrentDestroyable() {
			var destroyable = getDestroyableComponentForCurrentDestroyable();
			return destroyable.getWaybillPoints();
		}

		private Destroyable getDestroyableComponentForCurrentDestroyable() {
			var destroyable = getCurrentDestroyableByItsIndex();
			return destroyable.GetComponent<Destroyable>();
		}

		private int getValueForDestroyableBeingAllowedToTurnIntoPenetrableOne() {
			return 1;
		}

		private void moveOnToTheNextDestroyable() {
			int value = getValueForTheNextDestroyable();
			setCurrentDestroyableIndex(value);
		}

		private int getValueForTheNextDestroyable() {
			int index = getCurrentDestroyableIndex();
			int increment = getIncrementForMovingOnToTheNextDestroyable();
			return index + increment;
		}

		private int getIncrementForMovingOnToTheNextDestroyable() {
			return 1;
		}

		public void turnPenetrableDestroyablesIntoOrdinaryOnes() {
			resetCurrentDestroyableIndex();
			turnDestroyablesIntoOrdinaryOnes();
		}

		private void turnDestroyablesIntoOrdinaryOnes() {
			while (thereAreDestroyablesNeedToBeProcessed()) {
				turnCurrentDestroyableIntoOrdinaryOne();
			}
		}

		private void turnCurrentDestroyableIntoOrdinaryOne() {
			turnDestroyableIntoOrdinaryOne();
			moveOnToTheNextDestroyable();
		}

		private void turnDestroyableIntoOrdinaryOne() {
			var collider = getColliderForCurrentDestroyable();
			var value = getValueForDestroyableBeingTurnedIntoOrdinaryOne();
			collider.isTrigger = value;
		}

		private bool getValueForDestroyableBeingTurnedIntoOrdinaryOne() {
			return false;
		}
	}
}