﻿using AssemblyCSharp;
using UnityEngine;
using System;
using System.Xml;
using System.Collections.Generic;

using System.Xml.Serialization;
using System.IO;

public abstract class IndependentItem : MonoBehaviour {
	private List<Dictionary<string, string>> routePoints;
	private Dictionary<string, string> currentPoint;
	private bool dragModeIsEnabled;
	private bool firstTimeUpdating;
	private int currentPointIndex;
	
	private void Awake() {
		routePoints = new List<Dictionary<string, string>>();
		dragModeIsEnabled = false;
		firstTimeUpdating = true;
	}

	private void Update() {
		if (itIsFirstTimeUpdating()) {
			adjustIndependentItem();
		}

		updateFirstPointOfItemRoute();

	}

	private bool itIsFirstTimeUpdating() {
		return firstTimeUpdating;
	}

	private void adjustIndependentItem() {
		loadRoutePoints();
		addFirstPointToRouteIfItIsNecessary();
		markThatFirstTimeUpdatingHasBeenPassed();
		enableItemMoverIfItIsPossible();
	}

	private void loadRoutePoints() {
		try {
			loadRoute();
		} catch (Exception e) {
			handle(e);
		}
	}

	private void loadRoute() {		
		resetCurrentPointIndex();
		loadPoints();
	}

	private void resetCurrentPointIndex() {
		int value = getValueForCurrentPointIndexByDefault();
		setCurrentPointIndex(value);
	}

	private int getValueForCurrentPointIndexByDefault() {
		return 0;
	}

	private void setCurrentPointIndex(int value){
		currentPointIndex = value;
	}

	private void loadPoints() {
		while (thereArePointsNeedToBeLoaded()) {
			processCurrentPoint();
		}	
	}

	private bool thereArePointsNeedToBeLoaded() {
		int i = getCurrentPointIndex();
		int n = getNumberOfRoutePoints();
		return i < n;
	}

	private int getCurrentPointIndex() {
		return currentPointIndex;
	}

	private int getNumberOfRoutePoints() {
		var points = getRoutePointsFromChosenLevel();
		return points.Count;	
	}

	private List<Point> getRoutePointsFromChosenLevel() {
		var r = getRouteFromChosenLevel();
		return r.points;
	}

	private Route getRouteFromChosenLevel() {
		var routes = getRoutesFromChosenLevel();
		var index = getIndexForItem();
		return routes[index];
	}

	private List<Route> getRoutesFromChosenLevel() {
		var l = getChosenLevelFromLevelsSet();
		return l.routes;	
	}

	private Level getChosenLevelFromLevelsSet() {
		if (campaignModeIsOn()) {
			return getChosenCampaignLevelFromLevelsSet();
		} else {
			return getChosenFreeLevelFromLevelsSet();
		}			
	}

	private bool campaignModeIsOn() {
		var key = getKeyForChosenCampaignIndex();
		return PlayerPrefs.HasKey(key);
	}

	private string getKeyForChosenCampaignIndex() {
		return "Chosen Campaign Index";
	}

	private Level getChosenCampaignLevelFromLevelsSet() {
		var levels = getChosenCampaignLevelsFromLevelsSet();
		var index = getIndexOfChosenLevel();
		return levels[index];
	}

	private List<Level> getChosenCampaignLevelsFromLevelsSet() {
		var c = getChosenCampaignFromLevelsSet();
		return c.levels;
	}

	private Campaign getChosenCampaignFromLevelsSet() {
		var campaigns = getCampaignsFromLevelsSet();
		var index = getIndexOfChosenCampaign();
		return campaigns[index];	
	}

	private List<Campaign> getCampaignsFromLevelsSet() {
		var s = getSetOfLevels();
		return s.campaigns;
	}

	private Levels getSetOfLevels() {
		
		//У скрипті MainMenu.cs провернути щось подібне:
		//https://stackoverflow.com/questions/2407302/convert-xmldocument-to-string
		//(це аби можна було запихувати "Levels Set.xml" в string навіть в стані (режимі) редактора рівнів).
		//		if (levelEditorModeIsOn()) {
		//			return getSetOfLevelsInLevelEditorMode();
		//		} else {
		//			return getSetOfLevelsInPlayMode();
		//		}
		//levelEditorModeIsOn
		if (PlayerPrefs.HasKey ("Level Editor Mode")) {
			//getSetOfLevelsInLevelEditorMode
			var serializer = getSerializerForLevelsSet();
			var stream = new FileStream (PlayerPrefs.GetString ("Path To Levels"), FileMode.OpenOrCreate);
			var o = serializer.Deserialize (stream) as Levels;
			stream.Close();
			return o;

			//float
		} else {
			//getSetOfLevelsInPlayMode
			var serializer = getSerializerForLevelsSet();
			var textReader = getTextReaderForLevelsSet();
			return serializer.Deserialize(textReader) as Levels;	
		}

//		var serializer = getSerializerForLevelsSet();
//		var textReader = getTextReaderForLevelsSet();
//		return serializer.Deserialize(textReader) as Levels;
	}

	private XmlSerializer getSerializerForLevelsSet() {
		var type = getTypeForLevelsSet();
		return new XmlSerializer(type);	
	}

	private Type getTypeForLevelsSet() {
		return typeof(Levels);
	}

	private TextReader getTextReaderForLevelsSet() {
		var s = getStringForLevelsSet();
		return new StringReader(s);
	}

	private string getStringForLevelsSet() {
		var key = getKeyForLevelsSet();
		return PlayerPrefs.GetString(key);
	}

	private string getKeyForLevelsSet() {
		return "Levels";
	}

	private int getIndexOfChosenCampaign() {
		var key = getKeyForChosenCampaignIndex();
		return PlayerPrefs.GetInt(key);
	}

	private int getIndexOfChosenLevel() {
		var key = getKeyForChosenLevelIndex();
		return PlayerPrefs.GetInt(key);	
	}

	private string getKeyForChosenLevelIndex() {
		return "Chosen Level Index";
	}

	private Level getChosenFreeLevelFromLevelsSet() {
		var levels = getFreeLevelsFromLevelsSet();
		var index = getIndexOfChosenLevel();
		return levels[index];
	}

	private List<Level> getFreeLevelsFromLevelsSet() {
		var s = getSetOfLevels();
		return s.freeLevels;
	}

	private void processCurrentPoint() {
		updateCurrentPoint();
		setUpCurrentPoint();
		addCurrentPointToRoute();
		moveOnToTheNextPoint();	
	}

	private void updateCurrentPoint() {
		var value = getValueForCurrentPointByDefault();
		setCurrentPoint(value);
	}

	private Dictionary<string, string> getValueForCurrentPointByDefault() {
		return new Dictionary<string, string>();
	}

	private void setCurrentPoint(Dictionary<string, string> value) {
		currentPoint = value;
	}

	private void setUpCurrentPoint() {
		setUpCurrentPointPositionOnX();
		setUpCurrentPointPositionOnY();
		setUpTimeToNextPointFromCurrentOne();
	}

	private void setUpCurrentPointPositionOnX() {
		var point = getCurrentPoint();
		var key = getKeyForCurrentPointPositionOnX();
		var value = getValueForCurrentPointPositionOnX();
		point[key] = value;
	}

	private Dictionary<string, string> getCurrentPoint() {
		return currentPoint;
	}

	private string getKeyForCurrentPointPositionOnX() {
		return "x";
	}

	private string getValueForCurrentPointPositionOnX() {
		var x = getCurrentPointPositionOnAbscissaFromChosenLevel();
		return x.ToString();
	}

	private float getCurrentPointPositionOnAbscissaFromChosenLevel() {
		var p = getCurrentPointFromChosenLevel();
		return p.x;
	}

	private Point getCurrentPointFromChosenLevel() {
		var points = getPointsFromChosenLevel();
		var index = getCurrentPointIndex();
		return points[index];
	}

	private List<Point> getPointsFromChosenLevel() {
		var r = getRouteFromChosenLevel();
		return r.points;
	}

	private void setUpCurrentPointPositionOnY() {
		var point = getCurrentPoint();
		var key = getKeyForCurrentPointPositionOnY();
		var value = getValueForCurrentPointPositionOnY();
		point[key] = value;	
	}

	private string getKeyForCurrentPointPositionOnY() {
		return "y";
	}

	private string getValueForCurrentPointPositionOnY() {
		var y = getCurrentPointPositionOnOrdinateFromChosenLevel();
		return y.ToString();
	}

	private float getCurrentPointPositionOnOrdinateFromChosenLevel() {
		var p = getCurrentPointFromChosenLevel();
		return p.y;
	}

	private void setUpTimeToNextPointFromCurrentOne() {
		var point = getCurrentPoint();
		var key = getKeyForTimeToNextPointFromCurrentOne();
		var value = getValueForTimeToNextPointFromCurrentOne();
		point[key] = value;
	}

	private string getKeyForTimeToNextPointFromCurrentOne() {
		return "Time to next point";
	}

	private string getValueForTimeToNextPointFromCurrentOne() {
		int t = getTimeFromCurrentPointToNextOneFromChosenLevel();
		return t.ToString();
	}

	private int getTimeFromCurrentPointToNextOneFromChosenLevel() {
		var p = getCurrentPointFromChosenLevel();
		return p.timeToNextPoint;	
	}

	private void addCurrentPointToRoute() {
		var route = getRoutePoints();
		var item = getCurrentPoint();
		route.Add(item);
	}

	private List<Dictionary<string, string>> getRoutePoints() {
		return routePoints;
	}

	private void moveOnToTheNextPoint() {
		int value = getValueForNextPointIndex();
		setCurrentPointIndex(value);
	}

	private int getValueForNextPointIndex() {
		int index = getCurrentPointIndex();
		int increment = getValueForMovingOnToTheNextPoint();
		return index + increment;
	}

	private int getValueForMovingOnToTheNextPoint() {
		return 1;
	}

	private XmlNodeList getRoutesInXmlForm() {
		const string XPATH = "Level/Routes/Route";
		var level = getLoadedLevel();
		return level.SelectNodes(XPATH);
	}
	
	private XmlDocument getLoadedLevel() {
		LevelLoader l = new LevelLoader();
		return l.getLoadedLevel();
	}

	private int getIndexForItem() {
		int i = transform.GetSiblingIndex();
		int n = getNumberOfDestroyables();
		bool b = itemIsUndestroyable();
		return b ? n + i : i;
	}

	private int getNumberOfDestroyables() {
		var sf = getSharedFunctionsScriptFromLevelObject();
		var d = sf.getDestroyables();
		return d.Length;
	}

	private SharedFunctions getSharedFunctionsScriptFromLevelObject() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		return go.GetComponent<SharedFunctions>();
	}
	
	private bool itemIsUndestroyable() {
		return tag == "Undestroy";
	}
	
	private void handle(Exception e) {
		string message = e.StackTrace;
		Debug.Log(message);
	}

	private void addFirstPointToRouteIfItIsNecessary() {
		if (routePointsListIsEmpty()) {
			addFirstPointToRoute();
		}
	}
	
	private bool routePointsListIsEmpty() {
		int n = routePoints.Count;
		return n == 0;
	}
	
	private void addFirstPointToRoute() {
		const string KEY_1 = "x";
		const string KEY_2 = "y";
		const string KEY_3 = "Time to next point";
		var point = new Dictionary<string, string>();
		var position = transform.position;
		point[KEY_1] = position.x + "";
		point[KEY_2] = position.y + "";
		point[KEY_3] = "0";
		routePoints.Add(point);
	}

	private void markThatFirstTimeUpdatingHasBeenPassed() {
		firstTimeUpdating = false;
	}

	private void enableItemMoverIfItIsPossible() {
		
		if (playModeIsEnabled() &&
			!PlayerPrefs.HasKey ("Survival Mode Is On")) {
			enableItemMover();
		}

	}
	
	private bool playModeIsEnabled() {
		return !levelEditorModeIsEnabled();
	}
	
	protected bool levelEditorModeIsEnabled() {
		var sf = getSharedFunctionsScriptFromLevelObject();
		return sf.levelEditorModeIsEnabled();
	}
	
	private void enableItemMover() {
		ItemMover im = GetComponent<ItemMover>();
		im.enabled = true;
	}

	protected abstract void highlightIndependentItem();

	private void OnCollisionExit2D(Collision2D collision) {
		unhighlightIndependentItem();
	}

	public abstract void unhighlightIndependentItem();

	private void OnMouseDown() {
		makeItemLookLikeItIsSelected();
		enableDragMode();
	}

	private void makeItemLookLikeItIsSelected() {
		var sf = getSharedFunctionsScriptFromLevelObject();
		var item = gameObject;
		sf.makeItemLookLikeItIsSelected(item);
	}

	private void enableDragMode() {
		dragModeIsEnabled = true;
	}
	
	private void OnMouseDrag() {
		if (independentItemDraggingIsPossible()) {
			moveIndependentItemToInputPosition();
		}
	}

	private bool independentItemDraggingIsPossible() {
		bool ba = levelEditorModeIsEnabled();
		bool bb = dragModeIsEnabled;
		bool bc = routeMenuIsInvisible();
		bool bd = independentItemMenuIsInvisible();
		return ba && bb && bc && bd;
	}

	private bool routeMenuIsInvisible() {
		PlayScreen l = getLevelScriptFromLevelObject();
		GameObject menu = l.getItemRouteMenu();
		return !menu.activeInHierarchy;
	}

	private PlayScreen getLevelScriptFromLevelObject() {
		const string TAG = "Level";
		var go = GameObject.FindGameObjectWithTag(TAG);
		return go.GetComponent<PlayScreen>();
	}

	private bool independentItemMenuIsInvisible() {
		PlayScreen l = getLevelScriptFromLevelObject();
		GameObject menu = l.getIndependentItemMenu();
		return !menu.activeInHierarchy;
	}

	private void moveIndependentItemToInputPosition() {
		Vector3 position = Input.mousePosition;
		transform.position = projectInput(position);
	}
	
	private Vector3 projectInput(Vector3 inputPosition) {
		var sf = getSharedFunctionsScriptFromLevelObject();
		return sf.projectInput(inputPosition);
	}

	private void OnMouseUp() {
		makeItemLookLikeItIsDeselected();
		disableDragMode();
		showIndependentItemMenuIfItIsPossible();
		markItemAsClickedIfItIsPossible();
		updateFirstPointOfItemRoute();

		  
		GameObject.FindWithTag("Level").
		GetComponent<RouteMenuButtonsBehaviour>().
		setSelectedItem(gameObject);
		 //0 v " ) 

	}

	private void makeItemLookLikeItIsDeselected() {
		var sf = getSharedFunctionsScriptFromLevelObject();
		var item = gameObject;
		sf.makeItemLookLikeItIsDeselected(item);
	}

	private void disableDragMode() {
		dragModeIsEnabled = false;
	}

	private void showIndependentItemMenuIfItIsPossible() {
		if (thereAreNoObstaclesForIndependentItemMenuShowing()) {
			showIndependentItemMenu();
		}
	}

	private bool thereAreNoObstaclesForIndependentItemMenuShowing() {
		bool a = routeMenuIsInvisible();
		bool b = levelEditorModeIsEnabled();
		return a && b;
	}

	private void showIndependentItemMenu() {
		PlayScreen l = getLevelScriptFromLevelObject();
		GameObject menu = l.getIndependentItemMenu();
		menu.SetActive(true);
	}

	private void markItemAsClickedIfItIsPossible() {
		if (thereAreNoClickedItemsAmongOtherItems()) {
			markItemAsClicked();
		}
	}

	private bool thereAreNoClickedItemsAmongOtherItems() {
		const string TAG = "Clicked Item";
		var go = GameObject.FindGameObjectsWithTag(TAG);
		int n = go.Length;
		return n == 0;
	}

	private void markItemAsClicked() {
		const string TAG = "Clicked Item";
		tag = TAG;
	}

	private void updateFirstPointOfItemRoute() {
		const int INDEX = 0;
		const string KEY_1 = "x";
		const string KEY_2 = "y";
		var position = transform.position;
		var point = routePoints[INDEX];
		point[KEY_1] = position.x + "";
		point[KEY_2] = position.y + "";
	}

	public abstract string getItemOrdinaryTag();

	public List<Dictionary<string, string>> getWaybillPoints() {
		int n = routePoints.Count;
		var a = new Dictionary<string, string>[n];
		var l = new List<Dictionary<string, string>>();
		routePoints.CopyTo(a);


//		l.AddRange(a);
//		l.AddRange(routePoints.ToArray());

		for (int i = 0; i < routePoints.Count; ++i) {

//			l.Add(routePoints[i]);

			var item = new Dictionary<string, string>();
			item["x"] = routePoints[i]["x"];
			item["y"] = routePoints[i]["y"];
			item["Time to next point"] = routePoints[i]["Time to next point"];
			l.Add(item);
//			 0 

		}

		return l;

	}

	public void setRoutePoints(List<Dictionary<string, string>> route) {
		routePoints = route;
	}
}