﻿using UnityEngine;
using AssemblyCSharp;

public class TripleBallBonus : Bonus {
	protected override void apply() {
		tripleBalls();
		deleteBonusItem();
	}

	private void tripleBalls() {
		
//		var tripler = new BallsTripler();
//		tripler.adjustBallsOnStage();

		var firstBall = getCollisionInitiator().gameObject;

		Object.Instantiate (firstBall).GetComponent<Rigidbody2D> ().velocity = Vector2.left * 5f;
		Object.Instantiate (firstBall).GetComponent<Rigidbody2D> ().velocity = Vector2.right * 5f;



//		GameObject firstBall = getCurrentBallByItsPosition();
//		GameObject secondBall = Object.Instantiate(firstBall);
//		GameObject thirdBall = 	Object.Instantiate(firstBall);
//		Rigidbody2D a = secondBall.GetComponent<Rigidbody2D>();
//		Rigidbody2D b = thirdBall.GetComponent<Rigidbody2D>();
//		a.velocity = getVelocityForSecondBall();
//		b.velocity = getVelocityForThirdBall();
	}	
}