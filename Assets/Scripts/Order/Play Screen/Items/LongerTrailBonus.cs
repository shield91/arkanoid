﻿using UnityEngine;
using System.Collections;

public class LongerTrailBonus : Bonus {
	protected override void apply() {
		IEnumerator routine = makeTrailShorterTemporarily();
		StartCoroutine(routine);
	}

	private IEnumerator makeTrailShorterTemporarily() {
		makeTrailLonger();
		tagBonusItemAsAnActiveOne();
		yield return waitForSomeTime();
		makeTrailShorterAtTheFirstOpportunity();
		deleteBonusItem();
	}

	private void makeTrailLonger() {
		var value = getValueForTrailEnlarging();
		setTrailMaximalLengthMultiplier(value);
	}

	private float getValueForTrailEnlarging() {
		const float A = 3f;
		const float B = 2f;
		return A / B;
	}

	private void setTrailMaximalLengthMultiplier(float value) {
		var drawingRegion = getDrawingRegionComponentFromDrawingRegion();
		drawingRegion.setTrailMaximalLengthMultiplier(value);
	}

	private DrawingRegion getDrawingRegionComponentFromDrawingRegion() {
		var drawingRegion = getDrawingRegionItself();
		return drawingRegion.GetComponent<DrawingRegion>();
	}

	private GameObject getDrawingRegionItself() {
		var tag = getTagForDrawingRegion();
		return GameObject.FindWithTag(tag);
	}

	private string getTagForDrawingRegion() {
		return "Drawing Region";
	}

	private void tagBonusItemAsAnActiveOne() {
		tag = getTagForActiveBonusItem();
	}

	private string getTagForActiveBonusItem() {
		return "Active Longer Trail Bonus";
	}

	private WaitForSeconds waitForSomeTime() {
		var seconds = getTimeForWaitingInSeconds();
		return new WaitForSeconds(seconds);
	}

	private float getTimeForWaitingInSeconds() {
		return 10f;
	}

	private void makeTrailShorterAtTheFirstOpportunity() {
		if (activeBonusItemIsTheLastOne()) {
			makeTrailShorter();
		}
	}

	private bool activeBonusItemIsTheLastOne() {
		int number = getNumberOfActiveBonusItems();
		int value = getValueForActiveBonusItemBeingTheLastOne();
		return number <= value;
	}

	private int getNumberOfActiveBonusItems() {
		var items = getActiveBonusItemsThemselves();
		return items.Length;
	}

	private GameObject[] getActiveBonusItemsThemselves() {
		return GameObject.FindGameObjectsWithTag(tag);
	}

	private int getValueForActiveBonusItemBeingTheLastOne() {
		return 1;
	}

	private void makeTrailShorter() {
		var value = getValueForTrailShrinking();
		setTrailMaximalLengthMultiplier(value);
	}

	private float getValueForTrailShrinking() {
		return 1f;
	}
}