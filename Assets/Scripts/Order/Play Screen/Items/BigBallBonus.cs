﻿using UnityEngine;
using AssemblyCSharp;
using System.Collections;

public class BigBallBonus : Bonus {
	protected override void apply() {
		IEnumerator routine = enlargeBallsTemporarily();
		StartCoroutine(routine);
	}

	private IEnumerator enlargeBallsTemporarily() {
		enlargeBallsOnStage();
		tagBonusItemAsAnActiveOne();
		yield return waitForSomeTime();
		shrinkBallsOnStageAtTheFirstOpportunity();
		deleteBonusItem();
	}

	private void enlargeBallsOnStage() {
		const float SIZE = 0.6f;
		var enlarger = new BallsSizeTransformer(SIZE);
		enlarger.adjustBallsOnStage();
	}

	private void tagBonusItemAsAnActiveOne() {
		tag = getTagForActiveBonusItem();
	}

	private string getTagForActiveBonusItem() {
		return "Active Big Ball Bonus";
	}

	private WaitForSeconds waitForSomeTime() {
		var seconds = getTimeForWaitingInSeconds();
		return new WaitForSeconds(seconds);
	}

	private float getTimeForWaitingInSeconds() {
		return 10f;
	}

	private void shrinkBallsOnStageAtTheFirstOpportunity() {
		if (activeBonusItemIsTheLastOne()) {
			shrinkBallsOnStage();
		}
	}

	private bool activeBonusItemIsTheLastOne() {
		int number = getNumberOfActiveBonusItems();
		int value = getValueForActiveBonusItemBeingTheLastOne();
		return number <= value;
	}

	private int getNumberOfActiveBonusItems() {
		var items = getActiveBonusItemsThemselves();
		return items.Length;
	}

	private GameObject[] getActiveBonusItemsThemselves() {
		return GameObject.FindGameObjectsWithTag(tag);
	}

	private int getValueForActiveBonusItemBeingTheLastOne() {
		return 1;
	}		

	private void shrinkBallsOnStage() {
		var shrinker = new BallsSizeTransformer();
		shrinker.adjustBallsOnStage();
	}
}