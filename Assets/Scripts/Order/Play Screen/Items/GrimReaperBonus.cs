﻿using UnityEngine;
using UnityEngine.UI;

public class GrimReaperBonus : Bonus {
	protected override void apply() {
		if (itIsLastBallFromReserve()) {
			removeBallFromScene();
		} else {
			decreaseBallsInReserve();
		}
	}

	private bool itIsLastBallFromReserve() {
		int number = getNumberOfBalls();
		int value = getValueForBallBeingTheLastOneFromReserve();
		return number == value;
	}

	private int getNumberOfBalls() {
		var s = getStringRepresentationOfNumberOfBalls();
		return int.Parse(s);
	}

	private string getStringRepresentationOfNumberOfBalls() {
		var t = getTextComponentForNumberOfBalls();
		return t.text;
	}

	private Text getTextComponentForNumberOfBalls() {
		var t = getTransformForNumberOfBalls();
		return t.GetComponent<Text>();
	}

	private Transform getTransformForNumberOfBalls() {
		var template = getTemplateOfLevel();
		var name = getNameForNumberOfBalls();
		return template.Find(name);
	}

	private Transform getTemplateOfLevel() {
		var t = getTransformFromLevel();
		return t.parent;
	}

	private Transform getTransformFromLevel() {
		var go = getGameObjectForLevel();
		return go.transform;
	}

	private GameObject getGameObjectForLevel() {
		var tag = getTagForLevel();
		return GameObject.FindWithTag(tag);
	}

	private string getTagForLevel() {
		return "Level";
	}

	private string getNameForNumberOfBalls() {
		return "Canvas/Balls/Value";
	}

	private int getValueForBallBeingTheLastOneFromReserve() {
		return 1;
	}

	private void removeBallFromScene() {
		var b = getBallComponentFromCollisionInitiator();
		b.destroyBall();
	}

	private Ball getBallComponentFromCollisionInitiator() {
		var initiator = getCollisionInitiator();
		return initiator.GetComponent<Ball>();
	}

	private void decreaseBallsInReserve() {
		var t = getTextComponentForNumberOfBalls();
		var value = getValueForUpdatedNumberOfBalls();
		t.text = value;
	}

	private string getValueForUpdatedNumberOfBalls() {
		int value = getIntegerRepresentationOfValueForUpdatedNumberOfBalls();
		return value.ToString();
	}

	private int getIntegerRepresentationOfValueForUpdatedNumberOfBalls() {
		int number = getNumberOfBalls();
		int decrement = getValueForUpdatingNumberOfBalls();
		return number - decrement;
	}

	private int getValueForUpdatingNumberOfBalls() {
		return 1;
	}
}