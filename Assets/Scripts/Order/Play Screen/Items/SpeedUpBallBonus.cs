﻿using UnityEngine;
using AssemblyCSharp;
using System.Collections;

public class SpeedUpBallBonus : Bonus {
	protected override void apply() {
		IEnumerator routine = speedUpBallsTemporarily();
		StartCoroutine(routine);
	}

	private IEnumerator speedUpBallsTemporarily() {
		speedUpBallsOnStage();
		const float SECONDS = 10f;
		yield return new WaitForSeconds(SECONDS);
		slowDownBallsOnStage();
		deleteBonusItem();
	}

	private void speedUpBallsOnStage() {
		var accelerator = new BallsAccelerator();
		accelerator.adjustBallsOnStage();
	}
	
	private void slowDownBallsOnStage() {
		var decelerator = new BallsDecelerator();
		decelerator.adjustBallsOnStage();
	}
}