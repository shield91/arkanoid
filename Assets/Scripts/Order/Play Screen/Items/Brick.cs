﻿using UnityEngine;

public class Brick : IndependentItem {
	void OnCollisionEnter2D(Collision2D c) {
		if (levelEditorModeIsEnabled()) {
			highlightIndependentItem();
		}
	}

	protected override void highlightIndependentItem() {
		const string PROPERTY_NAME = "_Color";
		const float R = 1f;
		const float G = 0f;
		const float B = 0f;
		Color color = new Color(R, G, B);
		Renderer r = GetComponent<Renderer>();
		Material m = r.material;
		m.SetColor(PROPERTY_NAME, color);
	}

	public override void unhighlightIndependentItem() {
		const string PROPERTY_NAME = "_Color";
		const float R = 163f / 255f;
		const float G = 163f / 255f;
		const float B = 163f / 255f;
		Color color = new Color(R, G, B);
		Renderer r = GetComponent<Renderer>();
		Material m = r.material;
		m.SetColor(PROPERTY_NAME, color);
	}

	public override string getItemOrdinaryTag() {
		return "Undestroy";
	}
}