﻿using UnityEngine;
using AssemblyCSharp;
using System.Collections;

public class FireballBonus : Bonus {
	protected override void apply() {
		IEnumerator routine = turnBallsIntoFireballsTemporarily();
		StartCoroutine(routine);
	}

	private IEnumerator turnBallsIntoFireballsTemporarily() {
		turnBallsIntoFireballs();
		tagBonusItemAsAnActiveOne();
		yield return waitForSomeTime();
		turnFireballsIntoOrdinaryBallsAtTheFirstOpportunity();
		deleteBonusItem();
	}

	private void turnBallsIntoFireballs() {
		transformBallsIntoFireballs();
		turnOrdinaryDestroyablesIntoPenetrableOnes();
	}

	private void tagBonusItemAsAnActiveOne() {
		tag = getTagForActiveBonusItem();
	}

	private string getTagForActiveBonusItem() {
		return "Active Fireball Bonus";
	}

	private WaitForSeconds waitForSomeTime() {
		var seconds = getTimeForWaitingInSeconds();
		return new WaitForSeconds(seconds);
	}

	private float getTimeForWaitingInSeconds() {
		return 10f;
	}

	private void turnFireballsIntoOrdinaryBallsAtTheFirstOpportunity() {
		if (activeBonusItemIsTheLastOne()) {
			turnFireballsIntoOrdinaryBalls();
		}
	}

	private bool activeBonusItemIsTheLastOne() {
		int number = getNumberOfActiveBonusItems();
		int value = getValueForActiveBonusItemBeingTheLastOne();
		return number <= value;
	}

	private int getNumberOfActiveBonusItems() {
		var items = getActiveBonusItemsThemselves();
		return items.Length;
	}

	private GameObject[] getActiveBonusItemsThemselves() {
		return GameObject.FindGameObjectsWithTag(tag);
	}

	private int getValueForActiveBonusItemBeingTheLastOne() {
		return 1;
	}

	private void transformBallsIntoFireballs() {
		var transformer = new FireballsTransformer();
		transformer.adjustBallsOnStage();
	}

	private void turnOrdinaryDestroyablesIntoPenetrableOnes() {
		var transformer = new DestroyablesTransformer();
		transformer.turnOrdinaryDestroyablesIntoPenetrableOnes();
	}

	private void turnFireballsIntoOrdinaryBalls() {
		turnFireballsIntoOrdinaryOnes();
		turnPenetrableDestroyablesIntoOrdinaryOnes();
	}

	private void turnFireballsIntoOrdinaryOnes() {
		var transformer = new OrdinaryBallsTransformer();
		transformer.adjustBallsOnStage();
	}

	private void turnPenetrableDestroyablesIntoOrdinaryOnes() {
		var transformer = new DestroyablesTransformer();
		transformer.turnPenetrableDestroyablesIntoOrdinaryOnes();
	}
}