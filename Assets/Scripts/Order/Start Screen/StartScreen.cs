﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScreen : MonoBehaviour {
	private void Start() {
		setUpSurvivalButtonVisibility();
	}

	private void setUpSurvivalButtonVisibility() {
		var go = getGameObjectForSurvivalButton();
		var value = getValueForSurvivalButtonVisibility();
		go.SetActive(value);
	}

	private GameObject getGameObjectForSurvivalButton() {
		var t = getTransformForSurvivalButton();
		return t.gameObject;
	}

	private Transform getTransformForSurvivalButton() {
		var name = getNameForSurvivalButton();
		return transform.Find(name);
	}

	private string getNameForSurvivalButton() {
		return "Survival";
	}

	private bool getValueForSurvivalButtonVisibility() {
		var b = getValueForLevelEditorModeBeingTurnedOn();
		return !b;
	}

	private bool getValueForLevelEditorModeBeingTurnedOn() {
		var key = getKeyForLevelEditorMode();
		return PlayerPrefs.HasKey(key);
	}

	private string getKeyForLevelEditorMode() {
		return "Level Editor Mode";
	}

	public void goToLevels() {
		var sceneName = getSceneNameForLevelsScreen();
		SceneManager.LoadScene(sceneName);
	}

	private string getSceneNameForLevelsScreen() {
		return "LevelSelect";
	}

	public void goToSurvival() {
		turnOnSurvivalMode();
		loadPlayScreen();
	}

	private void turnOnSurvivalMode() {
		const string KEY = "Survival Mode Is On";
		const string VALUE = "Yes";
		PlayerPrefs.SetString(KEY, VALUE);
	}

	private void loadPlayScreen() {
		var sceneName = getSceneNameForPlayScreen();
		SceneManager.LoadScene(sceneName);
	}

	private string getSceneNameForPlayScreen() {
		return "Level Base";
	}

	public void goToPreviousScreen() {
		var sceneName = getSceneNameForPreviousScreen();
		SceneManager.LoadScene(sceneName);
	}

	private string getSceneNameForPreviousScreen() {
		return "MainMenu";
	}
}